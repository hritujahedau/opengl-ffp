#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>
#include <SOIL/SOIL.h>

#include "ogl.h"

#define TRANSFER_Z 7.0f

//namespace
using namespace std;

//global variable declarations
bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
GLXContext gGLXContext; // HGLRC


GLfloat x = 1.0f, y = 1.0f, z = 1.0f;
GLfloat appex = 1.0f;

RECTANGLE rectangles_h[] = 
{
	{ {x, y, z}, {-x, y, z}, {-x, -y, z}, {x, -y, z} }, // front
	{ {x, y, -z}, {x, y, z}, {x, -y, z}, {x, -y, -z} }, // right
	{ {x, -y, -z}, {-x, -y, -z}, {-x, y, -z}, {x, y,-z}}, // back
	{ {-x, y, z}, {-x, y, -z}, {-x, -y, -z}, {-x, -y, z}}, // left
	{ {x, y, -z}, {-x, y, -z}, {-x, y, z}, {x, y, z} }, // top
	{ {x, -y, z}, {-x, -y, z}, {-x, -y, -z}, {x, -y,-z} } // bottom
};


const int numberOfRectangles_h = sizeof(rectangles_h) / sizeof(rectangles_h[0]);

RECTANGLE rectangle_texcoord_h[] = {
	{ {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 0.0f} }, // front
	{ {1.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f} }, // right
	{ {1.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f} }, // back
	{ {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 0.0f} }, // left
	{ {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 0.0f} }, // top
	{ {1.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f} }  // bottom
};

TRIANGLE triangle_h[] = 
{
	{ {0.0f, appex, 0.0f }, {-x, -y, z}, {x, -y,z} },
	{ {0.0f, appex, 0.0f }, {x, -y, z }, {x, -y, -z }},
	{ {0.0f, appex, 0.0f }, {x, -y, -z }, {-x, -y, -z}},
	{ {0.0f, appex, 0.0f }, {-x, -y, z}, {-x, -y, -z}}
};

TRIANGLE triangle_texcoords_h[] =
{
	{ {0.5f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f} },
	//{ {0.5f, 1.0f, 0.0f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f} },
	{ {0.5f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f} },
	{ {0.5f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f} },
	{ {0.5f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f} },
	//{ {0.5f, 1.0f, 0.0f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f} }
};

GLuint kundali_h = 0, stone_h = 0;

const int numberOfTriangles_h = sizeof(triangle_h) / sizeof(triangle_h[0]);

//entry point function
int main(void)
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Initialize();
	void Resize(int,int);
	void Render();
	void Uninitialize();

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	//code
	CreateWindow();
	Initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;
	bool bDone=false;
	XKeyEvent xKeyEvent;
	char keys[26];

	while(1)
	{
		while(XPending(gpDisplay))
	    	{
		XNextEvent(gpDisplay, &event);
		switch(event.type)
		{
			case MapNotify:
				break;
			case KeyPress:
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0,0);
				switch(keysym)
				{
					case XK_Escape:
						bDone=true;
						Uninitialize();
						exit(0);
					default:
						break;
	
				}
				xKeyEvent.display = gpDisplay;
				xKeyEvent.window = gWindow;
				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);

				switch(keys[0])
				{
					case 'f':
					case 'F':
						if(bFullScreen == false)
						{
							ToggleFullScreen();
							bFullScreen = true;
						}
						else
						{
							ToggleFullScreen();
							bFullScreen = false;
						}
						break;
				}
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					default:
						break;
				}
				break;
			case MotionNotify:
				break;
			case ConfigureNotify:
				winWidth=event.xconfigure.width;
				winHeight=event.xconfigure.height;
				Resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33:
				bDone = true;
				Uninitialize();
				exit(0); 
			default:
				break;
			
		} // switch
	    } // XPending Loop
	    Render();
	}
	Uninitialize();
	return 0;
}

void CreateWindow(void)
{

	//function declaration
	void Uninitialize();

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
//	int defaultDepth;
	int styleMask;

	static int framebufferAttribes[] = 
				{
					GLX_DOUBLEBUFFER, True,
					GLX_RGBA,
					GLX_RED_SIZE, 8, 
					GLX_GREEN_SIZE, 8, 
					GLX_BLUE_SIZE, 8,
					GLX_ALPHA_SIZE, 8, 
					GLX_DEPTH_SIZE, 24,
					None // NULL o or None
				};

	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error: Uable To Open X Display.\nExiting Now...\n ");
		Uninitialize();
		exit(1);
	}
//	defaultScreen=XDefaultScreen(gpDisplay);
//	defaultDepth=DefaultDepth(gpDisplay, defaultScreen);
	gpXVisualInfo=glXChooseVisual(gpDisplay, defaultScreen, framebufferAttribes);
	if(gpXVisualInfo==NULL)
	{
		printf("Error: Unable To Allocate Memory For XVisual Info.\nExiting Now..\n");
		Uninitialize();
		exit(1);
	}

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			gpXVisualInfo->visual,
			AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask
				| KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);
	if(!gWindow)
	{
		printf("Error: Failed To Create Main Window.\nExiting Now...\n");
		Uninitialize();	
		exit(1);
	}
	XStoreName(gpDisplay, gWindow, "OGL");
	Atom windowManagerDelete=XInternAtom(gpDisplay, "WM_DELETE_WINDOW", true);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state=XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(XEvent));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen ? 0 : 1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN", false);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay, 
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);
}

void Initialize()
{
	// Function Declaration
	void Resize(int, int);
	GLuint LoadBitmapAsTexture(const char *);
	
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	kundali_h = LoadBitmapAsTexture("Vijay_Kundali.bmp");
	stone_h = LoadBitmapAsTexture("Stone.bmp");
	glEnable(GL_TEXTURE_2D);

	Resize(giWindowWidth, giWindowHeight);

}

GLuint LoadBitmapAsTexture(const char *path)
{
		int w, h;
		unsigned char *ImageData = NULL;
		GLuint texture = 0;

		ImageData = SOIL_load_image(path, &w, &h, NULL, SOIL_LOAD_RGB);
	
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, ImageData);
		
		SOIL_free_image_data(ImageData);
		
		return texture;		
}


void Resize(int width, int hight)
{
	if (hight == 0) {
		hight = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,((GLfloat)width / (GLfloat) hight), 0.1f, 100.0f);

	
}

void Render()
{
	//FUNCTION DECLARATION
	void Pyramid();
	void Cube();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	Pyramid();
	Cube();

	glXSwapBuffers(gpDisplay, gWindow);
}

void Pyramid()
{
	static GLfloat angle = 0.0f;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-2.0f, 0.0f, -6.0);
	glRotatef(angle, 0.0f, 1.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, stone_h);
	
	for (int i = 0 ; i < numberOfTriangles_h; i++)
	{
		glBegin(GL_TRIANGLES);
		glTexCoord2f(triangle_texcoords_h[i].appex.x, triangle_texcoords_h[i].appex.y);
		glVertex3f(triangle_h[i].appex.x, triangle_h[i].appex.y, triangle_h[i].appex.z);

		glTexCoord2f(triangle_texcoords_h[i].left.x, triangle_texcoords_h[i].left.y);
		glVertex3f(triangle_h[i].left.x, triangle_h[i].left.y, triangle_h[i].left.z);

		glTexCoord2f(triangle_texcoords_h[i].right.x, triangle_texcoords_h[i].right.y);
		glVertex3f(triangle_h[i].right.x, triangle_h[i].right.y, triangle_h[i].right.z);
		glEnd();
	}
	angle = angle + 0.1f;
}

void Cube()
{
	static GLfloat angle = 0.0f;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.0f, 0.0f, -7.0);
	glRotatef(angle, 1.0f, 1.0f, 1.0f);
	
	glBindTexture(GL_TEXTURE_2D, kundali_h);
	for (int i = 0; i < numberOfRectangles_h; i++)
	{
		glBegin(GL_QUADS);
		glTexCoord2f(rectangle_texcoord_h[i].a.x, rectangle_texcoord_h[i].a.y);
		glVertex3f(rectangles_h[i].a.x, rectangles_h[i].a.y, rectangles_h[i].a.z);

		glTexCoord2f(rectangle_texcoord_h[i].b.x, rectangle_texcoord_h[i].b.y);
		glVertex3f(rectangles_h[i].b.x, rectangles_h[i].b.y, rectangles_h[i].b.z);

		glTexCoord2f(rectangle_texcoord_h[i].c.x, rectangle_texcoord_h[i].c.y);
		glVertex3f(rectangles_h[i].c.x, rectangles_h[i].c.y, rectangles_h[i].c.z);

		glTexCoord2f(rectangle_texcoord_h[i].d.x, rectangle_texcoord_h[i].d.y);
		glVertex3f(rectangles_h[i].d.x, rectangles_h[i].d.y, rectangles_h[i].d.z);
		glEnd();
	}

	angle = angle + 0.1f;
}

void Update()
{
	
	
}

void Uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext = gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}









































