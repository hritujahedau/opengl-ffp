#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>
#include "ogl.h"

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

//namespace
using namespace std;

//global variable declarations
bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
GLXContext gGLXContext; // HGLRC

GLfloat radius = 1.0f;
POINTF a, b, c, d, e;

//entry point function
int main(void)
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Initialize();
	void Resize(int,int);
	void Render();
	void Uninitialize();

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	//code
	CreateWindow();
	Initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;
	bool bDone=false;

	while(bDone==false)
	{
	    while(XPending(gpDisplay))
	    {
		XNextEvent(gpDisplay, &event);
		switch(event.type)
		{
			case MapNotify:
				break;
			case KeyPress:
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0,0);
				switch(keysym)
				{
					case XK_Escape:
						bDone=true;
						Uninitialize();
						//exit(0); no need as we are checking in while condition
					case XK_F:
					case XK_f:
						if(bFullScreen == false)
						{
							ToggleFullScreen();
							bFullScreen=true;
						}
						else
						{
							ToggleFullScreen();
							bFullScreen=false;
						}
						break;
					default:
						break;
	
				}
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					default:
						break;
				}
				break;
			case MotionNotify:
				break;
			case ConfigureNotify:
				winWidth=event.xconfigure.width;
				winHeight=event.xconfigure.height;
				Resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33:
				bDone = true;
				Uninitialize();
				//exit(0); no need as we are checking in while condition
			default:
				break;
			
		} // switch
	    } // XPending Loop
	    Render();
	}
	Uninitialize();
	return 0;
}

void CreateWindow(void)
{

	//function declaration
	void Uninitialize();

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
//	int defaultDepth;
	int styleMask;

	static int framebufferAttribes[] = 
				{
					GLX_RGBA,
					GLX_RED_SIZE, 1, 
					GLX_GREEN_SIZE, 1, 
					GLX_BLUE_SIZE, 1, 
					GLX_ALPHA_SIZE, 1, 
					None // NULL o or None
				};

	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error: Uable To Open X Display.\nExiting Now...\n ");
		Uninitialize();
		exit(1);
	}
//	defaultScreen=XDefaultScreen(gpDisplay);
//	defaultDepth=DefaultDepth(gpDisplay, defaultScreen);
	gpXVisualInfo=glXChooseVisual(gpDisplay, defaultScreen, framebufferAttribes);
	if(gpXVisualInfo==NULL)
	{
		printf("Error: Unable To Allocate Memory For XVisual Info.\nExiting Now..");
		Uninitialize();
		exit(1);
	}

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			gpXVisualInfo->visual,
			AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask
				| KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);
	if(!gWindow)
	{
		printf("Error: Failed To Create Main Window.\nExiting Now...\n");
		Uninitialize();	
		exit(1);
	}
	XStoreName(gpDisplay, gWindow, "OGL");
	Atom windowManagerDelete=XInternAtom(gpDisplay, "WM_DELETE_WINDOW", true);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state=XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(XEvent));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen ? 0 : 1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN", false);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay, 
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);
}

void Initialize()
{
	// Function Declaration
	void Resize(int, int);
	
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	Resize(giWindowWidth, giWindowHeight);

}

void Resize(int width, int hight)
{
	if (hight == 0) {
		hight = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,((GLfloat)width / (GLfloat) hight), 0.1f, 100.0f);

	
}

void Render()
{
	void IncircleRectangle();
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glLineWidth(3.0f);
	IncircleRectangle();

	glFlush();
}


void IncircleRectangle()
{
	void bigCircle();
	void rectangle();
	void triangle();
	void smallCircle();	
	bigCircle();
	rectangle();
	triangle();
	smallCircle();
}

void smallCircle()
{
	GLfloat getLengthFromVertises(GLfloat, GLfloat, GLfloat, GLfloat);
	GLfloat s = 0.0f, length_ec = 0.0f, length_cd = 0.0f, length_de =0.0f;
	GLfloat smallCircleRadius = 0.0f;
	length_ec = getLengthFromVertises(e.x, e.y, c.x, c.y);
	length_cd = getLengthFromVertises(c.x, c.y, d.x, d.y);
	length_de = getLengthFromVertises(d.x, d.y, e.x, e.y);

	s = (length_ec + length_cd + length_de) / 2;
	smallCircleRadius = sqrt(s * (s - length_ec) * (s - length_cd) * (s - length_de)) / s;
	glBegin(GL_LINE_LOOP);
	for (int angle = 0; angle < 360; angle++)
	{
		glVertex3f(smallCircleRadius * (cos(angle * 0.01745329)), -0.15f + (smallCircleRadius * (sin(angle * 0.01745329))), 0.0f);
	}
	glEnd();
}

GLfloat getLengthFromVertises(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2)
{
	// eqaution for calculating legth between 2 points
	return sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
}

void triangle()
{
	e.x = (a.x + b.x) / 2;
	e.y = (a.y + b.y) / 2;
	glBegin(GL_LINE_LOOP);
	glVertex3f(e.x, e.y, 0.0f);
	glVertex3f(c.x, c.y, 0.0f);
	glVertex3f(d.x, d.y, 0.0f);
	glEnd();
}

void bigCircle()
{
	
	glBegin(GL_LINE_LOOP);
	for (int angle = 0; angle < 360; angle++)
	{
		glVertex2f(radius * (cos(angle * 0.01745329)), radius * (sin(angle * 0.01745329)));
	}
	glEnd();
}

void rectangle()
{
	
	a.x = radius * (cos(35.0f * 0.01745329));
	a.y = radius * (sin(35.0f * 0.01745329));

	b.x = radius * (cos(145.0f * 0.01745329));
	b.y = radius * (sin(145.0f * 0.01745329));

	c.x = radius * (cos(215.0f * 0.01745329));
	c.y = radius * (sin(215.0f * 0.01745329));

	d.x = radius * (cos(325.0f * 0.01745329));
	d.y = radius * (sin(325.0f * 0.01745329));

	glBegin(GL_LINE_LOOP);
	glVertex2f(a.x, a.y);
	glVertex2f(b.x, b.y);
	glVertex2f(c.x, c.y);
	glVertex2f(d.x, d.y);
	glEnd();
}


void Uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext = gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}









































