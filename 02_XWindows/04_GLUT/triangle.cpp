#include<GL\freeglut.h>

bool bFullScrean = false;

int main(int argc, char** argv) {
	//code
	//function declaration

	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void unitialize(void);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("GLUT:Hrituja");

	initialize();
	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutMouseFunc(mouse);
	glutCloseFunc(unitialize);

	glutMainLoop();
	return (0);
}

void initialize() {
	//code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//set window color to black
}

void resize(int width, int height) {
	// code
	if (height <= 0) {
		height = 1;		
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
}

void display(void) {
	// code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glBegin(GL_TRIANGLES);

	/*glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(2.0f, 1.0f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);

	glColor3f(3.0f, 0.0f, 1.0f);
	glVertex3f(0.5f, -0.5f, 0.0f);*/

	//3rd triangle

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.4f, 0.2f, 0.0f);

	glColor3f(2.0f, 1.0f, 0.0f);
	glVertex3f(0.3f, 0.8f, 0.0f);

	glColor3f(3.0f, 0.0f, 1.0f);
	glVertex3f(0.3f, -0.8f, 0.0f);

	// 2nd triangle

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);

	glColor3f(2.0f, 1.0f, 0.0f);
	glVertex3f(0.5f, 0.8f, 0.0f);

	glColor3f(3.0f, 0.0f, 1.0f);
	glVertex3f(0.5f, -0.8f, 0.0f);

	//1st triangle

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.2f, 0.2f, 0.0f);

	glColor3f(2.0f, 1.0f, 0.0f);
	glVertex3f(0.8f, 0.8f, 0.0f);

	glColor3f(3.0f, 0.0f, 1.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);

	// 2nd triangle

	

	// reverse triangle
	/*glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, -0.7f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.5f, 0.5f, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.5f, 0.0f);*/

	glEnd();
	glFlush();
}



void keyboard(unsigned char key, int x, int y) {
	switch (key) {
	case 27: // ESC key
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullScrean == false) {
			glutFullScreen();
			bFullScrean = true;
		}
		else {
			glutLeaveFullScreen();
			bFullScrean = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y) {
	// code
	switch (button) {
	case GLUT_LEFT_BUTTON:
		break;
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void unitialize(void) {
	//code
}