#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

#include "ogl.h"

#define TRANSFER_Z 7.0f

//namespace
using namespace std;

//global variable declarations
bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
GLXContext gGLXContext; // HGLRC

GLfloat f_rectangle_points_x[] = { 0.0f, 1.0f, 2.0f, 3.0f };
GLfloat f_rectangle_points_y[] = { 0.0f, 1.0f, 2.0f, 3.0f };

//entry point function
int main(void)
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Initialize();
	void Resize(int,int);
	void Render();
	void Uninitialize();

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	//code
	CreateWindow();
	Initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;
	bool bDone=false;
	XKeyEvent xKeyEvent;
	char keys[26];

	while(1)
	{
		while(XPending(gpDisplay))
	    	{
		XNextEvent(gpDisplay, &event);
		switch(event.type)
		{
			case MapNotify:
				break;
			case KeyPress:
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0,0);
				switch(keysym)
				{
					case XK_Escape:
						bDone=true;
						Uninitialize();
						exit(0);
					default:
						break;
	
				}
				xKeyEvent.display = gpDisplay;
				xKeyEvent.window = gWindow;
				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);

				switch(keys[0])
				{
					case 'f':
					case 'F':
						if(bFullScreen == false)
						{
							ToggleFullScreen();
							bFullScreen = true;
						}
						else
						{
							ToggleFullScreen();
							bFullScreen = false;
						}
						break;
				}
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					default:
						break;
				}
				break;
			case MotionNotify:
				break;
			case ConfigureNotify:
				winWidth=event.xconfigure.width;
				winHeight=event.xconfigure.height;
				Resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33:
				bDone = true;
				Uninitialize();
				exit(0); 
			default:
				break;
			
		} // switch
	    } // XPending Loop
	    Render();
	}
	Uninitialize();
	return 0;
}

void CreateWindow(void)
{

	//function declaration
	void Uninitialize();

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
//	int defaultDepth;
	int styleMask;

	static int framebufferAttribes[] = 
				{
					GLX_DOUBLEBUFFER, True,
					GLX_RGBA,
					GLX_RED_SIZE, 8, 
					GLX_GREEN_SIZE, 8, 
					GLX_BLUE_SIZE, 8,
					GLX_ALPHA_SIZE, 8, 
					GLX_DEPTH_SIZE, 24,
					None // NULL o or None
				};

	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error: Uable To Open X Display.\nExiting Now...\n ");
		Uninitialize();
		exit(1);
	}
//	defaultScreen=XDefaultScreen(gpDisplay);
//	defaultDepth=DefaultDepth(gpDisplay, defaultScreen);
	gpXVisualInfo=glXChooseVisual(gpDisplay, defaultScreen, framebufferAttribes);
	if(gpXVisualInfo==NULL)
	{
		printf("Error: Unable To Allocate Memory For XVisual Info.\nExiting Now..\n");
		Uninitialize();
		exit(1);
	}

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			gpXVisualInfo->visual,
			AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask
				| KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);
	if(!gWindow)
	{
		printf("Error: Failed To Create Main Window.\nExiting Now...\n");
		Uninitialize();	
		exit(1);
	}
	XStoreName(gpDisplay, gWindow, "OGL");
	Atom windowManagerDelete=XInternAtom(gpDisplay, "WM_DELETE_WINDOW", true);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state=XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(XEvent));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen ? 0 : 1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN", false);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay, 
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);
}

void Initialize()
{
	// Function Declaration
	void Resize(int, int);
	
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	Resize(giWindowWidth, giWindowHeight);

}

void Resize(int width, int hight)
{
	if (hight == 0) {
		hight = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,((GLfloat)width / (GLfloat) hight), 0.1f, 100.0f);

	
}

void Render()
{
	//FUNCTION DECLARATION
	void Dots();
	void Pattern1();
	void Pattern2();
	void Pattern3();
	void Pattern4();
	void Pattern5();
	void Pattern6();

	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0f, 0.0f, 20.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	glLineWidth(2.0f);
	glPushMatrix();
	Dots();
	glPopMatrix();
	glPushMatrix();
	Pattern1();
	glPopMatrix();
	glPushMatrix();	
	Pattern2();
	glPopMatrix();
	glPushMatrix();
	Pattern4();
	glPopMatrix();
	glPushMatrix();
	Pattern5();
	glPopMatrix();
	glPushMatrix();
	Pattern6();
	glPopMatrix();

	glXSwapBuffers(gpDisplay, gWindow);
}


void Pattern6()
{
	glTranslatef(10.0f, -4.0f, 0.0f);
	for (int i = 0; i < 3; i++)
	{
		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < 4; j++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
		glEnd();
	}
	for (int j = 1; j < 4; j++)
	{
		glBegin(GL_LINE_LOOP);
		for (int i = 0; i < 4; i++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
		glEnd();
	}

	for (int i = 1; i < 4; i++)
	{
		glBegin(GL_LINE_STRIP);
		for (int j = 3; j > 0; j--)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
			glVertex2f(f_rectangle_points_x[i - 1], f_rectangle_points_y[j - 1]);
		}
		glEnd();
	}
}

void Pattern5()
{
	glTranslatef(0.0f, -4.0f, 0.0f);
	for (int i = 0; i < 4; i++)
	{
		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < 4; j++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
		glEnd();
	}
	for (int j = 0; j < 4; j++)
	{
		glBegin(GL_LINE_LOOP);
		for (int i = 0; i < 4; i++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
		glEnd();
	}

	for (int i = 1; i < 4; i++)
	{
		glBegin(GL_LINE_STRIP);
		for (int j = 3; j > 0; j--)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
			glVertex2f(f_rectangle_points_x[i-1], f_rectangle_points_y[j-1]);
		}
		glEnd();
	}

}

void Pattern4()
{
	glTranslatef(-10.0f, -4.0f, 0.0f);
	for (int i = 3; i < 4; i++)
	{
		glBegin(GL_LINE_STRIP);
		for (int j = 0; j < 4; j++)
		{
			glVertex2f(0.0f, 3.0f);
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
			if (j>0)
			{
				glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j-1]);
				glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
			}

		}
		glEnd();
	}

	for (int i = 0; i < 4; i++)
	{
		glBegin(GL_LINE_STRIP);
		for (int j = 0; j < 1; j++)
		{
			glVertex2f(0.0f, 3.0f);
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
			if (i>0)
			{
				glVertex2f(f_rectangle_points_x[i-1], f_rectangle_points_y[j]);
				glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
			}
		}
		glEnd();
	}
}

void Pattern2()
{
	glTranslatef(10.0f, 4.0f, 0.0f);

	glLineWidth(2.0f);

	//glBegin(GL_QUADS);

	for (int i = 0; i < 3; i++)
	{
		if (i == 0)
		{
			glColor3f(1.0f, 0.0f, 0.0f);
		}
		else if (i == 1)
		{
			glColor3f(0.0f, 1.0f, 0.0f);
		}
		else
		{
			glColor3f(0.0f, 0.0f, 1.0f);
		}
		glBegin(GL_QUADS);
		glVertex2f(f_rectangle_points_x[i+1], f_rectangle_points_y[0]);
		glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[0]);
		glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[3]);
		glVertex2f(f_rectangle_points_x[i + 1], f_rectangle_points_y[3]);
		glEnd();
	}

	glColor3f(1.0f, 1.0f, 1.0f);

	for (int i = 0; i < 4; i++)
	{
		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < 4; j++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
		glEnd();
	}


	for (int j = 0; j < 4; j++)
	{
		glBegin(GL_LINE_LOOP);
		for (int i = 0; i < 4; i++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
		glEnd();
	}

	
}

void Pattern1()
{
	glTranslatef(0.0f, 4.0f, 0.0f);
	
	for (int i = 0; i < 4; i++)
	{
		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < 4; j++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);			
		}
		glEnd();
	}

	
	for (int j = 0; j < 4; j++)
	{
		glBegin(GL_LINE_LOOP);
		for (int i = 0; i < 4; i++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
		glEnd();
	}	
}

void Dots()
{
	glTranslatef(-10.0f, 4.0f, 0.0f);

	glPointSize(5.0f);
	glBegin(GL_POINTS);

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
	}
	
	glEnd();

}



void Update()
{
	
	
}

void Uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext = gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}









































