typedef struct __POINTF {
	GLfloat x, y, z;
} POINTF;

typedef struct Tringle {
	POINTF appex, left, right;
} TRIANGLE;

typedef struct __COLORS {
	GLfloat red, green, blue;
} COLORS;

typedef struct __TRAINGLE_COLORS {
	COLORS side1, side2, side3;
} TRIANGLE_COLORS;

typedef struct __RECTANGLE {
	POINTF a, b, c, d;
} RECTANGLE;
