
typedef struct __POINTF {
	GLfloat x, y, z;
} POINTF;

typedef struct __RECTANGLE {
	POINTF a, b, c ,d;
} RECTANGLE;

typedef struct __COLORS {
	GLfloat red, green, blue;
} COLORS;

