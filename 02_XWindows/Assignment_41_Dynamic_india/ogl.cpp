#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

#include "ogl.h"

#define TRANSFER_Z 7.0f

//namespace
using namespace std;

//global variable declarations
bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
GLXContext gGLXContext; // HGLRC

GLfloat positionForI_1 = -7.5f, positionForN = 3.8f;
GLfloat positionForI_2 = -(positionForN - 0.5), positionForA = 7.0f;
GLfloat xpositionForIAFPlane = -5.8f, ypositionForIAFPlane = 0.05f;
GLfloat xpositionForUpIAFPlane = xpositionForIAFPlane, ypositionForUpIAFPlane = 0.0f, angleForUpIAFPlane = 270; // CHECK
GLfloat xpositionForDownIAFPlane = xpositionForIAFPlane, ypositionForDownIAFPlane = 0.0f, angleForDownIAFPlane = 90; // CHECK
int showFlag = 0, startPlane = 0;
//GLfloat speed = 0.0033f, planSpeed = 0.022f, planAngle = 0.09f; // 0.0035f; speed = 0.0033f
GLfloat speed = 0.03f, planSpeed = 0.022f, planAngle = 0.09f; // 0.0035f; speed = 0.0033f
int showI_1 = 1, showN = 0, showD = 0, showI_2 = 0, showA = 0;
int showCharacter = 1, countForRotation = 0;
int scatterPlane = 0;

GLfloat radius = 2.0f, i_for_plane_up = 180, i_for_plane_down = 180;
//GLfloat angle_for_up_plane = 270;


//entry point function
int main(void)
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Initialize();
	void Resize(int,int);
	void Render();
	void Uninitialize();
	void Update();

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	//code
	CreateWindow();
	Initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;
	bool bDone=false;
	XKeyEvent xKeyEvent;
	char keys[26];

	while(1)
	{
		while(XPending(gpDisplay))
	    	{
		XNextEvent(gpDisplay, &event);
		switch(event.type)
		{
			case MapNotify:
				break;
			case KeyPress:
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0,0);
				switch(keysym)
				{
					case XK_Escape:
						bDone=true;
						Uninitialize();
						exit(0);
					default:
						break;
	
				}
				xKeyEvent.display = gpDisplay;
				xKeyEvent.window = gWindow;
				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);

				switch(keys[0])
				{
					case 'f':
					case 'F':
						if(bFullScreen == false)
						{
							ToggleFullScreen();
							bFullScreen = true;
						}
						else
						{
							ToggleFullScreen();
							bFullScreen = false;
						}
						break;
				}
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					default:
						break;
				}
				break;
			case MotionNotify:
				break;
			case ConfigureNotify:
				winWidth=event.xconfigure.width;
				winHeight=event.xconfigure.height;
				Resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33:
				bDone = true;
				Uninitialize();
				exit(0); 
			default:
				break;
			
		} // switch
	    } // XPending Loop
	    Render();
	    Update();
	}
	Uninitialize();
	return 0;
}

void CreateWindow(void)
{

	//function declaration
	void Uninitialize();

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
//	int defaultDepth;
	int styleMask;

	static int framebufferAttribes[] = 
				{
					GLX_DOUBLEBUFFER, True,
					GLX_RGBA,
					GLX_RED_SIZE, 8, 
					GLX_GREEN_SIZE, 8, 
					GLX_BLUE_SIZE, 8,
					GLX_ALPHA_SIZE, 8, 
					GLX_DEPTH_SIZE, 24,
					None // NULL o or None
				};

	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error: Uable To Open X Display.\nExiting Now...\n ");
		Uninitialize();
		exit(1);
	}
//	defaultScreen=XDefaultScreen(gpDisplay);
//	defaultDepth=DefaultDepth(gpDisplay, defaultScreen);
	gpXVisualInfo=glXChooseVisual(gpDisplay, defaultScreen, framebufferAttribes);
	if(gpXVisualInfo==NULL)
	{
		printf("Error: Unable To Allocate Memory For XVisual Info.\nExiting Now..\n");
		Uninitialize();
		exit(1);
	}

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			gpXVisualInfo->visual,
			AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask
				| KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);
	if(!gWindow)
	{
		printf("Error: Failed To Create Main Window.\nExiting Now...\n");
		Uninitialize();	
		exit(1);
	}
	XStoreName(gpDisplay, gWindow, "OGL");
	Atom windowManagerDelete=XInternAtom(gpDisplay, "WM_DELETE_WINDOW", true);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state=XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(XEvent));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen ? 0 : 1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN", false);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay, 
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);
}

void Initialize()
{
	// Function Declaration
	void Resize(int, int);
	
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	Resize(giWindowWidth, giWindowHeight);

}

void Resize(int width, int hight)
{
	if (hight == 0) {
		hight = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,((GLfloat)width / (GLfloat) hight), 0.1f, 100.0f);

	
}

void Render()
{
	
	static GLfloat  i_for_plane_up = 180;
	GLfloat angle = 0.0f,  tempx, tempy;

	//function declaration
	void I(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void N(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void D(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void A(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void Tiranga(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void IAFPlane(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);

	// VARIABLE DECLARATION
	typedef void (*FUNCTION) (GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	FUNCTION  function[] = { I, A , D , N, I , IAFPlane , IAFPlane , IAFPlane }; // 8
	const int NumberOfCharacters = (sizeof(function) / sizeof(function[0]));
	GLfloat coordinates[NumberOfCharacters][5] = {
									{ // I_1
										positionForI_1, 0.0f, 0.0f, 0.0f, 0.0f
									},
									{ // A
										positionForA, 0.0F, 0.0f, 0.0f, 0.0f
									},
									{ // D
										0.0f, 0.0f, 0.0f, 0.0f, 0.0f
									},
									{ // N
										-1.5f, positionForN , 0.0f,0.0f, 0.0f
									},									
									{ // I_2
										1.5f, positionForI_2, 0.0f, 0.0f, 0.0f
									},									
									{ // IAFPlane
										xpositionForIAFPlane, ypositionForIAFPlane, 0.0f, -0.05f, 0.0f
									},
									{ // IAFPlane for up plane
										xpositionForUpIAFPlane, ypositionForUpIAFPlane, 0.0f, 0.0f,angleForUpIAFPlane
									},
									{ // IAFPlane for down plane
										xpositionForDownIAFPlane, ypositionForDownIAFPlane, 0.0f, 0.0f,angleForDownIAFPlane
									}
	};

	glClear(GL_COLOR_BUFFER_BIT);
	if (showFlag == 1) {
		Tiranga(5.0f, 0.0f, -2.0f, 0.0f, 0.0f);
	}
	for (int i = 0; i < showCharacter; i++) {
		(*function[i])(coordinates[i][0], coordinates[i][1], coordinates[i][2], coordinates[i][3], coordinates[i][4]);
	}
	

	glXSwapBuffers(gpDisplay, gWindow);
}


void DrawRectangles(RECTANGLE* rectangles, int numberOfRectangle, int* arrayOfColor) {
	//FUNCTION DECLARATION
	void DrawColor(int, TRICOLORS);
	for (int i = 0; i < numberOfRectangle; i++) {
		glBegin(GL_QUADS);
		DrawColor(arrayOfColor[i * 2], tricolors);
		glVertex3f(rectangles[i].a.x, rectangles[i].a.y, rectangles[i].a.z);
		glVertex3f(rectangles[i].b.x, rectangles[i].b.y, rectangles[i].b.z);
		DrawColor(arrayOfColor[i * 2 + 1], tricolors);
		glVertex3f(rectangles[i].c.x, rectangles[i].c.y, rectangles[i].c.z);
		glVertex3f(rectangles[i].d.x, rectangles[i].d.y, rectangles[i].d.z);
		glEnd();
	}
}

void DrawColor(int color, TRICOLORS tricolors) {
	if (color == ORANGE_ID) {
		glColor3f(tricolors.ORANGE.red, tricolors.ORANGE.green, tricolors.ORANGE.blue);
	}
	else if (color == GREEN_ID) {
		glColor3f(tricolors.GREEN.red, tricolors.GREEN.green, tricolors.GREEN.blue);
	}
	else if (color == WHITE_ID) {
		glColor3f(tricolors.WHITE.red, tricolors.WHITE.green, tricolors.WHITE.blue);
	}
	else if (color == NAVYBLUE_ID) {
		glColor3f(0.0f, 0.19f, 0.56f);
	}
	else if (color == BLACK_ID) {
		glColor3f(0.0f, 0.0f, 0.0f);
	}
	else {
		glColor3f(1.0f, 1.0f, 1.0f);
	}
}


void UpdateColor() {
	//ORANGE
	if (tricolorsForD.ORANGE.red < tricolors.ORANGE.red) {
		tricolorsForD.ORANGE.red += icreaseColorBy;
	}
	if (tricolorsForD.ORANGE.green < tricolors.ORANGE.green) {
		tricolorsForD.ORANGE.green += icreaseColorBy;
	}

	// GREEN
	if (tricolorsForD.GREEN.red < tricolors.GREEN.red) {
		tricolorsForD.GREEN.red += icreaseColorBy;
	}
	if (tricolorsForD.GREEN.green < tricolors.GREEN.green) {
		tricolorsForD.GREEN.green += icreaseColorBy;
	}

	// WHITE
	if (tricolorsForD.WHITE.red < tricolors.WHITE.red) {
		tricolorsForD.WHITE.red += icreaseColorBy;
	}
	if (tricolorsForD.WHITE.green < tricolors.WHITE.green) {
		tricolorsForD.WHITE.green += icreaseColorBy;
	}
	if (tricolorsForD.WHITE.blue < tricolors.WHITE.blue) {
		tricolorsForD.WHITE.blue += icreaseColorBy;
	}
}


// FOR I
void I(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y, GLfloat angle) {
	//FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);
	// VARIABLE DECLARATION
	RECTANGLE rectangleForI[] = {
						{ // ORANGE RECTANGLE
							{ x - hight / 2, y + hight / 2 + width, 0.0f },
							{ x - hight / 2  , y + hight / 2, 0.0f},
							{ x + hight / 2 , y + hight / 2 , 0.0f },
							{ x + hight / 2 , y + hight / 2 + width , 0.0f }
						},

						{ // GREEN RECTANGLE
							{ x - hight / 2, y - hight / 2 , 0.0f },
							{ x - hight / 2  , y - hight / 2 - width, 0.0f},
							{ x + hight / 2 , y - hight / 2 - width, 0.0f },
							{ x + hight / 2 , y - hight / 2  , 0.0f }
						},
						{ // MIDDLE VERTICLE UP RECTANGLE
							{x + width / 2, y + hight / 2, 0.0f },
							{x - width / 2, y + hight / 2, 0.0f },
							{x - width / 2, y , 0.0f },
							{x + width / 2 , y , 0.0f }

						},
						{ // MIDDLE VERTICLE DOWN RECTANGLE
							{x + width / 2 , y , 0.0f },
							{x - width / 2, y , 0.0f },
							{x - width / 2, y - hight / 2 },
							{x + width / 2, y - hight / 2 , 0.0f}
						}
	};
	const int numberOfRectangleForI = sizeof(rectangleForI) / sizeof(rectangleForI[0]);

	int arrayColorForI[numberOfRectangleForI * 2] = { ORANGE_ID, ORANGE_ID, GREEN_ID, GREEN_ID, ORANGE_ID, WHITE_ID, WHITE_ID, GREEN_ID };

	// CODE
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, translatefy, -TRANSLATEZ);

	DrawRectangles(rectangleForI, numberOfRectangleForI, arrayColorForI);
}

void N(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y, GLfloat angle) {
	// FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);
	// VARIABLE DECLARATION
	RECTANGLE rectangleForN[] = {
		// RIGHT TOP SQAURE
		{
			{ x - hight / 2 + width, y + hight / 2 + width, 0.0f },
			{ x - hight / 2  , y + hight / 2 + width, 0.0f},
			{ x - hight / 2  , y + hight / 2, 0.0f},
			{ x - hight / 2 + width, y + hight / 2 , 0.0f }
		},
		{ // RIGHT UP SIDE RECTANGLE
			{ x - hight / 2 + width, y + hight / 2 , 0.0f },
			{ x - hight / 2  , y + hight / 2, 0.0f},
			{ x - hight / 2, y , 0.0f},
			{ x - hight / 2 + width, y , 0.0f }
		},
		{ // RIGHT DOWN SIDE RECTANGLE
			{ x - hight / 2 + width, y , 0.0f },
			{ x - hight / 2, y , 0.0f},
			{ x - hight / 2 , y - hight / 2 , 0.0f },
			{ x - hight / 2 + width, y - hight / 2 , 0.0f }
		},
		{// LEFT BOTTOM SQAURE
			{ x - hight / 2 + width, y - hight / 2 , 0.0f },
			{ x - hight / 2 , y - hight / 2 , 0.0f },
			{ x - hight / 2 , y - hight / 2 - width , 0.0f},
			{ x - hight / 2 + width, y - hight / 2 - width , 0.0f}
		},
		{   // LEFT TOP SQUARE
			{ x + hight / 2 - width , y + hight / 2 + width, 0.0f},
			{ x + hight / 2  , y + hight / 2 + width, 0.0f},
			{ x + hight / 2  , y + hight / 2, 0.0f},
			{ x + hight / 2 - width, y + hight / 2 , 0.0f }
		},
		{ // LEFT UP SIDE RECTANGLE
			{ x + hight / 2 - width, y + hight / 2 , 0.0f },
			{ x + hight / 2  , y + hight / 2, 0.0f},
			{ x + hight / 2, y , 0.0f},
			{ x + hight / 2 - width, y , 0.0f }
		},
		{ // LEFT DOWN SIDE RECTANGLE
			{ x + hight / 2 - width, y , 0.0f },
			{ x + hight / 2, y , 0.0f},
			{ x + hight / 2 , y - hight / 2 , 0.0f },
			{ x + hight / 2 - width, y - hight / 2 , 0.0f }
		},
		{ // LEFT DOWN SQUARE
			{ x + hight / 2 , y - hight / 2 , 0.0f },
			{ x + hight / 2 - width, y - hight / 2 , 0.0f },
			{ x + hight / 2 - width , y - hight / 2 - width, 0.0f },
			{ x + hight / 2  , y - hight / 2 - width , 0.0f }
		},
		{ // MIDDLE UP RECTANGLE
			{ x - hight / 2 + width, y + hight / 2 + width, 0.0f },
			{ x - hight / 2 + width, y + hight / 2 , 0.0f },
			{ x - 0.06f, y , 0.0f },
			{ x + 0.06f, y , 0.0f }
		},
		{// MIDDLE DOWN RECTANGLE
			{ x + 0.06f, y , 0.0f},
			{ x - 0.06f, y , 0.0f},
			{ x + (hight / 2) - width, y - hight / 2 - width , 0.0f },
			{ x + (hight / 2) - width, y - hight / 2 , 0.0f }
		}

	};

	const int numberOfRectangleForN = sizeof(rectangleForN) / sizeof(rectangleForN[0]);

	int arrayColorForN[numberOfRectangleForN * 2] = {
														ORANGE_ID , ORANGE_ID, // RIGHT TOP SQAURE
														ORANGE_ID, WHITE_ID,   // RIGHT UP SIDE RECTANGLE
														WHITE_ID, GREEN_ID,    // RIGHT DOWN SIDE RECTANGLE
														GREEN_ID , GREEN_ID,	 // RIGHT DOWN SQUARE
														ORANGE_ID, ORANGE_ID,	  // LEFT TOP SQAURE
														ORANGE_ID, WHITE_ID,	 // LEFT UP SIDE RECTANGLE
														WHITE_ID, GREEN_ID,	 // LEFT DOWN SIDE RECTANGLE
														GREEN_ID, GREEN_ID,   // LEFT DOWN SQUARE
														ORANGE_ID, WHITE_ID,	 // MIDDLE UP RECTANGLE
														WHITE_ID, GREEN_ID	 // MIDDLE DOWN RECTANGLE
	};

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, translatefy, -TRANSLATEZ);
	DrawRectangles(rectangleForN, numberOfRectangleForN, arrayColorForN);

}

void D(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y, GLfloat angle) {
	// FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);
	void DrawColor(int, TRICOLORS);
	void UpdateColor();

	// VARIABLE DECLARATION
	RECTANGLE rectangleForD[] = {
						{ // ORANGE RECTANGLE
							{ x - hight / 2, y + hight / 2 + width, 0.0f },
							{ x - hight / 2  , y + hight / 2, 0.0f},
							{ x + hight / 2 , y + hight / 2 , 0.0f },
							{ x + hight / 2 , y + hight / 2 + width , 0.0f }
						},
						{ // GREEN RECTANGLE
							{ x - hight / 2, y - hight / 2 , 0.0f },
							{ x - hight / 2  , y - hight / 2 - width, 0.0f},
							{ x + hight / 2 , y - hight / 2 - width, 0.0f },
							{ x + hight / 2 , y - hight / 2  , 0.0f }
						},
						{ // RIGHT VERTICLE UP RECTANGLE
							{x + hight / 2, y + hight / 2, 0.0f },
							{x + hight / 2 - width , y + hight / 2, 0.0f },
							{x + hight / 2 - width , y , 0.0f },
							{x + hight / 2   , y , 0.0f }

						},
						{ // RIGHT VERTICLE DOWN RECTANGLE
							{x + hight / 2   , y , 0.0f },
							{x + hight / 2 - width , y , 0.0f },
							{x + hight / 2 - width , y - hight / 2, 0.0f },
							{x + hight / 2 , y - hight / 2, 0.0f }
						},
						{ // LEFT VERTICLE UP RECTANGLE
							{x - hight / 2 + width , y + hight / 2, 0.0f },
							{x - hight / 2 + width + width , y + hight / 2, 0.0f },
							{x - hight / 2 + width + width , y , 0.0f },
							{x - hight / 2 + width  , y , 0.0f }

						},
						{ // LEFT VERTICLE DOWN RECTANGLE
							{x - hight / 2 + width  , y , 0.0f },
							{x - hight / 2 + width + width , y , 0.0f },
							{x - hight / 2 + width + width , y - hight / 2, 0.0f },
							{x - hight / 2 + width  , y - hight / 2, 0.0f },

						}
	};

	const int numberOfRectangleForD = sizeof(rectangleForD) / sizeof(rectangleForD[0]);

	int arrayColorForD[numberOfRectangleForD * 2] = { ORANGE_ID, ORANGE_ID, GREEN_ID, GREEN_ID, ORANGE_ID, WHITE_ID, WHITE_ID ,GREEN_ID , ORANGE_ID, WHITE_ID, WHITE_ID, GREEN_ID };

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, translatefy, -TRANSLATEZ);
	for (int i = 0; i < numberOfRectangleForD; i++) {
		glBegin(GL_QUADS);
		DrawColor(arrayColorForD[i * 2], tricolorsForD);
		glVertex3f(rectangleForD[i].a.x, rectangleForD[i].a.y, rectangleForD[i].a.z);
		glVertex3f(rectangleForD[i].b.x, rectangleForD[i].b.y, rectangleForD[i].b.z);
		DrawColor(arrayColorForD[i * 2 + 1], tricolorsForD);
		glVertex3f(rectangleForD[i].c.x, rectangleForD[i].c.y, rectangleForD[i].c.z);
		glVertex3f(rectangleForD[i].d.x, rectangleForD[i].d.y, rectangleForD[i].d.z);
		glEnd();
		UpdateColor();		
	}
}

void A(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y, GLfloat angle) {
	// FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);

	//VARIABLE DECLARATION

	RECTANGLE rectangleForA[] = {
								{ // TOP RECTANGLE
									{x + 0.14F , y + hight / 2 + width, 0.0f },
									{x - 0.14F , y + hight / 2 + width, 0.0f},
									{x - width , y + hight / 2 , 0.0f },
									{x + width , y + hight / 2, 0.0f }
								},
								{ // LEFT TOP RACTANGLE
									{x  , y + hight / 2, 0.0f },
									{x - width , y + hight / 2, 0.0f },
									{x - 0.40f , y , 0.0f },
									{x - 0.20f , y , 0.0f }
								},
								{ // LEFT DOWN RECTANGLE
									{x - 0.20f , y , 0.0f },
									{x - 0.40f , y , 0.0f },
									{x - hight / 2 , y - hight / 2, 0.0f },
									{x - hight / 2 + width, y - hight / 2, 0.0f }
								},
								{ // RIGHT TOP RACTANGLE
									{x + width , y + hight / 2, 0.0f },
									{x  , y + hight / 2, 0.0f },
									{x + 0.20f , y , 0.0f },
									{x + 0.40f , y , 0.0f }
								},
								{ // RIGHT DOWN RECTANGLE
									{x + 0.40f , y , 0.0f },
									{x + 0.20f , y , 0.0f },
									{x + hight / 2 - width, y - hight / 2, 0.0f },
									{x + hight / 2 , y - hight / 2, 0.0f }
								},
								{ // RIGHT DOWN SQUARE
									{x + hight / 2 , y - hight / 2, 0.0f },
									{x + hight / 2 - width, y - hight / 2, 0.0f },
									{x + hight / 2 - width + 0.075f , y - hight / 2 - width, 0.0f },
									{x + hight / 2 + 0.075f , y - hight / 2 - width, 0.0f }
								},
								{ // LEFT DOWN RECTANGLE
									{x - hight / 2 + width, y - hight / 2, 0.0f },
									{x - hight / 2 , y - hight / 2, 0.0f },
									{x - hight / 2 - 0.075f, y - hight / 2 - width, 0.0f },
									{x - hight / 2 + width - 0.075f, y - hight / 2 - width, 0.0f }
								},
	};

	const int numberOfRectangleForA = sizeof(rectangleForA) / sizeof(rectangleForA[0]);

	int arrayColorForA[numberOfRectangleForA * 2] = { ORANGE_ID, ORANGE_ID, ORANGE_ID, WHITE_ID, WHITE_ID , GREEN_ID, ORANGE_ID, WHITE_ID, WHITE_ID, GREEN_ID, GREEN_ID , GREEN_ID, GREEN_ID, GREEN_ID };

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, 0.0f, -TRANSLATEZ);
	DrawRectangles(rectangleForA, numberOfRectangleForA, arrayColorForA);
}

void Tiranga(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y, GLfloat angle) {
	// FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);

	RECTANGLE rectangleForTiranga[] = {
										{ // TOP RECTANGLE
											{x + 0.3f , y + 0.1f, 0.0f },
											{x - 0.3f , y + 0.1f, 0.0f},
											{x - 0.3f, y , 0.0f },
											{x + 0.3f , y , 0.0f }
										},
										{ // TOP RECTANGLE
											{x + 0.3f , y , 0.0f },
											{x - 0.3f , y , 0.0f},
											{x - 0.3f, y - 0.1f, 0.0f },
											{x + 0.3f , y - 0.1f, 0.0f }
										},
										{ // TOP RECTANGLE
											{x + 0.3f , y - 0.1f , 0.0f },
											{x - 0.3f , y - 0.1f , 0.0f},
											{x - 0.3f, y - 0.2f, 0.0f },
											{x + 0.3f , y - 0.2f, 0.0f }
										}
	};
	const int numberOfRectangleForTiranga = sizeof(rectangleForTiranga) / sizeof(rectangleForTiranga[0]);

	int arrayColorForTiranga[numberOfRectangleForTiranga * 2] = { ORANGE_ID, ORANGE_ID, WHITE_ID, WHITE_ID, GREEN_ID, GREEN_ID };

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, translatefy, -TRANSLATEZ);
	DrawRectangles(rectangleForTiranga, numberOfRectangleForTiranga, arrayColorForTiranga);
}

// down Plane
void IAFPlane(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y, GLfloat angle) {
	// FUNCTION DECLARATION
	void DrawColor(int, TRICOLORS);
	RECTANGLE rectangleForIAFPlane[] = {
											{
												{ x + 0.6f, y + 0.1f, 0.0f},
												{ x - 0.3f, y + 0.1f, 0.0f },
												{ x - 0.3f, y - 0.1f, 0.0f },
												{ x + 0.6f, y - 0.1f, 0.0f}
											},
											{
												{ x + 0.6f, y + 0.1f, 0.0f},
												{ x + 0.6f, y - 0.1f, 0.0f},
												{ x + 0.8f, y , 0.0f},
												{ x + 0.8f, y , 0.0f}
											},
											{
												{ x + 0.3f, y + 0.1f, 0.0f},
												{ x - 0.3f, y + 0.2f, 0.0f},
												{ x - 0.3f, y - 0.2f, 0.0f},
												{ x + 0.3f, y - 0.1f, 0.0f}
											},
											{
												{ x + 0.4f, y + 0.1f, 0.0f },
												{ x , y + 0.4f, 0.0f },
												{ x - 0.1f, y + 0.4f, 0.0f },
												{ x - 0.1f, y + 0.1f, 0.0f }
											},
											{
												{ x + 0.4f, y - 0.1f, 0.0f },
												{ x - 0.1f, y - 0.1f, 0.0f },
												{ x - 0.1f, y - 0.4f, 0.0f},
												{ x , y - 0.4f, 0.0f},
											},
											{ 
												{ x - 0.3f, y + 0.07f, 0.0f },
												{ x - 0.3f, y + 0.19f, 0.0f },
												{ x - 1.0f, y + 0.19f, 0.0f },
												{ x - 1.0f, y + 0.07f, 0.0f }
											},
											{ 
												{ x - 0.3f, y - 0.07f, 0.0f },
												{ x - 0.3f, y + 0.07f, 0.0f },
												{ x - 1.0f, y + 0.07f, 0.0f },
												{ x - 1.0f, y - 0.07f, 0.0f }
											},
											{ 
												{ x - 0.3f, y - 0.19f, 0.0f },
												{ x - 0.3f, y - 0.07f, 0.0f },
												{ x - 1.0f, y - 0.07f, 0.0f },
												{ x - 1.0f, y - 0.19f, 0.0f },
											}
	};
	const int numberOfRectangleForIAFPlane = sizeof(rectangleForIAFPlane) / sizeof(rectangleForIAFPlane[0]);

	int arrayColorForTiranga[numberOfRectangleForIAFPlane * 2] = { NAVYBLUE_ID, NAVYBLUE_ID,NAVYBLUE_ID, NAVYBLUE_ID,NAVYBLUE_ID, NAVYBLUE_ID,NAVYBLUE_ID, NAVYBLUE_ID,NAVYBLUE_ID, NAVYBLUE_ID, ORANGE_ID, BLACK_ID, WHITE_ID, BLACK_ID, GREEN_ID, BLACK_ID };
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, translatefy, -8.0f);
	glRotatef(angle, 0.0f, 0.0f, 1.0f);
	DrawRectangles(rectangleForIAFPlane, numberOfRectangleForIAFPlane, arrayColorForTiranga);

}


void Update()
{
	
	GLfloat angle = 0.0f;
	// FUNCTION DECLARATION
	if (showI_1 == 1){
		if (positionForI_1 <= FINAL_POSITION_FOR_I_1) {
			positionForI_1 += speed;
		}
		else {
			showA = 1;
			showCharacter = 2;
		}
	}
	if (showA == 1) {
		if (positionForA >= FINAL_POSITION_FOR_A) {
			positionForA -= speed;
		}
		else {
			showD = 1;
			showCharacter = 3;
		}
	}

	if (tricolorsForD.WHITE.red >= 1) {
		showCharacter = 4;
		showN = 1;
	}

	if (showN == 1) {
		if (positionForN >= FINAL_POSITION_FOR_N) {
			positionForN -= speed;
		}
		else {
			showCharacter = 5;
			showI_2 = 1;
		}
	}

	if (showI_2 == 1) 
	{
		if (positionForI_2 <= FINAL_POSITION_FOR_I_2) {
			positionForI_2 += speed;
		}
		else {
			showCharacter = 8;
			startPlane = 1;
		}
	}

	if (startPlane == 1 && scatterPlane == 0) {
		if (countForRotation % 7 == 0 && i_for_plane_up < 270) //i_for_plane_up < 270  xpositionForUpIAFPlane < -4.0
		{
			// for up plane
			angleForUpIAFPlane += 1;
			angle = i_for_plane_up * 0.01745329;
			i_for_plane_up++;
			xpositionForUpIAFPlane = -3.8f + (radius * cos(angle));
			ypositionForUpIAFPlane = 3.5f + ((radius + 1.5f) * sin(angle));

			// for down plane
			angleForDownIAFPlane -= 1;

			angle = i_for_plane_down * 0.01745329;
			i_for_plane_down--;
			xpositionForDownIAFPlane = -3.8f + (radius * cos(angle));
			ypositionForDownIAFPlane = -3.5f + ((radius + 1.5f) * sin(angle));

			xpositionForIAFPlane = xpositionForIAFPlane + planSpeed;
		}
		else if(countForRotation % 7 == 0)
		{
			xpositionForIAFPlane = xpositionForIAFPlane + planSpeed;
			xpositionForUpIAFPlane = xpositionForUpIAFPlane + planSpeed;
			xpositionForDownIAFPlane = xpositionForDownIAFPlane + planSpeed;
		}

	}
	
	if (scatterPlane == 1) {
		if (countForRotation % 7 == 0 && i_for_plane_up < 402) //i_for_plane_up < 270  xpositionForUpIAFPlane < -4.0
		{
			// for up plane
			angleForUpIAFPlane += 1;
			angle = i_for_plane_up * 0.01745329;
			i_for_plane_up++;
			xpositionForUpIAFPlane = 3.9f + ((radius ) * cos(angle));
			ypositionForUpIAFPlane = 5.0f + ((radius + 3.0f) * sin(angle));

			// for down plane
			angleForDownIAFPlane -= 1;

			angle = i_for_plane_down * 0.01745329;
			i_for_plane_down--;
			xpositionForDownIAFPlane = 3.9f + ((radius ) * cos(angle));
			ypositionForDownIAFPlane = -5.5f + ((radius + 3.5f) * sin(angle));
			
			xpositionForIAFPlane = xpositionForIAFPlane + planSpeed;
		}
	}

	if (xpositionForIAFPlane >= FINAL_POSITION_FOR_A) {
		showFlag = 1;
	}
	if (xpositionForIAFPlane >= 3.9f) {
		scatterPlane = 1;
	}

	countForRotation++;

	
}

void Uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext = gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}









































