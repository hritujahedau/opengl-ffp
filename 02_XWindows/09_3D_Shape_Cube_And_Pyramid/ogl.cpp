#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

#include "ogl.h"
//namespace
using namespace std;

//global variable declarations
bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
GLXContext gGLXContext; // HGLRC

GLfloat x = 0.5f, y = 0.5f, z = 0.5f;
GLfloat appex = 0.5f, depth = 0.5;

RECTANGLE rectangles[] = {  {
								{ -x, y, z } , {-x,-y,z}, {x,-y,z}, {x,y,z} // 1st
							}, 
							{ 
								{x, y, z}, { x, -y, z }, { x,-y,-z }, { x,y,-z } // 2nd
							},   
							{ 
								{x,y,-z}, {x,-y,-z}, {-x,-y,-z}, {-x,y,-z} // 3rd
							},
							{
								{-x,y,-z}, {-x,-y,-z}, {-x,-y,z}, {-x,y,z} //4th
							},
							{
								{-x,y,z}, {x,y,z}, {x,y,-z}, {-x,y,-z}
							},
							{
								{-x,-y,z}, {x,-y,z},{z,-y,-z},{-x,-y,-z}
							}
						};

TRIANGLE triangles[] = {
	{  { 0.0f,appex,0.0f	},  {-x, -x, depth}, { x,-y,depth}},
	{  { 0.0f,appex,0.0f },  { -x,-x,depth },  {-x,-y,-depth} },
	{  { 0.0f,appex,0.0f },  {-x,-x,-depth} ,  {x,-y,-depth} },
	{  { 0.0f,appex,0.0f },  {x,-x,-depth} ,  {x,-y,depth} }
};
						
const int numberOfRectangles = sizeof(rectangles) / sizeof(rectangles[0]);

COLORS rectangleColors[] = { { 1.0f, 0.0f, 0.0f },
					{ 0.0f, 1.0f, 0.0f},
					{ 0.0f, 0.0f, 1.0f},
					{ 1.0f, 1.0f, 0.0f},
					{ 0.0f, 1.0f, 1.0f},
					{ 1.0f, 0.0f, 1.0f},
					{ 1.0f , 0.5f, 0.0f },
					{0.0f, 1.0f, 0.5f}
};


TRIANGLE_COLORS triangleColors[] = { {1.0f, 0.5f, 0.5f},
					{ 0.5f , 0.5f, 1.0f },
					{ 0.0f, 1.0f, 0.5f},
					{ 1.0f, 1.0f, 0.5f}
					};


//entry point function
int main(void)
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Initialize();
	void Resize(int,int);
	void Render();
	void Uninitialize();

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	//code
	CreateWindow();
	Initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;
	bool bDone=false;

	while(bDone==false)
	{
	    while(XPending(gpDisplay))
	    {
		XNextEvent(gpDisplay, &event);
		switch(event.type)
		{
			case MapNotify:
				break;
			case KeyPress:
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0,0);
				switch(keysym)
				{
					case XK_Escape:
						bDone=true;
						Uninitialize();
						//exit(0); no need as we are checking in while condition
					case XK_F:
					case XK_f:
						if(bFullScreen == false)
						{
							ToggleFullScreen();
							bFullScreen=true;
						}
						else
						{
							ToggleFullScreen();
							bFullScreen=false;
						}
						break;
					default:
						break;
	
				}
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					default:
						break;
				}
				break;
			case MotionNotify:
				break;
			case ConfigureNotify:
				winWidth=event.xconfigure.width;
				winHeight=event.xconfigure.height;
				Resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33:
				bDone = true;
				Uninitialize();
				//exit(0); no need as we are checking in while condition
			default:
				break;
			
		} // switch
	    } // XPending Loop
	    Render();
	}
	Uninitialize();
	return 0;
}

void CreateWindow(void)
{

	//function declaration
	void Uninitialize();

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
//	int defaultDepth;
	int styleMask;

	static int framebufferAttribes[] = 
				{
					GLX_DOUBLEBUFFER, True,
					GLX_RGBA,
					GLX_RED_SIZE, 8, 
					GLX_GREEN_SIZE, 8,
					GLX_BLUE_SIZE, 8,
					GLX_ALPHA_SIZE, 8,
					GLX_DEPTH_SIZE, 24,
					None // NULL o or None
				};

	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error: Uable To Open X Display.\nExiting Now...\n ");
		Uninitialize();
		exit(1);
	}
//	defaultScreen=XDefaultScreen(gpDisplay);
//	defaultDepth=DefaultDepth(gpDisplay, defaultScreen);
	gpXVisualInfo=glXChooseVisual(gpDisplay, defaultScreen, framebufferAttribes);
	if(gpXVisualInfo==NULL)
	{
		printf("Error: Unable To Allocate Memory For XVisual Info.\nExiting Now..");
		Uninitialize();
		exit(1);
	}

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			gpXVisualInfo->visual,
			AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask
				| KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);
	if(!gWindow)
	{
		printf("Error: Failed To Create Main Window.\nExiting Now...\n");
		Uninitialize();	
		exit(1);
	}
	XStoreName(gpDisplay, gWindow, "OGL");
	Atom windowManagerDelete=XInternAtom(gpDisplay, "WM_DELETE_WINDOW", true);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state=XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(XEvent));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen ? 0 : 1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN", false);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay, 
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);
}

void Initialize()
{
	// Function Declaration
	void Resize(int, int);
	
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE); //3 shared ahe ka? 4th hw redernder = true
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	Resize(giWindowWidth, giWindowHeight);

}

void Resize(int width, int hight)
{
	if (hight == 0) {
		hight = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,((GLfloat)width / (GLfloat) hight), 0.1f, 100.0f);

	
}

void Render()
{
	//FUNCTION DECLARATION
	void Rectangle();
	void Triangle();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Rectangle();
	Triangle();

	glEnd();

	glXSwapBuffers(gpDisplay, gWindow);
}


void Triangle() {
	//VARIABLE DECLARATION
	static GLfloat angle = 0.0f;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(-1.5f, 0.0f, -6.0f);
	//glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(angle, 0.0f, 1.0f, 0.0f);

	for (int i = 0; i < 4; i++) {
		glBegin(GL_TRIANGLES);
		glColor3f(triangleColors[i].side1.red, triangleColors[i].side1.green, triangleColors[i].side1.blue);
		glVertex3f(triangles[i].appex.x, triangles[i].appex.y, triangles[i].appex.z);

		//glColor3f(triangleColors[i].side2.red, triangleColors[i].side2.green, triangleColors[i].side2.blue);
		glVertex3f(triangles[i].x.x, triangles[i].x.y, triangles[i].x.z);

		//glColor3f(triangleColors[i].side3.red, triangleColors[i].side3.green, triangleColors[i].side3.blue);
		glVertex3f(triangles[i].y.x, triangles[i].y.y, triangles[i].y.z);
		glEnd();
	}

	angle += 0.01f;
}

void Rectangle() {
	//VARIABLE DECLARATION
	static GLfloat angle = 0.0f;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 0.0f, -6.0f);
	glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(angle, 1.0f, 1.0f, 1.0f);

	for (int i = 0; i < numberOfRectangles; i++) {
		glBegin(GL_QUADS);

		glColor3f(rectangleColors[i].red, rectangleColors[i].green, rectangleColors[i].blue);
		glVertex3f(rectangles[i].a.x, rectangles[i].a.y, rectangles[i].a.z);

		glVertex3f(rectangles[i].b.x, rectangles[i].b.y, rectangles[i].b.z);

		glVertex3f(rectangles[i].c.x, rectangles[i].c.y, rectangles[i].c.z);

		glVertex3f(rectangles[i].d.x, rectangles[i].d.y, rectangles[i].d.z);
		
		glEnd();
	}
	angle += 0.01f;
}


void Uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext = gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}









































