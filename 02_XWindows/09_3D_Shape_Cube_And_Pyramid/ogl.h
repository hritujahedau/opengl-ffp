typedef struct __POINTF {
	GLfloat x, y, z;
} POINTF;

typedef struct __RECTANGLE {
	POINTF a, b, c ,d;
} RECTANGLE;

typedef struct __COLORS {
	GLfloat red, green, blue;
} COLORS;

typedef struct Tringle {
	POINTF appex, x, y;
} TRIANGLE;

typedef struct __TRAINGLE_COLORS {
	COLORS side1, side2, side3;
} TRIANGLE_COLORS;