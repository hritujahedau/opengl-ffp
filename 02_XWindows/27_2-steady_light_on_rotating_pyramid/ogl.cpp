#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

#include "ogl.h"
//namespace
using namespace std;

//global variable declarations
bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
GLXContext gGLXContext; // HGLRC


GLfloat glLightPosition_0[] = {100.0f, 100.0f, 100.0f, 1.0f};
GLfloat glLightAmbient_0[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat glLightDiffuse_0[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat glLightSpecular_0[] = { 0.0f, 0.0f, 1.0f, 1.0f };

GLfloat glLightPosition_1[] = {-100.0f, 100.0f, 100.0f, 1.0f };
GLfloat glLightAmbient_1[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat glLightDiffuse_1[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat glLightSpecular_1[] = { 0.0f, 0.0f, 1.0f, 1.0f };


GLfloat glMaterialAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat glMaterialDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat glMaterialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat glMaterialShininess = 128.0f;

bool gbLight = false;

//entry point function
int main(void)
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Initialize();
	void Resize(int,int);
	void Render();
	void Uninitialize();

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	//code
	CreateWindow();
	Initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;
	bool bDone=false;
	XKeyEvent xKeyEvent;
	char keys[26];

	while(1)
	{
		XNextEvent(gpDisplay, &event);
		switch(event.type)
		{
			case MapNotify:
				break;
			case KeyPress:
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0,0);
				switch(keysym)
				{
					case XK_Escape:
						bDone=true;
						Uninitialize();
						exit(0);
					default:
						break;
	
				}
				xKeyEvent.display = gpDisplay;
				xKeyEvent.window = gWindow;
				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);

				switch(keys[0])
				{
					case 'f':
					case 'F':
						if(bFullScreen == false)
						{
							ToggleFullScreen();
							bFullScreen = true;
						}
						else
						{
							ToggleFullScreen();
							bFullScreen = false;
						}
						break;
					case 'l':
					case 'L':
						if (gbLight == true)
						{
							gbLight = false;
							glDisable(GL_LIGHTING);
						}
						else 
						{
							gbLight = true;
							glEnable(GL_LIGHTING);					
						}
						break;
				}
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					default:
						break;
				}
				break;
			case MotionNotify:
				break;
			case ConfigureNotify:
				winWidth=event.xconfigure.width;
				winHeight=event.xconfigure.height;
				Resize(winWidth, winHeight);
				break;
			case Expose:
				break;
			case DestroyNotify:
				break;
			case 33:
				bDone = true;
				Uninitialize();
				exit(0); 
			default:
				break;
			
		} // switch
	    Render();
	}
	Uninitialize();
	return 0;
}

void CreateWindow(void)
{

	//function declaration
	void Uninitialize();

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
//	int defaultDepth;
	int styleMask;

	static int framebufferAttribes[] = 
				{
					GLX_DOUBLEBUFFER, True,
					GLX_RGBA,
					GLX_RED_SIZE, 8, 
					GLX_GREEN_SIZE, 8, 
					GLX_BLUE_SIZE, 8, 
					GLX_ALPHA_SIZE, 8, 
					GLX_DEPTH_SIZE, 24,
					None // NULL o or None
				};

	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("Error: Uable To Open X Display.\nExiting Now...\n ");
		Uninitialize();
		exit(1);
	}
//	defaultScreen=XDefaultScreen(gpDisplay);
//	defaultDepth=DefaultDepth(gpDisplay, defaultScreen);
	gpXVisualInfo=glXChooseVisual(gpDisplay, defaultScreen, framebufferAttribes);
	if(gpXVisualInfo==NULL)
	{
		printf("Error: Unable To Allocate Memory For XVisual Info.\nExiting Now..");
		Uninitialize();
		exit(1);
	}

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			gpXVisualInfo->visual,
			AllocNone);
	gColormap = winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask
				| KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);
	if(!gWindow)
	{
		printf("Error: Failed To Create Main Window.\nExiting Now...\n");
		Uninitialize();	
		exit(1);
	}
	XStoreName(gpDisplay, gWindow, "OGL");
	Atom windowManagerDelete=XInternAtom(gpDisplay, "WM_DELETE_WINDOW", true);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
	//variable declaration
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	//code
	wm_state=XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(XEvent));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen ? 0 : 1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN", false);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay, 
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);
}

void Initialize()
{
	// Function Declaration
	void Resize(int, int);
	
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glLightfv(GL_LIGHT0, GL_POSITION, glLightPosition_0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, glLightAmbient_0);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, glLightDiffuse_0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, glLightSpecular_0);
	glEnable(GL_LIGHT0);


	glLightfv(GL_LIGHT1, GL_POSITION, glLightPosition_1);
	glLightfv(GL_LIGHT1, GL_AMBIENT, glLightAmbient_1);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, glLightDiffuse_1);
	glLightfv(GL_LIGHT1, GL_SPECULAR, glLightSpecular_1);
	glEnable(GL_LIGHT1);


	glMaterialfv(GL_FRONT, GL_AMBIENT, glMaterialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, glMaterialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, glMaterialSpecular);
	glMaterialf(GL_FRONT, GL_SHININESS, glMaterialShininess);

	
	Resize(giWindowWidth, giWindowHeight);

}

void Resize(int width, int hight)
{
	if (hight == 0) {
		hight = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,((GLfloat)width / (GLfloat) hight), 0.1f, 100.0f);

	
}

void Render()
{
	// VARIABLE DECLARATION
	static GLfloat angle = 0.0f;

	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -6.0f);
	glRotatef(angle, 0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);

	// front face
	glNormal3f(0.0f, 0.447214f, 0.894427f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glVertex3f(-1.0f, -1.0f, 1.0f);

	glVertex3f(1.0f, -1.0f, 1.0f);

	glEnd();

	// right face

	glBegin(GL_TRIANGLES);

	glNormal3f(0.894427f, 0.447214f, 0.0f);

	glVertex3f(0.0f, 1.0f, 0.0f);

	glVertex3f(1.0f, -1.0f, 1.0f);

	glVertex3f(1.0f, -1.0f, -1.0f);

	glEnd();

	// back face

	glBegin(GL_TRIANGLES);
	glNormal3f(0.0f, 0.447214f, -0.894427f);

	glVertex3f(0.0f, 1.0f, 0.0f);

	glVertex3f(-1.0f, -1.0f, -1.0f);

	glVertex3f(1.0f, -1.0f, -1.0f);

	glEnd();

	// left face

	glBegin(GL_TRIANGLES);

	glNormal3f(-0.894427f, 0.447214f, 0.0f);

	glVertex3f(0.0f, 1.0f, 0.0f);

	glVertex3f(-1.0, -1.0f, 1.0f);

	glVertex3f(-1.0f, -1.0f, -1.0f);

	glEnd();

	angle = angle + 0.5f;

	glXSwapBuffers(gpDisplay, gWindow);
}



void Uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext = gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}









































