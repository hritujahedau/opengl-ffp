//Header File
#include<windows.h>
#include<stdio.h>
#include<gl/gl.h>
#include <mmsystem.h>
#include "headerfile.h"
#include <tchar.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "winmm.lib")

#define WIN_WIDTH 800
#define WIN_HIGHT 600
#define TIMER_ID 1234
#define MY_TIME 500

// Global Function Decalaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK PlaySoundForMe(LPVOID);

//global variable
bool gbActiveWindow = false;
FILE* gpFile;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
HWND ghwnd;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool bFullScreen = false;


float greenYBottom = -0.78f, greenYTop = greenYBottom + 0.1f, increaseYBy = 0.00068f; 
float whiteYBottom = greenYBottom + 0.1, whiteYTop = greenYTop + 0.1;
float orangeYBottom = greenYBottom + 0.2, orangeYTop = greenYTop + 0.2;
float flagx1 = -0.19f, flagx2 = flagx1 + 0.3f ;

float bottom1YBottom = -1.0f, bottom1YTop = -1.15f, increaseBottomYBy = 0.0007f; 
float bottom2YBottom = -1.15f, bottom2YTop = -1.3f ;
float bottom3YBottom = -1.4f, bottom3YTop = -1.5f;
float bottomx1 = -0.2f, bottomx2 = bottomx1;
int showFlag = 0;
int showKathi = 0;
int showBoy = 0;
float boyXAxis = 1.0f, moveBoyBy = -0.0007f;
bool isFlagPosTop = false, isPauseOver = false;
int isStickPauseOver = 0;
HINSTANCE ghInstnace = NULL;

//Entry Point Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	//function declaration
	void Initialize(void);
	void Display(void);

	//local variable decalaration
	WNDCLASSEX wndClass;
	MSG msg;
	TCHAR szClassName[] = TEXT("triangle");
	bool bDone = false;


	ghInstnace = hInstance;
	//code

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;

	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndClass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);

	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szClassName;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);

	ghwnd = CreateWindow(szClassName,
		TEXT("FLAG"),
		WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_CAPTION | WS_OVERLAPPED,
		100,
		100,
		WIN_WIDTH,
		WIN_HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	if (fopen_s(&gpFile, "window.log", "a+") != 0) {
		MessageBox(NULL, TEXT("File Cannot open"), TEXT("File Operation FAILED!"), MB_OK);
		exit(0);
	}
	Initialize();
	fprintf(gpFile, "Intialize() completed");
	SetForegroundWindow(ghwnd);
	SetFocus(ghwnd);
	ShowWindow(ghwnd, iCmdShow);

	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow == true) {
				Display();
			}
		}
	}

	return (int)(msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	//vlocal function declaration
	void ToggleFullScreen();
	void Resize(int, int);
	void Uninitialize(void);
	void Display(void);

	// local variable
	static HANDLE hSound = NULL;
	//code
	switch (uMsg) {
	case WM_CREATE:
		//hSound = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)PlaySoundForMe, (LPVOID)hwnd, 0, NULL);
		if (PlaySound(MAKEINTRESOURCE(SOUND), GetModuleHandle(NULL), SND_RESOURCE| SND_ASYNC) == FALSE) {
			MessageBox(NULL, TEXT("FAILED TO PLAY FROM WNDPROC"), TEXT("ERROR"), MB_OK);
		}
		SetTimer(hwnd, TIMER_ID, MY_TIME, NULL);
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return 0L;
		//break;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_KEYDOWN:
		switch (LOWORD(wParam)) {
		case VK_ESCAPE:
		case 0X46:
		case 0X66:
			ToggleFullScreen();
			break;
		}
		break;
	case WM_TIMER:
		KillTimer(hwnd, TIMER_ID);
		Display();
		if (isStickPauseOver == 1) {
			for (LONG i = 0; i < LONG_MAX; i++);
			isStickPauseOver = 2;
			showFlag = true;
		}
		SetTimer(hwnd, TIMER_ID, MY_TIME, NULL);
		break;
	case WM_DESTROY:
		if (hSound) {
			CloseHandle(hSound);
			hSound = NULL;
		}
		Uninitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

BOOL CALLBACK PlaySoundForMe(LPVOID param) {

	if (PlaySound(MAKEINTRESOURCE(SOUND), GetModuleHandle(NULL), SND_RESOURCE) == FALSE) {
		MessageBox(NULL, TEXT("FAILED TO PLAY FROM WNDPROC"), TEXT("ERROR"), MB_OK);
	}
	
	return TRUE;
}

void ToggleFullScreen() {
	MONITORINFO mi = { sizeof(MONITORINFO) };


	//code
	if (bFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
	else {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right, mi.rcMonitor.bottom,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
}

void Initialize(void) {
	//function declaration
	void Resize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code

	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() failed \n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "SetPixelFormat() Failed \n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "wglCreateContext() \n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "wglMakeCurrent() failed\n");
		DestroyWindow(ghwnd);

	}

	SelectObject(ghdc, GetStockObject(SYSTEM_FONT));

	if (wglUseFontBitmaps(ghdc, 0, 255, 100)) {
		fprintf(gpFile, "wglUseFontBitmaps() failed\n");
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	Resize(WIN_WIDTH, WIN_HIGHT);
}

void Resize(int width, int hight) {
	if (hight == 0) {
		hight = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)hight);
}

void FirstSeen(void) {
	//function declaration
	void Bottom1();
	void Bottom2();
	void Bottom3();
	void Kathi();
	void DrawBoy();
	void Flag();
	void Chakra();

	if (showKathi) {	
		if (isStickPauseOver == 0) {
			isStickPauseOver = 1;
		}
		Kathi();
	}


	if (showFlag) {
		if (orangeYTop < 0.9f) {
			orangeYTop += increaseYBy;
			orangeYBottom += increaseYBy;

			whiteYTop += increaseYBy;
			whiteYBottom += increaseYBy;

			greenYBottom += increaseYBy;
			greenYTop += increaseYBy;

		}
		if (orangeYTop >= 0.9f && isPauseOver == false) {
			isFlagPosTop = true;
			isPauseOver = true;
			showBoy = 1;
			for (LONG i = 0; i < LONG_MAX; i++);
			//for (LONG i = 0; i < LONG_MAX; i++);
			//for (LONG i = 0; i < LONG_MAX; i++);

		}
		Flag();		
	}

	if (bottom2YTop < -0.85f) {
		bottom2YBottom += increaseBottomYBy;
		bottom2YTop += increaseBottomYBy;
	}
	else {	
		showKathi = 1;
	}

	Bottom2();

	if (bottom1YTop < -1.0f) {
		bottom1YBottom += increaseBottomYBy;
		bottom1YTop += increaseBottomYBy;
	}

	Bottom1();

	if (showBoy) {
		if (boyXAxis > 0.4)
			boyXAxis += moveBoyBy;
		DrawBoy();
	}

}
void Flag(void) {
	//function declaration
	void OrangeRectangle();
	void WhiteRectangle();
	void GreenRectangle();
	void Chakra();
	//code
	OrangeRectangle();
	WhiteRectangle();
	GreenRectangle();
	Chakra();
	
}

void Display(void)
{
	//function declaration
	void FirstSeen(void);
	
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	FirstSeen();
	SwapBuffers(ghdc);
}

void DrawBoy() {


	glColor3f(1.0f, 1.0f, 1.0f);

	//Head 

	glBegin(GL_QUADS);	

	glVertex3f(boyXAxis + 0.1f, 0.25, 0.0f);

	glVertex3f(boyXAxis -0.1f, 0.25, 0.0f);

	glVertex3f(boyXAxis -0.1f, 0.0, 0.0f);

	glVertex3f(boyXAxis + 0.1f, 0.0, 0.0f);

	glEnd();

	glColor3f(0.0f, 0.0f, 0.0f);
	glPointSize(7);
	glBegin(GL_POINTS);

	glVertex3f(boyXAxis + 0.04f, 0.20f,0.0f);
	glVertex3f(boyXAxis - 0.04f, 0.20f,0.0f);

	glEnd();

	//mask

	glColor3f(0.5f, 0.0f, 0.0f);

	glBegin(GL_QUADS);	

	glVertex3f(boyXAxis + 0.07f, 0.16, 0.0f);

	glVertex3f(boyXAxis -0.07f, 0.16, 0.0f);

	glVertex3f(boyXAxis -0.07f, 0.04, 0.0f);

	glVertex3f(boyXAxis + 0.07f, 0.04, 0.0f);

	glEnd();

	// Lines 
	// Line 1	

	glBegin(GL_LINES);
	
	glVertex3f(boyXAxis + 0.07f, 0.16, 0.0f);
	glVertex3f(boyXAxis + 0.1f, 0.22, 0.0f);

	glEnd();

	//Line 2

	glBegin(GL_LINES);

	glVertex3f(boyXAxis -0.07f, 0.16, 0.0f);
	glVertex3f(boyXAxis -0.1f, 0.22, 0.0f);

	glEnd();

	// Line 3

	glBegin(GL_LINES);

	glVertex3f(boyXAxis -0.07f, 0.04, 0.0f);
	glVertex3f(boyXAxis -0.1f, 0.0, 0.0f);

	glEnd();

	// Line 4

	glBegin(GL_LINES);

	glVertex3f(boyXAxis + 0.07f, 0.04, 0.0f);
	glVertex3f(boyXAxis + 0.1f, 0.0, 0.0f);

	glEnd();

	glColor3f(1.0f, 1.0f, 1.0f);


	// Body

	glBegin(GL_QUADS);

	glVertex3f(boyXAxis + 0.1f, -0.1, 0.0f);

	glVertex3f(boyXAxis -0.1f, -0.1, 0.0f);

	glVertex3f(boyXAxis -0.1f, -0.7, 0.0f);

	glVertex3f(boyXAxis + 0.1f, -0.7, 0.0f);

	glEnd();

	glLineWidth(4);

	// Neck

	glBegin(GL_LINES);

	glVertex3f(boyXAxis + 0.0f, 0.0f, 0.0f);
	glVertex3f(boyXAxis + 0.0f, -0.1f, 0.0f);

	glEnd();

	// Hand

	

	glBegin(GL_LINES);

	glVertex3f(boyXAxis -0.1, -0.2f, 0.0f);
	glVertex3f(boyXAxis -0.3, -0.2f, 0.0f);

	glEnd();

	glBegin(GL_LINES);
	glVertex3f(boyXAxis -0.3, -0.2f, 0.0f);
	glVertex3f(boyXAxis -0.1, 0.22f, 0.0f);
	glEnd();

	//right

	glBegin(GL_LINES);
	glVertex3f(boyXAxis + 0.1, -0.2f, 0.0f);
	glVertex3f(boyXAxis + 0.15, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_LINES);	
	glVertex3f(boyXAxis + 0.15, -0.2f, 0.0f);
	glVertex3f(boyXAxis + 0.15, -0.8f, 0.0f);
	glEnd();

	// Legs
	glBegin(GL_LINES);
	glVertex3f(boyXAxis + 0.05, -0.7f, 0.0f);
	glVertex3f(boyXAxis + 0.05, -1.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(boyXAxis -0.05, -0.7f, 0.0f);
	glVertex3f(boyXAxis -0.05, -1.0f, 0.0f);
	glEnd();

	glLineWidth(1);
}

void Kathi() {

	glColor3f(0.0f, 1.0f, 1.0f);

	glBegin(GL_TRIANGLES);

	glVertex3f(-0.22f, 1.0f, 0.0f);
	glVertex3f(-0.2f, 0.9f, 0.0f);
	glVertex3f(-0.23f, 0.9f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);


	glVertex3f(-0.2f, bottom3YBottom, 0.0f);
	glVertex3f(-0.23f, bottom3YBottom, 0.0f);
	glVertex3f(-0.23f, 0.9f, 0.0f);
	glVertex3f(-0.2f, 0.9f, 0.0f);

	glEnd();

	glBegin(GL_LINES);

	glVertex3f(-0.19f, bottom3YBottom, 0.0f);
	glVertex3f(-0.19f, 0.9f, 0.0f);

	glEnd();
	glColor3f(1.0f, 1.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(-0.19f, 0.9f, 0.0f);
	glVertex3f(-0.23f, 0.9f, 0.0f);

	glEnd();

}

void Bottom1() {
	glBegin(GL_QUADS);

	glColor3f(0.5f, 0.0f, 0.0f);

	glVertex3f(bottomx2 + 0.4f, bottom1YTop, 0.0f);

	glVertex3f(bottomx1 -0.4f, bottom1YTop, 0.0f);

	glVertex3f(bottomx1 -0.4f, bottom1YBottom, 0.0f);

	glVertex3f(bottomx2 + 0.4f, bottom1YBottom, 0.0f);

	glEnd();
}

void Bottom2() {

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(bottomx2 + 0.3f, bottom2YTop, 0.0f);

	glVertex3f(bottomx1 -0.3f, bottom2YTop, 0.0f);

	glVertex3f(bottomx1 -0.3f, bottom2YBottom, 0.0f);

	glVertex3f(bottomx2 + 0.3f, bottom2YBottom, 0.0f);

	glEnd();
}

void Bottom3() {

	glBegin(GL_QUADS);

	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex3f(bottomx2 + 0.2f, bottom3YTop, 0.0f);

	glVertex3f(bottomx1 -0.2f, bottom3YTop, 0.0f);

	glVertex3f(bottomx1 -0.2f, bottom3YBottom, 0.0f);

	glVertex3f(bottomx2 + 0.2f, bottom3YBottom, 0.0f);

	glEnd();
}


void Chakra() {
	int xAxis = -0.2;
	
	glColor3f(0.0f, 0.0f, 1.0f);

	//verticale line	
	glBegin(GL_LINES);	
	
	glVertex3f(flagx1 + 0.15f, orangeYBottom, 0.0f);
	glVertex3f(flagx1 +0.15f, greenYTop, 0.0f);

	glEnd();

	// horizontal line line

	glBegin(GL_LINES);

	glVertex3f(flagx1 + 0.11f, whiteYBottom + 0.05f, 0.0f);
	glVertex3f(flagx1 + 0.19f, whiteYBottom + 0.05f, 0.0f);

	glEnd();

	// third line
	glBegin(GL_LINES);

	glVertex3f(flagx1 + 0.12f, whiteYBottom + 0.01f, 0.0f);
	glVertex3f(flagx1 + 0.18f, whiteYBottom + 0.09f, 0.0f);

	glEnd();

	// forth line
	glBegin(GL_LINES);

	glVertex3f(flagx1 + 0.12f, whiteYBottom + 0.09f, 0.0f);
	glVertex3f(flagx1 + 0.18f, whiteYBottom + 0.01f, 0.0f);

	glEnd();

	// join lines

	glBegin(GL_LINES);
	glVertex3f(flagx1 + 0.15f, orangeYBottom, 0.0f);
	glVertex3f(flagx1 + 0.12f, whiteYBottom + 0.09f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(flagx1 + 0.12f, whiteYBottom + 0.09f, 0.0f);
	glVertex3f(flagx1 + 0.11f, whiteYBottom + 0.05f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(flagx1 + 0.11f, whiteYBottom + 0.05f, 0.0f);
	glVertex3f(flagx1 + 0.12f, whiteYBottom + 0.01f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(flagx1 + 0.12f, whiteYBottom + 0.01f, 0.0f);
	glVertex3f(flagx1 + 0.15f, greenYTop, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(flagx1 + 0.15f, greenYTop, 0.0f);
	glVertex3f(flagx1 + 0.18f, whiteYBottom + 0.01f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(flagx1 + 0.18f, whiteYBottom + 0.01f, 0.0f);
	glVertex3f(flagx1 + 0.19f, whiteYBottom + 0.05f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(flagx1 + 0.19f, whiteYBottom + 0.05f, 0.0f);
	glVertex3f(flagx1 + 0.18f, whiteYBottom + 0.09f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(flagx1 + 0.18f, whiteYBottom + 0.09f, 0.0f);
	glVertex3f(flagx1 + 0.15f, orangeYBottom, 0.0f);
	glEnd();

	
}

void OrangeRectangle() {


	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.25f, 0.0f);

	glVertex3f(flagx1, orangeYTop, 0.0f);

	glVertex3f(flagx2, orangeYTop, 0.0f);

	glVertex3f(flagx2, orangeYBottom, 0.0f);

	glVertex3f(flagx1, orangeYBottom, 0.0f);

	glEnd();
}

void WhiteRectangle() {

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(flagx1, whiteYTop, 0.0f);

	glVertex3f(flagx2, whiteYTop, 0.0f);

	glVertex3f(flagx2, whiteYBottom, 0.0f);

	glVertex3f(flagx1, whiteYBottom, 0.0f);

	glEnd();
}


void GreenRectangle() {

	glBegin(GL_QUADS);

	glColor3f(0.0f, 0.4f, 0.0f);

	glVertex3f(flagx1, greenYTop, 0.0f);

	glVertex3f(flagx2, greenYTop, 0.0f);

	glVertex3f(flagx2, greenYBottom, 0.0f);

	glVertex3f(flagx1, greenYBottom, 0.0f);

	glEnd();
}

void Uninitialize(void) {
	if (gpFile) {
		fclose(gpFile);
		gpFile = NULL;
	}

	if (bFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		bFullScreen = false;
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fclose(gpFile);
		gpFile = NULL;
	}
}