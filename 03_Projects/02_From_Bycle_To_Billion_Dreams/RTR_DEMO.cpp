// header file declaration
#include "RTR_DEMO.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib, "winmm.lib")

#define TIME_FOR_SONG  40000 //40000
#define TIMER_ID 8712

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width = WIDTH, hight = HIGHT;

GLfloat hrh_rectangle_height = 1.0f, hrh_rectangle_width = 1.5f;
bool hrh_go = false, hrh_up = false, hrh_down = false, hrh_animation = false;

/************************************RECTANGLES***************************************/
HRH_RECTANGLE hrh_rectangle_isro, hrh_AstroMediComp_logo;

/*********************************LINES******************************************/
HRH_LINES *hrh_lines_for_cycle, *hrh_lines_for_cycle_2;

/*******************************CIRCLES*****************************************/
HRH_CIRCLES* hrh_circles, * hrh_circles_2;

/**********************************ROKCET 1********************************/
HRH_LINES* hrh_rocket_1;
int hrh_rocket_1_lines = 1;

/**********************************ROKCET 2********************************/
HRH_LINES* hrh_rocket_2;
int hrh_rocket_2_lines = 1;

/************************************FIRE**************************/
HRH_LINES* hrh_lines_for_fire;

/**********************************CAMERA****************************/
GLfloat hrh_camera_translate_x =0.0f, hrh_camera_translate_y = 0.0f, hrh_camera_translate_z = 6.0f;
GLfloat hrh_camera_angle_x = -0.1f, hrh_camera_angle_y = 0.0f, hrh_camera_angle_z = 0.0f;

GLfloat hrh_cycle_1_translate_x = 0.0f, hrh_cycle_1_translate_y = 0.0f, hrh_cycle_1_translate_z = 0.0f;
GLfloat hrh_cycle_2_translate_x = 2.0f, hrh_cycle_2_translate_y = 0.0f, hrh_cycle_2_translate_z = 0.0f;

GLfloat angle_for_seen_2 = 360.0f, angle_for_seen_1 = 360.0f;
GLfloat angle_for_cycle = 330; 

bool startAnimation = false;
GLfloat clearColor_red = 0.0f;
bool rocket_1_up = false;

GLfloat hrh_rocket_1_translate_x = 1.5f, hrh_rocket_1_translate_y = 2.55f, hrh_rocket_1_translate_z = 0.0f, hrh_rocket_1_angle = 360.0f;
GLfloat hrh_rocket_2_translate_x = 4.0f, hrh_rocket_2_translate_y = 2.4f, hrh_rocket_2_translate_z = 0.0f, hrh_rocket_2_angle = 360.0f;

GLuint isro_texture_id = 0, logo_texture_id = 0;
short nFontList;
GLfloat scale_size = 0.18f;
GLfloat string_translate_x = 6.0f, string_translate_y = 0.0f;
int seen = 1;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();
	void Update();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_rtr_demo");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "a+") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("RTR_DEMO"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);
	SetTimer(hwnd, TIMER_ID, TIME_FOR_SONG, NULL);
	if (PlaySound(ID_SOUND_1, GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC) == FALSE) {
		//MessageBox(NULL, TEXT("FAILED TO PLAY MUSIC"), TEXT("ERROR"), MB_OK);
	}

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			Display();
			Update();
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_CREATE:
			break;
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			width = LOWORD(lParam);
			hight = HIWORD(lParam);
			break;
		case WM_TIMER:
			KillTimer(hwnd, TIMER_ID);
			startAnimation = true;
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case 'g':
			case 'G':
				hrh_go = true;
				break;
			case 'u':
			case 'U':
				hrh_up = true;
				break;
			case 'd':
			case 'D':
				hrh_down = true;
				break;
			}
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int, int);
	void hrh_InitializeCycle1();
	void hrh_InitializeCycle2();
	void hrh_InitializeRocket_1();
	void hrh_InitializeRocket_2();
	void hrh_CopyLines(HRH_LINES*, HRH_LINES*);
	void hrh_CopyCircles(HRH_CIRCLES*, HRH_CIRCLES*);
	void hrh_InitializeFire();
	bool LoadGLTexture(GLuint*, TCHAR[]);
	void SetUpRC();

	// VARIABLR declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	// CODE
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	glClearColor(clearColor_red, 0.0f, 0.0f, 1.0f); // 0.15
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	hrh_InitializeCycle1();
	hrh_InitializeCycle2();
	hrh_InitializeRocket_1();
	hrh_InitializeRocket_2();
	hrh_InitializeFire();	
	SetUpRC();

	// Load Texture
	LoadGLTexture(&isro_texture_id, ID_ISRO_TEXTURE);
	LoadGLTexture(&logo_texture_id, ID_LOGO_TEXTURE);

	// Initialize rectangle for isro image
	hrh_rectangle_isro.x = 0.0f;
	hrh_rectangle_isro.y = -5.1f;
	hrh_rectangle_isro.z = 0.0f;
	hrh_rectangle_isro.height = 7;
	hrh_rectangle_isro.width = 4;

	// intialize rectangle for AstroMediComp logo
	hrh_AstroMediComp_logo.x = -3.0f;
	hrh_AstroMediComp_logo.y = -3.2f;
	hrh_AstroMediComp_logo.z = 0.0f;
	hrh_AstroMediComp_logo.height = 1.0;
	hrh_AstroMediComp_logo.width = 1.0;

	// warm up call
	Resize(WIDTH, HIGHT);
}

void SetUpRC()
{
	// VARIABLE DECLARATION
	HFONT hFont;
	GLYPHMETRICSFLOAT agmf[128];
	LOGFONT logfont;

	// CODE
	logfont.lfHeight = -10;
	logfont.lfWidth = 0;
	logfont.lfEscapement = 0;
	logfont.lfOrientation = 0;
	logfont.lfWeight = FW_BOLD;
	logfont.lfItalic = FALSE;
	logfont.lfUnderline = FALSE;
	logfont.lfStrikeOut = FALSE;
	logfont.lfCharSet = ANSI_CHARSET;
	logfont.lfOutPrecision = OUT_DEFAULT_PRECIS;
	logfont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	logfont.lfQuality = DEFAULT_QUALITY;
	logfont.lfPitchAndFamily = DEFAULT_PITCH;
	strcpy(logfont.lfFaceName, "Arial");

	hFont = CreateFontIndirect(&logfont);
	SelectObject(ghdc, hFont);

	nFontList = glGenLists(128);
	wglUseFontOutlines(ghdc, 0, 128, nFontList, 0.0f, 0.0f, WGL_FONT_POLYGONS, agmf);
	DeleteObject(hFont);
}


bool LoadGLTexture(GLuint* texture, char resourceId[])
{
	// VARIABLE DECLARATION
	bool bResult = false;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	// CODE
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bResult = true;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);

		// from here start OpenGL Texture code
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);

		//setting texture parameter
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		// following call will actually push the data with help of graphics driver
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);

		DeleteObject(hBitmap);
	}

	return bResult;
}

void Resize(int width, int hight) {
	if (hight == 0)
		hight = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, ((GLfloat)width/ (GLfloat)hight), 0.1f, 100.0f);
}

void Display() {
	//FUNCTION DECLARATION
	void hrh_DrawLines();
	void hrh_DrawCircles();
	void hrh_DrawRectangles();
	void hrh_DrawCycle();
	void hrh_DrawRocket_1();
	void hrh_DrawRocket_2();
	void hrh_Seen1_Cycle_Rockets();
	void hrh_Seen2_Cycle_Rockets();
	void hrh_Seen3();
	void hrh_Seen4();
	void hrh_Seen5();

	// VARIABLE DECLARATION
	GLfloat camera_up_x = 0.0f, camera_up_y = 1.0f, camera_up_z = 0.0f;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, ((GLfloat)width / (GLfloat)hight), 0.1f, 100.0f);

	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	gluLookAt(hrh_camera_translate_x, hrh_camera_translate_y, hrh_camera_translate_z, // WHERE CAMERA
		hrh_camera_angle_x, hrh_camera_angle_y, hrh_camera_angle_z,  // EYES OF CAMERA
		camera_up_x, camera_up_y, camera_up_z // UP AXIS OF CAMERA
		);

	if (startAnimation == false)
	{
		SwapBuffers(ghdc);
		return ;
	}	
	
	glPushMatrix();
	hrh_Seen1_Cycle_Rockets();
	glPopMatrix();
	glPushMatrix();
	if (seen == 2)
	{
		hrh_Seen2_Cycle_Rockets();
	}
	if (angle_for_seen_2 <= angle_for_cycle)
	{
		if (hrh_cycle_2_translate_x <= -2.2f)
		{
			hrh_Seen3(); 
		}
	}
	glPopMatrix();
	glPushMatrix();
	if (hrh_rocket_1_translate_y >= 7.0f && rocket_1_up == true) 
	{
		hrh_Seen4();
	}
	glPopMatrix();	
	glPushMatrix();
	if (hrh_rectangle_isro.x <= -4.0)
	{
		hrh_Seen5();
	}
	glPopMatrix();
	SwapBuffers(ghdc);
}

void Update()
{
	// FUNCTION DECLARATION
	void hrh_UpdateISRORectangle();

	// VARIABLE DECLARATION
	
	// CODE
	if (hrh_rocket_1_translate_y >= 7.0f && rocket_1_up == true)
	{
		hrh_UpdateISRORectangle();
	}

}

void hrh_UpdateISRORectangle()
{
	// VARIABLE DECLARATION
	static bool move_up = false;

	// CODE
	if (clearColor_red < 0.15)
	{
		clearColor_red = clearColor_red + 0.0001f;
	}

	if (hrh_rectangle_isro.y < 0)
	{
		hrh_rectangle_isro.y = hrh_rectangle_isro.y + 0.0022; 
	}
	if (hrh_rectangle_isro.y >= 0 && hrh_rectangle_isro.z < 1.2f && move_up == false) 
	{
		hrh_rectangle_isro.z = hrh_rectangle_isro.z + 0.0030; 
	}
	else if (hrh_rectangle_isro.y >= 0 && hrh_rectangle_isro.z >= 1.2f)
	{
		move_up = true;
	}

	if (move_up == true)
	{
		if (hrh_rectangle_isro.x > -4.0)
		{
			hrh_rectangle_isro.z = hrh_rectangle_isro.z - 0.004;
			hrh_rectangle_isro.x = hrh_rectangle_isro.x - 0.003;
		}
		else if (string_translate_y > 35) 
		{
			hrh_rectangle_isro.y = hrh_rectangle_isro.y + 0.004;		
		}
	}
}

void Uninitialize() 
{
	// FUNCTION DECLARATION
	void hrh_UninitializeLines();
	void hrh_UninitializeCircles();

	// CODE
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
	}

	glDeleteTextures(1, &isro_texture_id);
	glDeleteTextures(1, &logo_texture_id);

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	hrh_UninitializeLines();
	hrh_UninitializeCircles();

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}
		
}
