#pragma once
#include <windows.h>
#include <gl\GL.h>
#include <math.h>
#include <windows.h>
#include<stdio.h>
#include <stdlib.h>
#include <gl/glu.h>

#include <mmsystem.h>

#define MYICON 1287
#define SOUND_1  1288
#define ISRO_BITMAP 1289
#define LOGO_BITMAP 1290

#define IDI_MYICON MAKEINTRESOURCE(MYICON)
#define ID_SOUND_1 MAKEINTRESOURCE(SOUND_1)
#define ID_ISRO_TEXTURE MAKEINTRESOURCE(ISRO_BITMAP)
#define ID_LOGO_TEXTURE MAKEINTRESOURCE(LOGO_BITMAP)

#define WIDTH 1000
#define HIGHT 800
#define PI 22/7
#define RAD_TO_ANGLE 0.01745329
#define NR_RECTANGLES 5
#define NR_CIRCLES 4
#define NR_LINES 22

#define LINE_SPEED  0.0015 //0.0015 


enum {
	GREATER_THAN = 1,
	GREATER_THAN_EQUAL_TO,
	LESS_THAN,
	LESS_THAN_EQUAL_TO,
	HORIZONTAL,
	VERTICLE,
	ALL
};

typedef struct __HRH_POINT {
	GLfloat x, y, z;
} HRH_POINT;

typedef struct __RECTANGLE
{
	GLfloat x, y, z;
	GLfloat height, width;
} HRH_RECTANGLE;

typedef struct __HRH_CIRCLE
{
	HRH_POINT hrh_point;
	int currentCount;
	bool isCompleted;
	GLfloat radius;
} HRH_CIRCLE;

typedef struct __HRH_CIRCLES
{
	HRH_CIRCLE* hrh_circle;
	int current_circle, total_circles;
} HRH_CIRCLES;

typedef struct __HRH_LINE
{
	HRH_POINT fromPoint, toPoint, currentPoint, tillPoint;
	GLfloat m, distance;
	int condition, direction;
	bool isCompleted;
} HRH_LINE;

typedef struct __HRH_LINES
{
	HRH_LINE* list_of_line;
	int current_line, total_lines;
} HRH_LINES;

typedef struct __HRH_TWO_RECTANGLES
{
	HRH_RECTANGLE *hrh_left_rectangle, *hrh_right_rectangle;
} HRH_TWO_RECTANGLES;
