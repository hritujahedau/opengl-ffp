#include "RTR_DEMO.h"

/*************************LINES****************************************************/
extern HRH_LINES* hrh_lines_for_cycle, * hrh_lines_for_cycle_2;

/*************************CIRCLES********************************************************/
extern HRH_CIRCLES* hrh_circles, * hrh_circles_2;

/**********************************ROKCET 1********************************/
extern HRH_LINES* hrh_rocket_1;
extern int hrh_rocket_1_lines;

/**********************************ROKCET 2********************************/
extern HRH_LINES* hrh_rocket_2;
extern int hrh_rocket_2_lines;

/**********************************CAMERA****************************/
extern GLfloat hrh_camera_translate_x, hrh_camera_translate_y, hrh_camera_translate_z;
extern GLfloat hrh_camera_angle_x, hrh_camera_angle_y, hrh_camera_angle_z;

/*********************************FIRE*********************/
extern HRH_LINES* hrh_lines_for_fire;

/***************************************************************************************************************************************/


void hrh_InitializeFire()
{
	int i = 0;
	int number_of_lines_for_fire = 7;
	hrh_lines_for_fire = (HRH_LINES*)malloc(sizeof(HRH_LINES));
	hrh_lines_for_fire->list_of_line = (HRH_LINE*)malloc(sizeof(HRH_LINE) * number_of_lines_for_fire);
	hrh_lines_for_fire->current_line = 0;
	hrh_lines_for_fire->total_lines = number_of_lines_for_fire;


	(hrh_lines_for_fire->list_of_line + i)->fromPoint.x = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.y = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.z = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.x = -0.3f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.y = -0.5f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.z = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	(hrh_lines_for_fire->list_of_line + i)->m = ((hrh_lines_for_fire->list_of_line + i)->toPoint.y - (hrh_lines_for_fire->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_fire->list_of_line + i)->toPoint.x - (hrh_lines_for_fire->list_of_line + i)->fromPoint.x);
	(hrh_lines_for_fire->list_of_line + i)->distance = -LINE_SPEED;
	(hrh_lines_for_fire->list_of_line + i)->direction = VERTICLE;
	(hrh_lines_for_fire->list_of_line + i)->tillPoint.x = (hrh_lines_for_fire->list_of_line + i)->fromPoint.x;
	(hrh_lines_for_fire->list_of_line + i)->tillPoint.y = (hrh_lines_for_fire->list_of_line + i)->fromPoint.y;

	i = 1;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.x = 0.1f;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.y = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.z = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.x = -0.1f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.y = -0.7f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.z = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	(hrh_lines_for_fire->list_of_line + i)->m = ((hrh_lines_for_fire->list_of_line + i)->toPoint.y - (hrh_lines_for_fire->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_fire->list_of_line + i)->toPoint.x - (hrh_lines_for_fire->list_of_line + i)->fromPoint.x);
	(hrh_lines_for_fire->list_of_line + i)->distance = -LINE_SPEED;
	(hrh_lines_for_fire->list_of_line + i)->direction = VERTICLE;
	(hrh_lines_for_fire->list_of_line + i)->tillPoint.x = (hrh_lines_for_fire->list_of_line + i)->fromPoint.x;
	(hrh_lines_for_fire->list_of_line + i)->tillPoint.y = (hrh_lines_for_fire->list_of_line + i)->fromPoint.y;

	i = 2;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.x = 0.2f;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.y = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.z = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.x = 0.05f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.y = -0.8f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.z = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	(hrh_lines_for_fire->list_of_line + i)->m = ((hrh_lines_for_fire->list_of_line + i)->toPoint.y - (hrh_lines_for_fire->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_fire->list_of_line + i)->toPoint.x - (hrh_lines_for_fire->list_of_line + i)->fromPoint.x);
	(hrh_lines_for_fire->list_of_line + i)->distance = -LINE_SPEED;
	(hrh_lines_for_fire->list_of_line + i)->direction = VERTICLE;
	(hrh_lines_for_fire->list_of_line + i)->tillPoint.x = (hrh_lines_for_fire->list_of_line + i)->fromPoint.x;
	(hrh_lines_for_fire->list_of_line + i)->tillPoint.y = (hrh_lines_for_fire->list_of_line + i)->fromPoint.y;

	i = 3;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.x = 0.3f;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.y = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.z = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.x = 0.3f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.y = -0.85f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.z = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	(hrh_lines_for_fire->list_of_line + i)->m = ((hrh_lines_for_fire->list_of_line + i)->toPoint.y - (hrh_lines_for_fire->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_fire->list_of_line + i)->toPoint.x - (hrh_lines_for_fire->list_of_line + i)->fromPoint.x);
	(hrh_lines_for_fire->list_of_line + i)->distance = -LINE_SPEED;
	(hrh_lines_for_fire->list_of_line + i)->direction = VERTICLE;
	(hrh_lines_for_fire->list_of_line + i)->tillPoint.x = (hrh_lines_for_fire->list_of_line + i)->fromPoint.x;
	(hrh_lines_for_fire->list_of_line + i)->tillPoint.y = (hrh_lines_for_fire->list_of_line + i)->fromPoint.y;

	i = 4;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.x = 0.4f;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.y = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.z = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.x = 0.55f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.y = -0.8f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.z = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	(hrh_lines_for_fire->list_of_line + i)->m = ((hrh_lines_for_fire->list_of_line + i)->toPoint.y - (hrh_lines_for_fire->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_fire->list_of_line + i)->toPoint.x - (hrh_lines_for_fire->list_of_line + i)->fromPoint.x);
	(hrh_lines_for_fire->list_of_line + i)->distance = -LINE_SPEED;
	(hrh_lines_for_fire->list_of_line + i)->direction = VERTICLE;
	(hrh_lines_for_fire->list_of_line + i)->tillPoint.x = (hrh_lines_for_fire->list_of_line + i)->fromPoint.x;
	(hrh_lines_for_fire->list_of_line + i)->tillPoint.y = (hrh_lines_for_fire->list_of_line + i)->fromPoint.y;

	i = 5;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.x = 0.5f;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.y = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.z = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.x = 0.7f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.y = -0.6f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.z = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	(hrh_lines_for_fire->list_of_line + i)->m = ((hrh_lines_for_fire->list_of_line + i)->toPoint.y - (hrh_lines_for_fire->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_fire->list_of_line + i)->toPoint.x - (hrh_lines_for_fire->list_of_line + i)->fromPoint.x);
	(hrh_lines_for_fire->list_of_line + i)->distance = -LINE_SPEED;
	(hrh_lines_for_fire->list_of_line + i)->direction = VERTICLE;
	(hrh_lines_for_fire->list_of_line + i)->tillPoint.x = (hrh_lines_for_fire->list_of_line + i)->fromPoint.x;
	(hrh_lines_for_fire->list_of_line + i)->tillPoint.y = (hrh_lines_for_fire->list_of_line + i)->fromPoint.y;

	i = 6;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.x = 0.6f;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.y = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->fromPoint.z = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.x = 0.8f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.y = -0.6f;
	(hrh_lines_for_fire->list_of_line + i)->toPoint.z = 0.0f;
	(hrh_lines_for_fire->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	(hrh_lines_for_fire->list_of_line + i)->m = ((hrh_lines_for_fire->list_of_line + i)->toPoint.y - (hrh_lines_for_fire->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_fire->list_of_line + i)->toPoint.x - (hrh_lines_for_fire->list_of_line + i)->fromPoint.x);
	(hrh_lines_for_fire->list_of_line + i)->distance = -LINE_SPEED;
	(hrh_lines_for_fire->list_of_line + i)->direction = VERTICLE;
	(hrh_lines_for_fire->list_of_line + i)->tillPoint.x = (hrh_lines_for_fire->list_of_line + i)->fromPoint.x;
	(hrh_lines_for_fire->list_of_line + i)->tillPoint.y = (hrh_lines_for_fire->list_of_line + i)->fromPoint.y;
}

void hrh_InitializeRocket_1()
{
	int i = 0;
	hrh_rocket_1_lines = 16;
	hrh_rocket_1 = (HRH_LINES*)malloc(sizeof(HRH_LINES));
	hrh_rocket_1->list_of_line = (HRH_LINE*)malloc(sizeof(HRH_LINE) * hrh_rocket_1_lines);
	hrh_rocket_1->current_line = 0;
	hrh_rocket_1->total_lines = hrh_rocket_1_lines;

	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = 0.0f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = 0.0f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.y = -1.7f;
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line +i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = VERTICLE;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	}

	i = 1;
	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = 0.0f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = -1.7f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = 0.5f;
		(hrh_rocket_1->list_of_line + i)->toPoint.y = -1.7f;
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	i = 2;
	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = 0.5f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = -1.7f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = 0.5f;
		(hrh_rocket_1->list_of_line + i)->toPoint.y = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = VERTICLE;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	i = 3;
	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = 0.5f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = 0.0f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.y = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	}

	i = 4;
	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = 0.0f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = 0.0f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = 0.25f;
		(hrh_rocket_1->list_of_line + i)->toPoint.y = 0.8f;
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = VERTICLE;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	i = 5;
	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = 0.25f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = 0.8f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = 0.5f;
		(hrh_rocket_1->list_of_line + i)->toPoint.y = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = VERTICLE;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	}

	// LEFT
	i = 6;
	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = 0.0f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = -0.9f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = -0.3f;
		(hrh_rocket_1->list_of_line + i)->toPoint.y = -1.4f;
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	}

	i = 7;
	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = -0.3f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = -1.4f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = -0.3f;
		(hrh_rocket_1->list_of_line + i)->toPoint.y = -1.7f;
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = VERTICLE;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	}

	i = 8;
	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = -0.3f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = -1.7f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.y = -1.7f;
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	//RIGHT
	
	i = 9;
	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = 0.5f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = -1.7f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = 0.8f;
		(hrh_rocket_1->list_of_line + i)->toPoint.y = -1.7f;
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	i = 10;
	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = 0.8f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = -1.7f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = 0.8f;
		(hrh_rocket_1->list_of_line + i)->toPoint.y = -1.4f;
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = VERTICLE;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	i = 11;
	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = 0.8f; // 0.5
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = -1.4f; // -1.4
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = 0.5f; // 0.8
		(hrh_rocket_1->list_of_line + i)->toPoint.y = -0.9f; // -0.9
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	}

	// RIGHT UP

	i = 12;
	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = 0.5f; //0.8
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = -0.5f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = 0.8f; //0.5
		(hrh_rocket_1->list_of_line + i)->toPoint.y = -0.5f;
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	i = 13;

	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = 0.8f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = -0.5f; // 0.0f
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = 0.5f;
		(hrh_rocket_1->list_of_line + i)->toPoint.y = 0.0f; // -0.5f
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = VERTICLE;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	// LEFT UP
	i = 14;

	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = 0.0f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = 0.0f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = -0.3f;
		(hrh_rocket_1->list_of_line + i)->toPoint.y = -0.5f;
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = VERTICLE;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	}

	i = 15;
	{
		(hrh_rocket_1->list_of_line + i)->fromPoint.x = -0.3f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.y = -0.5f;
		(hrh_rocket_1->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.x = 0.0f;
		(hrh_rocket_1->list_of_line + i)->toPoint.y = -0.5f;
		(hrh_rocket_1->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_1->list_of_line + i)->tillPoint.x = ((hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->tillPoint.y = ((hrh_rocket_1->list_of_line + i)->fromPoint.y);
		(hrh_rocket_1->list_of_line + i)->tillPoint.z = ((hrh_rocket_1->list_of_line + i)->fromPoint.z);
		(hrh_rocket_1->list_of_line + i)->m = ((hrh_rocket_1->list_of_line + i)->toPoint.y - (hrh_rocket_1->list_of_line + i)->fromPoint.y) / ((hrh_rocket_1->list_of_line + i)->toPoint.x - (hrh_rocket_1->list_of_line + i)->fromPoint.x);
		(hrh_rocket_1->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_1->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_1->list_of_line + i)->isCompleted = false;
		(hrh_rocket_1->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

}

void hrh_InitializeRocket_2()
{
	int i = 0;
	hrh_rocket_2_lines = 13;
	hrh_rocket_2 = (HRH_LINES*)malloc(sizeof(HRH_LINES));
	hrh_rocket_2->list_of_line = (HRH_LINE*)malloc(sizeof(HRH_LINE) * hrh_rocket_2_lines);
	hrh_rocket_2->current_line = 0;
	hrh_rocket_2->total_lines = hrh_rocket_2_lines;

	{
		(hrh_rocket_2->list_of_line + i)->fromPoint.x = -0.5f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.y = 0.0f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.x = -0.5f;
		(hrh_rocket_2->list_of_line + i)->toPoint.y = -1.7f;
		(hrh_rocket_2->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->tillPoint.x = ((hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->tillPoint.y = ((hrh_rocket_2->list_of_line + i)->fromPoint.y);
		(hrh_rocket_2->list_of_line + i)->tillPoint.z = ((hrh_rocket_2->list_of_line + i)->fromPoint.z);
		(hrh_rocket_2->list_of_line + i)->m = ((hrh_rocket_2->list_of_line + i)->toPoint.y - (hrh_rocket_2->list_of_line + i)->fromPoint.y) / ((hrh_rocket_2->list_of_line + i)->toPoint.x - (hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_rocket_2->list_of_line + i)->direction = VERTICLE;
		(hrh_rocket_2->list_of_line + i)->isCompleted = false;
		(hrh_rocket_2->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	}

	i = 1;
	{
		(hrh_rocket_2->list_of_line + i)->fromPoint.x = -0.5f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.y = -1.7f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.x = 0.5f;
		(hrh_rocket_2->list_of_line + i)->toPoint.y = -1.7f;
		(hrh_rocket_2->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->tillPoint.x = ((hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->tillPoint.y = ((hrh_rocket_2->list_of_line + i)->fromPoint.y);
		(hrh_rocket_2->list_of_line + i)->tillPoint.z = ((hrh_rocket_2->list_of_line + i)->fromPoint.z);
		(hrh_rocket_2->list_of_line + i)->m = ((hrh_rocket_2->list_of_line + i)->toPoint.y - (hrh_rocket_2->list_of_line + i)->fromPoint.y) / ((hrh_rocket_2->list_of_line + i)->toPoint.x - (hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_2->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_2->list_of_line + i)->isCompleted = false;
		(hrh_rocket_2->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	i = 2;
	{
		(hrh_rocket_2->list_of_line + i)->fromPoint.x = 0.5f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.y = -1.7f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.x = 0.5f;
		(hrh_rocket_2->list_of_line + i)->toPoint.y = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->tillPoint.x = ((hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->tillPoint.y = ((hrh_rocket_2->list_of_line + i)->fromPoint.y);
		(hrh_rocket_2->list_of_line + i)->tillPoint.z = ((hrh_rocket_2->list_of_line + i)->fromPoint.z);
		(hrh_rocket_2->list_of_line + i)->m = ((hrh_rocket_2->list_of_line + i)->toPoint.y - (hrh_rocket_2->list_of_line + i)->fromPoint.y) / ((hrh_rocket_2->list_of_line + i)->toPoint.x - (hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_2->list_of_line + i)->direction = VERTICLE;
		(hrh_rocket_2->list_of_line + i)->isCompleted = false;
		(hrh_rocket_2->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	i = 3;
	{
		(hrh_rocket_2->list_of_line + i)->fromPoint.x = 0.5f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.y = 0.0f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.x = -0.5f;
		(hrh_rocket_2->list_of_line + i)->toPoint.y = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->tillPoint.x = ((hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->tillPoint.y = ((hrh_rocket_2->list_of_line + i)->fromPoint.y);
		(hrh_rocket_2->list_of_line + i)->tillPoint.z = ((hrh_rocket_2->list_of_line + i)->fromPoint.z);
		(hrh_rocket_2->list_of_line + i)->m = ((hrh_rocket_2->list_of_line + i)->toPoint.y - (hrh_rocket_2->list_of_line + i)->fromPoint.y) / ((hrh_rocket_2->list_of_line + i)->toPoint.x - (hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_rocket_2->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_2->list_of_line + i)->isCompleted = false;
		(hrh_rocket_2->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	}

	i = 4;
	{
		(hrh_rocket_2->list_of_line + i)->fromPoint.x = -0.5f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.y = 0.0f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.x = -0.3f;
		(hrh_rocket_2->list_of_line + i)->toPoint.y = 0.5f;
		(hrh_rocket_2->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->tillPoint.x = ((hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->tillPoint.y = ((hrh_rocket_2->list_of_line + i)->fromPoint.y);
		(hrh_rocket_2->list_of_line + i)->tillPoint.z = ((hrh_rocket_2->list_of_line + i)->fromPoint.z);
		(hrh_rocket_2->list_of_line + i)->m = ((hrh_rocket_2->list_of_line + i)->toPoint.y - (hrh_rocket_2->list_of_line + i)->fromPoint.y) / ((hrh_rocket_2->list_of_line + i)->toPoint.x - (hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_2->list_of_line + i)->direction = VERTICLE;
		(hrh_rocket_2->list_of_line + i)->isCompleted = false;
		(hrh_rocket_2->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	i = 5;
	{
		(hrh_rocket_2->list_of_line + i)->fromPoint.x = -0.3f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.y = 0.5f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.x = 0.3f;
		(hrh_rocket_2->list_of_line + i)->toPoint.y = 0.5f;
		(hrh_rocket_2->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->tillPoint.x = ((hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->tillPoint.y = ((hrh_rocket_2->list_of_line + i)->fromPoint.y);
		(hrh_rocket_2->list_of_line + i)->tillPoint.z = ((hrh_rocket_2->list_of_line + i)->fromPoint.z);
		(hrh_rocket_2->list_of_line + i)->m = ((hrh_rocket_2->list_of_line + i)->toPoint.y - (hrh_rocket_2->list_of_line + i)->fromPoint.y) / ((hrh_rocket_2->list_of_line + i)->toPoint.x - (hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_2->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_2->list_of_line + i)->isCompleted = false;
		(hrh_rocket_2->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	i = 6;
	{
		(hrh_rocket_2->list_of_line + i)->fromPoint.x = 0.3f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.y = 0.5f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.x = 0.5f;
		(hrh_rocket_2->list_of_line + i)->toPoint.y = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->tillPoint.x = ((hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->tillPoint.y = ((hrh_rocket_2->list_of_line + i)->fromPoint.y);
		(hrh_rocket_2->list_of_line + i)->tillPoint.z = ((hrh_rocket_2->list_of_line + i)->fromPoint.z);
		(hrh_rocket_2->list_of_line + i)->m = ((hrh_rocket_2->list_of_line + i)->toPoint.y - (hrh_rocket_2->list_of_line + i)->fromPoint.y) / ((hrh_rocket_2->list_of_line + i)->toPoint.x - (hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_rocket_2->list_of_line + i)->direction = VERTICLE;
		(hrh_rocket_2->list_of_line + i)->isCompleted = false;
		(hrh_rocket_2->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	}

	// LEFT
	i = 7;
	{
		(hrh_rocket_2->list_of_line + i)->fromPoint.x = -0.5f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.y = -0.9f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.x = -0.9f;
		(hrh_rocket_2->list_of_line + i)->toPoint.y = -1.4f;
		(hrh_rocket_2->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->tillPoint.x = ((hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->tillPoint.y = ((hrh_rocket_2->list_of_line + i)->fromPoint.y);
		(hrh_rocket_2->list_of_line + i)->tillPoint.z = ((hrh_rocket_2->list_of_line + i)->fromPoint.z);
		(hrh_rocket_2->list_of_line + i)->m = ((hrh_rocket_2->list_of_line + i)->toPoint.y - (hrh_rocket_2->list_of_line + i)->fromPoint.y) / ((hrh_rocket_2->list_of_line + i)->toPoint.x - (hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_rocket_2->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_2->list_of_line + i)->isCompleted = false;
		(hrh_rocket_2->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	}

	i = 8;
	{
		(hrh_rocket_2->list_of_line + i)->fromPoint.x = -0.9f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.y = -1.4f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.x = -0.9f;
		(hrh_rocket_2->list_of_line + i)->toPoint.y = -1.7f;
		(hrh_rocket_2->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->tillPoint.x = ((hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->tillPoint.y = ((hrh_rocket_2->list_of_line + i)->fromPoint.y);
		(hrh_rocket_2->list_of_line + i)->tillPoint.z = ((hrh_rocket_2->list_of_line + i)->fromPoint.z);
		(hrh_rocket_2->list_of_line + i)->m = ((hrh_rocket_2->list_of_line + i)->toPoint.y - (hrh_rocket_2->list_of_line + i)->fromPoint.y) / ((hrh_rocket_2->list_of_line + i)->toPoint.x - (hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_rocket_2->list_of_line + i)->direction = VERTICLE;
		(hrh_rocket_2->list_of_line + i)->isCompleted = false;
		(hrh_rocket_2->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	}

	i = 9;
	{
		(hrh_rocket_2->list_of_line + i)->fromPoint.x = -0.9f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.y = -1.7f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.x = -0.5f;
		(hrh_rocket_2->list_of_line + i)->toPoint.y = -1.7f;
		(hrh_rocket_2->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->tillPoint.x = ((hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->tillPoint.y = ((hrh_rocket_2->list_of_line + i)->fromPoint.y);
		(hrh_rocket_2->list_of_line + i)->tillPoint.z = ((hrh_rocket_2->list_of_line + i)->fromPoint.z);
		(hrh_rocket_2->list_of_line + i)->m = ((hrh_rocket_2->list_of_line + i)->toPoint.y - (hrh_rocket_2->list_of_line + i)->fromPoint.y) / ((hrh_rocket_2->list_of_line + i)->toPoint.x - (hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_2->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_2->list_of_line + i)->isCompleted = false;
		(hrh_rocket_2->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	// RIGHT

	i = 10;
	{
		(hrh_rocket_2->list_of_line + i)->fromPoint.x = 0.5f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.y = -1.7f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.x = 0.9f;
		(hrh_rocket_2->list_of_line + i)->toPoint.y = -1.7f;
		(hrh_rocket_2->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->tillPoint.x = ((hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->tillPoint.y = ((hrh_rocket_2->list_of_line + i)->fromPoint.y);
		(hrh_rocket_2->list_of_line + i)->tillPoint.z = ((hrh_rocket_2->list_of_line + i)->fromPoint.z);
		(hrh_rocket_2->list_of_line + i)->m = ((hrh_rocket_2->list_of_line + i)->toPoint.y - (hrh_rocket_2->list_of_line + i)->fromPoint.y) / ((hrh_rocket_2->list_of_line + i)->toPoint.x - (hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_2->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_2->list_of_line + i)->isCompleted = false;
		(hrh_rocket_2->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	i = 11;
	{
		(hrh_rocket_2->list_of_line + i)->fromPoint.x = 0.9f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.y = -1.7f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.x = 0.9f;
		(hrh_rocket_2->list_of_line + i)->toPoint.y = -1.4f;
		(hrh_rocket_2->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->tillPoint.x = ((hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->tillPoint.y = ((hrh_rocket_2->list_of_line + i)->fromPoint.y);
		(hrh_rocket_2->list_of_line + i)->tillPoint.z = ((hrh_rocket_2->list_of_line + i)->fromPoint.z);
		(hrh_rocket_2->list_of_line + i)->m = ((hrh_rocket_2->list_of_line + i)->toPoint.y - (hrh_rocket_2->list_of_line + i)->fromPoint.y) / ((hrh_rocket_2->list_of_line + i)->toPoint.x - (hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->distance = LINE_SPEED;
		(hrh_rocket_2->list_of_line + i)->direction = VERTICLE;
		(hrh_rocket_2->list_of_line + i)->isCompleted = false;
		(hrh_rocket_2->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
	}

	i = 12;
	{
		(hrh_rocket_2->list_of_line + i)->fromPoint.x = 0.9f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.y = -1.4f;
		(hrh_rocket_2->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->toPoint.x = 0.5f;
		(hrh_rocket_2->list_of_line + i)->toPoint.y = -0.9f;
		(hrh_rocket_2->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_rocket_2->list_of_line + i)->tillPoint.x = ((hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->tillPoint.y = ((hrh_rocket_2->list_of_line + i)->fromPoint.y);
		(hrh_rocket_2->list_of_line + i)->tillPoint.z = ((hrh_rocket_2->list_of_line + i)->fromPoint.z);
		(hrh_rocket_2->list_of_line + i)->m = ((hrh_rocket_2->list_of_line + i)->toPoint.y - (hrh_rocket_2->list_of_line + i)->fromPoint.y) / ((hrh_rocket_2->list_of_line + i)->toPoint.x - (hrh_rocket_2->list_of_line + i)->fromPoint.x);
		(hrh_rocket_2->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_rocket_2->list_of_line + i)->direction = HORIZONTAL;
		(hrh_rocket_2->list_of_line + i)->isCompleted = false;
		(hrh_rocket_2->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
	}

}

void hrh_InitializeCycle1()
{
	// FUNCTION DECLARATION
	void hrh_InitializeLines();
	void hrh_InitializeCircles();

	// CODE
	hrh_InitializeLines();
	hrh_InitializeCircles();
}

void hrh_InitializeCycle2()
{
	// Initialize for second cycle
	hrh_lines_for_cycle_2 = (HRH_LINES*)malloc(sizeof(HRH_LINES));
	hrh_lines_for_cycle_2->list_of_line = (HRH_LINE*)malloc(sizeof(HRH_LINE) * NR_LINES);
	hrh_lines_for_cycle_2->total_lines = NR_LINES;
	hrh_lines_for_cycle_2->current_line = 0;


	for (int i = 0; i < NR_LINES; i++)
	{
		(hrh_lines_for_cycle_2->list_of_line + i)->fromPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle_2->list_of_line + i)->fromPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle_2->list_of_line + i)->fromPoint.z = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.z;
		(hrh_lines_for_cycle_2->list_of_line + i)->toPoint.x = (hrh_lines_for_cycle->list_of_line + i)->toPoint.x;
		(hrh_lines_for_cycle_2->list_of_line + i)->toPoint.y = (hrh_lines_for_cycle->list_of_line + i)->toPoint.y;
		(hrh_lines_for_cycle_2->list_of_line + i)->toPoint.z = (hrh_lines_for_cycle->list_of_line + i)->toPoint.z;
		(hrh_lines_for_cycle_2->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->tillPoint.x;
		(hrh_lines_for_cycle_2->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->tillPoint.y;
		(hrh_lines_for_cycle_2->list_of_line + i)->m = (hrh_lines_for_cycle->list_of_line + i)->m;
		(hrh_lines_for_cycle_2->list_of_line + i)->distance = (hrh_lines_for_cycle->list_of_line + i)->distance;
		(hrh_lines_for_cycle_2->list_of_line + i)->condition = (hrh_lines_for_cycle->list_of_line + i)->condition;
		(hrh_lines_for_cycle_2->list_of_line + i)->direction = (hrh_lines_for_cycle->list_of_line + i)->direction;
		(hrh_lines_for_cycle_2->list_of_line + i)->isCompleted = (hrh_lines_for_cycle->list_of_line + i)->isCompleted;
	}

	hrh_circles_2 = (HRH_CIRCLES*)malloc(sizeof(HRH_CIRCLES));
	hrh_circles_2->total_circles = hrh_circles->total_circles;
	hrh_circles_2->hrh_circle = (HRH_CIRCLE*)malloc(sizeof(HRH_CIRCLE) * hrh_circles->total_circles);
	hrh_circles_2->current_circle = 0;

	for (int i = 0; i < hrh_circles_2->total_circles; i++)
	{
		(hrh_circles_2->hrh_circle + i)->hrh_point.x = (hrh_circles->hrh_circle + i)->hrh_point.x;
		(hrh_circles_2->hrh_circle + i)->hrh_point.y = (hrh_circles->hrh_circle + i)->hrh_point.y;
		(hrh_circles_2->hrh_circle + i)->hrh_point.z = (hrh_circles->hrh_circle + i)->hrh_point.z;
		(hrh_circles_2->hrh_circle + i)->radius = (hrh_circles->hrh_circle + i)->radius;
		(hrh_circles_2->hrh_circle + i)->currentCount = (hrh_circles->hrh_circle + i)->currentCount;
		(hrh_circles_2->hrh_circle + i)->isCompleted = (hrh_circles->hrh_circle + i)->isCompleted;
	}

}


void hrh_InitializeLines()
{
	hrh_lines_for_cycle = (HRH_LINES*)malloc(sizeof(HRH_LINES));
	hrh_lines_for_cycle->list_of_line = (HRH_LINE*)malloc(sizeof(HRH_LINE) * NR_LINES);
	hrh_lines_for_cycle->current_line = 0;
	hrh_lines_for_cycle->total_lines = NR_LINES;
	int i = 0;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = 1.5f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 0.2f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 1;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = -0.19f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 0.13f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = -0.9f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 1.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 2;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = -0.9f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 1.0f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 0.9f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 1.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 3;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = 0.9f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 1.0f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 1.5f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 4;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = 0.9f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 1.0f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 0.1f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 0.16f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 5;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = -0.9f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 1.0f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = -1.5f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 6;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = -0.9f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 1.0f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = -0.8f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 1.25f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	// CYCLE HANDLE

	i = 7;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = -0.5f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 1.2f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = -0.6f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 1.3f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 8;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = -0.6;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 1.3f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = -1.1f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 1.2f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 9;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = -1.1f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 1.2f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = -0.8f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 1.1f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	// CYCLE SEAT |
	i = 10;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = 0.9f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 1.0f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 0.9f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 1.1f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = VERTICLE;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	// CYCLE SEAT

	i = 11;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = 0.7f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 1.15f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 1.1f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 1.1f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 12;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = 1.1f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 1.1f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 1.1f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 1.35f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction =VERTICLE;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 13;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = 1.1f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 1.35f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 0.7f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 1.3f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 14;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = 0.7f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 1.3f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 0.7f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 1.15f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = VERTICLE;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}


	// ---------------|
	//----------------|
	i = 15;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = 0.9f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 0.95f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 2.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 0.95f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 16;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = 2.0f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 0.95f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 2.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 1.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = 0.001f;
		(hrh_lines_for_cycle->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = VERTICLE;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 17;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = 2.0f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 1.0f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 0.9f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 1.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	// CYCLE PEDAL

	// UP PEDAL
	i = 18;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 0.2f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 0.35f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = VERTICLE;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 19;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = -0.1f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = 0.35f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 0.1f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = 0.35f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = LESS_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	// DOWN PEDAL
	i = 20;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = -0.2f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = -0.35f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = VERTICLE;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

	i = 21;
	{
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.x = 0.1f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.y = -0.35f;
		(hrh_lines_for_cycle->list_of_line + i)->fromPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.x = -0.1f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.y = -0.35f;
		(hrh_lines_for_cycle->list_of_line + i)->toPoint.z = 0.0f;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.x = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x;
		(hrh_lines_for_cycle->list_of_line + i)->tillPoint.y = (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y;
		(hrh_lines_for_cycle->list_of_line + i)->m = ((hrh_lines_for_cycle->list_of_line + i)->toPoint.y - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.y) / ((hrh_lines_for_cycle->list_of_line + i)->toPoint.x - (hrh_lines_for_cycle->list_of_line + i)->fromPoint.x);
		(hrh_lines_for_cycle->list_of_line + i)->distance = -LINE_SPEED;
		(hrh_lines_for_cycle->list_of_line + i)->condition = GREATER_THAN_EQUAL_TO;
		(hrh_lines_for_cycle->list_of_line + i)->direction = HORIZONTAL;
		(hrh_lines_for_cycle->list_of_line + i)->isCompleted = false;
	}

}

void hrh_UninitializeLines()
{
	//CODE
	if (hrh_lines_for_cycle != NULL)
	{
		for (int i = hrh_lines_for_cycle->total_lines - 1; i > -1; i--)
		{
			free(hrh_lines_for_cycle->list_of_line + i);
		}
		free(hrh_lines_for_cycle);
	}

	if (hrh_lines_for_cycle_2 != NULL)
	{
		for (int i = hrh_lines_for_cycle_2->total_lines - 1; i > -1; i--)
		{
			free(hrh_lines_for_cycle_2->list_of_line + i);
		}
		free(hrh_lines_for_cycle_2);
	}

	if (hrh_rocket_1 != NULL)
	{
		for (int i = hrh_rocket_1->total_lines - 1; i > -1; i--)
		{
			free(hrh_rocket_1->list_of_line + i);
		}
		free(hrh_rocket_1);
	}

	if (hrh_rocket_2 != NULL)
	{
		for (int i = hrh_rocket_2->total_lines - 1; i > -1; i--)
		{
			free(hrh_rocket_2->list_of_line + i);
		}
		free(hrh_rocket_2);
	}

	if (hrh_lines_for_fire != NULL)
	{
		for (int i = hrh_lines_for_fire->total_lines - 1; i > -1; i--)
		{
			free(hrh_lines_for_fire->list_of_line + i);
		}
		free(hrh_lines_for_fire);
	}

}

void hrh_InitializeCircles()
{
	//VARIABLE DECLARATION
	int i;

	//CODE
	hrh_circles = (HRH_CIRCLES*)malloc(sizeof(HRH_CIRCLES));
	hrh_circles->total_circles = NR_CIRCLES;
	hrh_circles->current_circle = 0;
	hrh_circles->hrh_circle = (HRH_CIRCLE*)malloc(sizeof(HRH_CIRCLE) * NR_CIRCLES);

	i = 0;
	(hrh_circles->hrh_circle + i)->hrh_point.x = 0.0f;
	(hrh_circles->hrh_circle + i)->hrh_point.y = 0.0f;
	(hrh_circles->hrh_circle + i)->hrh_point.z = 0.0f;
	(hrh_circles->hrh_circle + i)->radius = 0.1f;
	(hrh_circles->hrh_circle + i)->currentCount = 0;
	(hrh_circles->hrh_circle + i)->isCompleted = false;

	i = 1;
	(hrh_circles->hrh_circle + i)->hrh_point.x = 0.0f;
	(hrh_circles->hrh_circle + i)->hrh_point.y = 0.0f;
	(hrh_circles->hrh_circle + i)->hrh_point.z = 0.0f;
	(hrh_circles->hrh_circle + i)->radius = 0.2f;
	(hrh_circles->hrh_circle + i)->currentCount = 0;
	(hrh_circles->hrh_circle + i)->isCompleted = false;

	i = 2;
	(hrh_circles->hrh_circle + i)->hrh_point.x = -1.5f;
	(hrh_circles->hrh_circle + i)->hrh_point.y = 0.0f;
	(hrh_circles->hrh_circle + i)->hrh_point.z = 0.0f;
	(hrh_circles->hrh_circle + i)->radius = 0.5f;
	(hrh_circles->hrh_circle + i)->currentCount = 0;
	(hrh_circles->hrh_circle + i)->isCompleted = false;

	i = 3;
	(hrh_circles->hrh_circle + i)->hrh_point.x = 1.5f;
	(hrh_circles->hrh_circle + i)->hrh_point.y = 0.0f;
	(hrh_circles->hrh_circle + i)->hrh_point.z = 0.0f;
	(hrh_circles->hrh_circle + i)->radius = 0.5f;
	(hrh_circles->hrh_circle + i)->currentCount = 0;
	(hrh_circles->hrh_circle + i)->isCompleted = false;

}

void hrh_UninitializeCircles()
{
	if (hrh_circles != NULL)
	{
		for (int i = hrh_circles->total_circles - 1; i >= 0; i--)
		{
			free(hrh_circles->hrh_circle + i);
		}
		free(hrh_circles);
	}

	if (hrh_circles_2 != NULL)
	{
		for (int i = hrh_circles_2->total_circles - 1; i >= 0; i--)
		{
			free(hrh_circles_2->hrh_circle + i);
		}
		free(hrh_circles_2);
	}
}

void SetRectangle(HRH_RECTANGLE* hrh_rectangle, GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height)
{
	hrh_rectangle->height = height;
	hrh_rectangle->width = width;
	hrh_rectangle->x = x;
	hrh_rectangle->y = y;
	hrh_rectangle->z = z;
}

