#include "RTR_DEMO.h"

/*************************LINES****************************************************/
extern HRH_LINES* hrh_lines_for_cycle, * hrh_lines_for_cycle_2;

/************************************RECTANGLES***************************************/
extern HRH_RECTANGLE hrh_rectangle_isro, hrh_AstroMediComp_logo;

/*************************CIRCLES********************************************************/
extern HRH_CIRCLES* hrh_circles, * hrh_circles_2;

/**********************************ROKCET 1********************************/
extern HRH_LINES* hrh_rocket_1;
extern int hrh_rocket_1_lines;

/**********************************ROKCET 2********************************/
extern HRH_LINES* hrh_rocket_2;
extern int hrh_rocket_2_lines;

/*********************************FIRE*********************/
extern HRH_LINES* hrh_lines_for_fire;

/**********************************CAMERA****************************/
extern GLfloat hrh_camera_translate_x, hrh_camera_translate_y, hrh_camera_translate_z;
extern GLfloat hrh_camera_angle_x, hrh_camera_angle_y, hrh_camera_angle_z;

extern GLfloat hrh_cycle_1_translate_x, hrh_cycle_1_translate_y, hrh_cycle_1_translate_z;
extern GLfloat hrh_cycle_2_translate_x, hrh_cycle_2_translate_y, hrh_cycle_2_translate_z;

extern GLfloat hrh_rocket_1_translate_x, hrh_rocket_1_translate_y, hrh_rocket_1_translate_z, hrh_rocket_1_angle;
extern GLfloat hrh_rocket_2_translate_x, hrh_rocket_2_translate_y, hrh_rocket_2_translate_z, hrh_rocket_2_angle;

extern GLfloat angle_for_seen_2, angle_for_seen_1;
extern GLfloat angle_for_cycle;
GLfloat move_camera_for_cycle = 0.05, move_cycle_horizontally = 0.0016f;
GLfloat rocket_1_scale_size = 0.9f, rocket_2_scale_size = 0.8f;
extern bool rocket_1_up;
extern FILE* gpFile;
extern int seen;
extern GLfloat hrh_fire_translate_y = - 1.4f;
extern GLuint isro_texture_id, logo_texture_id;

/******************************** FONT *****************************************/
extern GLfloat string_translate_x, string_translate_y;
extern short nFontList;
extern GLfloat scale_size;

void hrh_DrawFire()
{
	// FUNCTION DECLARATION
	void hrh_DrawLines(HRH_LINES*, int);

	// CODE
	glTranslatef(-0.3f, hrh_fire_translate_y, 0.0f);
	glPushMatrix();
	glScalef(1.0f, 1.0f, 1.0f);
	hrh_lines_for_fire->current_line = hrh_lines_for_fire->total_lines - 1;
	hrh_DrawLines(hrh_lines_for_fire, hrh_lines_for_fire->total_lines);
	glPopMatrix();
}

void hrh_DrawRocket_1()
{
	// FUNCTION DECLARATION
	void hrh_DrawLines(HRH_LINES * hrh_lines, int);

	// CODE
	glPushMatrix();
	glScalef(rocket_1_scale_size, rocket_1_scale_size, rocket_1_scale_size);
	hrh_DrawLines(hrh_rocket_1, hrh_rocket_1_lines);
	glPopMatrix();
}

void hrh_DrawRocket_2()
{
	// FUNCTION DECLARATION
	void hrh_DrawLines(HRH_LINES * hrh_lines, int);

	// CODE
	glPushMatrix();
	glScalef(rocket_2_scale_size, rocket_2_scale_size, rocket_2_scale_size);
	hrh_DrawLines(hrh_rocket_2, hrh_rocket_2_lines);
	glPopMatrix();
}


void hrh_Seen4()
{
	// FUNCTION DECLARATION
	void hrh_DrawRectangles(HRH_RECTANGLE *, GLuint);

	// VARIABLE DECLARATION
	int x, y, z, height, width;

	// CODE
	glLineWidth(5.0f);
	hrh_DrawRectangles(&hrh_rectangle_isro, isro_texture_id);
	if (hrh_camera_translate_z <= 6)
	{
		glLineWidth(3.0f);
		hrh_DrawRectangles(&hrh_AstroMediComp_logo, logo_texture_id);
	}

}

void hrh_Seen3()
{
	// FUNCTION DECLARATION
	void hrh_DrawFire();

	// CODE
	if (hrh_rocket_1_angle < 360.0f)
	{
		hrh_rocket_1_angle = hrh_rocket_1_angle + move_camera_for_cycle;
		hrh_rocket_2_angle = hrh_rocket_2_angle + move_camera_for_cycle;
		if (rocket_1_scale_size < 1)
		{
			rocket_1_scale_size = rocket_1_scale_size + 0.001f;
		}
		
	}
	if (hrh_rocket_2_translate_x < 0)
	{
		hrh_rocket_2_translate_x = hrh_rocket_2_translate_x + 0.005;
		hrh_rocket_1_translate_y = hrh_rocket_1_translate_y + 0.005;
	}

	if (hrh_rocket_1_translate_x < 0)
	{
		hrh_rocket_2_translate_y = hrh_rocket_2_translate_y - 0.005;
		hrh_rocket_1_translate_x = hrh_rocket_1_translate_x + 0.005;
	}

	if (hrh_rocket_1_translate_x >= 0 && hrh_rocket_1_translate_x < 1)
	{
		hrh_rocket_1_translate_x = hrh_rocket_1_translate_x + 0.0051;
		hrh_rocket_2_translate_x = hrh_rocket_2_translate_x + 0.005;
	}
	
	if (hrh_rocket_1_translate_x >= 1 && hrh_rocket_2_translate_y < -0.3f)
	{
		hrh_rocket_2_translate_y = hrh_rocket_2_translate_y + 0.005f;
	}

	if (hrh_rocket_1_translate_x >= 1 && hrh_rocket_2_translate_y > -0.3f)
	{
		hrh_DrawFire();
		if (hrh_camera_translate_z <= 10)
		{
			if (rocket_1_up == false)
			{
				hrh_camera_translate_z = hrh_camera_translate_z + 0.0016;
			}
		}
		else
		{
			rocket_1_up = true;
		}
	}

	if (rocket_1_up == true)
	{
		if (hrh_rocket_1_translate_y < 7.5f)
		{
			hrh_rocket_1_translate_y = hrh_rocket_1_translate_y + 0.002;
		}
		if (hrh_camera_translate_z >= 6)
		{
			hrh_camera_translate_z = hrh_camera_translate_z - 0.0016; 
		}
	}

	if (hrh_rocket_1_translate_y >= 5.5f && rocket_1_up == true)
	{
		if (hrh_cycle_1_translate_x > -8.0f)
		{
			GLfloat move = 0.0030f;
			hrh_cycle_1_translate_x = hrh_cycle_1_translate_x - move;
			hrh_cycle_2_translate_x = hrh_cycle_2_translate_x - move;
			hrh_rocket_2_translate_x = hrh_rocket_2_translate_x + move;
			hrh_fire_translate_y = hrh_fire_translate_y - move;
		}
	}

	
}

void hrh_Seen1_Cycle_Rockets()
{
	// FUNCTION DECLARATION
	void hrh_DrawRocket_1();
	void hrh_DrawCycle_1();

	// CODE
	glPointSize(3.0f);
	glPushMatrix();

	glTranslatef(hrh_cycle_1_translate_x, hrh_cycle_1_translate_y, hrh_cycle_1_translate_z);
	glRotatef(angle_for_seen_1, 0.0f, 1.0f, 0.0f);

	hrh_DrawCycle_1();
	glPopMatrix();

	glTranslatef(hrh_rocket_1_translate_x, hrh_rocket_1_translate_y, hrh_rocket_1_translate_z);
	glRotatef(hrh_rocket_1_angle, 0.0f, 1.0f, 0.0f);

	if (hrh_cycle_1_translate_y < -0.5)
	{
		hrh_DrawRocket_1();
	}

	if ((hrh_lines_for_cycle->list_of_line + (hrh_lines_for_cycle->total_lines - 1))->isCompleted == true)
	{
		if (hrh_cycle_1_translate_y >= -1.2)
		{
			hrh_cycle_1_translate_y = hrh_cycle_1_translate_y - 0.001;
			hrh_rocket_1_translate_y = hrh_rocket_1_translate_y - 0.001f;
			hrh_cycle_1_translate_x = hrh_cycle_1_translate_x - 0.002;
			hrh_rocket_1_translate_x = hrh_rocket_1_translate_x - 0.002f;
		}
	}

	if ((hrh_rocket_1->list_of_line + (hrh_rocket_1->total_lines - 1))->isCompleted == true)
	{
		if (hrh_camera_translate_z <= 8 && seen == 1)
		{
			hrh_camera_translate_z = hrh_camera_translate_z + 0.0016;
		}
		if (hrh_camera_translate_z >= 8)
		{
			seen = 2;
		}
	}

	if ((hrh_rocket_2->list_of_line + (hrh_rocket_2->total_lines - 1))->isCompleted == true)
	{
		if (angle_for_seen_1 > angle_for_cycle)
		{
			angle_for_seen_1 = angle_for_seen_1 - move_camera_for_cycle;
			hrh_rocket_1_angle = hrh_rocket_1_angle - move_camera_for_cycle;
			hrh_rocket_1_translate_x = hrh_rocket_1_translate_x - 0.0005;
		}
	}
	if (angle_for_seen_1 <= angle_for_cycle)
	{
		if (hrh_cycle_1_translate_x > -3.4f)
		{
			hrh_cycle_1_translate_x = hrh_cycle_1_translate_x - 0.0025f;
			hrh_rocket_1_translate_x = hrh_rocket_1_translate_x - 0.0026f;
		}
	}
}

void hrh_Seen2_Cycle_Rockets()
{
	// FUNCTION DECLARATION
	void hrh_DrawRocket_2();
	void hrh_DrawCycle_2();


	//CODE
	glPointSize(3.0f);

	(hrh_circles_2->hrh_circle + (hrh_circles_2->total_circles - 1))->isCompleted = true;
	hrh_lines_for_cycle_2->current_line = hrh_lines_for_cycle_2->total_lines - 1;

	glPushMatrix();
	glTranslatef(hrh_cycle_2_translate_x, hrh_cycle_2_translate_y, hrh_cycle_2_translate_z);
	glRotatef(angle_for_seen_2, 0.0f, 1.0f, 0.0f);

	hrh_DrawCycle_2();
	glPopMatrix();
	glTranslatef(hrh_rocket_2_translate_x, hrh_rocket_2_translate_y, hrh_rocket_2_translate_z);
	glRotatef(hrh_rocket_2_angle, 0.0f, 1.0f, 0.0f);

	if ((hrh_lines_for_cycle_2->list_of_line + (hrh_lines_for_cycle_2->total_lines - 1))->isCompleted == true)
	{
		hrh_DrawRocket_2();
	}

	if(hrh_rocket_2->current_line == 4)
	{
		hrh_rocket_2->current_line = hrh_rocket_2->total_lines - 1;
	}

	if ((hrh_rocket_2->list_of_line + (hrh_rocket_2->total_lines - 1))->isCompleted == true)
	{
		if (angle_for_seen_2 > angle_for_cycle)
		{
			angle_for_seen_2 = angle_for_seen_2 - move_camera_for_cycle;
			hrh_rocket_2_angle = hrh_rocket_2_angle - move_camera_for_cycle;
			hrh_rocket_2_translate_x = hrh_rocket_2_translate_x - 0.0005;
			if (hrh_cycle_2_translate_y >= -1)
			{
				hrh_cycle_2_translate_y = hrh_cycle_2_translate_y - 0.001f;
				hrh_cycle_2_translate_z = hrh_cycle_2_translate_z + 0.001f;
				hrh_cycle_2_translate_x = hrh_cycle_2_translate_x - 0.002f;

				hrh_rocket_2_translate_x = hrh_rocket_2_translate_x - 0.002f;
				hrh_rocket_2_translate_y = hrh_rocket_2_translate_y - 0.001f;
				hrh_rocket_2_translate_z = hrh_rocket_2_translate_z + 0.003f;
			}
		}
		if (angle_for_seen_2 <= angle_for_cycle)
		{
			if (hrh_cycle_2_translate_x > -2.2f)
			{
				hrh_cycle_2_translate_x = hrh_cycle_2_translate_x - move_cycle_horizontally;
				hrh_rocket_2_translate_x = hrh_rocket_2_translate_x - move_cycle_horizontally;
			}			
		}
	}
}

void hrh_DrawCycle_2()
{
	// FUNCTION DECLARATION
	void hrh_DrawCircles(int);
	void hrh_DrawLines(HRH_LINES*, int);

	// VARIABLE DECLARATION
	// CODE
	hrh_DrawCircles(hrh_circles_2->total_circles);

	if ((hrh_circles_2->hrh_circle + (hrh_circles_2->total_circles - 1))->isCompleted == true)
	{
		hrh_DrawLines(hrh_lines_for_cycle_2, hrh_lines_for_cycle_2->total_lines);
	}
}

void hrh_DrawCycle_1()
{
	// FUNCTION DECLARATION
	void hrh_DrawCircles(int);
	void hrh_DrawLines(HRH_LINES*, int);

	// VARIABLE DECLARATION

	// CODE
	hrh_DrawCircles(hrh_circles->total_circles);

	if ((hrh_circles->hrh_circle + (hrh_circles->total_circles - 1))->isCompleted == true)
	{
		hrh_DrawLines(hrh_lines_for_cycle, hrh_lines_for_cycle->total_lines);
	}
}

/*************************LINES********************************************/
void hrh_DrawLines(HRH_LINES* hrh_lines, int total_lines)
{
	//FUNCTION DECLARATION
	void hrh_DrawVirticleLine(HRH_LINE * hrh_line);
	void hrh_DrawHorizontalLine(HRH_LINE * hrh_line);

	//CODE
	for (int i = 0; i <= hrh_lines->current_line; i = i + 1) 
	{
		if ((hrh_lines->list_of_line + i)->direction == HORIZONTAL)
		{
			hrh_DrawHorizontalLine(hrh_lines->list_of_line + i);
		}
		else if ((hrh_lines->list_of_line + i)->direction == VERTICLE)
		{
			hrh_DrawVirticleLine(hrh_lines->list_of_line + i);
		}
		if ((hrh_lines->list_of_line + (hrh_lines->current_line))->isCompleted == true && hrh_lines->current_line < total_lines - 1)
		{
			hrh_lines->current_line = hrh_lines->current_line + 1;
		}
	}
}

void hrh_DrawVirticleLine(HRH_LINE* hrh_line)
{
	//VARIABLE DECLARATION
	GLfloat i;

	//CODE
	if (hrh_line->condition == GREATER_THAN_EQUAL_TO)
	{
		glBegin(GL_POINTS);
		for (i = hrh_line->fromPoint.y; i >= hrh_line->tillPoint.y; i = i + hrh_line->distance)
		{
			hrh_line->currentPoint.x = (i / hrh_line->m) - (hrh_line->fromPoint.y / hrh_line->m) + hrh_line->fromPoint.x;
			hrh_line->currentPoint.y = i;
			glVertex3f(hrh_line->currentPoint.x, hrh_line->currentPoint.y, 0.0f);
		}
		glEnd();
		if (hrh_line->tillPoint.y >= hrh_line->toPoint.y)
		{
			hrh_line->tillPoint.y = i + hrh_line->distance;
		}
		else
		{
			hrh_line->isCompleted = true;
		}
	}
	else if (hrh_line->condition == LESS_THAN_EQUAL_TO)
	{
		glBegin(GL_POINTS);
		for (i = hrh_line->fromPoint.y; i <= hrh_line->tillPoint.y; i = i + hrh_line->distance)
		{
			hrh_line->currentPoint.x = (i / hrh_line->m) - (hrh_line->fromPoint.y / hrh_line->m) + hrh_line->fromPoint.x;
			hrh_line->currentPoint.y = i;
			glVertex3f(hrh_line->currentPoint.x, hrh_line->currentPoint.y, 0.0f);
		}
		glEnd();
		if (hrh_line->tillPoint.y <= hrh_line->toPoint.y)
		{
			hrh_line->tillPoint.y = i + hrh_line->distance;
		}
		else
		{
			hrh_line->isCompleted = true;
		}
	}
}

void hrh_DrawHorizontalLine(HRH_LINE* hrh_line)
{
	//VARIABLE DECLARATION
	GLfloat i;

	//CODE
	if (hrh_line->condition == GREATER_THAN_EQUAL_TO)
	{
		glBegin(GL_POINTS);
		for (i = hrh_line->fromPoint.x; i >= hrh_line->tillPoint.x; i = i + hrh_line->distance)
		{
			hrh_line->currentPoint.y = (hrh_line->m * i) - (hrh_line->m * hrh_line->fromPoint.x) + (hrh_line->fromPoint.y);
			hrh_line->currentPoint.x = i;
			glVertex3f(hrh_line->currentPoint.x, hrh_line->currentPoint.y, 0.0f);
			glVertex3f(hrh_line->currentPoint.x, hrh_line->currentPoint.y, 0.0f);
		}
		glEnd();
		if (hrh_line->tillPoint.x >= hrh_line->toPoint.x)
		{
			hrh_line->tillPoint.x = i + hrh_line->distance;
			hrh_line->tillPoint.y = 0.0f;
		}
		else
		{
			hrh_line->isCompleted = true;
		}
	}
	else if (hrh_line->condition == LESS_THAN_EQUAL_TO)
	{
		glBegin(GL_POINTS);
		for (i = hrh_line->fromPoint.x; i <= hrh_line->tillPoint.x; i = i + hrh_line->distance)
		{
			hrh_line->currentPoint.y = (hrh_line->m * i) - (hrh_line->m * hrh_line->fromPoint.x) + (hrh_line->fromPoint.y);
			hrh_line->currentPoint.x = i;
			glVertex3f(hrh_line->currentPoint.x, hrh_line->currentPoint.y, 0.0f);
			glVertex3f(hrh_line->currentPoint.x, hrh_line->currentPoint.y, 0.0f);
		}
		glEnd();
		if (hrh_line->tillPoint.x <= hrh_line->toPoint.x)
		{
			hrh_line->tillPoint.x = i + hrh_line->distance;
			hrh_line->tillPoint.y = 0.0f;
		}
		else
		{
			hrh_line->isCompleted = true;
		}
	}
}


/********************CIRCLES********************************************************/
void hrh_DrawCircles(int total_lines)
{
	// FUNCTION DECLARATION
	void hrh_DrawCircle(HRH_CIRCLE * hrh_circle);

	//CODE
	for (int i = 0; i <= hrh_circles->current_circle; i++)
	{
		hrh_DrawCircle(hrh_circles->hrh_circle + i);
		if ((hrh_circles->hrh_circle + hrh_circles->current_circle)->isCompleted == true && hrh_circles->current_circle < total_lines - 1)
		{
			hrh_circles->current_circle += 1;
		}
	}
}

void hrh_DrawCircle(HRH_CIRCLE* hrh_circle)
{
	// VARIABLE DECLARATION
	GLfloat angle = 0.0f;
	GLfloat circle_x = 0, circle_y = 0;

	// CODE
	glBegin(GL_POINTS);
	for (int i = 0; i < hrh_circle->currentCount; i++)
	{
		angle = i * RAD_TO_ANGLE;
		circle_x = hrh_circle->hrh_point.x + (hrh_circle->radius * cos(angle));
		circle_y = hrh_circle->hrh_point.y + (hrh_circle->radius * sin(angle));
		glVertex3f(circle_x, circle_y, 0.0f);
	}
	glEnd();
	if (hrh_circle->currentCount <= 360)
	{
		hrh_circle->currentCount = hrh_circle->currentCount + 1;
	}
	else
	{
		hrh_circle->isCompleted = true;
	}
}

void hrh_DrawRectangles(HRH_RECTANGLE *hrh_rectangle, GLuint texture_id)
{
	// FUNCTION DECLARATION
	void Rectangle(GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height, GLuint texture_id);

	// CODE
	Rectangle(hrh_rectangle->x, hrh_rectangle->y, hrh_rectangle->z, hrh_rectangle->height, hrh_rectangle->width, texture_id);
}

void Rectangle(GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height, GLuint texture_id)
{
	//CODE
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(x + width / 2, y + height / 2, z);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(x - width / 2, y + height / 2, z);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(x - width / 2, y - height / 2, z);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(x + width / 2, y - height / 2, z);
		
	}
	glEnd();
	glDisable(GL_TEXTURE_2D);

	glBegin(GL_LINE_LOOP);
	{
		glVertex3f(x + 0.01f + width / 2, y + 0.01f + height / 2, z);
		glVertex3f(x - 0.01f - width / 2, y + 0.01f + height / 2, z);
		glVertex3f(x - 0.01f - width / 2, y - 0.01f - height / 2, z);
		glVertex3f(x + 0.01f + width / 2, y - 0.01f - height / 2, z);
	}
	glEnd();
}

/*************************** DRAW STRINGS *************************************/
void hrh_Seen5()
{
	// FUNCTION DECLARATION
	void printStringBicycle(GLfloat, GLfloat, GLfloat);
	void printStringDialogue(GLfloat, GLfloat, GLfloat);
	void printStringSong(GLfloat, GLfloat, GLfloat);
	void printStringGroup(GLfloat, GLfloat, GLfloat);
	void printBlessings(GLfloat, GLfloat, GLfloat);
	void printAstroMediComp(GLfloat, GLfloat, GLfloat);

	// VARIABLE DECLARATIONS
	static GLfloat logo_speed = 0.0f, first_string_speed = 0.0f;

	// CODE
	glPushMatrix();
	printStringBicycle(string_translate_x, first_string_speed, 0.0f);
	glPopMatrix();

	string_translate_x = string_translate_x + 1.0f;
	glPushMatrix();
	printStringDialogue(string_translate_x, string_translate_y - 18.0f, 0.0f);
	glPopMatrix();

	glPushMatrix();
	printStringSong(string_translate_x, string_translate_y - 25.0f, 0.0f);
	glPopMatrix();

	glPushMatrix();
	printStringGroup(string_translate_x, string_translate_y - 35.0f, 0.0f);
	glPopMatrix();

	glPushMatrix();
	printBlessings(string_translate_x, string_translate_y - 45.0f, 0.0f);
	glPopMatrix();

	glPushMatrix();
	printAstroMediComp(-1.5f, (hrh_AstroMediComp_logo.y - 2.1f) + logo_speed, 0.0f);
	glColor3ub(255, 255, 255);
	glPopMatrix();
	string_translate_x = string_translate_x - 1.0f;

	if (string_translate_x > 0.5f)
	{
		string_translate_x = string_translate_x - 0.007;
	}
	else
	{
		if (first_string_speed < 7 && string_translate_y > 5)
		{
			first_string_speed = first_string_speed + 0.006; 
		}
		if (string_translate_y > 34)
		{
			logo_speed = logo_speed + 0.00051;
			hrh_AstroMediComp_logo.y = hrh_AstroMediComp_logo.y + 0.0021;
		}
		string_translate_y = string_translate_y + 0.015f;
	}
}

void printAstroMediComp(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATION
	char string_AstroMediComp[] = "AstroMediComp";
	GLfloat scale_size = 0.8f;

	// CODE
	glScalef(scale_size, scale_size, 1.0f);
	glColor3ub(255, 255, 255);

	glPushMatrix();
	glTranslatef(translate_x - 1.5f, translate_y + 1.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_AstroMediComp) - 1, GL_UNSIGNED_BYTE, string_AstroMediComp);
	glPopMatrix();
}

void printBlessings(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATIONS
	char string_blessings[] = "WITH BLESSINGS";
	char string_mam[] = "DR. RAMA VIJAY GOKHALE MAM";
	char string_sir[] = "DR. VIJAY D. GOKHALE SIR";
	char string_and[] = "AND";

	// CODE
	glScalef(scale_size, scale_size, 1.0f);
	glColor3ub(0, 125, 125);

	glPushMatrix();
	glTranslatef(translate_x + 6.0f, translate_y + 1.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_blessings) - 1, GL_UNSIGNED_BYTE, string_blessings);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 3.0f, translate_y + 0.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_mam) - 1, GL_UNSIGNED_BYTE, string_mam);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 8.0f, translate_y - 1.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_and) - 1, GL_UNSIGNED_BYTE, string_and);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 3.5f, translate_y - 2.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_sir) - 1, GL_UNSIGNED_BYTE, string_sir);
	glPopMatrix();
}


void printStringGroup(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATION
	char string_group_details[] = "PROJECT DETAILS";
	char string_group[] = "GROUP NAME   FRAGMENT"; 
	char string_group_leader[] = "GROUP LEADER   AJAY AMBURE"; 
	char string_music_edit[] = " MUSIC EDIT BY   AKASH & HRITUJA HEDAU";
	char string_project[] = "PROJECT BY   HRITUJA HEDAU";
	char string_project_year[] = "DATE   28TH FEB 2021";
	char string_project_rendering[] = "  RENDERING   OpenGL";
	char string_project_language[] = "    LANGUAGE   C & CPP";

	// CODE
	glScalef(scale_size, scale_size, 1.0f);
	glPushMatrix();
	glTranslatef(translate_x + 6.0F, translate_y + 1.0f, 0.0f);
	glColor3ub(0, 125, 125);
	glListBase(nFontList);
	glCallLists(sizeof(string_group_details) - 1, GL_UNSIGNED_BYTE, string_group_details);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 1.9F, translate_y + 0.0f, 0.0f);
	glColor3ub(0, 125, 125);
	glListBase(nFontList);
	glCallLists(sizeof(string_project_rendering) - 1, GL_UNSIGNED_BYTE, string_project_rendering);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 1.8F, translate_y - 1.0f, 0.0f);
	glColor3ub(0, 125, 125);
	glListBase(nFontList);
	glCallLists(sizeof(string_project_language) - 1, GL_UNSIGNED_BYTE, string_project_language);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 1.8f, translate_y - 2.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_group) - 1, GL_UNSIGNED_BYTE, string_group);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 0.8f, translate_y - 3.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_group_leader) - 1, GL_UNSIGNED_BYTE, string_group_leader);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 1.0f, translate_y - 4.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_music_edit) - 1, GL_UNSIGNED_BYTE, string_music_edit);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 2.2f, translate_y - 5.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_project) - 1, GL_UNSIGNED_BYTE, string_project);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 5.3f, translate_y - 6.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_project_year) - 1, GL_UNSIGNED_BYTE, string_project_year);
	glPopMatrix();
}


void printStringSong(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATION
	char string_song_title[] = "SONG";
	char string_song[] = "SONG NAME  SHAABAASHIYAAN"; 
	char string_lyricist[] = "LYRICIST   AMITABH BHATTACHARYA"; 
	char string_music[] = "MUSIC DIRECTOR  AMIT TRIVEDI";
	char string_movie[] = "MOVIE   MISSION MANGAL";
	char string_movie_director[] = "MOVIE DIRECTOR   JAGAN SHAKTI";
	char string_movie_year[] = "YEAR   2019";

	// CODE
	glScalef(scale_size, scale_size, 1.0f);
	glPushMatrix();

	glTranslatef(translate_x + 6.0F, translate_y + 1.0f, 0.0f);

	glColor3ub(0, 125, 125);
	glListBase(nFontList);
	glCallLists(sizeof(string_song_title) - 1, GL_UNSIGNED_BYTE, string_song_title);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 2.0f, translate_y + 0.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_song) - 1, GL_UNSIGNED_BYTE, string_song);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 3.5f, translate_y - 1.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_lyricist) - 1, GL_UNSIGNED_BYTE, string_lyricist);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x - 0.2f, translate_y - 2.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_music) - 1, GL_UNSIGNED_BYTE, string_music);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 4.6f, translate_y - 3.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_movie) - 1, GL_UNSIGNED_BYTE, string_movie);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x - 0.2f, translate_y - 4.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_movie_director) - 1, GL_UNSIGNED_BYTE, string_movie_director);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 5.1f, translate_y - 5.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_movie_year) - 1, GL_UNSIGNED_BYTE, string_movie_year);
	glPopMatrix();
}

void printStringDialogue(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATION
	char string_from_dialogue[] = "DIALOGUE";
	char string_movie[] = "MOVIE   PURAB AUR PACHHIM";
	char string_movie_director[] = "MOVIE DIRECTOR   MANOJ KUMAR";
	char string_movie_year[] = "YEAR   1970";

	// CODE
	glScalef(scale_size, scale_size, 1.0f);
	glPushMatrix();

	glTranslatef(translate_x + 6.0F, translate_y + 1.0f, 0.0f);

	glColor3ub(0, 125, 125);
	glListBase(nFontList);
	glCallLists(sizeof(string_from_dialogue) - 1, GL_UNSIGNED_BYTE, string_from_dialogue);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 4.6f, translate_y + 0.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_movie) - 1, GL_UNSIGNED_BYTE, string_movie);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x, translate_y - 1.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_movie_director) - 1, GL_UNSIGNED_BYTE, string_movie_director);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 5.2f, translate_y - 2.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_movie_year) - 1, GL_UNSIGNED_BYTE, string_movie_year);
	glPopMatrix();
}

void printStringBicycle(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATION
	char string_from_bicycle[] = "FROM BICYCLE";
	char string_to[] = "TO";
	char string_billion_dreams[] = "BILLION DREAMS";
	GLfloat scale_size = 0.5f;

	// CODE
	glScalef(scale_size, scale_size, 1.0f);
	glPushMatrix();
	glTranslatef(translate_x, translate_y + 1.0f, 0.0f);

	glColor3ub(0, 125, 125);
	glListBase(nFontList);
	glCallLists(sizeof(string_from_bicycle) - 1, GL_UNSIGNED_BYTE, string_from_bicycle);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 3.0f, translate_y + 0.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_to) - 1, GL_UNSIGNED_BYTE, string_to);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x, translate_y - 1.0f, 0.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_billion_dreams) - 1, GL_UNSIGNED_BYTE, string_billion_dreams);
	glPopMatrix();
}
