/********************
* 
* Fixed Functional Pipeline Project :
* Name: Hrituja Ramkrishna Maya Hedau
* Batch: RTR 3.0f
* Group Name: Fragment Group
* Grooup Leader: Ajay Ambure
* Date: 29-March-2021
* Technology: c/c++, win32 sdk, OpegnGL API
* 
***************************/

// header file declaration
#include "FFPproject.h"

#define WIDTH 1000
#define HIGHT 800

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;

int width = WIDTH, hight = HIGHT;

// textures
GLuint stone_wall_texture = 0, roof_texture = 0, grass_texture = 0, black_texture = 0, left_door_texture = 0, right_door_texture = 0, window_texture = 0, warali_texture =0, tree_texture = 0, marble = 0, boy_face = 0, wooden = 0;
GLuint bird_body_texture = 0, bird_feather_texture = 0, tree_base_texture = 0;

// quadric
GLUquadric* quadric = NULL, * quadric_for_human = NULL, * quadric_for_tree = NULL, *quadric_for_sun = NULL;

/************************************************BIRD******************************************/
GLfloat bird_head_angle_x = 0, bird_head_angle_y = 0.0f, bird_head_angle_z = 0.0f, bird_body = 0;
GLfloat move_wings_right = 0.0f, move_wings_left = 0.0f;
GLfloat hrh_move_human_x = -10.0f, hrh_move_human_y = 0.0f, hrh_move_human_z = 30.0f, bird_beak_up = 0.0f, bird_beak_down = 360.0f;
GLfloat angle_for_bird_x = 0.0f, angle_for_bird_y = 0.0f, angle_for_bird_z = 0.0f;
GLfloat translate_bird_x = 40.0f, translate_bird_y = -1.6f, translate_bird_z = 40.0f;
GLfloat tempz = 0.0f;
GLfloat hrh_move_left_hands = 360.0f, hrh_move_right_hands = 360.0f;
bool bird_chippering = true, stop_bird_chippering = true, bFlyBird = false;
GLfloat bird_chippering_speed = 0.2f;
GLfloat translate_bowl_x = 0.0f, translate_bowl_y = 0.0f, translate_bowl_z = -10.0f;

/**************************************************CAMERA*************************************/
GLfloat angle_for_camera_x = 0.0f, angle_for_camera_y = 0.0f, angle_for_camera_z = 0.0f;
GLfloat translate_camera_x = 0.0f, translate_camera_y = 0.0f, translate_camera_z = 40.0f; //40.0f
bool fly_up = true, fly_down = false;
//GLfloat angle_for_sun_y = -15.0f;
bool move_camera_1 = true, move_camera_2 = false;

/*********************************************SUN************************************************/
GLfloat angle_for_sun_x = -25.0f, angle_for_sun_y = -10.0f, angle_for_sun_z = -30.0f;
GLfloat sun_colo_r = 1, sun_color_g = 1.0f, sun_color_b = 0.0f;

/************************************************HUMAN*******************************************/
GLfloat hrh_radiusForBodyParts = 0.15f;
GLfloat human_translate_x = 15.0f, human_translate_y = -1.6f, human_translate_z = 32;  //human_translate_x = 20.0f
int human_angle_x, human_angle_y = 270, human_angle_z;
GLfloat hrh_move_from_sholder_left_hand_right_leg = 0, hrh_move_from_sholder_right_hand_left_leg = 0;
GLfloat  hrh_sholder = 0, hrh_angle = 0; //hrh_move_elbow = 270.0f
GLfloat hrh_move_left_elbow = 270.0f, hrh_move_right_elbow = 270.0f;
bool bIn = true, bOut = false;
//GLfloat hrh_move_elbow_x = 20; 
GLfloat hrh_move_left_elbow_x = 20.0f, hrh_move_right_elbow_x = 20.0f;
bool boy_walk_1 = false, boy_walk_2 = false ;
GLint emoji_state = 0;
GLfloat human_head_rotate_x = 0.0f, translate_eye_y = 0.0f;
GLfloat human_main_body_angle = 0.0f;
GLfloat hrh_move_left_hands_y = 0.0f, hrh_move_right_hands_y = 0.0f;
GLfloat hrh_move_right_elbow_y = 0.0f, hrh_move_left_elbow_y = 0.0f;


/**********************************************BACKGROUND COLOR*******************************************/
GLfloat background_red = 0, background_blue = 0, background_green = 0;

/***********************************************LIGHT****************************************/
GLfloat glFloatLightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat glFloatLightDiffuse[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat glFloatLightSpecular[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat glFloatLightPosition[] = { 100.0f, 100.0f,100.0f, 1.0 };

/**********************************************SEEN CHANGES*********************************************/
int seen_number = 0;
INITIAL_STATE current_state, seen1_state, seen2_state, seen3_state, seen4_state, seen6_state, seen7_state, seen10_state, seen11_state, seen12_state;
bool rotate_human = false;
GLfloat  open_right_door_angle = 360.0f, open_right_door_translate_x = 0.0f;
GLfloat open_left_door_angle = 180.0f, open_left_door_translate_x = -11.0f, open_right_door_translate_z = 0.0f, open_left_door_translate_z = 6.0f;

bool open_cage_door = false, close_cage_door =false;

GLfloat calf_angle = 0.0f;
GLfloat move_hand;

short nFontList, nFontList_underlines;
GLfloat font_z = -10.0f;
bool stopAnimation = false, startAnimation = false;
HFONT hFont, hFontWithUnderLine;
GLYPHMETRICSFLOAT agmf[128];
short title_r = 51, title_g = 51, title_b = 255;
short text_r = 255, text_g = 0, text_b = 0;
GLfloat scale_title_size = 0.25f;
GLfloat scale_text_size = 0.15f;
GLfloat font_translate_z = 62.0f;
bool print_credits = false;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();
	void Update();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("ffp_project");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code
	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("FFP PROJECT"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
				Update();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);
	TCHAR str[255] = TEXT("");

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			width = LOWORD(lParam);
			hight = HIWORD(lParam);
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case 'w':
			case 'W':
				hrh_move_human_z = (hrh_move_human_z - 0.1);
				break;
			case 's':
			case 'S':
				hrh_move_human_z = (hrh_move_human_z + 0.1);
				break;
			case 'D':
			case 'd':
				hrh_move_human_x = (hrh_move_human_x + 0.1);
				break;
			case 'h':
			case 'H':
				translate_camera_x = translate_camera_x - 1.0f;
				break;
			case 'k':
			case 'K':
				translate_camera_x = translate_camera_x + 1.0f;
				break;
			case 'u':
			case 'U':
				translate_camera_z = translate_camera_z - 1.0f;
				break;
			case 'j':
			case 'J':
				translate_camera_z = translate_camera_z + 1.0f;
				break;
			case 'm':
			case 'M':
				angle_for_camera_x = angle_for_camera_x + 1.0f;
				break;
			case 'n':
			case 'N':
				angle_for_camera_x = angle_for_camera_x - 1.0f;
				break;
			case 'o':
				open_left_door_angle = open_left_door_angle + 1.0f;
				open_right_door_translate_x = open_right_door_translate_x - 0.07;
				open_right_door_angle = open_right_door_angle - 1.0f;
				fprintf(gpFile, "\nopen_left_door_angle = %f, open_right_door_angle = %f", open_left_door_angle, open_right_door_angle);
				break;
			case 'O':
				open_left_door_angle = open_left_door_angle - 1.0f;
				open_right_door_angle = open_right_door_angle + 1.0f;
				fprintf(gpFile, "\nopen_left_door_angle = %f, open_right_door_angle = %f", open_left_door_angle, open_right_door_angle);
				break;
			case 'c':
				open_cage_door = true;
				close_cage_door = false;
				break;
			case 'C':
				close_cage_door = true;
				open_cage_door = false;
				break;
			case 'v':
				human_head_rotate_x = human_head_rotate_x + 3.0f;
				break;
			case 'V':
				human_head_rotate_x = human_head_rotate_x - 3.0f;
				break;
			case 'a':
				font_z = font_z + 3.0f;
				break;
			case 'A':
				font_z = font_z - 3.0f;
				break;
			case '0':
				startAnimation = true;
				break;
			}
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
				if (startAnimation == false)
				{
					if (PlaySound(ID_BACKGORUND_MUSIC, GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC) == FALSE)
					{
						MessageBox(NULL, TEXT("FAILED TO PLAY MUSIC"), TEXT("ERROR"), MB_OK);
					}
					startAnimation = true;
				}
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	bool LoadGLTexture(GLuint*, TCHAR[]);
	void SetInitialSeenState();
	void SetUpRC();

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	LoadGLTexture(&stone_wall_texture, ID_STONE_WALL);
	LoadGLTexture(&roof_texture, ID_ROOF);
	LoadGLTexture(&grass_texture, ID_GRASS);
	LoadGLTexture(&black_texture, ID_BLACK_TEXTURE);
	LoadGLTexture(&left_door_texture, ID_LEFT_DOOR_TEXTURE);
	LoadGLTexture(&right_door_texture, ID_RIGHT_DOOR_TEXTURE);
	LoadGLTexture(&window_texture, ID_WINDOW_TEXTURE);
	LoadGLTexture(&bird_body_texture, ID_BIRD_BODY_TEXTURE);
	LoadGLTexture(&bird_feather_texture, ID_BIRD_FEATHER_TEXTURE);
	LoadGLTexture(&warali_texture, ID_WARALI_TEXTURE);
	LoadGLTexture(&tree_texture, ID_TREE_TEXTURE);
	LoadGLTexture(&marble, ID_MARBLE_TEXTURE);
	LoadGLTexture(&boy_face, ID_BOY_FACE_TEXTURE);
	LoadGLTexture(&wooden, ID_WOODEN_TEXTURE);
	LoadGLTexture(&tree_base_texture, ID_TREE_BASE_TEXTURE);

	glLightfv(GL_LIGHT0, GL_DIFFUSE, glFloatLightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, glFloatLightSpecular);
	glLightfv(GL_LIGHT0, GL_AMBIENT, glFloatLightAmbient);
	glLightfv(GL_LIGHT0, GL_POSITION, glFloatLightPosition);
	glDisable(GL_LIGHT0);

	SetUpRC();

	SetInitialSeenState();

	Resize(WIDTH, HIGHT);
}

bool LoadGLTexture(GLuint* texture, TCHAR resourceID[])
{
	bool bResult = false;
	HBITMAP hBitmap;
	BITMAP bmp;

	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

	if (hBitmap)
	{	
		bResult = true;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		DeleteObject(hBitmap);
	}

	if (bResult == false)
	{
		fprintf(gpFile, "\nTexture Not Loaded");
	}

	return bResult;
}

void Resize(int width, int hight) {
	if (hight == 0)
		hight = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, ((GLfloat)width / (GLfloat)hight), 0.1f, 100.0f);
}
  
void SetInitialSeenState()
{
	memset(&current_state, 0.0f, sizeof(INITIAL_STATE));
	memset(&seen1_state, 0.0f, sizeof(INITIAL_STATE));
	memset(&seen2_state, 0.0f, sizeof(INITIAL_STATE));
	memset(&seen3_state, 0.0f, sizeof(INITIAL_STATE));
	memset(&seen4_state, 0.0f, sizeof(INITIAL_STATE));
	memset(&seen6_state, 0.0f, sizeof(INITIAL_STATE));
	memset(&seen7_state, 0.0f, sizeof(INITIAL_STATE));
	memset(&seen10_state, 0.0f, sizeof(INITIAL_STATE));
	memset(&seen11_state, 0.0f, sizeof(INITIAL_STATE));

	seen2_state.init_translate_bird_z = 40;
	seen3_state.isCopiedIntialState = false;
	seen4_state.isCopiedIntialState = false;
	seen6_state.isCopiedIntialState = false;
	seen7_state.isCopiedIntialState = false;
	seen10_state.isCopiedIntialState = false;
	seen11_state.isCopiedIntialState = false;
}

void Display() {
	// FUNCTION DECLARATION

	void AnimationScreen();
	void Seen0();
	void Seen13();

	// CODE
	glClearColor(0.0f, background_green, background_blue, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(translate_camera_x, translate_camera_y, translate_camera_z, // camera position
		angle_for_camera_x, angle_for_camera_y, angle_for_camera_z,  // camera focus points
		0.0f, 1.0f, 0.0f); // camera up axis 

	if (seen_number == 0 && startAnimation == true)
	{
		glPushMatrix();
		Seen0();
		glPopMatrix();
	}

	AnimationScreen();
	if (print_credits == true)
	{
		Seen13();
	}

	SwapBuffers(ghdc);
}

void AnimationScreen()
{
	// FUNCTION DECLARATION
	void House();
	void HumanBody();
	void Bird();
	void TreeWithBase();
	void Sun();
	void OnlyWalls();
	void cage();
	void Bowl();
	void BirdFly();

	// CODE
	quadric = gluNewQuadric();
	quadric_for_human = gluNewQuadric();
	quadric_for_tree = gluNewQuadric();
	quadric_for_sun = gluNewQuadric();

	glPushMatrix();
	Sun();
	glPopMatrix();	

	glPushMatrix();
	HumanBody();
	glPopMatrix();
	if (seen_number == 3 || seen_number == 4 || seen_number == 5)
	{
		glPushMatrix();
		Bowl();
		glPopMatrix();
	}
	BirdFly();

	if (!(seen_number == 6 || seen_number == 7 || seen_number == 8 || seen_number == 9 || seen_number == 10))
	{
		glEnable(GL_TEXTURE_2D);
		glColor3f(1.0f, 1.0f, 1.0f);
		gluQuadricTexture(quadric, GL_TRUE);
		glPushMatrix();
		Bird();
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
	}

	glPushMatrix();
	if (!(seen_number == 3 || seen_number == 4 || seen_number == 5))
	{
		House();
	}
	else
	{
		OnlyWalls();
		glPushMatrix();
		cage();
		glPopMatrix();
	}
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
	glPushMatrix();
	if (!(seen_number == 3 || seen_number == 4 || seen_number == 5))
	{
		glEnable(GL_TEXTURE_2D);
		glColor3f(1.0f, 1.0f, 1.0f);
		gluQuadricTexture(quadric_for_tree, GL_TRUE);
		TreeWithBase();
		glDisable(GL_TEXTURE_2D);
	}
	glPopMatrix();

}

void Update()
{
	//FUNCTION DECLARATION
	void BirdFly();
	void BoyWalk();
	void Seen0();
	void Seen1();
	void Seen2();
	void Seen3();
	void Seen4();
	void Seen5();
	void Seen6();
	void Seen7();
	void Seen8();
	void Seen9();
	void Seen10();
	void Seen11();
	void Seen12();
	void copyInitialStateForSeen2();
	void copyInitialStateForSeen3();
	void copyInitialStateForSeen4();
	void copyInitialStateForSeen6();
	void copyInitialStateForSeen7();
	void copyInitialStateForSeen10();
	void copyInitialStateForSeen11();
	void copyInitialStateForSeen12();

	// VARIABLE DECLARATION

	//CODE
	if (seen_number == 1)
	{
		Seen1();
	}
	else if (seen_number == 2)
	{
		if (seen2_state.isCopiedIntialState == false)
		{
			copyInitialStateForSeen2();
		}
		Seen2();
	}
	else if (seen_number == 3)
	{
		if (seen3_state.isCopiedIntialState == false)
		{
			copyInitialStateForSeen3();
		}
		Seen3();
	}
	else if (seen_number == 4)
	{
		if (seen4_state.isCopiedIntialState == false)
		{
			copyInitialStateForSeen4();
		}
		Seen4();
	}
	else if (seen_number == 5)
	{
		Seen5();
	}
	else if (seen_number == 6)
	{
		if (seen6_state.isCopiedIntialState == false)
		{
			copyInitialStateForSeen6();
		}
		Seen6();
	}
	else if (seen_number == 7)
	{
		if (seen7_state.isCopiedIntialState == false)
		{
			copyInitialStateForSeen7();
		}
		Seen7();
	}
	else if (seen_number == 8)
	{
		Seen8();
	}
	else if (seen_number == 9)
	{
		Seen9();
	}
	else if (seen_number == 10)
	{
		if (seen10_state.isCopiedIntialState == false)
		{
			copyInitialStateForSeen10();
		}
		Seen10();
	}
	else if (seen_number == 11)
	{
		if (seen11_state.isCopiedIntialState == false)
		{
			copyInitialStateForSeen11();
		}
		Seen11();
	}
	else if (seen_number == 12)
	{
		if (seen12_state.isCopiedIntialState == false)
		{
			copyInitialStateForSeen12();
		}
		Seen12();
	}
}

void Seen13()
{
	// FUNCTION DECLARATION
	void printMusic(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z); 
	void printGroupName(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z); 
	void printGroupLeader(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z); 
	void printProjectBy(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z); 
	void printWithBlessing(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z);  
	void printWithIgnitedBy(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z); 
	void printThankYou(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z); 

	// VARIABLE DECLARATION
	static int count_for_string_change = 1, current_credit_function_index = 0, wait_count = 500;
	typedef void (*FUNCTION) (GLfloat, GLfloat, GLfloat);
	static FUNCTION list_credit_function[] = { printMusic , printGroupName , printGroupLeader , printProjectBy , printWithBlessing , printWithIgnitedBy , printThankYou };
	static int length_of_list_credit_function = sizeof(list_credit_function) / sizeof(list_credit_function[0]);


	// CODE
	(list_credit_function[current_credit_function_index])(0.0f, -0.5f, font_translate_z);

	if (count_for_string_change % wait_count == 0)
	{
		if (current_credit_function_index < length_of_list_credit_function - 2) 
		{
			current_credit_function_index = current_credit_function_index + 1;
			if (current_credit_function_index > 3 ) 
			{
				wait_count = 1150;				
			}			
		}
		count_for_string_change = 0;
	}

	if (stopAnimation == true)
	{
		current_credit_function_index = length_of_list_credit_function - 1;
	}

	count_for_string_change = count_for_string_change + 1;

}

void Seen0()
{
	// FUNCTION DECLARATION
	void printAstroMediComp(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z);
	void printBoyBirdAndLove(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z);

	// VARIABLE DECLARATION
	static int string = 1;

	// CODE
	if (string == 1)
	{
		printAstroMediComp(-2.5f, 0.0f, font_z);
		if (font_z > 40)
		{
			string = 2;
			font_z = 5.0f;
		}
	}
	else if (string == 2)
	{
		printBoyBirdAndLove(3.0f, 5.0f, font_z);
		if (font_z > 40)
		{
			string = 0;
			font_z = 5.0f;
			seen_number = 1;
		}
	}
	

	if (font_z < 40)
	{
		font_z = font_z + 0.04;
	}
}

void Seen12()
{

	// VARIABLE DECLARATION
	static bool rRotateCameraToRight = false, bRotateBirdToLeft = false, bStartBirdFly1 = true, bMoveBirdHeadDown = false, bMoveCameraToBoy = false;
	static bool bMoveCameraToLeft = true, bMoveHand = false, bToLeft = true, bToRight = false, bMoveCameraToBird = false, bRotateBirdToRight= false;
	static bool bStartBirdFly2 = false, bTurnOnBirdCamera = false;;
	static int countForWait = 0;

	if (bStartBirdFly1 == true)
	{
		translate_bird_z = translate_bird_z + 0.004;
		translate_bird_y = translate_bird_y + 0.0006;
		if (translate_bird_z > 33.0f)
		{
			bRotateBirdToLeft = true;
			bStartBirdFly1 = false;
		}
		if (angle_for_bird_x > 330.0f) // Move Head Up
		{
			angle_for_bird_x = angle_for_bird_x - 0.001f;
			bird_head_angle_x = bird_head_angle_x - 0.001f;
		}
	}

	if (bRotateBirdToLeft == true)
	{
		if (angle_for_bird_y > 250.0f)
		{
			angle_for_bird_y = angle_for_bird_y - 0.1f;
		}
		else
		{
			bRotateBirdToLeft = false;
			bMoveBirdHeadDown = true;
		}
	}

	if (bMoveBirdHeadDown == true)
	{
		if (angle_for_bird_x < 360.0f) 
		{
			angle_for_bird_x = angle_for_bird_x + 0.01f;
		}

		if (bird_head_angle_x < 400.0f) // move bird head down
		{
			bird_head_angle_x = bird_head_angle_x + 0.08f;
		}
		else
		{
			bMoveBirdHeadDown = false;
			bMoveCameraToBoy = true;
			human_angle_y = 380.0f;
			emoji_state = HAPPY;
			hrh_move_right_hands = 290.0f; 
			hrh_move_right_elbow_x = 90.0f;
			hrh_move_right_hands_y = 270.0f; 
			hrh_move_right_elbow_y = 270.0f; 
			bMoveHand = true;
		}
	}

	if (bMoveHand == true)
	{
		countForWait = countForWait + 1;
		if (countForWait == 1250)
		{
			fprintf(gpFile, "\ncountForWait = %d\n", countForWait);
			bMoveCameraToBird = true;
		}
		if (stopAnimation == false)
		{
			if (bToLeft == true)
			{
				if (hrh_move_right_elbow_x > 70.0f)
				{
					hrh_move_right_elbow_x = hrh_move_right_elbow_x - 0.1;
				}
				else
				{
					bToLeft = false;
					bToRight = true;
				}
			}
			else if (bToRight == true)
			{
				if (hrh_move_right_elbow_x < 110.0f)
				{
					hrh_move_right_elbow_x = hrh_move_right_elbow_x + 0.1;
				}
				else
				{
					bToLeft = true;
					bToRight = false;
				}
			}
		}
	}

	if (bMoveCameraToBoy == true)
	{
		if (angle_for_camera_x > 20)
		{
			angle_for_camera_x = angle_for_camera_x - 0.2;
		}
		else
		{
			bMoveCameraToBoy = false;
		}
	}

	if (bMoveCameraToLeft == true)
	{
		if (translate_camera_x > -10)
		{
			translate_camera_x = translate_camera_x - 0.007f;
		}
		else
		{
			bMoveCameraToLeft = false;			
		}
		if(translate_camera_x < -8)
		{
			rRotateCameraToRight = true;
		}
	}

	if (rRotateCameraToRight == true)
	{
		if (angle_for_camera_x < 90)
		{
			angle_for_camera_x = angle_for_camera_x + 0.03;
		}
		else
		{
			rRotateCameraToRight = false;			
		}
	}

	if (bMoveCameraToBird == true)
	{
		if (angle_for_camera_x < 90)
		{
			angle_for_camera_x = angle_for_camera_x + 0.05;
		}
		else
		{
			bRotateBirdToRight = true;
		}
	}

	if (bRotateBirdToRight == true)
	{
		if (angle_for_bird_x > 360.0f)
		{
			angle_for_bird_x = angle_for_bird_x - 0.02f;
		}
		if (bird_head_angle_x > 360.0f) // move bird head up again
		{
			bird_head_angle_x = bird_head_angle_x - 0.08f;
		}
		if (angle_for_bird_y < 360.0f)
		{
			angle_for_bird_y = angle_for_bird_y + 0.15f;
		}
		else
		{
			bRotateBirdToRight = false;
			bStartBirdFly2 = true;
		}
	}

	if (bStartBirdFly2 == true)
	{
		translate_bird_z = translate_bird_z + 0.003f;
		if (angle_for_camera_x <= 90.0f) 
		{
			angle_for_camera_x = angle_for_camera_x + 0.1f;
		}

		if (translate_bird_z > 37)
		{
			bStartBirdFly2 = false;
			bTurnOnBirdCamera = true;
		}
	}

	if (bTurnOnBirdCamera == true)
	{
		if (stopAnimation == false)
		{
			translate_bird_z = translate_bird_z + 0.0036f;
			translate_bird_y = translate_bird_y + 0.0002;
		}

		translate_camera_z = 65.0f;
		angle_for_camera_x = 0.0f;
		translate_camera_x = 0.0f;

		if (translate_bird_z > 55)
		{
			stopAnimation = true;
			bFlyBird = false;
			bird_chippering = false;
			stop_bird_chippering = false;
		}
	}
	if (translate_bird_z > 38)
	{
		print_credits = true;
	}

}

void Seen11()
{
	// FUNCTION DECLARATION
	void BoyWalk(bool left, bool right, bool z_out, bool z_in);

	// VARIABLE DECLARATION
	static bool bMoveBoyToLeft = true, bFirstRotateBoyToLeft = false, bSecoandRotateBoyToLeft = false, bMoveOut = false, bMoveBoyToRight = false;
	static bool bBoyBendDown = false, bBoyBendUp = false, bMoveBoyBack = false, bRotateBird = false;

	// CODE
	if (bMoveBoyToLeft == true)
	{
		if (human_translate_x > -6.0f)
		{
			BoyWalk(true, false, false, false);
			translate_bird_x = human_translate_x - 1.5;
		}
		else
		{
			bFirstRotateBoyToLeft = true;
			bMoveBoyToLeft = false;
		}
	}
	if (bFirstRotateBoyToLeft == true)
	{
		if (human_angle_y < 360)
		{
			human_angle_y = human_angle_y + 1.0f;
			angle_for_bird_y = angle_for_bird_y + 1.0f;
			translate_bird_x = human_translate_x;
			translate_bird_z = human_translate_z + 1.5f;
		}
		else
		{
			bMoveOut = true;
			bFirstRotateBoyToLeft = false;
		}
	}
	if (bMoveOut == true)
	{
		if (human_translate_z < 15.0f) 
		{
			BoyWalk(false, false, true, false);
			translate_bird_z = human_translate_z + 1.5f;
		}
		else
		{
			bSecoandRotateBoyToLeft = true;
			bMoveOut = false;
		}
	}
	if (bSecoandRotateBoyToLeft == true)
	{
		if (human_angle_y < 360 + 90.0f)
		{
			human_angle_y = human_angle_y + 1.0f;
			angle_for_bird_y = angle_for_bird_y + 1.0f;
			translate_bird_x = human_translate_x;
			translate_bird_z = human_translate_z + 1.0f;
		}
		else
		{
			bMoveBoyToRight = true;
			bSecoandRotateBoyToLeft = false;
			translate_bird_z = human_translate_z;
		}
	}
	if (bMoveBoyToRight == true)
	{
		if (human_translate_x < 4.0f)
		{
			BoyWalk(false, true, false, false);
			translate_bird_x = human_translate_x + 1.5f;
		}
		else
		{
			bMoveBoyToRight = false;
			hrh_move_from_sholder_left_hand_right_leg = 360.0f;
			hrh_move_from_sholder_right_hand_left_leg = 360.0f;
			bBoyBendDown = true;
		}
	}
	if (bBoyBendDown == true)
	{
		if (human_main_body_angle < 10)
		{
			human_main_body_angle = human_main_body_angle + 0.03f;
			hrh_move_from_sholder_left_hand_right_leg = hrh_move_from_sholder_left_hand_right_leg - 0.03;
			hrh_move_from_sholder_right_hand_left_leg = hrh_move_from_sholder_right_hand_left_leg - 0.03f;
		}
		else
		{
			bBoyBendDown = false;
			bBoyBendUp = true;
		}
	}
	if (bBoyBendUp == true)
	{
		if (human_main_body_angle > 0)
		{
			human_main_body_angle = human_main_body_angle - 0.03f;
			hrh_move_from_sholder_left_hand_right_leg = hrh_move_from_sholder_left_hand_right_leg + 0.03;
			hrh_move_from_sholder_right_hand_left_leg = hrh_move_from_sholder_right_hand_left_leg + 0.03f;
			translate_bird_z = translate_bird_z - 0.01f;
		}
		else
		{
			bBoyBendUp = false;
			bMoveBoyBack = true;
			hrh_move_from_sholder_left_hand_right_leg = 0.0f;
			hrh_move_from_sholder_right_hand_left_leg = 0.0f;
		}
	}

	if (bMoveBoyBack == true)
	{
		if (human_translate_x > -6.0f)
		{
			BoyWalk(true, false, false, false);
		}
		else
		{
			bMoveBoyBack = false;
			hrh_move_from_sholder_left_hand_right_leg = 0.0f;
			hrh_move_from_sholder_right_hand_left_leg = 0.0f;
			bRotateBird = true;
		}
		if (hrh_move_right_elbow < 360.0f)
		{
			hrh_move_left_elbow = hrh_move_left_elbow + 1.0f;
			hrh_move_right_elbow = hrh_move_right_elbow + 1.0f;
		}
	}
	if (bRotateBird == true)
	{
		if (angle_for_bird_y > 360)
		{
			angle_for_bird_y = angle_for_bird_y - 0.1f;
		}
		else
		{
			seen_number = 12;
			bFlyBird = true;
		}
	}
}

void Seen7()
{
	// FUNCTION DECLARATION
	void BoySitDown();

	// CODE
	BoySitDown();
}

void Seen8()
{
	//	FUNCTION DECLARATION
	void moveCameraUpAndDown();

	// CODE
	moveCameraUpAndDown();
}

void Seen9()
{
	// FUNCTION DECLARATION
	void BoySitUp();

	// CODE
	BoySitUp();
}

void Seen10()
{
	// FUNCTION DECLARATION
	void BoyWalk(bool, bool, bool, bool);

	// VARIABLE DECLARATION
	static bool bBoySecondRight = false , bMoveBoyIntoHouse = false, bTurnBoyRight = false, bBoyFirstRight = true, bBoyThirdRight = false, bMoveBoyToRight = false;

	// CODE
	if (bBoyFirstRight == true)
	{
		if (human_angle_y > 270)
		{
			human_angle_y = human_angle_y - 0.5f;
		}
		else
		{
			bBoySecondRight = true;
			bBoyFirstRight = false;
		}
	}
	if (bBoySecondRight == true)
	{
		if (human_translate_x > -6.0f)
		{
			BoyWalk(true, false, false, false);
		}
		else
		{
			if (human_angle_y > 180)
			{
				human_angle_y = human_angle_y - 0.5f;
			}
			else
			{
				bMoveBoyIntoHouse = true;
				bBoySecondRight = false;
			}
		}
	}
	if (bMoveBoyIntoHouse == true)
	{
		if (human_translate_z > -3.0f) 
		{
			BoyWalk(false, false, false, true);
		}
		else
		{
			bBoyThirdRight = true;
			bMoveBoyIntoHouse = false;
		}
	}
	if (bBoyThirdRight == true)
	{
		if (human_angle_y > 90)
		{
			human_angle_y = human_angle_y - 0.5f;
		}
		else
		{
			bMoveBoyToRight = true;
			bBoyThirdRight = false;
		}
	}
	if (bMoveBoyToRight == true)
	{
		if (human_translate_x < 2)
		{
			BoyWalk(false, true, false, false);
		}
		else
		{
			seen_number = 11;
		}
	}
}

void moveCameraUpAndDown()
{
	static bool camera_up = true, camera_down = false;
	if (camera_up == true)
	{
		if (angle_for_camera_y < 10)
		{
			translate_camera_z = translate_camera_z - 0.01f;
			angle_for_camera_y = angle_for_camera_y + 0.008f;
			translate_camera_y = translate_camera_y + 0.008;
		}
		else
		{
			camera_up = false;
			camera_down = true;
		}
	}
	if (camera_down == true)
	{
		if (angle_for_camera_y > 0)
		{
			translate_camera_z = translate_camera_z + 0.01f;
			angle_for_camera_y = angle_for_camera_y - 0.008f;
			translate_camera_y = translate_camera_y - 0.008;
		}
		else
		{
			seen_number = 9;
		}
	}
}

void BoySitDown()
{
	// VARIABLE DECLARATION
	bool bSitDown = false, move_hand_up = false;

	// CODE
	if (hrh_move_from_sholder_left_hand_right_leg > 250.0f)
	{
		hrh_move_from_sholder_left_hand_right_leg = hrh_move_from_sholder_left_hand_right_leg - 0.1f;
		hrh_move_from_sholder_right_hand_left_leg = hrh_move_from_sholder_right_hand_left_leg - 0.1;
		if (calf_angle < 90.0f)
		{
			calf_angle = calf_angle + 0.1;
		}
		human_main_body_angle = human_main_body_angle + 0.03f;
		if (human_head_rotate_x < 20.0f)
		{
			human_head_rotate_x = human_head_rotate_x + 0.05f;
		}
	}
	else
	{
		bSitDown = true;
	}
	

	if (bSitDown == true)
	{
		if (hrh_move_left_hands > 280.0f)
		{
			hrh_move_left_hands = hrh_move_left_hands - 0.5f;
			hrh_move_right_hands = hrh_move_right_hands - 0.5f;
		} else	if (hrh_move_right_elbow > 240)
		{
			hrh_move_right_elbow = hrh_move_right_elbow - 0.1f;
			hrh_move_left_elbow = hrh_move_left_elbow - 0.1f;

			if (hrh_move_left_elbow_x < 400.0f)
			{
				hrh_move_left_elbow_x = hrh_move_left_elbow_x + 0.5f;
				hrh_move_right_elbow_x = hrh_move_right_elbow_x + 0.5f;
			}
			human_translate_z = human_translate_z - 0.001;
		}
		else
		{
			seen_number = 8;
		}
	}
}

void BoySitUp()
{
	// VARIABLE DECLARATION
	static bool bBoyUp = false;
	if (hrh_move_right_elbow < 360)
	{
		hrh_move_right_elbow = hrh_move_right_elbow + 0.1f;
		hrh_move_left_elbow = hrh_move_left_elbow + 0.1f;

		if (hrh_move_left_elbow_x > 360.0f)
		{
			hrh_move_left_elbow_x = hrh_move_left_elbow_x - 0.5f;
			hrh_move_right_elbow_x = hrh_move_right_elbow_x - 0.5f;
		}
		human_translate_z = human_translate_z + 0.001;
	}
	else
	{
		bBoyUp = true;
		emoji_state = HAPPY;
	}

	if (bBoyUp == true)
	{
		if (hrh_move_from_sholder_left_hand_right_leg < 360.0f)
		{
			hrh_move_from_sholder_left_hand_right_leg = hrh_move_from_sholder_left_hand_right_leg + 0.1f;
			hrh_move_from_sholder_right_hand_left_leg = hrh_move_from_sholder_right_hand_left_leg + 0.1;
			if (calf_angle > 0.0f)
			{
				calf_angle = calf_angle - 0.1;
			}
			human_main_body_angle = human_main_body_angle - 0.03f;
		}
		else {
			seen_number = 10;
		}

		if (hrh_move_left_hands < 360.0f)
		{
			hrh_move_left_hands = hrh_move_left_hands + 0.5f;
			hrh_move_right_hands = hrh_move_right_hands + 0.5f;
		}
		if (human_head_rotate_x > 0.0f)
		{
			human_head_rotate_x = human_head_rotate_x - 0.05f;					
		}
		if (translate_eye_y < 0.0f)
		{
			translate_eye_y = translate_eye_y + 0.0002;
		}
	}
}

void Seen6()
{
	// FUNCTION DECLARATION
	void BoyWalk(bool, bool, bool, bool);

	// VARIABLE DECLARATION
	static bool bOutFromHouse = false, bTurnRight = false, bGoRight = false, bTurnLeft = false, bMoveBoyToLeft = true, bFirstRotateBoyToLeft = false;

	// CODE
	if (bMoveBoyToLeft == true)
	{
		if (human_translate_x > -6.0f)
		{
			BoyWalk(true, false, false, false);
		}
		else
		{
			bFirstRotateBoyToLeft = true;
			bMoveBoyToLeft = false;
		}
	}

	if (bFirstRotateBoyToLeft == true)
	{
		if (human_angle_y < 360)
		{
			human_angle_y = human_angle_y + 1.0f;
			angle_for_bird_y = angle_for_bird_y + 1.0f;
			translate_bird_x = human_translate_x;
			translate_bird_z = human_translate_z + 1.5f;
		}
		else
		{
			bOutFromHouse = true;
			bFirstRotateBoyToLeft = false;
		}
	}

	if (bOutFromHouse == true)
	{
		if (human_translate_z < 19)
		{
			BoyWalk(false, false, true, false);
		}
		else
		{
			bTurnRight = true;
			bOutFromHouse = false;
		}
	}

	if (bTurnRight == true)
	{
		if (human_angle_y < 360 + 90.0f)
		{
			human_angle_y = human_angle_y + 1.0f;
		}
		else
		{
			bGoRight = true;
			bTurnRight = false;
		}
	}

	if (bGoRight == true)
	{
		if (human_translate_x < 9)
		{
			BoyWalk(false, true, false, false);
		}
		else
		{
			bTurnLeft = true;
			bGoRight = false;
		}		
	}

	if (bTurnLeft == true)
	{
		if (human_angle_y > 360.0f)
		{
			human_angle_y = human_angle_y - 1.0f;
		}
		else
		{
			seen_number = 7;
		}
	}
}


void Seen3()
{
	// FUNCTION DECLARATION
	void BoyWalk(bool, bool, bool, bool);

	// VARIABLE DECLARATION
	static bool move_hand_up = false, place_bird = false, move_hand_down = false, boy_enter = true, boy_exit = false;

	// CODE
	if (boy_enter == true)
	{
		if (human_translate_z > 22.0f)
		{
			BoyWalk(false, false, false, true);
			translate_bird_z = translate_bird_z - 0.007;
		}
		else
		{
			if (boy_enter == true)
			{
				boy_enter = false;
				open_cage_door = true;
				hrh_move_from_sholder_left_hand_right_leg = 0.0f;
				hrh_move_from_sholder_right_hand_left_leg = 0.0f;
			}
			
		}
	}
	if (human_translate_z <= 22.0f)
	{
		if (human_angle_y > 90.0f)
		{
			human_angle_y = human_angle_y - 0.00003f;
			angle_for_bird_y = angle_for_bird_y - 1.0f;
			translate_bird_x = human_translate_x + 1;
		}
		else
		{
			if (move_hand_up == false && move_hand_down == false)
			{
				move_hand_up = true;
				translate_bird_x = human_translate_x + 1.3f;
			}
			
		}
	}

	if (move_hand_up == true)
	{	
		if (hrh_move_left_hands > 240.0f) 
		{
			if (hrh_move_left_hands > 300.0f)
			{
				hrh_move_right_elbow = hrh_move_right_elbow + 0.15f;
				hrh_move_left_elbow = hrh_move_left_elbow + 0.15f;

				translate_bird_x = translate_bird_x + 0.001f;
				hrh_move_left_hands = hrh_move_left_hands - 0.1;
				hrh_move_right_hands = hrh_move_right_hands - 0.1f;
			}
			else
			{
				hrh_move_left_hands = hrh_move_left_hands - 0.1;
				hrh_move_right_hands = hrh_move_right_hands - 0.1f;
				translate_bird_y = translate_bird_y + 0.0026;
			}			
		}
		else
		{
			place_bird = true;
			move_hand_down = true;
			move_hand_up = false;
			open_cage_door = false;
			close_cage_door = true;
			
		}
	}
	
	if (place_bird == true)
	{
		angle_for_bird_y = 320.0f;
		translate_bird_x = 3.2f;
		translate_bird_y = 2.1f;
		translate_bird_z = 18.0f;
	}

	if (move_hand_down == true)
	{
		if (hrh_move_left_hands <= 360)
		{
			if (hrh_move_left_hands <= 300.0f)
			{
				hrh_move_right_elbow = hrh_move_right_elbow - 0.03f;
				hrh_move_left_elbow = hrh_move_left_elbow - 0.03f;

				hrh_move_left_hands = hrh_move_left_hands + 0.1;
				hrh_move_right_hands = hrh_move_right_hands + 0.1f;
			}
			else
			{
				hrh_move_left_hands = hrh_move_left_hands + 0.1;
				hrh_move_right_hands = hrh_move_right_hands + 0.1f;
			}
		}
		else
		{
			boy_exit = true;
		}
	}

	if (boy_exit == true)
	{
		
		if (human_angle_y > 0)
		{
			human_angle_y = human_angle_y - 0.00003f;
		}
		else
		{
			if (human_translate_z < 40) 
			{
				BoyWalk(false, false, true, false);
			}
			else
			{
				seen_number = 4;
			}
		}		
	}
	
}

void Seen4()
{
	// FUNCTION DECLARATION
	void BoyWalk(bool, bool, bool, bool);

	// VARIABLE DECLARATION
	static bool move_hand_up = false, place_bowl = false, move_hand_down = false, boy_enter = true, boy_exit = false;

	// CODE
	if (boy_enter == true)
	{
		if (human_translate_z > 22.0f)
		{
			BoyWalk(false, false, false, true);
			translate_bowl_z = translate_bowl_z - 0.007;
		}
		else
		{
			if (boy_enter == true)
			{
				boy_enter = false;
				open_cage_door = true;
				hrh_move_from_sholder_left_hand_right_leg = 0.0f;
				hrh_move_from_sholder_right_hand_left_leg = 0.0f;
			}

		}
	}

	if (human_translate_z <= 22.0f)
	{		
		if (human_angle_y > 90)
		{
			human_angle_y = human_angle_y - 0.00003f;		
		}
		else
		{
			if (move_hand_up == false && move_hand_down == false)
			{
				move_hand_up = true;
				translate_bowl_x = 0.7f;
			}
		}
	}

	if (move_hand_up == true)
	{
		if (hrh_move_left_hands > 240.0f)
		{
			if (hrh_move_left_hands > 300.0f)
			{
				hrh_move_right_elbow = hrh_move_right_elbow + 0.15f;
				hrh_move_left_elbow = hrh_move_left_elbow + 0.15f;
				translate_bowl_x = translate_bowl_x + 0.001f;
				hrh_move_left_hands = hrh_move_left_hands - 0.1;
				hrh_move_right_hands = hrh_move_right_hands - 0.1f;
			}
			else
			{
				hrh_move_left_hands = hrh_move_left_hands - 0.1;
				hrh_move_right_hands = hrh_move_right_hands - 0.1f;
				translate_bowl_y = translate_bowl_y + 0.0035;
			}
		}
		else
		{
			place_bowl = true;
			move_hand_down = true;
			move_hand_up = false;
			open_cage_door = false;
			close_cage_door = true;

		}
	}

	if (place_bowl == true)
	{
		translate_bowl_x = 2.0f;
		translate_bowl_y = 0.4f;
		translate_bowl_z = 20.0f;
	}

	if (move_hand_down == true)
	{
		if (hrh_move_left_hands <= 360)
		{
			if (hrh_move_left_hands <= 300.0f)
			{
				hrh_move_right_elbow = hrh_move_right_elbow - 0.03f;
				hrh_move_left_elbow = hrh_move_left_elbow - 0.03f;
				hrh_move_left_hands = hrh_move_left_hands + 0.1;
				hrh_move_right_hands = hrh_move_right_hands + 0.1f;
			}
			else
			{
				hrh_move_left_hands = hrh_move_left_hands + 0.1;
				hrh_move_right_hands = hrh_move_right_hands + 0.1f;
			}
		}
		else
		{
			boy_exit = true;			
		}
	}

	if (boy_exit == true)
	{		
		if (human_angle_y > 0)
		{
			human_angle_y = human_angle_y - 0.00003f;
		}
		else
		{
			if (human_translate_z < 40) 
			{
				BoyWalk(false, false, true, false);
			}
			else
			{
				seen_number = 5;
				human_angle_y = 180.0f;
				stop_bird_chippering = false;
			}
		}
	}

}

void Seen5()
{
	// FUNCTION DECLARATION
	void BoyWalk(bool, bool, bool, bool);

	// VARIABLE DECLARATION
	static bool move_hand_up = false, move_bird_neck = false, move_hand_down = false, boy_enter = true, boy_exit = false, bBirdHeadDown = true, bBirdHeadUp = true;
	static int count_for_wait = 0;

	if (bBirdHeadDown == true)
	{
		if (bird_head_angle_x < 400.0f) // move bird head down
		{
			bird_head_angle_x = bird_head_angle_x + 0.08f;
		}
		else
		{
			bBirdHeadDown = false;
		}
	}

	// CODE
	if (boy_enter == true)
	{
		if (human_translate_z > 22.0f) 
		{
			BoyWalk(false, false, false, true);
		}
		else
		{
			if (boy_enter == true)
			{
				boy_enter = false;
				hrh_move_from_sholder_left_hand_right_leg = 0.0f;
				hrh_move_from_sholder_right_hand_left_leg = 0.0f;
			}
		}
		if (human_translate_z < 28)
		{
			if (bird_head_angle_x > 360.0f) // Move Head Up
			{
				bird_head_angle_x = bird_head_angle_x - 0.08f;
			}
		}
	}

	if (human_translate_z <= 22.0f) 
	{
		if (human_angle_y > 90)
		{
			human_angle_y = human_angle_y - 0.00003f;
		}
		else
		{
			move_bird_neck = true;			
		}
	}

	if (move_bird_neck == true)
	{
		if (bird_head_angle_y < 90)
		{
			bird_head_angle_y = bird_head_angle_y + 0.1f;
			bird_head_angle_x = bird_head_angle_x + 0.03f;
		}
		else
		{
			count_for_wait = count_for_wait + 1;
			emoji_state = SAD;
		}
	}

	if (count_for_wait == 180)
	{
		boy_exit = true;
	}

	if (boy_exit == true)
	{
		if (human_angle_y > 0)
		{
			human_angle_y = human_angle_y - 0.00003f;
		}
		else
		{
			if (human_translate_z < 38) 
			{
				BoyWalk(false, false, true, false);
				if (human_head_rotate_x < 15.0f)
				{
					human_head_rotate_x = human_head_rotate_x + 0.05f;
					translate_eye_y = translate_eye_y - 0.0002;
				}
			}
			else
			{
				seen_number = 6;
			}
		}
	}
}

void Seen1()
{
	//FUNCTION DECLARATION
	void BoyWalk();

	//CODE
	if (hrh_angle > 360)
	{
		hrh_angle = 0.0f;
	}
	hrh_angle = hrh_angle + 0.2f;

	if (angle_for_sun_y < 25)
	{
		angle_for_sun_y = angle_for_sun_y + 0.02;
		background_green = background_green + 0.0004f;
		background_blue = background_blue + 0.0004f;
	}
	else
	{
		seen_number = 2;
	}
}

void Seen2()
{
	// FUNCTION DECLARATION
	void BoyWalk(bool, bool, bool, bool);
		
	// VARIABLE DECLARATION
	static bool setBird_z_position = false;

	// CODE
	if (move_camera_1 == true && angle_for_camera_x < 90)
	{
		translate_camera_x = translate_camera_x - 0.02f;
		angle_for_camera_x = angle_for_camera_x + 0.05;	
		if (angle_for_camera_x > 0)
		{
			boy_walk_1 = true;
		}
	}

	if (boy_walk_1 == true)
	{
		if (human_translate_x > -7.0f)
		{
			BoyWalk(true, false, false, false);
			translate_bird_x = translate_bird_x - 0.007f;
		}
		else
		{
			rotate_human = true;
			boy_walk_1 = false;
		}
	}

	
	if (true == rotate_human)
	{
		if (human_angle_y > 180)
		{
			human_angle_y = human_angle_y - 0.00003f;
			angle_for_bird_y = angle_for_bird_y - 1.0f;
		}
		else
		{
			rotate_human = false;
			move_camera_1 = false;
			move_camera_2 = true;
			boy_walk_2 = true;
		}
	}

	if (move_camera_2 == true && translate_camera_z > 8)
	{
		if (human_translate_z < 15 ) 
		{
			if (translate_camera_x < -8) 
			{
				translate_camera_x = translate_camera_x + 0.12;	
			}
			if (angle_for_camera_x > 0) 
			{
				angle_for_camera_x = angle_for_camera_x - 0.35f;
			}
			if (human_translate_z > 10)
			{
				translate_camera_z = translate_camera_z - 0.007;
			}			
		}
		else
		{
			translate_camera_z = translate_camera_z - 0.007;
		}
		
	}

	if (boy_walk_2 == true)
	{
		if (human_translate_z > 3) 
		{
			BoyWalk(false, false, false, true);
			if (setBird_z_position == false)
			{
				translate_bird_z = human_translate_z - 1;
				translate_bird_x = translate_bird_x + 1;
				setBird_z_position = true;
			}
			translate_bird_z = translate_bird_z - 0.007;
		}
		else
		{
			seen_number = 3;
		}
		if (human_translate_z < 15)
		{
			if (open_left_door_angle < 230)
			{

				open_left_door_translate_z = open_left_door_translate_z + 0.0013;
				open_left_door_angle = open_left_door_angle + 0.04f;
				open_left_door_translate_x = open_left_door_translate_x + 0.003;

			}

			if (open_right_door_angle > 305)
			{
				open_right_door_angle = open_right_door_angle - 0.04f;
				open_right_door_translate_x = open_right_door_translate_x + 0.0015f;
				open_right_door_translate_z = open_right_door_translate_z + 0.001;
			}
		}
	}
}

void copyInitialStateForSeen2()
{
	translate_bird_x = human_translate_x -1;
	translate_bird_y = -1.6;
	translate_bird_z = human_translate_z;
	seen2_state.isCopiedIntialState = true;
}

void copyInitialStateForSeen3()
{
	human_translate_z = 35; 
	human_translate_x = -1.3f;
	translate_bird_x = human_translate_x;
	translate_camera_z = 38.0f; 
	translate_bird_y = -1.4f; 
	translate_bird_z = human_translate_z;
	angle_for_camera_x = -5.0f;
	translate_camera_x = 6.0f;
	human_angle_y = 180;
	angle_for_bird_y = 180.0f;
	hrh_move_left_elbow = 270.0f;
	hrh_move_right_elbow = 270.0f;
	hrh_move_left_hands = 360.0f;
	hrh_move_right_hands = 360.0f;
	bird_head_angle_x = 360.0f;
	seen3_state.isCopiedIntialState = true;
	angle_for_sun_y = 35.0f;
}

void copyInitialStateForSeen4()
{
	human_translate_z = 35; 
	human_angle_y = 180;
	human_translate_x = -1.3f;
	translate_bowl_x = human_translate_x;
	translate_bowl_y = -1.8f;
	translate_bowl_z = human_translate_z - 1.0f;
	hrh_move_left_elbow = 270.0f;
	hrh_move_right_elbow = 270.0f;
	seen4_state.isCopiedIntialState = true;
}

void copyInitialStateForSeen6()
{
	translate_camera_x = 10.0f; 
	translate_camera_z = 40.0f;
	human_translate_z = -3.0f;
	human_translate_x = -3.0f;
	human_angle_y = 270.0f;
	hrh_move_left_elbow = 0.0f;
	hrh_move_right_elbow = 0.0f;
	emoji_state = SAD;
	hrh_move_left_hands = 360.0f;
	hrh_move_right_hands = 360.0f;
	human_angle_y = 270.0F;
	bird_chippering = true;
	stop_bird_chippering = true;
	seen6_state.isCopiedIntialState = true;
}

void copyInitialStateForSeen7()
{
	translate_camera_x = 10.0f; 
	translate_camera_z = 40.0f;
	human_translate_z = 19.0f;
	human_translate_x = 9.0f;
	human_angle_y = 330.0f; 
	hrh_move_left_elbow = 360.0f;
	hrh_move_right_elbow = 360.0f;
	hrh_move_left_elbow_x = 360.f;
	hrh_move_right_elbow_x = 360.0f;
	emoji_state = SAD;
	hrh_move_left_hands = 360.0f;
	hrh_move_right_hands = 360.0f;
	bird_chippering = true;
	stop_bird_chippering = true;
	hrh_move_from_sholder_left_hand_right_leg = 360.0f;
	hrh_move_from_sholder_right_hand_left_leg = 360.0f;
	seen7_state.isCopiedIntialState = true;
}

void copyInitialStateForSeen10()
{
	translate_camera_x = 10.0f; 
	translate_camera_z = 40.0f;
	human_translate_z = 19.0f;
	human_translate_x = 9.0f;
	human_angle_y = 330.0f;
	hrh_move_left_elbow_x = 360.f;
	hrh_move_right_elbow_x = 360.0f;
	emoji_state = HAPPY;
	move_hand = 360;
	hrh_move_left_elbow = 0.0f;
	hrh_move_right_elbow = 0.0f;
	bird_chippering = true;
	stop_bird_chippering = true;
	hrh_move_from_sholder_left_hand_right_leg = 0.0f;
	hrh_move_from_sholder_right_hand_left_leg = 0.0f;
	seen10_state.isCopiedIntialState = true;
}

void copyInitialStateForSeen11()
{
	translate_camera_x = 10.0f;
	translate_camera_z = 40.0f;
	human_translate_z = -3.0f; 
	human_translate_x = -2.0f; 
	human_angle_y = 270.0f;
	hrh_move_left_elbow = 270.0f; 
	hrh_move_right_elbow = 270.0f; 
	hrh_move_right_elbow_y = 270.0f;
	hrh_move_left_elbow_y = 270.0f;
	hrh_move_left_elbow_x = 360.0f; 
	hrh_move_right_elbow_x = 360.0f; 
	emoji_state = HAPPY;
	bird_chippering = true;
	stop_bird_chippering = true;
	translate_bird_x = human_translate_x - 1;
	translate_bird_y = -1.6;
	translate_bird_z = human_translate_z;
	angle_for_bird_y = 270.0f;
	bird_head_angle_x = 0.0f;
	bird_head_angle_y = 0.0f;
	seen11_state.isCopiedIntialState = true;
}

void copyInitialStateForSeen12()
{
	hrh_move_left_elbow_x = 360.0f;
	hrh_move_right_elbow_x = 360.0f;
	bird_chippering = true;
	stop_bird_chippering = true;
	angle_for_bird_x = 360.0f;
	bird_head_angle_x = 360.0f;
	seen12_state.isCopiedIntialState = true;
}

void BoyWalk(bool left, bool right, bool z_out, bool z_in)
{
	GLfloat boyWalkSpeed = 0.007, boyLegSpeed = 0.1f; 
	if (bIn)
	{
		hrh_move_from_sholder_left_hand_right_leg = (hrh_move_from_sholder_left_hand_right_leg + boyLegSpeed); 
		hrh_move_from_sholder_right_hand_left_leg = (hrh_move_from_sholder_right_hand_left_leg - boyLegSpeed); 
		if (left)
		{
			human_translate_x = human_translate_x - boyWalkSpeed;  
		}
		else if (right)
		{
			human_translate_x = human_translate_x + boyWalkSpeed;
		} else if (z_in)
		{
			human_translate_z = human_translate_z - boyWalkSpeed; 
		}
		else if (z_out)
		{
			human_translate_z = human_translate_z + boyWalkSpeed; 
		}
		if (hrh_move_from_sholder_left_hand_right_leg >21.0f)
		{
			bIn = false;
			bOut = true;
		}
	}
	else if (bOut)
	{
		hrh_move_from_sholder_left_hand_right_leg = (hrh_move_from_sholder_left_hand_right_leg - boyLegSpeed);
		hrh_move_from_sholder_right_hand_left_leg = (hrh_move_from_sholder_right_hand_left_leg + boyLegSpeed); 
		if (left)
		{
			human_translate_x = human_translate_x - boyWalkSpeed; 
		}
		else if (right)
		{
			human_translate_x = human_translate_x + boyWalkSpeed; 
		} else if (z_in)
		{
			human_translate_z = human_translate_z - boyWalkSpeed; 
		}
		else if (z_out)
		{
			human_translate_z = human_translate_z + boyWalkSpeed; 
		}
		if (hrh_move_from_sholder_left_hand_right_leg < -21.0f)
		{
			bOut = false;
			bIn = true;
		}
	}
	
}


void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
	}

	DeleteObject(hFont);
	DeleteObject(hFontWithUnderLine);

	if (stone_wall_texture)
	{
		glDeleteTextures(1, &stone_wall_texture);
		stone_wall_texture = 0;
	}

	if (roof_texture)
	{
		glDeleteTextures(1,&roof_texture);
		roof_texture = 0;
	}

	if (grass_texture)
	{
		glDeleteTextures(1, &grass_texture);
		grass_texture = 0;
	}

	if (black_texture)
	{
		glDeleteTextures(1, &black_texture);
		black_texture = 0;
	}

	if (left_door_texture)
	{
		glDeleteTextures(1, &left_door_texture);
		left_door_texture = 0;
	}

	if (right_door_texture)
	{
		glDeleteTextures(1, &right_door_texture);
		right_door_texture = 0;
	}

	if (window_texture)
	{
		glDeleteTextures(1, &window_texture);
		window_texture = 0;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

}
