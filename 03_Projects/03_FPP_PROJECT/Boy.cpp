#include "FFPproject.h"

extern GLfloat hrh_move_human_x, hrh_move_human_y , hrh_move_human_z , bird_beak_up , bird_beak_down;
extern GLuint boy_face;
extern GLUquadric*  quadric_for_human;
extern GLfloat hrh_radiusForBodyParts;
extern GLfloat hrh_move_elbow , hrh_sholder , hrh_angle;
extern GLfloat hrh_move_from_sholder_left_hand_right_leg, hrh_move_from_sholder_right_hand_left_leg;
extern GLfloat hrh_move_left_hands, hrh_move_right_hands;
extern GLfloat human_translate_x, human_translate_y, human_translate_z;
extern int human_angle_x, human_angle_y, human_angle_z;
extern GLfloat hrh_move_left_hands_y, hrh_move_right_hands_y;
extern GLfloat human_main_body_angle;
extern int human_scale = 2.0f;
extern GLfloat human_head_rotate_x;
extern GLfloat hrh_move_left_elbow_x, hrh_move_right_elbow_x, translate_eye_y;
extern GLint emoji_state;
extern GLfloat hrh_move_right_elbow_y, hrh_move_left_elbow_y;
extern GLfloat hrh_move_left_elbow, hrh_move_right_elbow;

extern GLfloat thigh_angle , calf_angle ;


void HumanBody()
{
	// FUNCTION DECLARATION
	void MainHumanBody();
	void HumanHand(GLfloat, GLfloat, GLfloat,GLfloat, GLfloat, GLfloat);
	void HumanHead();
	void HumanLeg(GLfloat, GLfloat);

	glLineWidth(1.0f);
	glPushMatrix();				// push original changes before human body
	glTranslatef(human_translate_x, human_translate_y, human_translate_z);
	glScalef(human_scale, human_scale, human_scale);
	glRotatef(human_angle_x, 1.0f, 0.0f, 0.0f);
	glRotatef(human_angle_y, 0.0f, 1.0f, 0.0f);
	glRotatef(human_angle_z, 0.0f, 0.0f, 1.0f);
	glPushMatrix();
	MainHumanBody();
	glPushMatrix();
	HumanHead();
	glPopMatrix();
	glPushMatrix();
	//hrh_move_right_elbow_y
	HumanHand(0.65, hrh_move_left_hands, -hrh_move_left_elbow_x, hrh_move_left_hands_y, hrh_move_left_elbow_y, hrh_move_left_elbow);
	HumanHand(-0.65, hrh_move_right_hands, hrh_move_right_elbow_x, hrh_move_right_hands_y, hrh_move_right_elbow_y, hrh_move_right_elbow);
	glPopMatrix();
	HumanLeg(-0.25, hrh_move_from_sholder_left_hand_right_leg);
	HumanLeg(0.25, hrh_move_from_sholder_right_hand_left_leg);

	glPopMatrix();
	glPopMatrix();				// pop changes before human body
}

void HumanHead()
{
	//FUNCTION DECLARATION
	void HumanEye();
	void Spect();
	glColor3f(0.0f, 0.0f, 0.0f);
	glPushMatrix();		// changes before HumanHead

	glTranslatef(0.0f, 1.2f, 0.0f);
	glRotatef(human_head_rotate_x, 1.0f, 0.0f, 0.0f);

	glPushMatrix();
	glRotatef(270, 1.0f, 0.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	gluQuadricTexture(quadric_for_human, GL_TRUE);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, boy_face);
	gluSphere(quadric_for_human, 0.5f, 20, 20); // HEAD
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
	glPushMatrix();
	glColor3f(0.0f, 0.0f, 0.0f);
	glTranslatef(-0.15f, translate_eye_y, 0.5f);
	HumanEye();
	glPopMatrix();
	glPushMatrix();
	// RIGHT EYE
	glTranslatef(0.15f, translate_eye_y, 0.5f);
	HumanEye();

	glPopMatrix();
	Spect();

	glPopMatrix();

}

void HumanEye()
{
	GLfloat radius = 0.03f, langitude = 10.0f, angle = 0.0f;
	glBegin(GL_TRIANGLES);
	for (int i = 0; i < 360; i = i + (360.0f / langitude))
	{
		angle = i * RADIAN_VALUE;
		glVertex3f(0.0f, radius, 0.0f);
		glVertex3f(radius * cos(angle), radius * sin(angle), 0.0f);
		angle = (i + (360.0f / langitude)) * RADIAN_VALUE;
		glVertex3f(radius * cos(angle), radius * sin(angle), 0.0f);

	}
	glEnd();
}

void Spect()
{
	// FUNCTION  DECLARATION
	void OneSideSpect();
	void emoji(int fromAngle, int toAngle, GLfloat radius, int langitude, GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height);

	glLineWidth(1.0f);

	glPushMatrix();
	glTranslatef(0.15f, -0.013f, 0.5f);

	glPushMatrix();
	// left side spect
	glTranslatef(-0.3f, 0.0f, 0.0f);
	OneSideSpect();
	//right spect
	glTranslatef(0.30f, 0.0f, 0.0f);
	OneSideSpect();
	// line to connect circle
	glTranslatef(-0.15f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(0.07f, 0.0f, 0.0f);
	glVertex3f(-0.07f, 0.0f, 0.0f);
	glEnd();

	glTranslatef(0.0f, -0.25f, 0.0f);

	if (emoji_state == HAPPY)
	{
		glLineWidth(2.0f);
		emoji(90, 270, 0.06f, 15, 0.0f, 0.0f, -0.1f, 0.06f, 0.02f);
	}
	else if (emoji_state == SAD)
	{
		glLineWidth(2.0f);
		emoji(270, 360 + 90, 0.06f, 15, 0.0f, -0.09f, -0.07f, 0.06f, 0.02f);
	}
	else if (emoji_state == VERY_HAPPY)
	{
		glLineWidth(1.0f);
		emoji(90, 270, 0.09f, 15, 0.0f, 0.0f, -0.1f, 0.09f, 0.05f);
		glBegin(GL_LINES);
		glVertex3f(0.14f, 0.0f, 0.0f);
		glVertex3f(-0.22f, 0.0f, 0.0f);
		glEnd();
	}
	glLineWidth(1.0f);
	glPopMatrix();
	glPopMatrix();
}

void OneSideSpect()
{
	GLfloat radius = 0.1f, langitude = 15.0f, angle = 0.0f;
	glBegin(GL_LINES);
	for (int i = 0; i < 360; i = i + (360.0f / langitude))
	{
		angle = i * RADIAN_VALUE;
		glVertex3f(radius * cos(angle), radius * sin(angle), 0.0f);
		angle = (i + (360.0f / langitude)) * RADIAN_VALUE;
		glVertex3f(radius * cos(angle), radius * sin(angle), 0.0f);

	}
	glEnd();
}

void MainHumanBody()
{
	//FUNCTION DECLARATION
	void CylinderVerticle(GLfloat radiusForUpCircle, GLfloat radiusForDownCircle, GLfloat height, int langitude, int fromAngle, int toAngle,
		GLfloat x, GLfloat y, GLfloat z);

	//CODE	
	glPopMatrix();
	glPushMatrix();
	glTranslatef(0.0, 0.3f, 0.0f);
	glRotatef(human_main_body_angle, 1.0f, 0.0f, 0.0f);
	glColor3f(0.6, 0.0f, 0.0f);
	gluSphere(quadric_for_human, 0.5f, 20, 20);
	glTranslatef(0.0f, -0.3f, 0.0f);
	CylinderVerticle(0.5f, 0.5f, 0.5f, 10, 0, 360, 0.0f, 0.0f, 0.0f);
	glPushMatrix();
	glPopMatrix();
}


void HumanHand(GLfloat translate_x, GLfloat move_hand, GLfloat hrh_move_elbow_z, GLfloat move_hand_y, GLfloat hrh_move_elbow_y, GLfloat hrh_move_elbow_x)
{
	//FUNCTION DECLARATION
	void CylinderVerticle(GLfloat radiusForUpCircle, GLfloat radiusForDownCircle, GLfloat height, int langitude, int fromAngle, int toAngle,
		GLfloat x, GLfloat y, GLfloat z);

	//VARIABLE DECLARATIO
	GLfloat translate_z = 0.0f;

	//CODE
	// up shphere
	glPushMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);
	glTranslatef(translate_x, 0.35f, translate_z);
	gluSphere(quadric_for_human, 0.15, 20, 20);

	//sholder
	glRotatef(move_hand, 1.0f, 0.0f, 0.0f);
	glRotatef(move_hand_y, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.0f, -0.25f, translate_z);
	CylinderVerticle(hrh_radiusForBodyParts, hrh_radiusForBodyParts, 0.5f, 10, 0, 360, 0.0f, 0.0f, 0.0f);
	// sphere

	glTranslatef(0.0, -0.3f, translate_z);
	gluSphere(quadric_for_human, 0.15, 20, 20);

	glRotatef(hrh_move_elbow_x, 1.0f, 0.0f, 0.0f);
	glRotatef(hrh_move_elbow_y, 0.0f, 1.0f, 0.0f);
	glRotatef(hrh_move_elbow_z, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.0f, -0.20f, 0.0f);
	CylinderVerticle(hrh_radiusForBodyParts, hrh_radiusForBodyParts, 0.5f, 10, 0, 360, 0.0f, 0.0f, 0.0f);	 

	// palm
	glTranslatef(0.0f, -0.4f, translate_z);
	gluSphere(quadric_for_human, 0.15, 20, 20);
	glPopMatrix();
}

void HumanLeg(GLfloat translate_x, GLfloat move_leg)
{
	//FUNCTION DECLARATION
	void CylinderVerticle(GLfloat radiusForUpCircle, GLfloat radiusForDownCircle, GLfloat height, int langitude, int fromAngle, int toAngle,
		GLfloat x, GLfloat y, GLfloat z);

	//VARIABLE DECLARATION
	GLfloat translate_z = 0.0f;

	glColor3f(0.0f, 0.1f, 0.4f);

	glPushMatrix();			// push previous changes  

	glTranslatef(translate_x, -0.35f, translate_z);
	gluSphere(quadric_for_human, 0.15, 20, 20);

	glRotatef(move_leg, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.40f, translate_z);
	CylinderVerticle(hrh_radiusForBodyParts, hrh_radiusForBodyParts, 0.8f, 10, 0, 360, 0.0f, 0.0f, 0.0f);

	glTranslatef(0.0f, -0.4f, translate_z);
	gluSphere(quadric_for_human, 0.15, 20, 20);

	glRotatef(calf_angle, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.35f, translate_z);
	CylinderVerticle(hrh_radiusForBodyParts, hrh_radiusForBodyParts, 0.8f, 10, 0, 360, 0.0f, 0.0f, 0.0f);

	glTranslatef(0.0f, -0.4f, 0.0f);
	gluSphere(quadric_for_human, 0.15, 20, 20);

	glPopMatrix();

}
