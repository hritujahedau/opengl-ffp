#include "FFPproject.h"

GLfloat width = 7.0f, height = 4.0f, depth = 3.0f;
GLfloat terrenUpHeight = height + height + 1.0f, terrenUpDepth = 0.0f,
 terrenDownHeight = height - 1.5f, terrenDownDepth = depth + 1.0f,
 terrenWidth = width + 1.0f;
GLfloat grass_texture_coord = 6.0f;
GLfloat outer_area_width = width * grass_texture_coord, outer_area_height = height, outer_area_depth = outer_area_width;
GLfloat doorWidthFrom = -1.0f, doorWidthTo = doorWidthFrom - 4.0f, doorHeightFrom = -4.0f, doorHeightTo = doorHeightFrom + 6.0f, doorDepth = 6.0;
GLfloat windowWidthFrom = 3.0f, windowWidthTo = windowWidthFrom - 2.0f, windowHeightFrom = -1.5f, windowHeightTo = windowHeightFrom + 2.0f, WindowDepth = depth;

extern GLfloat hrh_angle;
extern GLuint stone_wall_texture, roof_texture , grass_texture, black_texture, left_door_texture, right_door_texture, window_texture, warali_texture, tree_texture, marble;
extern GLuint tree_base_texture;
extern GLUquadric* quadric_for_tree, *quadric_for_sun;
extern float angle_for_sun_x, angle_for_sun_y, angle_for_sun_z;
extern GLfloat sun_colo_r, sun_color_g, sun_color_b;
extern GLfloat open_left_door_angle , open_left_door_translate_x ,
open_right_door_angle , open_right_door_translate_x, open_right_door_translate_z, open_left_door_translate_z;


RECTANGLE house_h[] = {
	// WALL
	{ {width, -height, -depth}, {-width, -height, -depth}, {-width, height, -depth}, {width, height,-depth} }, // BACK
	{ {width, height, -depth} , { width, height, depth}, {width, -height, depth}, {width, -height, -depth} },    // RIGHT
	{ {-width, height, depth}, {-width, height, -depth}, {-width, -height, -depth}, {-width, -height, depth} }, // LEFT

	// TRIANGLE WALL
	{ {width, terrenUpHeight, terrenUpDepth}, {width, terrenUpHeight, terrenUpDepth}, {width, height, depth} , { width, height, -depth} }, // RIGHT UPPER
	{ {-width, height, depth}, {-width, terrenUpHeight, -terrenUpDepth}, {-width, height, -depth}, {-width, height, -depth} }, // LEFT UPPER

	// TEREN
	{ {terrenWidth , terrenDownHeight , terrenDownDepth}, {-terrenWidth, terrenDownHeight, terrenDownDepth}, {-terrenWidth, terrenUpHeight, terrenUpDepth}, {terrenWidth, terrenUpHeight, terrenUpDepth} },
	{ {terrenWidth, terrenDownHeight, -terrenDownDepth}, {-terrenWidth, terrenDownHeight, -terrenDownDepth}, {-terrenWidth, terrenUpHeight, terrenUpDepth}, {terrenWidth, terrenUpHeight, terrenUpDepth} },
	
	//HOUSE GROUND SIDE AREA
	{ {outer_area_width, -outer_area_height, outer_area_depth} , { -outer_area_width, -outer_area_height, outer_area_depth},
	{ -outer_area_width, -outer_area_height, -outer_area_depth }, {outer_area_width, -outer_area_height, -outer_area_depth} }, // bottom

	{ {width, -height, depth}, {-width+6, -height, depth}, {-width+6, height, depth}, {width, height,depth} }, // FRONT WALL
};

const int numberOfRectangledForhouse_h = sizeof(house_h) / sizeof(house_h[0]);

COLORS hous_colors_h[] =
{
	{1.0f, 0.0f,0.0f},
	{0.0f, 1.0f, 0.0f},
	{0.0f, 0.0f, 1.0f},
	{1.0f, 1.0f, 0.0f},
	{0.0f, 1.0f, 1.0f},
	{1.0f, 0.0f, 1.0f},
	{1.0f, 0.0f,0.0f},
	{0.0f, 1.0f, 0.0f},
	{0.0f, 0.0f, 1.0f},
	{1.0f, 1.0f, 0.0f},
	{0.0f, 1.0f, 1.0f},
	{1.0f, 0.0f, 1.0f},
};

void House()
{
	// VARIABLE DECLARATIONS
	static GLfloat angle = 0.0f, houseSize = 1.5f, wall_r = 0.3f, wall_g = 0.0f, wall_b = 0.0f;

	// CODE
	glScalef(houseSize, houseSize, houseSize);

	int i = 0;
	// FRONT AND BACK WALL
	glColor3f(1.0f, 1.0f, 1.0f);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, grass_texture);
	for (i = 7; i < 8; i++)
	{
		glBegin(GL_QUADS);
		{
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(house_h[i].a.x, house_h[i].a.y, house_h[i].a.z);

			glTexCoord2f(grass_texture_coord, 0.0f);
			glVertex3f(house_h[i].b.x, house_h[i].b.y, house_h[i].b.z);

			glTexCoord2f(grass_texture_coord, grass_texture_coord);
			glVertex3f(house_h[i].c.x, house_h[i].c.y, house_h[i].c.z);

			glTexCoord2f(0.0f, grass_texture_coord);
			glVertex3f(house_h[i].d.x, house_h[i].d.y, house_h[i].d.z);
		}
		glEnd();
	}
	
	glBindTexture(GL_TEXTURE_2D, stone_wall_texture);
	for (i = 0; i < 1; i++)
	{
		glBegin(GL_QUADS);
		{
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(house_h[i].a.x, house_h[i].a.y, house_h[i].a.z);

			glTexCoord2f(2.0f, 0.0f);
			glVertex3f(house_h[i].b.x, house_h[i].b.y, house_h[i].b.z);

			glTexCoord2f(2.0f, 1.0f);
			glVertex3f(house_h[i].c.x, house_h[i].c.y, house_h[i].c.z);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(house_h[i].d.x, house_h[i].d.y, house_h[i].d.z);
		}
		glEnd();
	}

	// RIGHT AND LEFT WALL
	
	for (; i < 3; i++)
	{
		glBegin(GL_QUADS);
		{
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(house_h[i].a.x, house_h[i].a.y, house_h[i].a.z);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(house_h[i].b.x, house_h[i].b.y, house_h[i].b.z);

			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(house_h[i].c.x, house_h[i].c.y, house_h[i].c.z);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(house_h[i].d.x, house_h[i].d.y, house_h[i].d.z);
		}
		glEnd();
	}

	// TRIANGLE WALL
	for (; i < 5; i++)
	{
		glBegin(GL_QUADS);
		{
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(house_h[i].a.x, house_h[i].a.y, house_h[i].a.z);

			glTexCoord2f(0.8f, 0.0f);
			glVertex3f(house_h[i].b.x, house_h[i].b.y, house_h[i].b.z);

			glTexCoord2f(0.8f, 0.8f);
			glVertex3f(house_h[i].c.x, house_h[i].c.y, house_h[i].c.z);

			glTexCoord2f(0.0f, 0.8f);
			glVertex3f(house_h[i].d.x, house_h[i].d.y, house_h[i].d.z);
		}
		glEnd();
	}

	// ROOF

	glBindTexture(GL_TEXTURE_2D, roof_texture);
	glColor3f(0.5f, 0.5f, 0.5f);
	for (; i < 7; i++)
	{
		glBegin(GL_QUADS);
		{
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(house_h[i].a.x, house_h[i].a.y, house_h[i].a.z);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(house_h[i].b.x, house_h[i].b.y, house_h[i].b.z);

			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(house_h[i].c.x, house_h[i].c.y, house_h[i].c.z);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(house_h[i].d.x, house_h[i].d.y, house_h[i].d.z);
		}
		glEnd();
	}
	glColor3f(1.0f, 1.0f, 1.0f);
	
	//front
	glBindTexture(GL_TEXTURE_2D, stone_wall_texture);
	for (i = 8; i < numberOfRectangledForhouse_h; i++)
	{
		glBegin(GL_QUADS);
		{
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(house_h[i].a.x, house_h[i].a.y, house_h[i].a.z);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(house_h[i].b.x, house_h[i].b.y, house_h[i].b.z);

			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(house_h[i].c.x, house_h[i].c.y, house_h[i].c.z);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(house_h[i].d.x, house_h[i].d.y, house_h[i].d.z);
		}
		glEnd();
	}

	
	glPushMatrix();
	// HOUSE RIGHT DOOR 
	glBindTexture(GL_TEXTURE_2D, right_door_texture);	
	glTranslatef(open_right_door_translate_x, 0.0f, open_right_door_translate_z);
	glRotatef(open_right_door_angle, 0.0f, 1.0f, 0.0f);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(house_h[8].b.x , house_h[8].b.y, house_h[8].b.z );

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(house_h[8].c.x , house_h[8].c.y-1, house_h[8].c.z );

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-width + 3 , height-1, depth );

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-width + 3 , -height, depth );
	glEnd();

	glPopMatrix();
	glPushMatrix();
	// HOUSE LEFT DOOR 
	glBindTexture(GL_TEXTURE_2D, left_door_texture);
	glTranslatef(open_left_door_translate_x, 0.0f, open_left_door_translate_z);
	glRotatef(open_left_door_angle, 0.0f, 1.0f, 0.0f); // open_right_door_angle
	
	glBegin(GL_QUADS);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-width + 3, -height, depth);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-width + 3, height - 1, depth);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-width, height - 1, depth);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-width, -height, depth);

	glEnd();

	glPopMatrix();

	glDisable(GL_TEXTURE_2D);
	angle = angle + 0.05;
}

/***********************ONLY WALLS**************************************/
void OnlyWalls()
{
	// VARIABLE DECLARATIONS
	static GLfloat angle = 0.0f, houseSize = 1.5f, wall_r = 0.3f, wall_g = 0.0f, wall_b = 0.0f;
	int i = 0;

	// CODE
	glScalef(houseSize, houseSize, houseSize);	

	// BACK WALL
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, marble);
	i = 7;
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(house_h[i].a.x, house_h[i].a.y, house_h[i].b.z);

		glTexCoord2f(grass_texture_coord, 0.0f);
		glVertex3f(house_h[i].b.x, house_h[i].b.y, house_h[i].b.z);

		glTexCoord2f(grass_texture_coord, grass_texture_coord);
		glVertex3f(house_h[i].c.x, house_h[i].c.y, house_h[i].c.z + 6);

		glTexCoord2f(0.0f, grass_texture_coord);
		glVertex3f(house_h[i].d.x, house_h[i].d.y, house_h[i].d.z);
	}
	glEnd();


	//	glColor3f(wall_r, wall_g, wall_b);
	glBindTexture(GL_TEXTURE_2D, stone_wall_texture);
	i = 0;
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(house_h[i].a.x, house_h[i].a.y, house_h[i].a.z);

		glTexCoord2f(2.0f, 0.0f);
		glVertex3f(house_h[i].b.x, house_h[i].b.y, house_h[i].b.z);

		glTexCoord2f(2.0f, 1.0f);
		glVertex3f(house_h[i].c.x, house_h[i].c.y + 3.0f, house_h[i].c.z);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(house_h[i].d.x, house_h[i].d.y + 3.0f, house_h[i].d.z);
	}
	glEnd();

	i = 0;
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(house_h[i].a.x, house_h[i].c.y + 3.0f, house_h[i].a.z);

		glTexCoord2f(2.0f, 0.0f);
		glVertex3f(house_h[i].b.x, house_h[i].d.y + 3.0f, house_h[i].b.z);

		glTexCoord2f(2.0f, 1.0f);
		glVertex3f(house_h[i].c.x, house_h[i].d.y + 9.0f, house_h[i].c.z);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(house_h[i].d.x, house_h[i].c.y + 9.0f, house_h[i].d.z);
	}
	glEnd();

	// RIGHT AND LEFT WALL
	
	i = 1;
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(house_h[i].a.x, house_h[i].a.y + 3.0f, house_h[i].a.z);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(house_h[i].b.x, house_h[i].b.y + 3.0f, house_h[i].b.z + 15);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(house_h[i].c.x, house_h[i].c.y, house_h[i].c.z + 15);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(house_h[i].d.x, house_h[i].d.y, house_h[i].d.z);
	}
	glEnd();

	i = 2;
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(house_h[i].a.x, house_h[i].a.y + 3.0f, house_h[i].a.z + 15);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(house_h[i].b.x, house_h[i].b.y + 3.0f, house_h[i].b.z );

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(house_h[i].c.x, house_h[i].c.y, house_h[i].c.z );

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(house_h[i].d.x, house_h[i].d.y, house_h[i].d.z + 15);
	}
	glEnd();
	

	// ROOF
	i = 5;
	glBindTexture(GL_TEXTURE_2D, roof_texture);
	glColor3f(0.5f, 0.5f, 0.5f);
	
	glPushMatrix();
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);

	glBegin(GL_QUADS);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(house_h[i].a.x, house_h[i].a.y + 3.0f, house_h[i].a.z + 5.0f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(house_h[i].b.x - 10.0f, house_h[i].b.y + 3.0f, house_h[i].b.z + 5.0f);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(house_h[i].c.x - 10.0f, house_h[i].c.y + 3.0f, house_h[i].c.z);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(house_h[i].d.x, house_h[i].d.y + 3.0f, house_h[i].d.z);
	}
	glEnd();

	i = 6;
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(house_h[i].a.x, house_h[i].a.y + 3.0f, house_h[i].a.z - 5.0f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(house_h[i].b.x - 10.0f, house_h[i].b.y + 3.0f, house_h[i].b.z - 5.0f);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(house_h[i].c.x - 10.0f, house_h[i].c.y + 3.0f, house_h[i].c.z);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(house_h[i].d.x, house_h[i].d.y + 3.0f, house_h[i].d.z);
	}
	glEnd();
	
	glPopMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);


	glColor3f(1.0f, 1.0f, 1.0f);	
	glDisable(GL_TEXTURE_2D);
	angle = angle + 0.05;
}


/***************************************************************************

						Tree

*****************************************************************************/


void TreeWithBase()
{
	void Tree();
	void TreeBase();

	glScalef(0.4f, 0.4f, 0.4f);
	glTranslatef(25.0f, 2.0f, 30.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glScalef(1.5f, 1.5f, 1.5f);
	Tree();
	glTranslatef(0.0f, -8.0f, 0.0f);
	TreeBase();
}

void TreeBase()
{
	// CODE
	void CylinderVerticleWithTexture(GLfloat radiusForUpCircle, GLfloat radiusForDownCircle, GLfloat height, int langitude, int fromAngle, int toAngle,
		GLfloat x, GLfloat y, GLfloat z, GLuint texture, GLfloat texCoord_y);

	CylinderVerticleWithTexture(10.0f, 10.0f, 5.0f, 15, 0, 360, 0.0f, 0.0f, 0.0f, tree_base_texture, 0.3f);

}

void Tree()
{
	// FUNCTION DECLARATION
	void Branch(GLfloat radius, GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat x3, GLfloat y3, GLfloat x4, GLfloat y4);
	void leaf();

	// TRUNC
	glPushMatrix();
	glRotatef(90, 0.0f, 0.0f, 1.0f);
	glColor3f(0.33f, 0.34f, 0.32f);
	Branch(2.0f, -13.0f, 0.0f, -3.0f, 2.0f, 4.0f, -2.0f, 20.0f, 2.0f);
	glTranslatef(10.0f, 0.0f, 0.0f);
	glScalef(2.0f, 5.0f, 5.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	leaf();
	glPopMatrix();
	glRotatef(10, 0.0f, 1.0f, 0.0f);
	glPushMatrix();

	glTranslatef(5.0f, 2.0f, -2.0f);
	glRotatef(30.0f, 0.0f, 1.0f, 0.0f);
	glRotatef(30.0f, 0.0f, 0.0f, 1.0f); // ROTATE BRANCH UP
	glScalef(1.0f, 0.5f, 0.50f);
	glColor3f(0.4f, 0.0f, 0.0f);
	Branch(1.0f, -6.0f, 0.0f, -3.0f, 2.0f, 4.0f, -2.0f, 6.0f, 2.0f);
	leaf();
	glPopMatrix();
	glPushMatrix();

	glTranslatef(5.0f, 10.0f, -3.0f);
	glRotatef(30.0f, 0.0f, 1.0f, 0.0f);
	glRotatef(30.0f, 0.0f, 0.0f, 1.0f); // ROTATE BRANCH UP
	glScalef(1.0f, 0.5f, 0.50f);
	glColor3f(0.4f, 0.0f, 0.0f);
	Branch(1.0f, -6.0f, 0.0f, -3.0f, 2.0f, 4.0f, -2.0f, 6.0f, 2.0f);
	leaf();

	glPopMatrix();
	glRotatef(30, 0.0f, 1.0f, 0.0f);
	glPushMatrix();

	glTranslatef(-5.0f, 1.0f, -1.0f);
	glRotatef(330.0f, 0.0f, 0.0f, 1.0f); //ROTATE BRANCH UP
	glRotatef(180, 0.0f, 1.0f, 0.0f);
	glScalef(1.1f, 0.5f, 0.50f);
	glColor3f(0.4f, 0.0f, 0.0f);
	Branch(1.0f, -6.0f, 0.0f, -3.0f, 3.0f, 4.0f, -3.0f, 6.0f, 2.0f);
	leaf();

	glPopMatrix();
	glPushMatrix();

	glTranslatef(7.0f, 6.0f, 0.0f);
	glRotatef(30.0f, 0.0f, 0.0f, 1.0f); // ROTATE BRANCH UP
	glScalef(1.2f, 0.5f, 0.50f);
	glColor3f(0.4f, 0.0f, 0.0f);
	Branch(1.0f, -6.0f, 0.0f, -3.0f, 2.0f, 4.0f, -2.0f, 6.0f, 2.0f);
	leaf();

	glPopMatrix();
	glRotatef(90, 0.0f, 1.0f, 0.0f);
	glPushMatrix();

	glPopMatrix();
	glRotatef(90, 0.0f, 1.0f, 0.0f);
	glPushMatrix();

	glTranslatef(7.0f, 6.0f, 0.0f);
	glRotatef(30.0f, 0.0f, 0.0f, 1.0f); // ROTATE BRANCH UP
	glScalef(1.2f, 0.5f, 0.50f);
	glColor3f(0.4f, 0.0f, 0.0f);
	Branch(1.0f, -6.0f, 0.0f, -3.0f, 2.0f, 4.0f, -2.0f, 6.0f, 2.0f);
	leaf();

	glPopMatrix();
	glPushMatrix();

	glTranslatef(5.0f, 10.0f, 0.0f);
	glRotatef(30.0f, 0.0f, 0.0f, 1.0f); // ROTATE BRANCH UP
	glScalef(0.8f, 0.5f, 0.50f);
	glColor3f(0.4f, 0.0f, 0.0f);
	Branch(1.0f, -6.0f, 0.0f, -3.0f, 2.0f, 4.0f, -2.0f, 6.0f, 2.0f);
	leaf();

	glPopMatrix();

}

void leaf()
{
	glColor3f(1.0f, 1.0f, 1.0f);
	glTranslatef(4.0f, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, tree_texture);
	gluSphere(quadric_for_tree, 3.0f, 20, 20);
}


/********************************************************************
		
						Sun

********************************************************************/

void Sun()
{
	glPushMatrix();
	glTranslatef(angle_for_sun_x, angle_for_sun_y, angle_for_sun_z);
	glRotatef(hrh_angle, 0.0f, 0.0f, 1.0f);
	glColor3f(sun_colo_r, sun_color_g, sun_color_b);
	gluSphere(quadric_for_sun, 2.0f, 20, 20);

	glPopMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);
}
