#include "FFPproject.h"

//VARIABLE DECLARATION
ControlPoints totalControlPintsForBeizerCurve[4];
PointsOnCurve pointsOnCurvep[10];
int totalNumberOfPointsOnCurve = 0;

extern short nFontList, nFontList_underlines;
extern HFONT hFont, hFontWithUnderLine;
extern HDC ghdc;
extern GLYPHMETRICSFLOAT agmf[128];
extern short title_r, title_g, title_b;
extern short text_r , text_g , text_b ;
extern GLfloat scale_title_size , scale_text_size; 



void printThankYou(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATION
	char string_thank_you[] = "Thank You...!!!";

	// CODE
	glColor3ub(title_r, title_g, title_b);
	glPushMatrix();
	glTranslatef(translate_x - 0.5, translate_y, translate_z);
	glScalef(scale_title_size, scale_title_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_thank_you) - 1, GL_UNSIGNED_BYTE, string_thank_you);
	glPopMatrix();

}


void printWithIgnitedBy(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATION
	char string_ignied_by[] = "Ignited By";
	char string_sir[] = "Dr. Vijay D. Gokhale Sir";

	// CODE
	glColor3ub(title_r, title_g, title_b);
	glPushMatrix();
	glTranslatef(translate_x - 0.5, translate_y, translate_z);
	glScalef(scale_title_size, scale_title_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_ignied_by) - 1, GL_UNSIGNED_BYTE, string_ignied_by);
	glPopMatrix();

	glColor3ub(text_r, text_g, text_b);
	glPushMatrix();
	glTranslatef(translate_x - 0.7f, translate_y - 0.25, translate_z);
	glScalef(scale_text_size, scale_text_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_sir) - 1, GL_UNSIGNED_BYTE, string_sir);
	glPopMatrix();

}


void printWithBlessing(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATION
	char string_with_Blessings[] = "With Blessings";
	char string_madam[] = "Dr. Rama Vijay Gokhale Madam";

	// CODE
	glColor3ub(title_r, title_g, title_b);
	glPushMatrix();
	glTranslatef(translate_x - 0.5, translate_y, translate_z);
	glScalef(scale_title_size, scale_title_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_with_Blessings) - 1, GL_UNSIGNED_BYTE, string_with_Blessings);
	glPopMatrix();

	glColor3ub(text_r, text_g, text_b);
	glPushMatrix();
	glTranslatef(translate_x - 0.7f, translate_y - 0.25, translate_z);
	glScalef(scale_text_size, scale_text_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_madam) - 1, GL_UNSIGNED_BYTE, string_madam);
	glPopMatrix();

}

void printProjectBy(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATION
	char string_project_by[] = "Project By";
	char string_hrituja[] = "Hrituja Ramkrishna Hedau";

	// CODE
	glColor3ub(title_r, title_g, title_b);
	glPushMatrix();
	glTranslatef(translate_x - 0.5, translate_y, translate_z);
	glScalef(scale_title_size, scale_title_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_project_by) - 1, GL_UNSIGNED_BYTE, string_project_by);
	glPopMatrix();

	glColor3ub(text_r, text_g, text_b);
	glPushMatrix();
	glTranslatef(translate_x - 0.7f, translate_y - 0.25, translate_z);
	glScalef(scale_text_size, scale_text_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_hrituja) - 1, GL_UNSIGNED_BYTE, string_hrituja);
	glPopMatrix();

}


void printGroupLeader(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATION
	char string_group_leader[] = "Group Leader";
	char string_group_leader_name[] = "Ajay Ambure";

	// CODE
	glColor3ub(title_r, title_g, title_b);
	glPushMatrix();
	glTranslatef(translate_x - 0.5, translate_y, translate_z);
	glScalef(scale_title_size, scale_title_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_group_leader) - 1, GL_UNSIGNED_BYTE, string_group_leader);
	glPopMatrix();

	glColor3ub(text_r, text_g, text_b);
	glPushMatrix();
	glTranslatef(translate_x - 0.4f, translate_y - 0.25, translate_z);
	glScalef(scale_text_size, scale_text_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_group_leader_name) - 1, GL_UNSIGNED_BYTE, string_group_leader_name);
	glPopMatrix();
}

void printGroupName(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATION
	char string_group[] = "Group Name";
	char string_fragment[] = "Fragment Group";

	// CODE

	glColor3ub(title_r, title_g, title_b);
	glPushMatrix();
	glTranslatef(translate_x - 0.5, translate_y, translate_z);
	glScalef(scale_title_size, scale_title_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_group) - 1, GL_UNSIGNED_BYTE, string_group);
	glPopMatrix();

	glColor3ub(text_r, text_g, text_b);
	glPushMatrix();
	glTranslatef(translate_x - 0.4f, translate_y - 0.25, translate_z);
	glScalef(scale_text_size, scale_text_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_fragment) - 1, GL_UNSIGNED_BYTE, string_fragment);
	glPopMatrix();

}


void printMusic(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATION
	char string_music[] = "Music";
	char string_music_name[] = "Genshin Impact - All Calm / Relaxing Music";



	// CODE
	glColor3ub(title_r, title_g, title_b);

	glPushMatrix();
	glTranslatef(translate_x - 0.5, translate_y, translate_z);
	glScalef(scale_title_size, scale_title_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_music) - 1, GL_UNSIGNED_BYTE, string_music);
	glPopMatrix();

	glColor3ub(text_r, text_g, text_b);
	glPushMatrix();
	glTranslatef(translate_x - 1.3f, translate_y - 0.25, translate_z);
	glScalef(scale_text_size, scale_text_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_music_name) - 1, GL_UNSIGNED_BYTE, string_music_name);
	glPopMatrix();
}



void printBoyBirdAndLove(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATION
	char string_Boy[] = "Boy";
	char string_Bird[] = "Bird";
	char string_and[] = "&";
	char string_Love[] = "Love";
	GLfloat scale_size = 8.0f; 

	// CODE

	glColor3ub(0, 191, 255);
	glPushMatrix();
	glTranslatef(translate_x - 20.0f, translate_y, translate_z);
	glScalef(scale_size, scale_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_Boy) - 1, GL_UNSIGNED_BYTE, string_Boy);
	glPopMatrix();

	glColor3ub(230, 0, 92);
	glPushMatrix();
	glTranslatef(translate_x - 7.0f, translate_y, translate_z);
	glScalef(scale_size, scale_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_Bird) - 1, GL_UNSIGNED_BYTE, string_Bird);
	glPopMatrix();
	
	glColor3ub(102, 51, 0);	
	glPushMatrix();
	glTranslatef(translate_x - 10.0f, translate_y - 5.0f, translate_z);
	glScalef(5.0f, 5.0f, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_and) - 1, GL_UNSIGNED_BYTE, string_and);
	glPopMatrix();

	glColor3ub(179, 0, 179);
	glPushMatrix();
	glTranslatef(translate_x - 15.0f, translate_y - 10.0f, translate_z);
	glScalef(scale_size, scale_size, 1.0f);
	glListBase(nFontList);
	glCallLists(sizeof(string_Love) - 1, GL_UNSIGNED_BYTE, string_Love);
	glPopMatrix();

}


void printAstroMediComp(GLfloat translate_x, GLfloat translate_y, GLfloat translate_z)
{
	// VARIABLE DECLARATION
	char string_AstroMediComp[] = "AstroMediComp";
	char string_presents[] = "Presents";
	GLfloat scale_size = 3.0f; 

	// CODE
	glScalef(scale_size, scale_size, 1.0f);
	glColor3ub(0, 125, 0);
		
	glPushMatrix();
	glTranslatef(translate_x, translate_y, translate_z);
	glListBase(nFontList);
	glCallLists(sizeof(string_AstroMediComp) - 1, GL_UNSIGNED_BYTE, string_AstroMediComp);
	glPopMatrix();
	
	glColor3ub(149, 137, 48);
	glPushMatrix();
	glTranslatef(translate_x + 2.0f, translate_y - 0.5, translate_z);
	glScalef(0.5f, 0.5f, 0.5f);
	glListBase(nFontList);
	glCallLists(sizeof(string_presents) - 1, GL_UNSIGNED_BYTE, string_presents);
	glPopMatrix();
}

void SetUpRC()
{
	// VARIABLE DECLARATION
	LOGFONT logfont, logFontWithUnderline;

	// CODE
	logfont.lfHeight = -10;
	logfont.lfWidth = 0;
	logfont.lfEscapement = 0;
	logfont.lfOrientation = 0;
	logfont.lfWeight = FW_BOLD;
	logfont.lfItalic = FALSE;
	logfont.lfUnderline = TRUE;
	logfont.lfStrikeOut = FALSE;
	logfont.lfCharSet = ANSI_CHARSET;
	logfont.lfOutPrecision = OUT_DEFAULT_PRECIS;
	logfont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	logfont.lfQuality = DEFAULT_QUALITY;
	logfont.lfPitchAndFamily = DEFAULT_PITCH;
	strcpy(logfont.lfFaceName, "Comic Sans MS");

	hFont = CreateFontIndirect(&logfont);
	SelectObject(ghdc, hFont);
	nFontList = glGenLists(128);                     
	wglUseFontOutlines(ghdc, 0, 128, nFontList, 0.0f, 0.0f, WGL_FONT_POLYGONS, agmf);

	
}


void emoji(int fromAngle, int toAngle, GLfloat radius, int langitude, GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height)
{
	GLfloat angle = 0.0f, rotation = 360;

	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	for (int i = fromAngle; i < toAngle;)
	{
		angle = i * RADIAN_VALUE;
		glVertex3f(x + ((width + radius) * sin(angle)), y + ((height + radius) * cos(angle)), z);
		angle = (i + (toAngle / langitude)) * RADIAN_VALUE;
		glVertex3f(x + ((width + radius) * sin(angle)), y + ((height + radius) * cos(angle)), z);
		i = i + (toAngle / langitude);
	}
	glEnd();
}

void CylinderVerticleWithTexture(GLfloat radiusForUpCircle, GLfloat radiusForDownCircle, GLfloat height, int langitude, int fromAngle, int toAngle,
	GLfloat x, GLfloat y, GLfloat z, GLuint texture, GLfloat texCoord_y)
{
	GLfloat angle = 0.0f;
	int i_for_color = 0;
	GLfloat heightFrom = -(height / 2.0f), heightTo = (height / 2.0f);
	GLfloat totalDividation = ((GLfloat)(toAngle - fromAngle) / ((GLfloat)toAngle / (GLfloat)langitude));
	GLfloat start_x = 0.0f;

	glColor3f(1.0f, 1.0f, 1.0f);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);
	glBegin(GL_QUADS);
	for (int i = fromAngle; i < toAngle; )
	{
		
		angle = i * RADIAN_VALUE;
		glTexCoord2f(start_x + (1.0f / totalDividation), texCoord_y);
		glVertex3f(x + radiusForDownCircle * cos(angle), y + heightFrom, z + radiusForDownCircle * sin(angle));
		glTexCoord2f(start_x, texCoord_y);
		glVertex3f(x + radiusForUpCircle * cos(angle), y + heightTo, z + radiusForUpCircle * sin(angle));
		i = i + (toAngle / langitude); // increase for next points
		angle = i * RADIAN_VALUE;
		glTexCoord2f(start_x, 0.0f);
		glVertex3f(x + radiusForUpCircle * cos(angle), y + heightTo, z + radiusForUpCircle * sin(angle));

		glTexCoord2f(start_x + (1.0f / totalDividation), 0.0f);
		glVertex3f(x + radiusForDownCircle * cos(angle), y + heightFrom, z + radiusForDownCircle * sin(angle));
		i_for_color = i_for_color + 1;
		start_x = start_x + (1.0f / totalDividation);
	}
	glEnd();
	

	glBegin(GL_TRIANGLES);
	for (int i = fromAngle; i < toAngle; i = i + (toAngle / langitude))
	{
		angle = i * RADIAN_VALUE;
		glVertex3f(x, y + (-heightFrom), z + radiusForDownCircle);
		glVertex3f(x + radiusForDownCircle * cos(angle), y + (-heightFrom), z + radiusForDownCircle * sin(angle));
		angle = (i + (toAngle / langitude)) * RADIAN_VALUE;
		glVertex3f(x + radiusForDownCircle * cos(angle), y + (-heightFrom), z + radiusForDownCircle * sin(angle));
	}
	glEnd();

	glBegin(GL_TRIANGLES);
	for (int i = fromAngle; i < toAngle; i = i + (toAngle / langitude))
	{
		angle = i * RADIAN_VALUE;
		glVertex3f(x, y + heightTo, z + radiusForUpCircle);
		glVertex3f(x + radiusForUpCircle * cos(angle), y + heightTo, z + radiusForUpCircle * sin(angle));
		angle = (i + (toAngle / langitude)) * RADIAN_VALUE;
		glVertex3f(x + radiusForUpCircle * cos(angle), y + heightTo, z + radiusForUpCircle * sin(angle));

	}
	glEnd();
	glDisable(GL_TEXTURE_2D);
}


void verticleCircle(GLfloat radius, int langitude, GLfloat x, GLfloat y, GLfloat z, int curveId)
{
	GLfloat angle = 0.0f, heightFrom = 0, heightTo = 0, rotation = 360;
	int countForlangitude = 0;

	glPointSize(5.0f);

	for (int i = 0; i < rotation; i = i + (rotation / langitude))
	{
		angle = i * RADIAN_VALUE;
		totalControlPintsForBeizerCurve[curveId].points[countForlangitude].x = x;
		totalControlPintsForBeizerCurve[curveId].points[countForlangitude].y = y + (radius * cos(angle));
		totalControlPintsForBeizerCurve[curveId].points[countForlangitude].z = z + (radius * sin(angle));
		countForlangitude = countForlangitude + 1;
	}

	totalNumberOfPointsOnCurve = countForlangitude;
}

void horizontalCircle(GLfloat radius, int langitude, GLfloat x, GLfloat y, GLfloat z, int curveId)
{
	GLfloat angle = 0.0f, heightFrom = 0, heightTo = 0, rotation = 360;
	int countForlangitude = 0;

	glPointSize(5.0f);

	for (int i = 0; i < rotation; i = i + (rotation / langitude))
	{
		angle = i * RADIAN_VALUE;
		totalControlPintsForBeizerCurve[curveId].points[countForlangitude].x = x + (radius * sin(angle));
		totalControlPintsForBeizerCurve[curveId].points[countForlangitude].y = y + (radius * cos(angle));
		totalControlPintsForBeizerCurve[curveId].points[countForlangitude].z = z ;
		countForlangitude = countForlangitude + 1;
	}

	totalNumberOfPointsOnCurve = countForlangitude;
}

void BezierCurvesUsingArray(int NumberOfcurve, GLfloat x11, GLfloat x12, GLfloat x13, GLfloat x14,
	GLfloat y11, GLfloat y12, GLfloat y13, GLfloat y14, GLfloat z11, GLfloat z12, GLfloat z13, GLfloat z14)
{
	GLfloat powOf2 = 0.0f, powOf3 = 0.0f, t = 0.0f, numberOfPoints = 0;
	int i = 0;

	for (GLfloat t = 0.0f; t <= 1.0f; numberOfPoints++, i = i + 1, t = t + 0.1f)
	{
		powOf3 = pow((1 - t), 3);
		powOf2 = pow((1 - t), 2);

		pointsOnCurvep[NumberOfcurve].point[i].x = (powOf3 * x11) + (3 * t * powOf2 * x12) + 3 * t * t * (1 - t) * x13 + (t * t * t * x14);
		pointsOnCurvep[NumberOfcurve].point[i].y = (powOf3 * y11) + (3 * t * powOf2 * y12) + 3 * t * t * (1 - t) * y13 + (t * t * t * y14);
		pointsOnCurvep[NumberOfcurve].point[i].z = (powOf3 * z11) + (3 * t * powOf2 * z12) + 3 * t * t * (1 - t) * z13 + (t * t * t * z14);
	}
}


void CylinderVerticle(GLfloat radiusForUpCircle, GLfloat radiusForDownCircle, GLfloat height, int langitude, int fromAngle, int toAngle,
	GLfloat x, GLfloat y, GLfloat z)
{
	GLfloat angle = 0.0f;
	int i_for_color = 0;
	GLfloat heightFrom = -(height / 2.0f), heightTo = (height / 2.0f);

	glBegin(GL_QUADS);
	for (int i = fromAngle; i < toAngle; )
	{
		angle = i * RADIAN_VALUE;
		glVertex3f(x + radiusForDownCircle * cos(angle), y + heightFrom, z + radiusForDownCircle * sin(angle));
		glVertex3f(x + radiusForUpCircle * cos(angle), y + heightTo, z + radiusForUpCircle * sin(angle));
		i = i + (toAngle / langitude); // increase for next points
		angle = i * RADIAN_VALUE;
		glVertex3f(x + radiusForUpCircle * cos(angle), y + heightTo, z + radiusForUpCircle * sin(angle));
		glVertex3f(x + radiusForDownCircle * cos(angle), y + heightFrom, z + radiusForDownCircle * sin(angle));
		i_for_color = i_for_color + 1;
	}
	glEnd();

	glBegin(GL_TRIANGLES);
	for (int i = fromAngle; i < toAngle; i = i + (toAngle / langitude))
	{
		angle = i * RADIAN_VALUE;
		glVertex3f(x, y + (-heightFrom), z + radiusForDownCircle);
		glVertex3f(x + radiusForDownCircle * cos(angle), y + (-heightFrom), z + radiusForDownCircle * sin(angle));
		angle = (i + (toAngle / langitude)) * RADIAN_VALUE;
		glVertex3f(x + radiusForDownCircle * cos(angle), y + (-heightFrom), z + radiusForDownCircle * sin(angle));
	}
	glEnd();

	glBegin(GL_TRIANGLES);
	for (int i = fromAngle; i < toAngle; i = i + (toAngle / langitude))
	{
		angle = i * RADIAN_VALUE;
		glVertex3f(x, y + heightTo, z + radiusForDownCircle);
		glVertex3f(x + radiusForDownCircle * cos(angle), y + heightTo, z + radiusForDownCircle * sin(angle));
		angle = (i + (toAngle / langitude)) * RADIAN_VALUE;
		glVertex3f(x + radiusForDownCircle * cos(angle), y + heightTo, z + radiusForDownCircle * sin(angle));

	}
	glEnd();
}


void Branch(GLfloat radius, GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat x3, GLfloat y3, GLfloat x4, GLfloat y4)
{
	// FUNCTION DECLARATION
	void verticleCircle(GLfloat radius, int langitude, GLfloat x, GLfloat y, GLfloat z, int curveId);
	void BezierCurvesUsingArray(int NumberOfcurve, GLfloat x11, GLfloat x12, GLfloat x13, GLfloat x14,
		GLfloat y11, GLfloat y12, GLfloat y13, GLfloat y14, GLfloat z11, GLfloat z12, GLfloat z13, GLfloat z14);

	static GLfloat angle = 0.0f;

	verticleCircle(radius, NUMBER_OF_POINTS_FOR_CONTROL_POINTS, x1, y1, 0, 0);
	verticleCircle(radius, NUMBER_OF_POINTS_FOR_CONTROL_POINTS, x2, y2, 0, 1);
	verticleCircle(radius, NUMBER_OF_POINTS_FOR_CONTROL_POINTS, x3, y3, 0, 2);


	for (int i = 0; i < NUMBER_OF_POINTS_FOR_CONTROL_POINTS; i++)
	{
		BezierCurvesUsingArray(i,
			totalControlPintsForBeizerCurve[0].points[i].x, totalControlPintsForBeizerCurve[1].points[i].x,
			totalControlPintsForBeizerCurve[2].points[i].x, x4,

			totalControlPintsForBeizerCurve[0].points[i].y, totalControlPintsForBeizerCurve[1].points[i].y,
			totalControlPintsForBeizerCurve[2].points[i].y, y4,

			totalControlPintsForBeizerCurve[0].points[i].z, totalControlPintsForBeizerCurve[1].points[i].z,
			totalControlPintsForBeizerCurve[2].points[i].z, 0.0f
		);
	}

	glBegin(GL_QUAD_STRIP);
	for (int NumberOfcurve = 0; NumberOfcurve < 10; NumberOfcurve++)
	{
		for (int i = 0; i < totalNumberOfPointsOnCurve; i++)
		{
			glVertex3f(
				pointsOnCurvep[NumberOfcurve].point[i].x,
				pointsOnCurvep[NumberOfcurve].point[i].y,
				pointsOnCurvep[NumberOfcurve].point[i].z);
			if (NumberOfcurve == 9)
			{
				glVertex3f(
					pointsOnCurvep[0].point[i].x,
					pointsOnCurvep[0].point[i].y,
					pointsOnCurvep[0].point[i].z);
			}
			else
			{
				glVertex3f(
					pointsOnCurvep[NumberOfcurve + 1].point[i].x,
					pointsOnCurvep[NumberOfcurve + 1].point[i].y,
					pointsOnCurvep[NumberOfcurve + 1].point[i].z);
			}

		}
	}
	glEnd();
}


