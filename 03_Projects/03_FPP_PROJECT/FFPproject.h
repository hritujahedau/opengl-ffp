#pragma once

#include <windows.h>
#include <windows.h>
#include <stdio.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <math.h>
#include <mmsystem.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib, "winmm.lib")

#define MYICON  1287
#define STONE_WALL_BITMAP 1288
#define ROOF_BITMAP 1289
#define GRASS_BITMAP 1290
#define BLACK_BITMAP 1291
#define LEFT_DOOR_BITMAP 1292
#define WINDOW_BITMAP 1293
#define NUMBER_OF_POINTS_FOR_CONTROL_POINTS 10
#define RADIAN_VALUE 3.14159/180
#define BIRD_BODY_TEXTURE 1294
#define BIRD_FEATHER_TEXTURE 1295
#define WARALI_BITMAP 1296
#define RIGHT_DOOR_BITMAP 1297
#define TREE_TEXTURE 1298
#define MARBLE_TEXTURE 1299
#define BOY_FACE_TEXTURE 1300
#define WOODEN_TEXTURE 1310
#define TREE_BASE_TEXTURE 1311
#define BACKGORUND_MUSIC 1312

#define IDI_MYICON MAKEINTRESOURCE(MYICON)
#define ID_STONE_WALL MAKEINTRESOURCE(STONE_WALL_BITMAP)
#define ID_ROOF MAKEINTRESOURCE(ROOF_BITMAP)
#define ID_GRASS MAKEINTRESOURCE(GRASS_BITMAP)
#define ID_BLACK_TEXTURE MAKEINTRESOURCE(BLACK_BITMAP)
#define ID_LEFT_DOOR_TEXTURE MAKEINTRESOURCE(LEFT_DOOR_BITMAP)
#define ID_RIGHT_DOOR_TEXTURE MAKEINTRESOURCE(RIGHT_DOOR_BITMAP)
#define ID_WINDOW_TEXTURE MAKEINTRESOURCE(WINDOW_BITMAP)
#define ID_BIRD_BODY_TEXTURE MAKEINTRESOURCE(BIRD_BODY_TEXTURE)
#define ID_BIRD_FEATHER_TEXTURE MAKEINTRESOURCE(BIRD_FEATHER_TEXTURE)
#define ID_WARALI_TEXTURE MAKEINTRESOURCE(WARALI_BITMAP)
#define ID_TREE_TEXTURE MAKEINTRESOURCE(TREE_TEXTURE)
#define ID_MARBLE_TEXTURE MAKEINTRESOURCE(MARBLE_TEXTURE)
#define ID_BOY_FACE_TEXTURE MAKEINTRESOURCE(BOY_FACE_TEXTURE)
#define ID_WOODEN_TEXTURE MAKEINTRESOURCE(WOODEN_TEXTURE)
#define ID_TREE_BASE_TEXTURE MAKEINTRESOURCE(TREE_BASE_TEXTURE)
#define ID_BACKGORUND_MUSIC MAKEINTRESOURCE(BACKGORUND_MUSIC)

typedef struct __POINTF {
	GLfloat x, y, z;
} POINTF;

typedef struct __RECTANGLE {
	POINTF a, b, c, d;
} RECTANGLE;

typedef struct __COLORS {
	GLfloat red, green, blue;
} COLORS;

typedef struct __CONTROL_POINTS
{
	POINTF points[NUMBER_OF_POINTS_FOR_CONTROL_POINTS];
} ControlPoints;

typedef struct __POINTS_ON_CURVE
{
	POINTF point[100];
} PointsOnCurve;

typedef struct __SEEN_INTIAL_STATE
{
	GLfloat init_human_translate_x, init_human_translate_y, init_human_translate_z;
	GLfloat init_human_angle_x, init_human_angle_y, init_human_angle_z;
	GLfloat init_translate_camera_x, init_translate_camera_y, init_translate_camera_z; // camera position
	GLfloat init_angle_for_camera_x, init_angle_for_camera_y, init_angle_for_camera_z;
	GLfloat init_angle_for_bird_x , init_angle_for_bird_y , init_angle_for_bird_z ;
	GLfloat init_translate_bird_x , init_translate_bird_y , init_translate_bird_z;
	bool isCopiedIntialState = false;
} INITIAL_STATE;

enum
{
	HAPPY = 0,
	SAD,
	VERY_HAPPY
};
