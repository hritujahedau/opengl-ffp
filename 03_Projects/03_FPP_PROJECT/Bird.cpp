#include "FFPproject.h"

extern GLfloat move_wings_right , move_wings_left;
extern GLuint bird_body_texture, bird_feather_texture, black_texture, wooden;
extern GLUquadric* quadric;
extern bool fly_up , fly_down, bFlyBird;
extern GLfloat bird_beak_up, bird_beak_down;
extern GLfloat bird_head_angle_x, bird_head_angle_y, bird_head_angle_z, hrh_angle;
extern GLfloat angle_for_bird_x , angle_for_bird_y , angle_for_bird_z;
extern GLfloat translate_bird_x , translate_bird_y , translate_bird_z;
extern bool bird_chippering, stop_bird_chippering;
extern GLfloat bird_chippering_speed;
static bool bird_beak_open = true, bird_beak_close = false;
extern bool open_cage_door , close_cage_door;
extern GLfloat translate_bowl_x , translate_bowl_y , translate_bowl_z;

void Bowl()
{
	//FUNCTION DECLARATION
	void CylinderVerticleWithTexture(GLfloat radiusForUpCircle, GLfloat radiusForDownCircle, GLfloat height, int langitude, int fromAngle, int toAngle,
		GLfloat x, GLfloat y, GLfloat z, GLuint texture, GLfloat texCoord_y);

	//CODE
	glTranslatef(translate_bowl_x, translate_bowl_y, translate_bowl_z);
	glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
	glScalef(0.9f, 0.9f, 0.9f);
	CylinderVerticleWithTexture(1.0f, 1.0f, 0.7f, 13, 0, 360, 0.0f, 0.0f, 0.0f, wooden, 0.2f);

}

void cage()
{
	static GLfloat angle = 45.0f;
	glTranslatef(2.0f, 0.0f, 15.0f);
	glRotatef(angle, 0.0f, 1.0f, 0.0f);

	glScalef(3.0f, 3.0f, 3.0f);
	GLfloat upper_y = 1.0f, bottom_y = 0.0f, top_upper_y = upper_y + upper_y / 2.0f;
	static GLfloat bottom_y_door = 0.0f, upper_y_door = upper_y;

	GLfloat front_and_back_lines[][3] =
	{
		// TOP VERTICLE LINE
		{0.5f, top_upper_y, -0.5f}, {0.5f, top_upper_y + top_upper_y, -0.5f},

		// FRONT LINE
		{1.0f, upper_y, 0.0f}, {0.0f, upper_y, 0.0f},
		{0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f},

		// RIGHT LINE
		{1.0f, upper_y, -1.0f}, {1.0f, upper_y, 0.0f},
		{1.0f, 0.0f, 0.0f}, {1.0f, 0.0f, -1.0f},


		// BACK LINE
		{1.0f, 0.0f, -1.0f}, {0.0f, 0.0f, -1.0f},
		{0.0f, upper_y, -1.0f}, {1.0f, upper_y, -1.0f},

		// LEFT LINE
		{0.0f, upper_y, 0.0f}, {0.0f, upper_y, -1.0f},
		{0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f},

		// FRONT
		{0.0f, 0.0f, 0.0f}, {0.0f, upper_y, 0.0f},
		{0.2f, 0.0f, 0.0f}, {0.2f, upper_y, 0.0f},
		{0.4f, 0.0f, 0.0f}, {0.4f, upper_y, 0.0f},
		{0.6f, 0.0f, 0.0f}, {0.6f, upper_y, 0.0f},
		{0.8f, 0.0f, 0.0f}, {0.8f, upper_y, 0.0f},
		{1.0f, 0.0f, 0.0f}, {1.0f, upper_y, 0.0f},

		// UPPER FRONT 0.5
		{0.0f, upper_y, 0.0f}, {0.5f, top_upper_y, -0.5f},
		{0.2f, upper_y, 0.0f}, {0.5f, top_upper_y, -0.5f},
		{0.4f, upper_y, 0.0f}, {0.5f, top_upper_y, -0.5f},
		{0.6f, upper_y, 0.0f}, {0.5f, top_upper_y, -0.5f},
		{0.8f, upper_y, 0.0f}, {0.5f, top_upper_y, -0.5f},
		{1.0f, upper_y, 0.0f}, {0.5f, top_upper_y, -0.5f},

		// BACK
		{0.0f, 0.0f, -1.0f}, {0.0f, upper_y, -1.0f},
		{0.2f, 0.0f, -1.0f}, {0.2f, upper_y, -1.0f},
		{0.4f, 0.0f, -1.0f}, {0.4f, upper_y, -1.0f},
		{0.6f, 0.0f, -1.0f}, {0.6f, upper_y, -1.0f},
		{0.8f, 0.0f, -1.0f}, {0.8f, upper_y, -1.0f},
		{1.0f, 0.0f, -1.0f}, {1.0f, upper_y, -1.0f},

		// BACK UPPER 0.5
		{0.0f, upper_y, -1.0f}, {0.5f, top_upper_y, -0.5f},
		{0.2f, upper_y, -1.0f}, {0.5f, top_upper_y, -0.5f},
		{0.4f, upper_y, -1.0f}, {0.5f, top_upper_y, -0.5f},
		{0.6f, upper_y, -1.0f}, {0.5f, top_upper_y, -0.5f},
		{0.8f, upper_y, -1.0f}, {0.5f, top_upper_y, -0.5f},
		{1.0f, upper_y, -1.0f}, {0.5f, top_upper_y, -0.5f},

		// LEFT
		{0.0f, bottom_y_door, -0.0f}, {0.0f, upper_y_door, -0.0f},
		{0.0f, bottom_y_door, -0.1f}, {0.0f, upper_y_door, -0.1f},
		{0.0f, bottom_y_door, -0.2f}, {0.0f, upper_y_door, -0.2f},
		{0.0f, bottom_y_door, -0.3f}, {0.0f, upper_y_door, -0.3f},
		{0.0f, bottom_y_door, -0.4f}, {0.0f, upper_y_door, -0.4f},
		{0.0f, bottom_y_door, -0.5f}, {0.0f, upper_y_door, -0.5f},
		{0.0f, bottom_y_door, -0.6f}, {0.0f, upper_y_door, -0.6f},
		{0.0f, bottom_y_door, -0.7f}, {0.0f, upper_y_door, -0.7f},
		{0.0f, bottom_y_door, -0.8f}, {0.0f, upper_y_door, -0.8f},
		{0.0f, bottom_y_door, -0.9f}, {0.0f, upper_y_door, -0.9f},
		{0.0f, bottom_y_door, -1.0f}, {0.0f, upper_y_door, -1.0f},

		// left horizontal line
		{0.0f, bottom_y_door, 0.0f}, {0.0f, bottom_y_door, -1.0f},
		{0.0f, upper_y_door, 0.0f}, {0.0f, upper_y_door, -1.0f},

		// LEFT UPPER 0.5
		{0.0f, upper_y, -0.1f}, {0.5f, top_upper_y, -0.5f},
		{0.0f, upper_y, -0.2f}, {0.5f, top_upper_y, -0.5f},
		{0.0f, upper_y, -0.3f}, {0.5f, top_upper_y, -0.5f},
		{0.0f, upper_y, -0.4f}, {0.5f, top_upper_y, -0.5f},
		{0.0f, upper_y, -0.5f}, {0.5f, top_upper_y, -0.5f},
		{0.0f, upper_y, -0.6f}, {0.5f, top_upper_y, -0.5f},
		{0.0f, upper_y, -0.7f}, {0.5f, top_upper_y, -0.5f},
		{0.0f, upper_y, -0.8f}, {0.5f, top_upper_y, -0.5f},
		{0.0f, upper_y, -0.9f}, {0.5f, top_upper_y, -0.5f},
		{0.0f, upper_y, -1.0f}, {0.5f, top_upper_y, -0.5f},

		// RIGHT
		{1.0f, 0.0f, -0.1f}, {1.0f, upper_y, -0.1f},
		{1.0f, 0.0f, -0.2f}, {1.0f, upper_y, -0.2f},
		{1.0f, 0.0f, -0.3f}, {1.0f, upper_y, -0.3f},
		{1.0f, 0.0f, -0.4f}, {1.0f, upper_y, -0.4f},
		{1.0f, 0.0f, -0.5f}, {1.0f, upper_y, -0.5f},
		{1.0f, 0.0f, -0.6f}, {1.0f, upper_y, -0.6f},
		{1.0f, 0.0f, -0.7f}, {1.0f, upper_y, -0.7f},
		{1.0f, 0.0f, -0.8f}, {1.0f, upper_y, -0.8f},
		{1.0f, 0.0f, -0.9f}, {1.0f, upper_y, -0.9f},
		{1.0f, 0.0f, -1.0f}, {1.0f, upper_y, -1.0f},

		// UPPER RIGHT 0.5
		{1.0f, upper_y, -0.1f}, {0.5f, top_upper_y, -0.5f},
		{1.0f, upper_y, -0.2f}, {0.5f, top_upper_y, -0.5f},
		{1.0f, upper_y, -0.3f}, {0.5f, top_upper_y, -0.5f},
		{1.0f, upper_y, -0.4f}, {0.5f, top_upper_y, -0.5f},
		{1.0f, upper_y, -0.5f}, {0.5f, top_upper_y, -0.5f},
		{1.0f, upper_y, -0.6f}, {0.5f, top_upper_y, -0.5f},
		{1.0f, upper_y, -0.7f}, {0.5f, top_upper_y, -0.5f},
		{1.0f, upper_y, -0.8f}, {0.5f, top_upper_y, -0.5f},
		{1.0f, upper_y, -0.9f}, {0.5f, top_upper_y, -0.5f},
		{1.0f, upper_y, -1.0f}, {0.5f, top_upper_y, -0.5f},

		// LINE FOR BIRD

	};

	int totalLineCount = sizeof(front_and_back_lines) / sizeof(front_and_back_lines[0]);

	glLineWidth(3.0);
	glColor3f(0.15f, 0.15f, 0.15f);
	glBegin(GL_LINES);
	for (int i = 0; i < totalLineCount; i++)
	{
		glVertex3f(front_and_back_lines[i][0], front_and_back_lines[i][1], front_and_back_lines[i][2]);
	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glVertex3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, -1.0f);
		glVertex3f(1.0f, 0.0f, -1.0f);
	}
	glEnd();

	glLineWidth(5.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0f, 0.24f, 0.0f);
	glVertex3f(0.5f, 0.24f, 0.f);
	glEnd();

	if (open_cage_door == true && bottom_y_door < 0.8f)
	{
		bottom_y_door = bottom_y_door + 0.001f;
		upper_y_door = upper_y_door + 0.001f;
	}
	else if (close_cage_door == true && bottom_y_door > 0.0f)
	{
		bottom_y_door = bottom_y_door - 0.001f;
		upper_y_door = upper_y_door - 0.001f;
	}
	glColor3f(1.0f, 1.0f, 1.0f);
}

void BirdFly()
{
	if (bFlyBird == true)
	{
		if (fly_down)
		{
			move_wings_right = move_wings_right - 0.3f;
			move_wings_left = move_wings_left + 0.3f;
			if (move_wings_right < 30)
			{
				fly_up = true;
				fly_down = false;
			}
		}
		else if (fly_up)
		{
			move_wings_right = move_wings_right + 0.3f;
			move_wings_left = move_wings_left - 0.3f;
			if (move_wings_right > 150)
			{
				fly_up = false;
				fly_down = true;
			}
		}
	}
}




void BirdWing(int rotate_x)
{
	// CODE
	glBindTexture(GL_TEXTURE_2D, bird_feather_texture);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glRotatef(rotate_x, 1.0f, 0.0f, 0.0f);
	glScalef(1.5, 0.8f, 0.0f);
	gluSphere(quadric, 0.5f, 30, 30);
}

void BirdBeak()
{
	// FUNCTION DECLARATION
	void CylinderVerticle(GLfloat radiusForUpCircle, GLfloat radiusForDownCircle, GLfloat height, int langitude, int fromAngle, int toAngle,
		GLfloat x, GLfloat y, GLfloat z);	

	// CODE
	glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.60f, -0.1f);
	glPushMatrix();
	glRotatef(bird_beak_up, 1.0, 0.0f, 0.0f);
	CylinderVerticle(0.2f, 0.01f, 0.5f, 10, 0, 180, 0.0f, 0.0f, 0.0f);
	glPopMatrix();
	glRotatef(bird_beak_down, 1.0, 0.0f, 0.0f);
	CylinderVerticle(0.2f, 0.01f, 0.5f, 10, 180, 360, 0.0f, 0.0f, 0.0f);
	
	if (bird_chippering == true)
	{
		if (bird_beak_open == true && bird_beak_down < 400.0f)
		{
			// bird_beak_up - , bird_beak_down +
			bird_beak_up = bird_beak_up - bird_chippering_speed;
			bird_beak_down = bird_beak_down + bird_chippering_speed;
		}
		else
		{
			bird_beak_open = false;
			bird_beak_close = true;
		}
		if (bird_beak_close == true && bird_beak_down > 360.0f)
		{
			// bird_beak_up + , bird_beak_down -
			bird_beak_up = bird_beak_up + bird_chippering_speed;
			bird_beak_down = bird_beak_down - bird_chippering_speed;
		}
		else
		{
			if (stop_bird_chippering == true)
			{
				bird_beak_open = true;
				bird_beak_close = false;
			}
						
		}
	}
}

void BirdEye()
{
	glBindTexture(GL_TEXTURE_2D, black_texture);
	gluSphere(quadric, 0.1f, 10, 10);
}
void Bird()
{
	//FUNCTION DECLARATION
	void BirdLeg();
	void BirdBeak();
	void BirdEye();
	void BirdWing(int);
	void CylinderVerticle(GLfloat radiusForUpCircle, GLfloat radiusForDownCircle, GLfloat height, int langitude, int fromAngle, int toAngle,
		GLfloat x, GLfloat y, GLfloat z);

	glBindTexture(GL_TEXTURE_2D, bird_body_texture);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glLineWidth(1.0f);
	glTranslatef(translate_bird_x, translate_bird_y, translate_bird_z); 
	glRotatef(angle_for_bird_x, 1.0f, 0.0f, 0.0f);
	glRotatef(angle_for_bird_y, 0.0f, 1.0f, 0.0f);
	glRotatef(angle_for_bird_z, 0.0f, 0.0f, 1.0f);
	glScalef(0.6f, 0.6f, 0.6f);
	glPushMatrix();

	glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
	glScalef(1.0, 0.8f, 1.0f);
	gluSphere(quadric, 1.0f, 30, 30);

	glPopMatrix();
	glPushMatrix();
	glPopMatrix();
	glPushMatrix();
	glTranslatef(1.0f, 0.0f, 0.0f);
	BirdWing(move_wings_left); // push bird body sphere


	glPopMatrix();
	glPushMatrix();
	glTranslatef(-1.0f, 0.0f, 0.0f);
	BirdWing(move_wings_right); // push bird body sphere

	glPopMatrix();
	glPushMatrix();

	glTranslatef(0.0f, 0.0f, -1.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);

	glPopMatrix();
	glPushMatrix();

	glBindTexture(GL_TEXTURE_2D, bird_body_texture);
	glTranslatef(0.0f, 0.8f, 1.0f);		// head of bird
	glRotatef(bird_head_angle_x, 1.0f, 0.0f, 0.0f);
	glRotatef(bird_head_angle_y, 0.0f, 1.0f, 0.0f);
	glRotatef(bird_head_angle_z, 0.0f, 0.0f, 1.0f);
	glScalef(1.0f, 1.0f, 1.0f);
	gluSphere(quadric, 0.5f, 30, 30);
	glPushMatrix();
	BirdBeak();
	glPopMatrix();
	glPushMatrix();
	glTranslatef(-0.35f, 0.15f, 0.3f);
	BirdEye();

	glPopMatrix();
	glTranslatef(0.35f, 0.15f, 0.3f);
	BirdEye();
	glPopMatrix();
	glPushMatrix();						// push matrix of main sphere for left leg

	glLineWidth(5.0f);
	glTranslatef(0.2f, -1.2f, 0.0f);
	BirdLeg();

	glPopMatrix();					// pop matrix of main sphere body
	glTranslatef(-0.2f, -1.2f, 0.0f);
	BirdLeg();
}

void BirdLeg()
{
	// CODE
	glBegin(GL_LINES);
	{
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.5f, 0.0f);
	}
	glEnd();

	glTranslatef(0.0f, 0.03f, 0.0f);
	glBegin(GL_LINES);
	{
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.15f, 0.0f, 0.15f);
	}
	glEnd();

	glBegin(GL_LINES);
	{
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.15f, 0.0f, 0.15f);
	}
	glEnd();
}

