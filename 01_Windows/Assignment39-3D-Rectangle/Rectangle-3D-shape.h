#pragma once
#include <windows.h>
#include <gl\GL.h>

#define MYICON 1287
#define IDI_MYICON MAKEINTRESOURCE(MYICON)

typedef struct __POINTF {
	GLfloat x, y, z;
} POINTF;

typedef struct __RECTANGLE {
	POINTF a, b, c ,d;
} RECTANGLE;

typedef struct __COLORS {
	GLfloat red, green, blue;
} COLORS;

