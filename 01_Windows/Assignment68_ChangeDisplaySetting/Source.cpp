#include<windows.h>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT )};
bool gbFullScreen = false;
HWND ghwnd = NULL;
int cx,cy, hight = 600, width = 800;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPreviousInstance, LPSTR lszptrCammandLine, int iCmdShow) {

	WNDCLASSEX wndClassEx;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	RECT rc;
	// code 
	//initialization 

	wndClassEx.hInstance = hInstance;
	wndClassEx.lpszClassName = szAppName;
	wndClassEx.lpfnWndProc = WndProc;
	wndClassEx.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClassEx.cbSize = sizeof(WNDCLASSEX);
	wndClassEx.cbClsExtra = 0;
	wndClassEx.cbWndExtra = 0;
	wndClassEx.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClassEx.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClassEx.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClassEx.lpszMenuName = NULL;
	wndClassEx.style = CS_HREDRAW | CS_VREDRAW;


	RegisterClassEx(&wndClassEx);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rc, 0);
	cx = (rc.right / 2) - (width/2);
	cy = (rc.bottom / 2) - (hight/2);
	hwnd = CreateWindow(szAppName, TEXT("Centering Window"), WS_OVERLAPPEDWINDOW, cx, cy, width, hight, NULL, NULL, hInstance, NULL);
	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);


	// message loop

	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen(void);

	// variable declaration
	TCHAR str[255] = TEXT("");

	// code
	switch (uMsg)
	{
	case WM_KEYDOWN:
		switch (wParam) {
		case 0x46:
		case 0x66:
			//MessageBox(NULL, TEXT("F"), TEXT("MSG"), MB_OK);
			ToggleFullScreen();
			break;
		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'Q':
		case 'q':
			PostQuitMessage(0);
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}

	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen(void) 
{
	//varibale declaration
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (!gbFullScreen) 
	{
		DEVMODE devMode;

		devMode.dmSize = sizeof(DEVMODE);

		EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &devMode);		

		LONG result = ChangeDisplaySettings(&devMode, CDS_FULLSCREEN);

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if ((dwStyle & WS_OVERLAPPEDWINDOW) && GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
			SetWindowPos(ghwnd, HWND_TOP,mi.rcMonitor.left, mi.rcMonitor.top, ( mi.rcMonitor.right - mi.rcMonitor.left),(mi.rcMonitor.bottom = mi.rcMonitor.right ) ,SWP_NOZORDER | SWP_FRAMECHANGED);
		}

		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else 
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		ChangeDisplaySettings(NULL, 0);
		gbFullScreen = false;
	}
}
