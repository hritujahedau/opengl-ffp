// header file declaration
#include <windows.h>
#include<stdio.h>
#include "Triangle-Rectangle-3D-shape.h"
#include <gl/gl.h>
#include <gl/glu.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define WIDTH 1000
#define HIGHT 800

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

GLfloat x = 0.5f, y = 0.5f, z = 0.5f;
GLfloat appex = 0.5f, left_t = 0.5f, right_t = 0.5f, depth = 0.5;

RECTANGLE rectangles[] = {  {
								{ -x, y, z } , {-x,-y,z}, {x,-y,z}, {x,y,z} // 1st
							}, 
							{ 
								{x, y, z}, { x, -y, z }, { x,-y,-z }, { x,y,-z } // 2nd
							},   
							{ 
								{x,y,-z}, {x,-y,-z}, {-x,-y,-z}, {-x,y,-z} // 3rd
							},
							{
								{-x,y,-z}, {-x,-y,-z}, {-x,-y,z}, {-x,y,z} //4th
							},
							{
								{-x,y,z}, {x,y,z}, {x,y,-z}, {-x,y,-z}
							},
							{
								{-x,-y,z}, {x,-y,z},{z,-y,-z},{-x,-y,-z}
							}
						};

TRIANGLE triangles[] = {
	{  { 0.0f,appex,0.0f	},  {-left_t, -right_t, depth}, { left_t,-right_t,depth}},
	{  { 0.0f,appex,0.0f },  { -right_t,-right_t,depth },  {-right_t,-right_t,-depth} },
	{  { 0.0f,appex,0.0f },  {-left_t,-right_t,-depth} ,  {left_t,-right_t ,-depth} },
	{  { 0.0f,appex,0.0f },  {left_t,-right_t,-depth} ,  {left_t,-right_t,depth} }
};
						
const int numberOfRectangles = sizeof(rectangles) / sizeof(rectangles[0]);

COLORS rectangleColors[] = { { 1.0f, 0.0f, 0.0f },
					{ 0.0f, 1.0f, 0.0f},
					{ 0.0f, 0.0f, 1.0f},
					{ 1.0f, 1.0f, 0.0f},
					{ 0.0f, 1.0f, 1.0f},
					{ 1.0f, 0.0f, 1.0f},
					{ 1.0f , 0.5f, 0.0f },
					{0.0f, 1.0f, 0.5f}
};

/*TRIANGLE_COLORS triangleColors[] = { {1.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.0f,0.0f,1.0f},
								{1.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.0f,0.0f,1.0f },
								{1.0f,0.0f,0.0f, 0.0f,0.0f,1.0f, 0.0f,1.0f,0.0f},
								{1.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.0f,0.0f,1.0f } };*/

TRIANGLE_COLORS triangleColors[] = { {1.0f, 0.5f, 0.5f},
					{ 0.5f , 0.5f, 1.0f },
					{ 0.0f, 1.0f, 0.5f},
					{ 1.0f, 1.0f, 0.5f}
					};

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("twotwodshape");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.logs", "a+") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("Two 2D Shape"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	Resize(WIDTH, HIGHT);
}

void Resize(int width, int hight) {
	if (hight == 0)
		hight = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, ((GLfloat)width/ (GLfloat)hight), 0.1f, 100.0f);
}

void Display() {
	//FUNCTION DECLARATION
	void Rectangle();
	void Triangle();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Rectangle();
	Triangle();

	SwapBuffers(ghdc);
}

void Triangle() {
	//VARIABLE DECLARATION
	static GLfloat angle = 0.0f;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(-1.5f, 0.0f, -6.0f);
	//glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(angle, 0.0f, 1.0f, 0.0f);

	for (int i = 0; i < 4; i++) {
		glBegin(GL_TRIANGLES);
		glColor3f(triangleColors[i].side1.red, triangleColors[i].side1.green, triangleColors[i].side1.blue);
		glVertex3f(triangles[i].appex.x, triangles[i].appex.y, triangles[i].appex.z);

		//glColor3f(triangleColors[i].side2.red, triangleColors[i].side2.green, triangleColors[i].side2.blue);
		glVertex3f(triangles[i].left.x, triangles[i].left.y, triangles[i].left.z);

		//glColor3f(triangleColors[i].side3.red, triangleColors[i].side3.green, triangleColors[i].side3.blue);
		glVertex3f(triangles[i].right.x, triangles[i].right.y, triangles[i].right.z);
		glEnd();
	}

	angle += 0.1f;
}

void Rectangle() {
	//VARIABLE DECLARATION
	static GLfloat angle = 0.0f;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 0.0f, -6.0f);
	glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(angle, 1.0f, 1.0f, 1.0f);

	for (int i = 0; i < numberOfRectangles; i++) {
		glBegin(GL_QUADS);

		glColor3f(rectangleColors[i].red, rectangleColors[i].green, rectangleColors[i].blue);
		glVertex3f(rectangles[i].a.x, rectangles[i].a.y, rectangles[i].a.z);

		glVertex3f(rectangles[i].b.x, rectangles[i].b.y, rectangles[i].b.z);

		glVertex3f(rectangles[i].c.x, rectangles[i].c.y, rectangles[i].c.z);

		glVertex3f(rectangles[i].d.x, rectangles[i].d.y, rectangles[i].d.z);
		
		glEnd();
	}
	angle += 0.1f;
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

}
