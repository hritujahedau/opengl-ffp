// HEADER FILES
#include "incircleRectangle.h"
#include <gl\gl.h>
#include <gl\glu.h>
#include <stdio.h>
#include <math.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define WIDTH 800
#define HIGHT 600

//GLOBAL FUNCTION DECLARATION
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//GLOBAL VARIABLE DECLARATION
bool gbActiveWindow = false, gbDone = false, gbFullScreen = false;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle = 0;

FILE* gpFile = NULL;

GLfloat radius = 1.0f;
POINTF a, b, c, d, e;
// ENTRY POINT FUNCTION

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow)
{
	// FUNCTION DECLARATION
	void Initialize();
	void Display();

	//VARIABLE DECLARATION
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("HGLULOOKAT");
	RECT rcMonitor;

	// CODE
	if (fopen_s(&gpFile, "opengl.log", "a+") != 0) {
		MessageBox(NULL, TEXT("Open File Operation Failed"), TEXT("FAILED"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;

	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);

	wndClass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rcMonitor, 0);
	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szClassName,
		TEXT("GRAPH"),
		WS_SYSMENU | WS_CAPTION | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_OVERLAPPED | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		rcMonitor.right / 2 - WIDTH / 2,
		rcMonitor.bottom / 2 - HIGHT / 2,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(ghwnd);
	SetFocus(ghwnd);
	ShowWindow(ghwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (true == gbActiveWindow) {
				Display();
			}
		}
	}
	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// FUNCTION DECLARATION
	void ToggleFullScreen();
	void Resize(int, int);
	void Uninitialize();

	switch (uMsg) {
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS :
		gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE :
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam) {
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;	
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void Initialize() {
	// FUNCTION DECLARATION
	void Resize(int, int);

	// VARIABLE DECLARATION
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// CODE
	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.dwFlags = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	
	iPixelFormatIndex = ChoosePixelFormat(ghdc,&pfd);
	if (0 == iPixelFormatIndex) {
		fprintf(gpFile, "\nChoosePixelFormat() Failed");
		DestroyWindow(ghwnd);
	}

	if (FALSE == SetPixelFormat(ghdc, iPixelFormatIndex, &pfd)) {
		fprintf(gpFile, "\nSetPixelFormat() Failed");
		DestroyWindow(ghwnd);
	}
	
	ghrc = wglCreateContext(ghdc);
	if (NULL == ghrc) {
		fprintf(gpFile, "\nwglCreateContext() Failed");
		DestroyWindow(ghwnd);
	}
	
	if (FALSE == wglMakeCurrent(ghdc, ghrc)) {
		fprintf(gpFile, "\nwglMakeCurrent() Failed");
		DestroyWindow(ghwnd);
	}
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	Resize(WIDTH, HIGHT);
}

void Resize(int width, int hight) {
	if (hight == 0) {
		hight = 1;
	}

	glViewport(0,0,(GLsizei)width, (GLsizei)hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, ((GLfloat)width / (GLfloat)hight), 0.1f, 100.0f);

}

void Display() {
	//FUNCTION DECLARATION
	void IncircleRectangle();
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glLineWidth(3.0f);
	IncircleRectangle();
	SwapBuffers(ghdc);
}

void IncircleRectangle()
{
	void bigCircle();
	void rectangle();
	void triangle();
	void smallCircle();	
	bigCircle();
	rectangle();
	triangle();
	smallCircle();
}

void smallCircle()
{
	GLfloat getLengthFromVertises(GLfloat, GLfloat, GLfloat, GLfloat);
	GLfloat s = 0.0f, length_ec = 0.0f, length_cd = 0.0f, length_de =0.0f;
	GLfloat smallCircleRadius = 0.0f;
	length_ec = getLengthFromVertises(e.x, e.y, c.x, c.y);
	length_cd = getLengthFromVertises(c.x, c.y, d.x, d.y);
	length_de = getLengthFromVertises(d.x, d.y, e.x, e.y);

	s = (length_ec + length_cd + length_de) / 2;
	smallCircleRadius = sqrt(s * (s - length_ec) * (s - length_cd) * (s - length_de)) / s;
	glBegin(GL_LINE_LOOP);
	for (int angle = 0; angle < 360; angle++)
	{
		glVertex3f(smallCircleRadius * (cos(angle * 0.01745329)), -0.15f + (smallCircleRadius * (sin(angle * 0.01745329))), 0.0f);
	}
	glEnd();
}

GLfloat getLengthFromVertises(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2)
{
	// eqaution for calculating legth between 2 points
	return sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
}

void triangle()
{
	e.x = (a.x + b.x) / 2;
	e.y = (a.y + b.y) / 2;
	glBegin(GL_LINE_LOOP);
	glVertex3f(e.x, e.y, 0.0f);
	glVertex3f(c.x, c.y, 0.0f);
	glVertex3f(d.x, d.y, 0.0f);
	glEnd();
}

void bigCircle()
{
	
	glBegin(GL_LINE_LOOP);
	for (int angle = 0; angle < 360; angle++)
	{
		glVertex2f(radius * (cos(angle * 0.01745329)), radius * (sin(angle * 0.01745329)));
	}
	glEnd();
}

void rectangle()
{
	
	a.x = radius * (cos(35.0f * 0.01745329));
	a.y = radius * (sin(35.0f * 0.01745329));

	b.x = radius * (cos(145.0f * 0.01745329));
	b.y = radius * (sin(145.0f * 0.01745329));

	c.x = radius * (cos(215.0f * 0.01745329));
	c.y = radius * (sin(215.0f * 0.01745329));

	d.x = radius * (cos(325.0f * 0.01745329));
	d.y = radius * (sin(325.0f * 0.01745329));

	glBegin(GL_LINE_LOOP);
	glVertex2f(a.x, a.y);
	glVertex2f(b.x, b.y);
	glVertex2f(c.x, c.y);
	glVertex2f(d.x, d.y);
	glEnd();
}

void ToggleFullScreen() {
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (false == gbFullScreen) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
					);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER );
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Uninitialize() {
	if (gpFile) {
		fclose(gpFile);
		gpFile = NULL;
	}

	if (true == gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}

	if (ghrc == wglGetCurrentContext()) {
		wglMakeCurrent(NULL,NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	
	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
}

