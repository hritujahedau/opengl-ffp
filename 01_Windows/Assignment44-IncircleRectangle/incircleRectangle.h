#pragma once
#include <windows.h>
#include <gl/gl.h>

#define MYICON 1231
#define IDI_MYICON MAKEINTRESOURCE(MYICON)

typedef struct __POINTF
{
	GLfloat x, y;
}POINTF;
