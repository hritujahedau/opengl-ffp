#pragma once
#include <windows.h>
#include <gl\GL.h>

#define MYICON 1287
#define STONE_BITMAP 1288
#define KUNDALI_BITMAP 1289

#define IDI_MYICON MAKEINTRESOURCE(MYICON)
#define IDI_STONE MAKEINTRESOURCE(STONE_BITMAP)
#define IDI_KUNDALI MAKEINTRESOURCE(KUNDALI_BITMAP)

typedef struct __POINTF {
	GLfloat x, y, z;
} POINTF;

typedef struct __RECTANGLE {
	POINTF a, b, c ,d;
} RECTANGLE;

typedef struct __COLORS {
	GLfloat red, green, blue;
} COLORS;

typedef struct __TRIANGLE {
	POINTF appex, left, right;
} TRIANGLE;

typedef struct __TRAINGLE_COLORS {
	COLORS side1, side2, side3;
} TRIANGLE_COLORS;
