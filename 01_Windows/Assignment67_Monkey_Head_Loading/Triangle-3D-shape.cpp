// header file declaration
#include <windows.h>
#include<stdio.h>
#include "Triangle-3D-shape.h"
#include <gl/gl.h>
#include <gl/glu.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define WIDTH 1000
#define HIGHT 800

#define TRUE	1
#define FALSE	0

#define		BUFFER_SIZE			256
#define		S_EQUAL				0
#define		NR_FACE_TOKEN		3
#define		NR_POINT_COORDS		3

#define		ERRORBOX1(lpszErrorMessage, lpszCaption) \
						{ \
							MessageBox(NULL, TEXT(lpszErrorMessage), TEXT(lszCaption), MB_OK | MB_ICONERROR); \
							exit(-1); \
						}

#define		ERRORBOX2(hwnd, lpszErrorMessage, lpszCaption) \
						{ \
							MessageBox(hwnd, TEXT(lpszErrorMessage), TEXT(lszCaption), MB_OK | MB_ICONERROR); \
							DestroyWindows(hwnd); \
						}

typedef struct vec_2d_int
{
	GLint** pp_arr;
	size_t size;
}vec_2d_int_t;

typedef struct vec_2d_float
{
	GLfloat** pp_arr;
	size_t size;
}vec_2d_float_t;

GLfloat gRotate;

vec_2d_float_t* gp_vertices;

vec_2d_float_t* gp_texture;

vec_2d_float_t* gp_normals;

vec_2d_int_t* gp_face_tri, * gp_face_texture, * gp_face_normals;

FILE* g_fp_meshfile = NULL;
FILE* g_fp_logfile = NULL;

char g_line[BUFFER_SIZE];

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

TRIANGLE triangles[] = {
	{  { 0.0f,0.5f,0.0f	},  {-0.5f, -0.5f, 0.5f}, { 0.5,-0.5,0.5f}},
	{  { 0.0f,0.5f,0.0f },  { -0.5f,-0.5f,0.5f },  {-0.5f,-0.5f,-0.5f} },
	{  { 0.0f,0.5f,0.0f },  {-0.5f,-0.5f,-0.5f} ,  {0.5f,-0.5f,-0.5f} },
	{  { 0.0f,0.5f,0.0f },  {0.5f,-0.5f,-0.5f} ,  {0.5f,-0.5f,0.5f} }
	};

TRIANGLE_COLORS colors[] = {	{1.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.0f,0.0f,1.0f},
								{1.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.0f,0.0f,1.0f },
								{1.0f,0.0f,0.0f, 0.0f,0.0f,1.0f, 0.0f,1.0f,0.0f},
								{1.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.0f,0.0f,1.0f } };

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("twotwodshape");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	fprintf(gpFile, "\nFile open successfully");
	fflush(gpFile);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("Two 2D Shape"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);
	void LoadMeshData(void);

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	LoadMeshData();

	Resize(WIDTH, HIGHT);
}

void Resize(int width, int hight) {
	if (hight == 0)
		hight = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, ((GLfloat)width/ (GLfloat)hight), 0.1f, 100.0f);
}

void LoadMeshData(void)
{
	vec_2d_int_t* create_vec_2d_int(void);
	vec_2d_float_t* create_vec_2d_float(void);
	void push_back_vec_2d_int(vec_2d_int_t * p_vec, int* p_arr);
	void push_back_vec_2d_float(vec_2d_float_t * p_vec, float* p_arr);
	void* xcalloc(int, size_t);

	// code
	fprintf(gpFile, "\nIn LoadMeshData()");
	fflush(gpFile);

	g_fp_meshfile = fopen("MonkeyHeadMeshObj.obj", "r");
	if (!g_fp_meshfile)
	{
		fprintf(gpFile, "\nFailed to open obj file");
		fflush(gpFile);
		DestroyWindow(ghwnd);
	}

	fprintf(gpFile, "\nobj file openned successfully");
	fflush(gpFile);

	gp_vertices = create_vec_2d_float();
	gp_texture = create_vec_2d_float();
	gp_normals = create_vec_2d_float();

	gp_face_tri = create_vec_2d_int();
	gp_face_texture = create_vec_2d_int();
	gp_face_normals = create_vec_2d_int();

	fprintf(gpFile, "\nAfter create functions");
	fflush(gpFile);

	char* sep_space = " ";
	char* sep_fslash = "/";
	char* first_token = NULL;
	char* token = NULL;
	char* face_token[NR_FACE_TOKEN];
	int nr_tokens;
	char* token_vertex_index = NULL;
	char* token_texture_index = NULL;
	char* token_normal_index = NULL;

	int k = 0;
	while (fgets(g_line, BUFFER_SIZE, g_fp_meshfile) != NULL)
	{
		first_token = strtok(g_line, sep_space);

		fprintf(gpFile, "\nk = %d, token = %s", k, first_token);
		fflush(gpFile);
		

		if (strcmp(first_token, "v") == S_EQUAL)
		{
			GLfloat* pvec_point_coord = (GLfloat*)xcalloc(NR_POINT_COORDS, sizeof(GLfloat));

			for (int i = 0; i != NR_POINT_COORDS; i++)
			{
				pvec_point_coord[i] = atof(strtok(NULL, sep_space));				
			}
			push_back_vec_2d_float(gp_vertices, pvec_point_coord);
		}
		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			GLfloat* pvec_texture_coord = (GLfloat*)xcalloc(2, sizeof(GLfloat));
			for (int i = 0; i != 2; i++)
			{
				pvec_texture_coord[i] = atof(strtok(NULL, sep_space));				
			}
			push_back_vec_2d_float(gp_texture, pvec_texture_coord);
		}
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			GLfloat* pvec_normal_coord = (GLfloat*)xcalloc(3, sizeof(float));
			for (int i = 0; i != 3; i++)
			{
				pvec_normal_coord[i] = atof(strtok(NULL, sep_space));				
			}
			push_back_vec_2d_float(gp_normals, pvec_normal_coord);
		}
		else if (strcmp(first_token, "f") == S_EQUAL)
		{
			fprintf(gpFile, "\nIn f");
			fflush(gpFile);

			GLint* pvec_vertex_indices = (GLint*)xcalloc(3, sizeof(GLint));
			GLint* pvec_texture_indices = (GLint*)xcalloc(3, sizeof(GLint));
			GLint* pvec_normal_indices = (GLint*)xcalloc(3, sizeof(GLint));

			fprintf(gpFile, "\nAfter all calloc()");
			fflush(gpFile);

			memset((void*)face_token, 0, NR_FACE_TOKEN);

			nr_tokens = 0;
			while (token = strtok(NULL, sep_space))
			{
				if (strlen(token) < 3)
				{
					break;
				}
				face_token[nr_tokens] = token;
				nr_tokens++;
			}
			
			fprintf(gpFile, "\nAfter while loop");
			fflush(gpFile);

			for (int i = 0; i != NR_FACE_TOKEN; i++)
			{
				fprintf(gpFile, "\nIn for loop i = %d", i);
				fflush(gpFile);
				token_vertex_index = strtok(face_token[i], sep_fslash);

				fprintf(gpFile, "\nIn for loop i = %d", i);
				fflush(gpFile);
				token_texture_index = strtok(NULL, sep_fslash);

				fprintf(gpFile, "\nIn for loop i = %d", i);
				fflush(gpFile);
				token_normal_index = strtok(NULL, sep_fslash);

				fprintf(gpFile, "\nIn for loop i = %d", i);
				fflush(gpFile);
				pvec_vertex_indices[i] = atoi(token_vertex_index);

				fprintf(gpFile, "\nIn for loop i = %d", i);
				fflush(gpFile);
				pvec_texture_indices[i] = atoi(token_texture_index);

				fprintf(gpFile, "\nIn for loop i = %d", i);
				fflush(gpFile);
				pvec_normal_indices[i] = atoi(token_normal_index);
			}

			fprintf(gpFile, "\nAfter for loop");
			fflush(gpFile);

			push_back_vec_2d_int(gp_face_tri, pvec_vertex_indices);
			push_back_vec_2d_int(gp_face_texture, pvec_texture_indices);
			push_back_vec_2d_int(gp_face_normals, pvec_normal_indices);
		}

		memset((void*)g_line, (int)'\0', BUFFER_SIZE);
		k++;
	}

	fprintf(gpFile, "\nAfter loop");
	fflush(gpFile);
	
	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;

	fprintf(gpFile, "gp_vertices->size : %llu gp_texture->size:%llu g_normals:%llu g_face_tri:%llu\n",
		gp_vertices->size, gp_texture->size, gp_normals->size, gp_face_tri->size);
	fprintf(gpFile, "\nIn LoadMeshData()");
	fflush(gpFile);
}

void Display() {
	//FUNCTION DECLARATION
	void Triangle();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Triangle();
	SwapBuffers(ghdc);
}

void Triangle() 
{
	// VARIABLE DECLARATION
	static GLfloat angle = 0.0f;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -3.0f);
	glRotatef(angle, 0.0f, 1.0f, 0.0f);

	glFrontFace(GL_CCW);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	for (int i = 0; i != gp_face_tri->size; ++i)
	{
		glBegin(GL_TRIANGLES);

		for (int j = 0 ; j != 3; j++)
		{
			int vi = gp_face_tri->pp_arr[i][j] - 1;
			glVertex3f(gp_vertices->pp_arr[vi][0], gp_vertices->pp_arr[vi][1], gp_vertices->pp_arr[vi][2]);
		}

		glEnd();
	}

	angle += 0.1f;
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
}

vec_2d_int_t* create_vec_2d_int(void)
{
	// function declarations
	void* xcalloc(int nr_elements, size_t size_per_element);

	// code
	return (vec_2d_int_t*)xcalloc(1, sizeof(vec_2d_int_t));
}

vec_2d_float_t* create_vec_2d_float(void)
{
	// function declarations
	void* xcalloc(int nr_elements, size_t size_per_element);

	// code
	return (vec_2d_float_t*)xcalloc(1, sizeof(vec_2d_float_t));
}

void push_back_vec_2d_int(vec_2d_int_t* p_vec, GLint* p_arr)
{
	// function declarations
	void* xrealloc(void* p, size_t new_size);

	p_vec->pp_arr = (GLint**)xrealloc(p_vec->pp_arr, (p_vec->size + 1) * sizeof(GLint**));
	p_vec->size++;
	p_vec->pp_arr[p_vec->size - 1] = p_arr;
}

void push_back_vec_2d_float(vec_2d_float_t* p_vec, GLfloat* p_arr)
{
	// function declarations
	void* xrealloc(void* p, size_t new_size);

	p_vec->pp_arr = (GLfloat**)xrealloc(p_vec->pp_arr, (p_vec->size + 1) * sizeof(GLfloat**));
	p_vec->size++;
	p_vec->pp_arr[p_vec->size - 1] = p_arr;
}

void clean_vec_2d_int(vec_2d_int_t** pp_vec)
{
	vec_2d_int_t* p_vec = *pp_vec;
	for (size_t i = 0; i != p_vec->size; i++)
	{
		free(p_vec->pp_arr[i]);
	}
	*pp_vec = NULL;
}

void clean_vec_2d_float(vec_2d_float_t** pp_vec)
{
	vec_2d_float_t* p_vec = *pp_vec;
	for (size_t i = 0; i != p_vec->size; i++)
	{
		free(p_vec->pp_arr[i]);
	}
	*pp_vec = NULL;
}

void* xcalloc(int nr_elements, size_t size_per_element)
{

	void* ptr = calloc(nr_elements, size_per_element);

	if (!ptr)
	{
		fprintf(gpFile, "Calloc:fatal:out of memory\n");
		DestroyWindow(ghwnd);
	}
	return ptr;
}

void* xrealloc(void* p, size_t new_size)
{

	void* ptr = realloc(p, new_size);
	if (!ptr)
	{
		fprintf(g_fp_logfile, "realloc:fatal:out of memory\n");
		DestroyWindow(ghwnd);
	}
	return ptr;
}
