#include <windows.h>
#include <gl\GL.h>

#define MYICON 5412
#define IDI_MYICON MAKEINTRESOURCE(MYICON)
#define TRANSLATEZ 5.0f

enum {
	ORANGE = 1,
	GREEN,
	WHITE
};

typedef struct __POINTF {
	GLfloat x, y, z;
} POINTF;

typedef struct __RECTANGLE {
	POINTF a, b, c, d;
} RECTANGLE;

GLfloat width = 0.2f, hight = 1.2f;

// FOR I

void I(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y) {
	//FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);
	// VARIABLE DECLARATION
	RECTANGLE rectangleForI[] = {
						{ // ORANGE RECTANGLE
							{ x - hight / 2, y + hight / 2 + width, 0.0f },
							{ x - hight / 2  , y + hight / 2, 0.0f},
							{ x + hight / 2 , y + hight / 2 , 0.0f },
							{ x + hight / 2 , y + hight / 2 + width , 0.0f }
						},

						{ // GREEN RECTANGLE
							{ x - hight / 2, y - hight / 2 , 0.0f },
							{ x - hight / 2  , y - hight / 2 - width, 0.0f},
							{ x + hight / 2 , y - hight / 2 - width, 0.0f },
							{ x + hight / 2 , y - hight / 2  , 0.0f }
						},
						{ // MIDDLE VERTICLE UP RECTANGLE
							{x + width / 2, y + hight / 2, 0.0f },
							{x - width / 2, y + hight / 2, 0.0f },
							{x - width / 2, y , 0.0f },
							{x + width / 2 , y , 0.0f }

						},
						{ // MIDDLE VERTICLE DOWN RECTANGLE
							{x + width / 2 , y , 0.0f },
							{x - width / 2, y , 0.0f },
							{x - width / 2, y - hight / 2 },
							{x + width / 2, y - hight / 2 , 0.0f}
						}
	};
	const int numberOfRectangleForI = sizeof(rectangleForI) / sizeof(rectangleForI[0]);

	int arrayColorForI[numberOfRectangleForI * 2] = { ORANGE, ORANGE, GREEN, GREEN, ORANGE, WHITE, WHITE, GREEN };

	// CODE
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, translatefy, -TRANSLATEZ);

	DrawRectangles(rectangleForI, numberOfRectangleForI, arrayColorForI);
}

void N(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y) {
	// FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);
	// VARIABLE DECLARATION
	RECTANGLE rectangleForN[] = {
		// RIGHT TOP SQAURE
		{
			{ x - hight / 2 + width, y + hight / 2 + width, 0.0f },
			{ x - hight / 2  , y + hight / 2 + width, 0.0f},
			{ x - hight / 2  , y + hight / 2, 0.0f},
			{ x - hight / 2 + width, y + hight / 2 , 0.0f }
		},
		{ // RIGHT UP SIDE RECTANGLE
			{ x - hight / 2 + width, y + hight / 2 , 0.0f },
			{ x - hight / 2  , y + hight / 2, 0.0f},
			{ x - hight / 2, y , 0.0f},
			{ x - hight / 2 + width, y , 0.0f }
		},
		{ // RIGHT DOWN SIDE RECTANGLE
			{ x - hight / 2 + width, y , 0.0f },
			{ x - hight / 2, y , 0.0f},
			{ x - hight / 2 , y - hight / 2 , 0.0f },
			{ x - hight / 2 + width, y - hight / 2 , 0.0f }
		},
		{// LEFT BOTTOM SQAURE
			{ x - hight / 2 + width, y - hight / 2 , 0.0f },
			{ x - hight / 2 , y - hight / 2 , 0.0f },
			{ x - hight / 2 , y - hight / 2 - width , 0.0f},
			{ x - hight / 2 + width, y - hight / 2 - width , 0.0f}
		},
		{   // LEFT TOP SQUARE
			{ x + hight / 2 - width , y + hight / 2 + width, 0.0f},
			{ x + hight / 2  , y + hight / 2 + width, 0.0f},
			{ x + hight / 2  , y + hight / 2, 0.0f},
			{ x + hight / 2 - width, y + hight / 2 , 0.0f }
		},
		{ // LEFT UP SIDE RECTANGLE
			{ x + hight / 2 - width, y + hight / 2 , 0.0f },
			{ x + hight / 2  , y + hight / 2, 0.0f},
			{ x + hight / 2, y , 0.0f},
			{ x + hight / 2 - width, y , 0.0f }
		},
		{ // LEFT DOWN SIDE RECTANGLE
			{ x + hight / 2 - width, y , 0.0f },
			{ x + hight / 2, y , 0.0f},
			{ x + hight / 2 , y - hight / 2 , 0.0f },
			{ x + hight / 2 - width, y - hight / 2 , 0.0f }
		},
		{ // LEFT DOWN SQUARE
			{ x + hight / 2 , y - hight / 2 , 0.0f },
			{ x + hight / 2 - width, y - hight / 2 , 0.0f },
			{ x + hight / 2 - width , y - hight / 2 - width, 0.0f },
			{ x + hight / 2  , y - hight / 2 - width , 0.0f }
		},
		{ // MIDDLE UP RECTANGLE
			{ x - hight / 2 + width, y + hight / 2 + width, 0.0f },
			{ x - hight / 2 + width, y + hight / 2 , 0.0f },
			{ x - 0.06f, y , 0.0f },
			{ x + 0.06f, y , 0.0f }
		},
		{// MIDDLE DOWN RECTANGLE
			{ x + 0.06f, y , 0.0f},
			{ x - 0.06f, y , 0.0f},
			{ x + (hight / 2) - width, y - hight / 2 - width , 0.0f },
			{ x + (hight / 2) - width, y - hight / 2 , 0.0f }
		}

	};

	const int numberOfRectangleForN = sizeof(rectangleForN) / sizeof(rectangleForN[0]);

	int arrayColorForN[numberOfRectangleForN * 2] = {
														ORANGE , ORANGE, // RIGHT TOP SQAURE
														ORANGE, WHITE,   // RIGHT UP SIDE RECTANGLE
														WHITE, GREEN,    // RIGHT DOWN SIDE RECTANGLE
														GREEN , GREEN,	 // RIGHT DOWN SQUARE
														ORANGE, ORANGE,	  // LEFT TOP SQAURE
														ORANGE, WHITE,	 // LEFT UP SIDE RECTANGLE
														WHITE, GREEN,	 // LEFT DOWN SIDE RECTANGLE
														GREEN, GREEN,   // LEFT DOWN SQUARE
														ORANGE, WHITE,	 // MIDDLE UP RECTANGLE
														WHITE, GREEN	 // MIDDLE DOWN RECTANGLE
	};

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, 0.0f, -TRANSLATEZ);
	DrawRectangles(rectangleForN, numberOfRectangleForN, arrayColorForN);

}

void D(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y) {
	// FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);

	// VARIABLE DECLARATION
	RECTANGLE rectangleForD[] = {
						{ // ORANGE RECTANGLE
							{ x - hight / 2, y + hight / 2 + width, 0.0f },
							{ x - hight / 2  , y + hight / 2, 0.0f},
							{ x + hight / 2 , y + hight / 2 , 0.0f },
							{ x + hight / 2 , y + hight / 2 + width , 0.0f }
						},
						{ // GREEN RECTANGLE
							{ x - hight / 2, y - hight / 2 , 0.0f },
							{ x - hight / 2  , y - hight / 2 - width, 0.0f},
							{ x + hight / 2 , y - hight / 2 - width, 0.0f },
							{ x + hight / 2 , y - hight / 2  , 0.0f }
						},
						{ // RIGHT VERTICLE UP RECTANGLE
							{x + hight / 2, y + hight / 2, 0.0f },
							{x + hight / 2 - width , y + hight / 2, 0.0f },
							{x + hight / 2 - width , y , 0.0f },
							{x + hight / 2   , y , 0.0f }

						},
						{ // RIGHT VERTICLE DOWN RECTANGLE
							{x + hight / 2   , y , 0.0f },
							{x + hight / 2 - width , y , 0.0f },
							{x + hight / 2 - width , y - hight / 2, 0.0f },
							{x + hight / 2 , y - hight / 2, 0.0f }
						},
						{ // LEFT VERTICLE UP RECTANGLE
							{x - hight / 2 + width , y + hight / 2, 0.0f },
							{x - hight / 2 + width + width , y + hight / 2, 0.0f },
							{x - hight / 2 + width + width , y , 0.0f },
							{x - hight / 2 + width  , y , 0.0f }

						},
						{ // LEFT VERTICLE DOWN RECTANGLE
							{x - hight / 2 + width  , y , 0.0f },
							{x - hight / 2 + width + width , y , 0.0f },
							{x - hight / 2 + width + width , y - hight / 2, 0.0f },
							{x - hight / 2 + width  , y - hight / 2, 0.0f },

						}
	};

	const int numberOfRectangleForD = sizeof(rectangleForD) / sizeof(rectangleForD[0]);

	int arrayColorForD[numberOfRectangleForD * 2] = { ORANGE, ORANGE, GREEN, GREEN, ORANGE, WHITE, WHITE ,GREEN , ORANGE, WHITE, WHITE, GREEN };

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, 0.0f, -TRANSLATEZ);
	DrawRectangles(rectangleForD, numberOfRectangleForD, arrayColorForD);

}

void A(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y) {
	// FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);

	//VARIABLE DECLARATION

	RECTANGLE rectangleForA[] = {
								{ // TOP RECTANGLE
									{x + 0.14F , y + hight / 2 + width, 0.0f },
									{x - 0.14F , y + hight / 2 + width, 0.0f},
									{x - width , y + hight / 2 , 0.0f },
									{x + width , y + hight / 2, 0.0f }
								},
								{ // LEFT TOP RACTANGLE
									{x  , y + hight / 2, 0.0f },
									{x - width , y + hight / 2, 0.0f },
									{x - 0.40f , y , 0.0f },
									{x - 0.20f , y , 0.0f }
								},
								{ // LEFT DOWN RECTANGLE
									{x - 0.20f , y , 0.0f },
									{x - 0.40f , y , 0.0f },
									{x - hight / 2 , y - hight / 2, 0.0f },
									{x - hight / 2 + width, y - hight / 2, 0.0f }
								},
								{ // RIGHT TOP RACTANGLE
									{x + width , y + hight / 2, 0.0f },
									{x  , y + hight / 2, 0.0f },
									{x + 0.20f , y , 0.0f },
									{x + 0.40f , y , 0.0f }
								},
								{ // RIGHT DOWN RECTANGLE
									{x + 0.40f , y , 0.0f },
									{x + 0.20f , y , 0.0f },
									{x + hight / 2 - width, y - hight / 2, 0.0f },
									{x + hight / 2 , y - hight / 2, 0.0f }
								},
								{ // RIGHT DOWN SQUARE
									{x + hight / 2 , y - hight / 2, 0.0f },
									{x + hight / 2 - width, y - hight / 2, 0.0f },
									{x + hight / 2 - width + 0.075f , y - hight / 2 - width, 0.0f },
									{x + hight / 2 + 0.075f , y - hight / 2 - width, 0.0f }
								},
								{ // LEFT DOWN RECTANGLE
									{x - hight / 2 + width, y - hight / 2, 0.0f },
									{x - hight / 2 , y - hight / 2, 0.0f },
									{x - hight / 2 - 0.075f, y - hight / 2 - width, 0.0f },
									{x - hight / 2 + width - 0.075f, y - hight / 2 - width, 0.0f }
								},
	};

	const int numberOfRectangleForA = sizeof(rectangleForA) / sizeof(rectangleForA[0]);

	int arrayColorForA[numberOfRectangleForA * 2] = { ORANGE, ORANGE, ORANGE, WHITE, WHITE , GREEN, ORANGE, WHITE, WHITE, GREEN, GREEN , GREEN, GREEN, GREEN };

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, 0.0f, -TRANSLATEZ);
	DrawRectangles(rectangleForA, numberOfRectangleForA, arrayColorForA);
}

void Tiranga(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y) {
	// FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);

	RECTANGLE rectangleForTiranga[] = {
										{ // TOP RECTANGLE
											{x + 0.3f , y + 0.1f, 0.0f },
											{x - 0.3f , y + 0.1f, 0.0f},
											{x - 0.3f, y , 0.0f },
											{x + 0.3f , y , 0.0f }
										},
										{ // TOP RECTANGLE
											{x + 0.3f , y , 0.0f },
											{x - 0.3f , y , 0.0f},
											{x - 0.3f, y - 0.1f, 0.0f },
											{x + 0.3f , y - 0.1f, 0.0f }
										},
										{ // TOP RECTANGLE
											{x + 0.3f , y - 0.1f , 0.0f },
											{x - 0.3f , y - 0.1f , 0.0f},
											{x - 0.3f, y - 0.2f, 0.0f },
											{x + 0.3f , y - 0.2f, 0.0f }
										}
									};
	const int numberOfRectangleForTiranga = sizeof(rectangleForTiranga) / sizeof(rectangleForTiranga[0]);

	int arrayColorForTiranga[numberOfRectangleForTiranga * 2] = { ORANGE, ORANGE, WHITE, WHITE, GREEN, GREEN };

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, 0.0f, -TRANSLATEZ);
	DrawRectangles(rectangleForTiranga, numberOfRectangleForTiranga, arrayColorForTiranga);
}
