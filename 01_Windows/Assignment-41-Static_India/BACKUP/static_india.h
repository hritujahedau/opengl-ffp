#include <windows.h>
#include <gl\GL.h>

#define MYICON 5412
#define IDI_MYICON MAKEINTRESOURCE(MYICON)

enum {
	ORANGE = 1,
	GREEN,
	WHITE
};

typedef struct __POINTF {
	GLfloat x, y, z;
} POINTF;

typedef struct __RECTANGLE {
	POINTF a, b, c, d;
} RECTANGLE;

GLfloat x = 0.5f, y = 1.1f, width = 0.2f, hight = 2.0f, extraWidth = 0.3f, bigGap = 0.8f, smallGap = 0.6f;

// FOR I

RECTANGLE rectangleForI[] = {
						{
							{ -x, y, 0.0f },
							{-x, y - width, 0.0f},
							{ x , y - width, 0.0f },
							{ x, y, 0.0f }
						},
						{
							{ x - extraWidth , y-width, 0.0f },
							{ -x + extraWidth, y-width, 0.0f} ,
							{-x + extraWidth, y - hight/2, 0.0f },
							{ x - extraWidth, y - hight/2, 0.0f },
						},
						{
							{ x - extraWidth , y - hight / 2, 0.0f },
							{ -x + extraWidth, y - hight / 2, 0.0f} ,
							{-x + extraWidth, y - hight, 0.0f },
							{ x - extraWidth, y - hight, 0.0f },
						},
						{
							{ -x, y - hight , 0.0f },
							{-x, y - width - hight, 0.0f},
							{ x , y - width - hight, 0.0f },
							{ x, y - hight, 0.0f }
						}
};

const int numberOfRectangleForI = sizeof(rectangleForI) / sizeof(rectangleForI[0]);

int arrayColorForI[numberOfRectangleForI * 2] = { ORANGE, ORANGE, ORANGE, WHITE, WHITE, GREEN, GREEN, GREEN };

// FOR N

RECTANGLE rectangleForN[] = {
						{
							{ x - extraWidth , y , 0.0f },
							{ -x + extraWidth, y , 0.0f} ,
							{-x + extraWidth, y - hight / 2, 0.0f },
							{ x - extraWidth, y - hight / 2, 0.0f },
						},
						{
							{ x - extraWidth , y - hight / 2, 0.0f },
							{ -x + extraWidth, y - hight / 2, 0.0f} ,
							{-x + extraWidth, y - hight - width, 0.0f },
							{ x - extraWidth, y - hight - width, 0.0f },
						},
						{
							{ x - extraWidth + bigGap , y , 0.0f },
							{ -x + extraWidth + bigGap, y , 0.0f} ,
							{-x + extraWidth + bigGap, y - hight / 2, 0.0f },
							{ x - extraWidth + bigGap, y - hight / 2, 0.0f },
						},
						{
							{ x - extraWidth + bigGap, y - hight / 2, 0.0f },
							{ -x + extraWidth + bigGap, y - hight / 2, 0.0f} ,
							{-x + extraWidth + bigGap, y - hight - width, 0.0f },
							{ x - extraWidth + bigGap, y - hight - width, 0.0f },
						},
						{
							{ x - extraWidth , y , 0.0f },
							{ x - extraWidth , y - width , 0.0f },
							{ x - extraWidth  + bigGap  , y - hight , 0.0f },
							{ x - extraWidth + bigGap  , y - hight + width , 0.0f }
						}
						};

const int numberOfRectangleForN = sizeof(rectangleForN) / (sizeof(rectangleForN[0]));

int arrayColorForN[numberOfRectangleForN * 2] = { ORANGE, WHITE, WHITE, GREEN,ORANGE, WHITE, WHITE, GREEN , GREEN , WHITE };

/*
{
							{ x - extraWidth , y , 0.0f },
							{ x - extraWidth , y - extraWidth , 0.0f },
							{ x  , y - width - (hight/2), 0.0f },
							{ x  , y - hight /2 , 0.0f }

						},
						{
							{ x , y - width - (hight / 2), 0.0f },
							{ x , y - hight / 2 , 0.0f },
							{ x + extraWidth , y - hight , 0.0f },
							{ x + extraWidth , y - width - hight, 0.0f }
						}
*/
// for D
RECTANGLE rectangleForD[] = {
						{
							{ -x, y, 0.0f },
							{-x, y - width, 0.0f},
							{ x + bigGap/2 , y - width, 0.0f },
							{ x + bigGap/2, y, 0.0f }
						},
						{
							{ -x, y - hight, 0.0f },
							{-x, y - width - hight, 0.0f},
							{ x + bigGap / 2 , y - width - hight, 0.0f },
							{ x + bigGap / 2, y - hight, 0.0f }
						},
						{
							{ x - smallGap , y - width, 0.0f },
							{ -x + smallGap, y - width, 0.0f} ,
							{-x + smallGap, y - hight / 2, 0.0f },
							{ x - smallGap, y - hight / 2, 0.0f },
						},
						{
							{ x - smallGap , y - hight / 2, 0.0f },
							{ -x + smallGap, y - hight / 2, 0.0f} ,
							{-x + smallGap, y - hight, 0.0f },
							{ x - smallGap, y - hight, 0.0f },
						},
						{
							{ x + bigGap / 2, y - width, 0.0f },
							{ -x + bigGap / 2 + smallGap, y - width, 0.0f} ,
							{-x + bigGap / 2 + smallGap, y - hight / 2, 0.0f },
							{ x + bigGap / 2, y - hight / 2, 0.0f },
						},
						{
							{ x + bigGap/2, y - hight / 2, 0.0f },
							{ -x + bigGap / 2 + smallGap, y - hight / 2, 0.0f} ,
							{-x + bigGap / 2 + smallGap, y - hight, 0.0f },
							{ x + bigGap / 2, y - hight, 0.0f },
						}
};

const int numberOfRectangleForD = sizeof(rectangleForD) / (sizeof(rectangleForD[0]));

int arrayColorForD[numberOfRectangleForD * 2] = { ORANGE, ORANGE , GREEN, GREEN,ORANGE, WHITE , WHITE, GREEN,ORANGE, WHITE , WHITE, GREEN };

// for A

RECTANGLE rectangleForA[] = {				
							{
								{-0.2f, y, 0.0f},
								{-0.2f + extraWidth, y, 0.0f},
								{0.4f + extraWidth , y - hight, 0.0f},
								{0.4f , y - hight, 0.0f}
							},
							{
								{0.2f, y, 0.0f},
								{0.2f - extraWidth, y, 0.0f},
								{-0.4f - extraWidth , y - hight, 0.0f},
								{-0.4f , y - hight, 0.0f}
							}
							};

const int numberOfRectangleForA = sizeof(rectangleForA) / sizeof(rectangleForA[0]);

int arrayColorForA[numberOfRectangleForA * 2] = { ORANGE, WHITE};
