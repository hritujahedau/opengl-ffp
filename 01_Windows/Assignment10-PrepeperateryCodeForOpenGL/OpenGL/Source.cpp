#include<windows.h>
#include<stdio.h>
#define		win_width		800
#define		win_hight		600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullScreen = false;
HWND ghwnd = NULL;
int cx, cy;
FILE* gpFile = NULL;
bool gbActiveWindow = false;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPreviousInstance, LPSTR lszptrCammandLine, int iCmdShow) {

	// variale declaration
	WNDCLASSEX wndClassEx;
	HWND hwnd;
	MSG msg = { NULL,NULL,0,0 };
	TCHAR szAppName[] = TEXT("MyWindow");
	
	bool bDone = false;
	RECT rc;

	// function declaration
	void Initialize(void);
	void Display(void);
	
	// code 
	//initialization 
	if (fopen_s(&gpFile, "LogFile.log", "a+") != 0) {
		MessageBox(NULL, TEXT("NOT Able To Open File"), TEXT("File Opened Failed"), MB_OK);
		exit(0);
	}

	wndClassEx.hInstance = hInstance;
	wndClassEx.lpszClassName = szAppName;
	wndClassEx.lpfnWndProc = WndProc;
	wndClassEx.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClassEx.cbSize = sizeof(WNDCLASSEX);
	wndClassEx.cbClsExtra = 0;
	wndClassEx.cbWndExtra = 0;
	wndClassEx.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClassEx.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClassEx.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClassEx.lpszMenuName = NULL;
	wndClassEx.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;


	RegisterClassEx(&wndClassEx);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rc, 0);
	cx = (rc.right / 2) - (win_width / 2);
	cy = (rc.bottom / 2) - (win_hight / 2);
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("Centering Window"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, cx, cy, win_width, win_hight, NULL, NULL, hInstance, NULL);
	ghwnd = hwnd;

	Initialize();
	ShowWindow(hwnd, iCmdShow);
	//UpdateWindow(hwnd);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);


	// message loop

	/*while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}*/

	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) 
				bDone = true;			
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow == true) {
				// here you should call UpdateWindow() for OpenGL Rendering  
				//here you should call dsiplay() for OpenGL rendering 

				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen(void);
	void Resize(int, int);
	void Uninitialize(void);

	// variable declaration
	TCHAR str[255] = TEXT("");

	// code
	switch (uMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		//return 0L;
		break;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam) {
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;		
		default:
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	default:
		break;
	}

	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen(void) {
	//varibale declaration

	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (!gbFullScreen) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if ((dwStyle & WS_OVERLAPPEDWINDOW) && GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
			SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom = mi.rcMonitor.right), SWP_NOZORDER | SWP_FRAMECHANGED);
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize(void) {
	// function declaration
	void Resize(int, int);

	//variable declarations

	//code

	Resize(win_width, win_hight);

	// warm up call to resize

}

void Resize(int width, int hight) {
	//code
	if (hight == 0) {
		hight = 1;
	}
}

void Display(void) {
	//code
}

void Uninitialize(void) {
	if (gpFile != NULL) {
		fclose(gpFile);
		gpFile = NULL;
	}
}
