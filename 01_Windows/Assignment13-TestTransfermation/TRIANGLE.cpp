//Header File
#include<windows.h>
#include<stdio.h>
#include<gl/gl.h>
#include "headerfile.h"

#pragma comment(lib, "opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HIGHT 600

// Global Function Decalaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable
bool gbActiveWindow = false;
FILE* gpFile;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
HWND ghwnd;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool bFullScreen = false;

//code 
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	//function declaration
	void Initialize(void);
	void Display(void);

	//local variable decalaration
	WNDCLASSEX wndClass;
	MSG msg;
	TCHAR szClassName[] = TEXT("triangle");
	bool bDone = false;

	

	//code

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;

	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndClass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);

	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szClassName;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);

	ghwnd = CreateWindow(szClassName,
		TEXT("Triangle"),
		WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_CAPTION | WS_OVERLAPPED,
		100,
		100,
		WIN_WIDTH,
		WIN_HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	if (fopen_s(&gpFile, "window.log", "a") != 0) {
		MessageBox(NULL, TEXT("File Cannot open"), TEXT("File Operation FAILED!"), MB_OK);
		exit(0);
	}
	Initialize();
	fprintf(gpFile, "Intialize() completed");
	SetForegroundWindow(ghwnd);
	SetFocus(ghwnd);
	ShowWindow(ghwnd, iCmdShow);

	/*while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}*/


	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow == true) {
				Display();
			}
		}
	}

	return (int)(msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	//vlocal function declaration
	void ToggleFullScreen();
	void Resize(int, int);
	void Uninitialize(void);

	//code
	switch (uMsg) {
	case WM_SETFOCUS :
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return 0L;
		//break;
	case WM_SIZE :
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_KEYDOWN:
		switch (LOWORD(wParam)) {
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0X46:
		case 0X66:
			ToggleFullScreen();
			break;
		}
		break;

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {
	//local variable declaration
	/*static DWORD dwStyle;
	static WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
	static bool bFullScreen = false;*/
	MONITORINFO mi = { sizeof(MONITORINFO) };
	

	//code
	if (bFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, 
		SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
	else {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right, mi.rcMonitor.bottom,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
}

void Initialize(void) {
	//function declaration
	void Resize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	
	//code

	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() failed \n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "SetPixelFormat() Failed \n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "wglCreateContext() \n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "wglMakeCurrent() failed\n");
		DestroyWindow(ghwnd);
	
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	Resize(WIN_WIDTH, WIN_HIGHT);
}

void Resize(int width, int hight) {
	if (hight == 0) {
		hight = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)hight);
	//glMatrixMode(GL_PROJECTION);
	//glLoadIdentity();
}

void Display()
{
	//function declaration
	void WhiteColoredTriangle(void);
	void MultiColoredTriangle(void);
	void WhiteColoredRectangle(void);

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	WhiteColoredTriangle();

	glTranslatef(0.5f, 0.0f, 0.0f);
	MultiColoredTriangle();

	glTranslatef(0.0f, 0.5f, 0.0f);
	WhiteColoredRectangle();

	SwapBuffers(ghdc);
}

void WhiteColoredTriangle() {
	//code
	glBegin(GL_TRIANGLES);

	glVertex3f(0.0f, 0.1f, 0.0f);

	glVertex3f(-0.1f, -0.1f, 0.0f);

	glVertex3f(0.1f, -0.1f, 0.0f);

	glEnd();
}

void MultiColoredTriangle() {
	//code
	glBegin(GL_TRIANGLES);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.1f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.1f, -0.1f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.1f, -0.1f, 0.0f);

	glEnd();
}

void WhiteColoredRectangle() {
	//code
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(0.1f, 0.1f, 0.0f);

	glVertex3f(-0.1f, 0.1f, 0.0f);

	glVertex3f(-0.1f, -0.1f, 0.0f);

	glVertex3f(0.1f, -0.1f, 0.0f);

	glEnd();
}

void Uninitialize(void) {
	if (gpFile) {
		fclose(gpFile);
		gpFile = NULL;
	}

	if (bFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		bFullScreen = false;
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	
	if (gpFile) {
		fclose(gpFile);
		gpFile = NULL;
	}
}