#include <windows.h>
#include <stdio.h>
#include "header.h"
#include <gl/gl.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "opengl32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HWND ghwnd = NULL;
FILE* gpFile = NULL;
HGLRC ghrc;

bool gbFullscreen = false;
bool bDone = false;
bool gbActivateWindow = false;
HDC ghdc;
DWORD dwStyle;
WINDOWPLACEMENT wndPrev;


// entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE HPrevInstance, LPSTR szCmdName, int iCmdShow) {
	//function declaration
	void Initialize();
	void Display();

	MSG msg;
	TCHAR szClassName[] = TEXT("TRIANGLE");
	WNDCLASSEX wndClass;

	if (fopen_s(&gpFile, "windows.log", "a") != 0) {
		MessageBox(NULL, TEXT("ERROR"), TEXT("File Cannot Open"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;

	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);

	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_HREDRAW | CS_VREDRAW;

	RegisterClassEx(&wndClass);

	ghwnd = CreateWindow(
		szClassName,
		TEXT("Viewport 0"),
		WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_CAPTION,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	
	//UpdateWindow(ghwnd);
	Initialize();
	SetForegroundWindow(ghwnd);
	//SetFocus(ghwnd);
	ShowWindow(ghwnd, SW_SHOWNORMAL);

	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActivateWindow == true) {
				Display();
			}					
		}
	}
	

	if (gpFile) {
		fprintf(gpFile, "Closing File\n");
		fclose(gpFile);
		gpFile = NULL;
	}

	return (int)(msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	
	void ToggleFullscreen();
	void Uninitialize();
	void Resize(int, int,int,int);
	void Display();
	RECT rcWinClientArea;
	GetClientRect(hwnd, &rcWinClientArea);
	int hightByTwo = rcWinClientArea.bottom / 2, widthByTwo = rcWinClientArea.right / 2;

	TCHAR str[255] = TEXT("");
	switch (uMsg) {
	case WM_SETFOCUS:
		gbActivateWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActivateWindow = false;
		break;
	case WM_ERASEBKGND:
		return 0L;
	case WM_KEYDOWN:
		switch (wParam) {
		case 0x30:
			Resize(0, 0, rcWinClientArea.right, rcWinClientArea.bottom);
			break;
		case 0x31:
			Resize(0, hightByTwo, widthByTwo, hightByTwo);
			break;
		case 0x32:
			Resize(widthByTwo, hightByTwo, widthByTwo, hightByTwo);
			break;
		case 0x33:
			Resize(widthByTwo, 0, widthByTwo, hightByTwo);
			break;
		case 0x34:
			Resize(0, 0, widthByTwo, hightByTwo);
			break;
		case 0x35:
			Resize(0, 0, widthByTwo, hightByTwo * 2);
			break;
		case 0x36:
			Resize(widthByTwo, 0, widthByTwo, hightByTwo * 2);
			break;
		case 0x37:
			Resize(0, hightByTwo, widthByTwo * 2, hightByTwo);
			break;
		case 0x38:
			Resize(0, 0, widthByTwo * 2, hightByTwo);
			break;
		case 0x39:
			Resize(widthByTwo / 2, hightByTwo / 2, widthByTwo, hightByTwo);
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x66:
		case 0x46:
			ToggleFullscreen();
			break;
		}
		break;
	case WM_SIZE:
		Resize(0,0,LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	

	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullscreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };
	TCHAR str[255] = TEXT("");

	if (gbFullscreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wndPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi) ) {
				
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right,
					mi.rcMonitor.bottom,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
		wsprintf(str,
			"In Fullscreen wndPrev flags = %ld , showCmd = %ld , ptMinPosition.x = %d ptMinPosition.y = %d ptMaxPosition = %d ptMaxPosition.y = %d rcNormalPossition.left %d rcNormalPosition.top %d rcNormalPosition.right = %d rcNormalPosition.bottom %d",
			wndPrev.flags, wndPrev.showCmd,
			wndPrev.ptMinPosition.x, wndPrev.ptMinPosition.y,
			wndPrev.ptMaxPosition.x, wndPrev.ptMaxPosition.y,
			wndPrev.rcNormalPosition.left, wndPrev.rcNormalPosition.top, wndPrev.rcNormalPosition.right, wndPrev.rcNormalPosition.bottom);
		fprintf(gpFile, str);
		
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wndPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullscreen = false;
	}

}

void Initialize() {
	// function declaration
	void Resize(int,int,int,int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "ChoosePixelFormat() Failed");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "SetPixelFormat() Failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (NULL == ghrc) {
		fprintf(gpFile, "wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "wglMakeCurrent() Failed");
		DestroyWindow(ghwnd);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.1f);

	Resize(0,0,WIN_WIDTH, WIN_HEIGHT);

}

void Resize(int x, int y,int width, int hight) {
	if (hight == 0) {
		hight = 1;
	}
	glViewport(x, y, (GLsizei)width, (GLsizei)hight);
}

void Display() {
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBegin(GL_TRIANGLES);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);

	glEnd();
	SwapBuffers(ghdc);
}

void Uninitialize() {
	if (gpFile) {
		fclose(gpFile);
		gpFile = NULL;
	}

	if (gbFullscreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wndPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullscreen = false;
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
}
