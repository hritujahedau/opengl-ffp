#pragma once
#include <windows.h>
#include <gl\GL.h>

#define MYICON 1287
#define IDI_MYICON MAKEINTRESOURCE(MYICON)

typedef struct __POINTF {
	GLfloat x, y, z;
} POINTF;

typedef struct Tringle {
	POINTF appex, left, right;
} TRIANGLE;

typedef struct __COLORS {
	GLfloat red, green, blue;
} COLORS;

typedef struct __TRAINGLE_COLORS {
	COLORS side1, side2, side3;
} TRIANGLE_COLORS;
