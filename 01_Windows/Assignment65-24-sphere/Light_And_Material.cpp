// header file declaration
#include <windows.h>
#include<stdio.h>
#include "Light_And_Material.h"
#include <gl/gl.h>
#include <gl/glu.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define WIDTH 1000
#define HIGHT 800

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

bool bLight = false;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f }; // gray light
GLfloat lightDefuse[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // white
GLfloat lightPossition[] = { 100.0f, 100.0f, 100.0f, 1.0f }; // light from z axis
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };

GLfloat angle_x_for_rotate_light = 0.0f, angle_y_for_rotate_light = 0.0f, angle_z_for_rotate_light = 0.0f;
int key_x = 0, key_y = 0, key_z = 0;

// first column
// 1
GLfloat materialAmbient_emerald[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
GLfloat materialDefuse_emerald[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
GLfloat materialSpecular_emerald[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
GLfloat materialShinyness_emerald = 0.6 * 128;

// 2
GLfloat materialAmbient_jade[] = { 0.135f, 0.2225f, 0.1575f, 1.0f };
GLfloat materialDefuse_jade[] = { 0.54f, 0.89f, 0.63f, 1.0f };
GLfloat materialSpecular_jade[] = { 0.316228f, 0.316228f, 0.316228f, 1.0f };
GLfloat materialShinyness_jade = 0.1 * 128;

// 3
GLfloat materialAmbient_obsidian[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
GLfloat materialDefuse_obsidian[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
GLfloat materialSpecular_obsidian[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
GLfloat materialShinyness_obsidian = 0.3 * 128;

// 4 pearl
GLfloat materialAmbient_pearl[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
GLfloat materialDefuse_pearl[] =  { 1.0f, 0.829f, 0.829f, 1.0f };
GLfloat materialSpecular_pearl[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
GLfloat materialShinyness_pearl = 0.088 * 128;

// 5 ruby 
GLfloat materialAmbient_ruby[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
GLfloat materialDefuse_ruby[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
GLfloat materialSpecular_ruby[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
GLfloat materialShinyness_ruby = 0.6 * 128;

// 6 turquoise
GLfloat materialAmbient_turquoise[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
GLfloat materialDefuse_turquoise[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
GLfloat materialSpecular_turquoise[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
GLfloat materialShinyness_turquoise = 0.1 * 128;

// second column
// 7 brass 
GLfloat materialAmbient_brass[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
GLfloat materialDefuse_brass[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
GLfloat materialSpecular_brass[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
GLfloat materialShinyness_brass = 0.21794872 * 128;

// 8 bronze
GLfloat materialAmbient_bronze[] = { 0.2125f, 0.1275f, 0.054f, 1.0f };
GLfloat materialDefuse_bronze[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
GLfloat materialSpecular_bronze[] = { 0.393548f, 0.271906f, 0.166721f, 1.0f };
GLfloat materialShinyness_bronze = 0.2 * 128;

// 9 chrome
GLfloat materialAmbient_chrome[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat materialDefuse_chrome[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat materialSpecular_chrome[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
GLfloat materialShinyness_chrome = 0.2 * 128;

// 10 4th sphere on 2nd column, copper  
GLfloat materialAmbient_copper[] = { 0.19125f, 0.19125f, 0.0225f, 1.0f };
GLfloat materialDefuse_copper[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
GLfloat materialSpecular_copper[] = { 0.256777f, 0.137622f, 0.086014f, 1.0f };
GLfloat materialShinyness_copper = 0.1 * 128;

// 11 5th sphere on 2nd column, gold
GLfloat materialAmbient_gold[] = { 0.24725f, 0.1995f, 0.0745f, 1.0f };
GLfloat materialDefuse_gold[] = { 0.75164f, 0.60648f, 0.22648f, 1.0f };
GLfloat materialSpecular_gold[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
GLfloat materialShinyness_gold = 0.4 * 128;

// 12 6th sphere on 2nd column, silver
GLfloat materialAmbient_silver[] = { 0.19225, 0.1995f, 0.19225f, 1.0f };
GLfloat materialDefuse_silver[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
GLfloat materialSpecular_silver[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
GLfloat materialShinyness_silver = 0.4 * 128;

// 13 1st sphere on 3rd column, black 
GLfloat materialAmbient_black[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_black[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat materialSpecular_black[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat materialShinyness_black = 0.25 * 128;

// 14 2nd sphere on 3rd column, cyan
GLfloat materialAmbient_cyan[] = { 0.0, 0.1f, 0.06f, 1.0f };
GLfloat materialDefuse_cyan[] = { 0.0f, 0.50980392f, 0.50980392f, 1.0f };
GLfloat materialSpecular_cyan[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
GLfloat materialShinyness_cyan = 0.25 * 128;

// 15 3rd sphere on 2nd column, green
GLfloat materialAmbient_green[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_green[] = { 0.1f, 0.35f, 0.1f, 1.0f };
GLfloat materialSpecular_green[] = { 0.45f, 0.55f, 0.45f, 1.0f };
GLfloat materialShinyness_green = 0.25 * 128;

// 16 4th sphere on 3rd column, red
GLfloat materialAmbient_red[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_red[] = { 0.5, 0.0f, 0.0f, 1.0f };
GLfloat materialSpecular_red[] = { 0.7, 0.6f, 0.6f, 1.0f };
GLfloat materialShinyness_red = 0.25 * 128;

// 17 5th sphere on 3rd column, white
GLfloat materialAmbient_white[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_white[] = { 0.55, 0.55f, 0.55f, 1.0f };
GLfloat materialSpecular_white[] = { 0.7, 0.7f, 0.7f, 1.0f };
GLfloat materialShinyness_white = 0.25 * 128;

// 18 6th sphere on 3rd column, yellow plastic
GLfloat materialAmbient_plastic[] = { 0.0, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_plastic[] = { 0.5, 0.5f, 0.0f, 1.0f };
GLfloat materialSpecular_plastic[] = { 0.60, 0.60f, 0.50f, 1.0f };
GLfloat materialShinyness_plastic = 0.25 * 128;

// 19  1st sphere on 4th column, black
GLfloat materialAmbient_black_2[] = { 0.02, 0.02f, 0.02f, 1.0f };
GLfloat materialDefuse_black_2[] = { 0.01, 0.01f, 0.01f, 1.0f };
GLfloat materialSpecular_black_2[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat materialShinyness_black_2 = 0.078125 * 128;

// 20  2nd sphere on 4th column, cyan
GLfloat materialAmbient_cyan_2[] = { 0.0, 0.05f, 0.05f, 1.0f };
GLfloat materialDefuse_cyan_2[] = { 0.4, 0.5f, 0.5f, 1.0f };
GLfloat materialSpecular_cyan_2[] = { 0.04f, 0.7f, 0.7f, 1.0f };
GLfloat materialShinyness_cyan_2 = 0.078125 * 128;

// 21  3rd sphere on 4th column, green
GLfloat materialAmbient_green_2[] = { 0.0, 0.05f, 0.05f, 1.0f };
GLfloat materialDefuse_green_2[] = { 0.4, 0.5f, 0.4f, 1.0f };
GLfloat materialSpecular_green_2[] = { 0.04f, 0.7f, 0.04f, 1.0f };
GLfloat materialShinyness_green_2 = 0.078125 * 128;

// 22   4th sphere on 4th column, red 
GLfloat materialAmbient_red_2[] = { 0.05, 0.0f, 0.0f, 1.0f };
GLfloat materialDefuse_red_2[] = { 0.5, 0.4f, 0.4f, 1.0f };
GLfloat materialSpecular_red_2[] = { 0.7f, 0.04f, 0.04f, 1.0f };
GLfloat materialShinyness_red_2 = 0.078125 * 128;

// 23   5th sphere on 4th column, white
GLfloat materialAmbient_white_2[] = { 0.05, 0.05f, 0.05f, 1.0f };
GLfloat materialDefuse_white_2[] = { 0.5, 0.5f, 0.5f, 1.0f };
GLfloat materialSpecular_white_2[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat materialShinyness_white_2 = 0.078125 * 128;

// 24   6th sphere on 4th column, yellow rubber
GLfloat materialAmbient_rubber[] = { 0.05, 0.05f, 0.0f, 1.0f };
GLfloat materialDefuse_rubber[] = { 0.5, 0.5f, 0.4f, 1.0f };
GLfloat materialSpecular_rubber[] = { 0.7f, 0.7f, 0.04f, 1.0f };
GLfloat materialShinyness_rubber = 0.078125 * 128;


GLUquadric *quadric = NULL;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("twotwodshape");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "a+") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("Two 2D Shape"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case 'l':
			case 'L':
				if (bLight == true)
				{					
					bLight = false;
					glDisable(GL_LIGHTING);
				}
				else
				{
					key_x = 0;
					key_y = 0;
					key_z = 0;
					angle_x_for_rotate_light = 0.0f;
					angle_y_for_rotate_light = 0.0f;
					angle_z_for_rotate_light = 0.0f;
					bLight = true;
					glEnable(GL_LIGHTING);
				}
				break;
			case 'x':
			case 'X':
				key_x = 1;
				key_y = 0;
				key_z = 0;
				break;
			case 'y':
			case 'Y':
				key_x = 0;
				key_y = 1;
				key_z = 0;
				break;
			case 'z':
			case 'Z':
				key_x = 0;
				key_y = 0;
				key_z = 1;
				break;
			}
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
				break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// LIGHT
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDefuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPossition);

	glEnable(GL_LIGHT0);

	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);

	Resize(WIDTH, HIGHT);
}

void Resize(int width, int hight) {
	if (hight == 0)
		hight = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, ((GLfloat)width/ (GLfloat)hight), 0.1f, 100.0f);
}

void Display() {
	//FUNCTION DECLARATION
	
	// VARIABLE DECLARATION
	GLfloat translate_x = 0.0f, translate_y = 2.0f, translate_z = 0.0f, diiference_y = 0.7;

	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -7.0f);

	if (bLight == true)
	{
		glEnable(GL_LIGHTING);
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	if (key_x == 1)
	{
		glPushMatrix();
		glRotatef(angle_x_for_rotate_light, 1.0f, 0.0f, 0.0f);
		lightPossition[0] = angle_x_for_rotate_light;
		glLightfv(GL_LIGHT0, GL_POSITION, lightPossition);
		glPopMatrix();
		angle_x_for_rotate_light = angle_x_for_rotate_light + 0.5f;
		angle_y_for_rotate_light = 0.0f;
		angle_z_for_rotate_light = 0.0f;
	}
	if (key_y == 1)
	{
		glPushMatrix();
		glRotatef(angle_y_for_rotate_light, 0.0f, 1.0f, 0.0f);
		lightPossition[1] = angle_y_for_rotate_light;
		glLightfv(GL_LIGHT0, GL_POSITION, lightPossition);
		glPopMatrix();
		angle_x_for_rotate_light = 0.0f;
		angle_y_for_rotate_light = angle_y_for_rotate_light + 0.5f;
		angle_z_for_rotate_light = 0.0f;
	}

	if (key_z == 1)
	{
		glPushMatrix();
		glRotatef(angle_z_for_rotate_light, 1.0f, 0.0f, 0.0f);
		lightPossition[2] = angle_z_for_rotate_light;
		glLightfv(GL_LIGHT0, GL_POSITION, lightPossition);
		glPopMatrix();
		angle_z_for_rotate_light = angle_z_for_rotate_light + 0.5f;
		angle_y_for_rotate_light = 0.0f;
		angle_x_for_rotate_light = 0.0f;
	}
	
	// first row
	glPushMatrix();
	glTranslatef(translate_x - 1.5f, translate_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_emerald);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_emerald);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_emerald);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_emerald);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x - 0.5f, translate_y , translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_jade);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_jade);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_jade);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_jade);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 0.5f, translate_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_obsidian);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_obsidian);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_obsidian);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_obsidian);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 1.5f, translate_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_pearl);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_pearl);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_pearl);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_pearl);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	// second row
	glPushMatrix();
	glTranslatef(translate_x - 1.5f, translate_y- diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ruby);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_ruby);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ruby);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_ruby);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x - 0.5f, translate_y - diiference_y,  translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_turquoise);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_turquoise);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_turquoise);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_turquoise);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 0.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_brass);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_brass);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_brass);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_brass);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 1.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_bronze);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_bronze);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_bronze);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_bronze);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	diiference_y = diiference_y + 0.7f;
	// 3rd row
	glPushMatrix();
	glTranslatef(translate_x - 1.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_chrome);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_chrome);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_chrome);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_chrome);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x - 0.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_copper);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_copper);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_copper);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_copper);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 0.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_gold);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_gold);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_gold);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_gold);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 1.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_silver);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_silver);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_silver);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_silver);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	// 4th row
	diiference_y = diiference_y + 0.7;
	glPushMatrix();
	glTranslatef(translate_x - 1.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_black);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_black);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_black);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_black);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x - 0.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_cyan);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_cyan);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_cyan);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_cyan);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 0.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_green);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_green);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_green);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_green);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 1.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_red);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_red);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_red);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_red);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	// 5th row
	diiference_y = diiference_y + 0.7;
	glPushMatrix();
	glTranslatef(translate_x - 1.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_white);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_white);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_white);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_white);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x - 0.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_plastic);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_plastic);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_plastic);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_plastic);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 0.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_black_2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_black_2);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_black_2);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_black_2);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 1.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_cyan_2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_cyan_2);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_cyan_2);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_cyan_2);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	// 6th row
	// 5th row
	diiference_y = diiference_y + 0.7;
	glPushMatrix();
	glTranslatef(translate_x - 1.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_green_2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_green_2);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_green_2);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_green_2);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x - 0.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_red_2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_red_2);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_red_2);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_red_2);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 0.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_white_2);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_white_2);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_white_2);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_white_2);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	glPushMatrix();
	glTranslatef(translate_x + 1.5f, translate_y - diiference_y, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_rubber);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDefuse_rubber);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_rubber);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShinyness_rubber);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 30, 30); // normals are caluclate by glu sphear library, we don't need to provide explicitly
	glPopMatrix();

	

	SwapBuffers(ghdc);
}
void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
	}

	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

}
