// header file declaration
#include <windows.h>
#include<stdio.h>
#include "shaped_and_pattern.h"
#include <gl/gl.h>
#include <gl/glu.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define WIDTH 1000
#define HIGHT 800

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

GLfloat f_rectangle_points_x[] = { 0.0f, 1.0f, 2.0f, 3.0f };
GLfloat f_rectangle_points_y[] = { 0.0f, 1.0f, 2.0f, 3.0f };

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("shapes_and_pattern");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "a+") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("Shapes And Pattern"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	Resize(WIDTH, HIGHT);
}

void Resize(int width, int hight) {
	if (hight == 0)
		hight = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, ((GLfloat)width/ (GLfloat)hight), 0.1f, 100.0f);
}

void Display() {
	//FUNCTION DECLARATION
	void Dots();
	void Pattern1();
	void Pattern2();
	void Pattern3();
	void Pattern4();
	void Pattern5();
	void Pattern6();

	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0f, 0.0f, 20.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	glLineWidth(2.0f);
	glPushMatrix();
	Dots();
	glPopMatrix();
	glPushMatrix();
	Pattern1();
	glPopMatrix();
	glPushMatrix();	
	Pattern2();
	glPopMatrix();
	glPushMatrix();
	Pattern4();
	glPopMatrix();
	glPushMatrix();
	Pattern5();
	glPopMatrix();
	glPushMatrix();
	Pattern6();
	glPopMatrix();

	SwapBuffers(ghdc);
}

void Pattern6()
{
	glTranslatef(10.0f, -4.0f, 0.0f);
	for (int i = 0; i < 3; i++)
	{
		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < 4; j++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
		glEnd();
	}
	for (int j = 1; j < 4; j++)
	{
		glBegin(GL_LINE_LOOP);
		for (int i = 0; i < 4; i++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
		glEnd();
	}

	for (int i = 1; i < 4; i++)
	{
		glBegin(GL_LINE_STRIP);
		for (int j = 3; j > 0; j--)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
			glVertex2f(f_rectangle_points_x[i - 1], f_rectangle_points_y[j - 1]);
		}
		glEnd();
	}
}

void Pattern5()
{
	glTranslatef(0.0f, -4.0f, 0.0f);
	for (int i = 0; i < 4; i++)
	{
		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < 4; j++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
		glEnd();
	}
	for (int j = 0; j < 4; j++)
	{
		glBegin(GL_LINE_LOOP);
		for (int i = 0; i < 4; i++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
		glEnd();
	}

	for (int i = 1; i < 4; i++)
	{
		glBegin(GL_LINE_STRIP);
		for (int j = 3; j > 0; j--)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
			glVertex2f(f_rectangle_points_x[i-1], f_rectangle_points_y[j-1]);
		}
		glEnd();
	}

}

void Pattern4()
{
	glTranslatef(-10.0f, -4.0f, 0.0f);
	for (int i = 3; i < 4; i++)
	{
		glBegin(GL_LINE_STRIP);
		for (int j = 0; j < 4; j++)
		{
			glVertex2f(0.0f, 3.0f);
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
			if (j>0)
			{
				glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j-1]);
				glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
			}

		}
		glEnd();
	}

	for (int i = 0; i < 4; i++)
	{
		glBegin(GL_LINE_STRIP);
		for (int j = 0; j < 1; j++)
		{
			glVertex2f(0.0f, 3.0f);
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
			if (i>0)
			{
				glVertex2f(f_rectangle_points_x[i-1], f_rectangle_points_y[j]);
				glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
			}
		}
		glEnd();
	}
}

void Pattern2()
{
	glTranslatef(10.0f, 4.0f, 0.0f);

	glLineWidth(2.0f);

	//glBegin(GL_QUADS);

	for (int i = 0; i < 3; i++)
	{
		if (i == 0)
		{
			glColor3f(1.0f, 0.0f, 0.0f);
		}
		else if (i == 1)
		{
			glColor3f(0.0f, 1.0f, 0.0f);
		}
		else
		{
			glColor3f(0.0f, 0.0f, 1.0f);
		}
		glBegin(GL_QUADS);
		glVertex2f(f_rectangle_points_x[i+1], f_rectangle_points_y[0]);
		glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[0]);
		glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[3]);
		glVertex2f(f_rectangle_points_x[i + 1], f_rectangle_points_y[3]);
		glEnd();
	}

	glColor3f(1.0f, 1.0f, 1.0f);

	for (int i = 0; i < 4; i++)
	{
		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < 4; j++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
		glEnd();
	}


	for (int j = 0; j < 4; j++)
	{
		glBegin(GL_LINE_LOOP);
		for (int i = 0; i < 4; i++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
		glEnd();
	}

	
}

void Pattern1()
{
	glTranslatef(0.0f, 4.0f, 0.0f);
	
	for (int i = 0; i < 4; i++)
	{
		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < 4; j++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);			
		}
		glEnd();
	}

	
	for (int j = 0; j < 4; j++)
	{
		glBegin(GL_LINE_LOOP);
		for (int i = 0; i < 4; i++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
		glEnd();
	}	
}

void Dots()
{
	glTranslatef(-10.0f, 4.0f, 0.0f);

	glPointSize(5.0f);
	glBegin(GL_POINTS);

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			glVertex2f(f_rectangle_points_x[i], f_rectangle_points_y[j]);
		}
	}
	
	glEnd();

}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

}
