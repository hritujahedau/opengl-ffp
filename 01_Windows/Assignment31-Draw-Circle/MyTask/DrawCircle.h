#pragma once
#include <windows.h>
#include <gl\gl.h>

#define MYICON 1231
#define IDI_MYICON MAKEINTRESOURCE(MYICON)

#define SOUND 1232
#define IDI_SOUND MAKEINTRESOURCE(SOUND)

typedef struct __COLOR {
	GLfloat red, green, blue;
} COLOR;
