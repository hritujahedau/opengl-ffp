// header file declaration
#include <windows.h>
#include<stdio.h>
#include "rotating_light_on_steady_sphere_.h"
#include <gl/gl.h>
#include <gl/glu.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define WIDTH 1000
#define HIGHT 800

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;


GLfloat glLightPosition_zero[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat glLightAmbient_zero[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat glLightDiffuse_zero[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat glLightSpecular_zero[] = { 1.0f, 0.0f, 0.0f, 1.0f };

GLfloat glLightPosition_one[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat glLightAmbient_one[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat glLightDiffuse_one[] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat glLightSpecular_one[] = { 0.0f, 1.0f, 0.0f, 1.0f };


GLfloat glLightPosition_two[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat glLightAmbient_two[] = { 0.0f, 0.0f,0.0f, 1.0f };
GLfloat glLightDiffuse_two[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat gllightSpecular_two[] = { 0.0f, 0.0f, 1.0f, 1.0f };

GLfloat glMaterialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat glMaterialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat glMaterialDiffuse[] = { 0.5f, 0.2f, 0.7f, 1.0f };
//GLfloat glMaterialDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat glMaterialShininess = 128.0f;

bool gbLight = false;
GLUquadric* quadric = NULL;

GLfloat angle_for_rotate_light = 0;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	void Initialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("H_Light_And_Material");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 

	if (fopen_s(&gpFile, "windows.log", "a+") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("Light And Material"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIDTH/2 ,
		monitorRC.bottom / 2 - HIGHT/2 ,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	Initialize();
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
				Display();
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	void Resize(int,int);

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case 'l':
			case 'L':
				if (gbLight)
				{
					glDisable(GL_LIGHTING);
					gbLight = false;
				}
				else
				{
					glEnable(GL_LIGHTING);
					gbLight = true;
				}
				break;
			}
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Initialize() {
	// function declaration
	void Resize(int,int);

	// Function declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() failed");
		DestroyWindow(ghwnd);
	}
	
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		fprintf(gpFile, "\n wglCreateContext() failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\n wglMakeCurrent() failed");
		DestroyWindow(ghwnd);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glLightfv(GL_LIGHT0, GL_AMBIENT, glLightAmbient_zero);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, glLightDiffuse_zero);
	glLightfv(GL_LIGHT0, GL_SPECULAR, glLightSpecular_zero);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT1, GL_AMBIENT, glLightAmbient_one);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, glLightDiffuse_one);
	glLightfv(GL_LIGHT1, GL_SPECULAR, glLightSpecular_one);
	glEnable(GL_LIGHT1);

	glLightfv(GL_LIGHT2, GL_AMBIENT, glLightAmbient_two);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, glLightDiffuse_two);
	glLightfv(GL_LIGHT1, GL_SPECULAR, gllightSpecular_two);
	glEnable(GL_LIGHT2);

	glMaterialfv(GL_FRONT, GL_AMBIENT, glMaterialAmbient);
	glMaterialfv(GL_FRONT, GL_SPECULAR, glMaterialSpecular);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, glMaterialDiffuse);
	glMaterialf(GL_FRONT, GL_SHININESS, glMaterialShininess);


	Resize(WIDTH, HIGHT);
}

void Resize(int width, int hight) {
	if (hight == 0)
		hight = 1;

	glViewport(0, 0, (GLsizei) width,(GLsizei) hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, ((GLfloat)width/ (GLfloat)hight), 0.1f, 100.0f);
}

void Display() {
	// CODE
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);

	glPushMatrix();
	glRotatef(angle_for_rotate_light, 0.0f, 1.0f, 0.0f);
	glLightPosition_zero[0] = angle_for_rotate_light;
	glLightfv(GL_LIGHT0, GL_POSITION, glLightPosition_zero);
	glPopMatrix();

	glPushMatrix();
	glRotatef(angle_for_rotate_light, 0.0f, 0.0f, 1.0f);
	glLightPosition_one[1] = angle_for_rotate_light;
	glLightfv(GL_LIGHT1, GL_POSITION, glLightPosition_one);
	glPopMatrix();

	glPushMatrix();
	glRotatef(angle_for_rotate_light, 1.0f, 0.0f, 0.0f);
	glLightPosition_two[2] = angle_for_rotate_light;
	glLightfv(GL_LIGHT2, GL_POSITION, glLightPosition_two);
	glPopMatrix();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 1.0f, 30, 30);

	angle_for_rotate_light = angle_for_rotate_light + 0.2f;
	if (angle_for_rotate_light > 360.0f)
	{
		angle_for_rotate_light = 0.0f;
	}
	SwapBuffers(ghdc);
}

void Uninitialize() {

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
	}

	if (gpFile) {
		fprintf(gpFile, "\n Closing File Successfully");
		fclose(gpFile);
	}

	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

}
