#include<windows.h>
#include<stdio.h>
#include<gl/gl.h>

#pragma comment(lib,"opengl32.lib" )

#include "myicon.h"
#define 	win_width 	800
#define 	win_hight 	600

LRESULT CALLBACK WndProc (HWND, UINT ,WPARAM, LPARAM);
void ToggleFullScreen();
void Initialize();
void Resize(int,int);
void Display();
void Uninitialize();

bool bDone = false;
bool gbFullScreen = false;
bool gbActiveWindow = false;
DWORD dwStyle ;
HWND ghwnd;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};
HDC ghdc;
FILE *gpFile = NULL;
HGLRC ghglrc = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE  hPrevInstance, LPSTR szCmdName, int iCmdShow){
	
	WNDCLASSEX wndClassEx ;
	TCHAR szClassName[] = TEXT("HRBlueScreen");
	MSG msg;

	wndClassEx.cbSize = sizeof(WNDCLASSEX);
	wndClassEx.cbClsExtra = 0;
	wndClassEx.cbWndExtra = 0;

	wndClassEx.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndClassEx.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndClassEx.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClassEx.hInstance = hInstance;

	wndClassEx.lpszClassName = szClassName ;
	wndClassEx.lpszMenuName= NULL;

	wndClassEx.lpfnWndProc = WndProc;
	wndClassEx.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClassEx.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC ;
	
	if( fopen_s(&gpFile,"Window.log","a+")  != 0) {
		MessageBox(NULL, TEXT("Not Able to open file"), TEXT("FILE OPERATION "), MB_OK );
		exit(0);
	}	
	fprintf(gpFile, "-------------------- File open successfully ----------------------");
	RegisterClassEx(&wndClassEx);
	fprintf(gpFile, "RegisterClassEx()");
	ghwnd = CreateWindowEx(WS_EX_APPWINDOW, szClassName , TEXT("BLUE SCREEN"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,  100, 100, win_width ,  win_hight, NULL, NULL, hInstance, NULL);
	
	Initialize();
	fprintf(gpFile, "Initialize()");

	ShowWindow(ghwnd, iCmdShow);	
	SetForegroundWindow(ghwnd);
	SetFocus(ghwnd);
	fprintf(gpFile, "SetFocus()");

	while(bDone == false) 
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message== WM_QUIT)
			{
				bDone = true;				
			}else{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else{
			if(gbActiveWindow == true)
			{
				//Display();
			}				
		}	
	}
	
	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
	void ToggleFullScreen();
	void Resize(int,int);
	void Uninitialize();

	switch(uMsg){
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS : 
			gbActiveWindow = false;
			break;
		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_ERASEBKGND:
			break;
		case WM_KEYDOWN:
			switch(wParam){
				case VK_ESCAPE :
					DestroyWindow(hwnd);
					break;
				case  'f':
				case  'F':
					ToggleFullScreen();
					break;
				default :
					break;			
			}
			break;
		case WM_PAINT:
			Display();
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;

		default:
			break;
	}

	return (DefWindowProc(hwnd,uMsg,wParam,lParam));
}

void ToggleFullScreen(void){
	MONITORINFO mi = { sizeof(MONITORINFO)};
	if(gbFullScreen == false){
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW){
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY), &mi ) )
			{
				SetWindowLong(ghwnd,GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd , HWND_TOP , mi.rcMonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right- mi.rcMonitor.left) , (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER |  SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbFullScreen = true;
		}
	}
	else{
		SetWindowLong(ghwnd,GWL_STYLE, dwStyle | WS_OVERLAPPED);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0,  0, 0,  0, SWP_NOMOVE | SWP_NOSIZE |  SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}	
}

void Initialize(void){
	//function declaration	
	void Resize(int,int);
	
	//local variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code

	 ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
	pfd.iPixelType = PFD_TYPE_RGBA ;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	
	if ( iPixelFormatIndex == 0 ){
		fprintf(gpFile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(ghwnd);
	}

	if ( SetPixelFormat (ghdc, iPixelFormatIndex, &pfd) == FALSE){
		fprintf ( gpFile , "SetPixelFormat() Failed");
		DestroyWindow(ghwnd);		
	}
	
	ghglrc = wglCreateContext(ghdc);
	if(NULL == ghglrc){
		fprintf(gpFile , "wglCreateContext() Failed");
		DestroyWindow(ghwnd);
	}

	if ( FALSE == wglMakeCurrent( ghdc, ghglrc) ){
		fprintf(gpFile, "wglMakeCurrent() Failed");
		DestroyWindow(ghwnd);
	}

	//set Clear color openGl First Program
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	Resize( win_width, win_hight);	
}

void Display(void){
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBegin(GL_TRIANGLES);

		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);

	glEnd();

	glFlush();
}

void Resize(int width , int hight){
	if(hight ==0)
		hight =1;
	glViewport(0,0,(GLsizei) width, (GLsizei) hight);
}

void Uninitialize(void){
	if(gbFullScreen == true){
		SetWindowLong(ghwnd, GWL_STYLE , WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd , &wpPrev ); 
		SetWindowPos(ghwnd , HWND_TOP , 0 , 0 , 0 , 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED );
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
	if(wglGetCurrentContext() == ghglrc){
		wglMakeCurrent(NULL, NULL);
	}

	if(ghglrc){
		wglDeleteContext(ghglrc);
	}
	
	if(ghdc){
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if(gpFile){
		fclose(gpFile);
		gpFile = NULL;
	}
}
