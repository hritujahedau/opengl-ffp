#pragma once
#include <windows.h>
#include <gl\GL.h>

#define MYICON 1287
#define KUNDAL_BITMAP 1288
#define STONE_BITMAP 12889

#define IDI_MYICON MAKEINTRESOURCE(MYICON)
#define ID_KUNDALI MAKEINTRESOURCE(KUNDAL_BITMAP)
#define ID_STONE MAKEINTRESOURCE(STONE_BITMAP)


typedef struct __POINTF {
	GLfloat x, y, z;
} POINTF;

typedef struct __RECTANGLE {
	POINTF a, b, c, d;
} RECTANGLE;

typedef struct Tringle {
	POINTF appex, left, right;
} TRIANGLE;
