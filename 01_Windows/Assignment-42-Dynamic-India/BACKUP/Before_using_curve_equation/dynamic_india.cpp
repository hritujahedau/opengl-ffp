// HEADER FILES
#include "dynamic_india.h"
#include <stdio.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <mmsystem.h>
#include <math.h>

// MACRO
#define WIDTH 800
#define HIGHT 600
#define TIMER_ID 1212
#define TIME_FOR_TIMER 6000

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib, "winmm.lib")

// GLOBAL FUNCTIONS
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// GLOBAL VARIABLES
bool gbActiveWindow = false;
bool gbDone = false;
bool gbFullScreen = false;

HWND ghwnd = NULL;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle = 0;

HDC ghdc = NULL;
HGLRC ghrc = NULL;

FILE* gpFile = NULL;

RECT rcMonitor;

GLfloat positionForI_1 = -7.5f, positionForN = 3.8f;
GLfloat positionForI_2 = -(positionForN - 0.5), positionForA = 7.0f;
GLfloat xpositionForIAFPlane = -6.3f;
GLfloat xpositionForUpIAFPlane = xpositionForIAFPlane, ypositionForUpIAFPlane = 4.0f, angleForUpIAFPlane = 300.f;
GLfloat xpositionForDownIAFPlane = xpositionForIAFPlane, ypositionForDownIAFPlane = -4.0f, angleForDownIAFPlane = 60.0f;
int showFlag = 0, startPlane = 0;
GLfloat rotateFlag = 0.0f;
GLfloat speed = 0.0033f, planSpeed = 0.0035f, planAngle = 0.09f; // 0.0035f;
int showI_1 = 0, showN = 0, showD = 0, showI_2 = 0, showA = 0;
int showCharacter = 1;
int scatterPlane = 0;

// ENTRY POINT FUNCTION
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// FUNCTION DECLARATION 
	void Initialize();
	void Display();
	void Update();

	// VARIABLE DECLARATION 
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("DYNAMICINDIA");
	WNDCLASSEX wndClass;

	//CODE
	if (fopen_s(&gpFile, "openGL_Logs.log", "a+") != 0) {
		MessageBox(NULL, TEXT("Open File Operation Failes"), TEXT("FAILED"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;

	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);

	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW;

	RegisterClassEx(&wndClass);

	SystemParametersInfo(SPI_GETWORKAREA, 0, &rcMonitor, 0);
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("DYNAMIC INDIA"),
		WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_OVERLAPPED | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		rcMonitor.right / 2 - WIDTH / 2,
		rcMonitor.bottom / 2 - HIGHT / 2,
		WIDTH,
		HIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;
	Initialize();
	if (PlaySound(IDI_SOUND, GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC) == FALSE) {
		MessageBox(ghwnd, TEXT("Error in playing music"), TEXT("Error"), MB_OK);
	}
	SetForegroundWindow(ghwnd);
	SetFocus(ghwnd);
	ShowWindow(ghwnd, iCmdShow);

	while (gbDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gbDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow == true) {
				Update();
				Display();
			}
		}
	}

	return ((int)(msg.wParam));

}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// FUNCTION DECLARATION
	void ToggleFullScreen();
	void Resize(int, int);
	void Uninitialize();

	switch (uMsg) {
	case WM_CREATE:
		SetTimer(hwnd, TIMER_ID, TIME_FOR_TIMER, NULL);
		break;
	case WM_TIMER:
		KillTimer(hwnd, TIMER_ID);
		showI_1 = 1;
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return 0L;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_XBUTTONDBLCLK:
		ToggleFullScreen();
		break;
	case WM_KEYDOWN:
		switch (wParam) {
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}

	return (DefWindowProc(hwnd, uMsg, wParam, lParam));

}

void Initialize() {
	//FUNCTION DECLARATION 
	void Resize(int, int);

	//VARIABLE DECLARATION
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	//CODE
	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		fprintf(gpFile, "\nChoosePixelFormat() Failed");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		fprintf(gpFile, "\nSetPixelFormat() Failed");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (NULL == ghrc) {
		fprintf(gpFile, "\nwglCreateContext() Failed");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		fprintf(gpFile, "\nwglMakeCurrent() Failed");
		DestroyWindow(ghwnd);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	Resize(WIDTH, HIGHT);
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

void Resize(int width, int hight) {
	if (hight == 0) {
		hight = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)hight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, ((GLfloat)width / (GLfloat)hight), 0.1f, 100.0f);
}

void Uninitialize() {
	if (gpFile) {
		fclose(gpFile);
		gpFile = NULL;
	}

	if (gbFullScreen == true) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

}

void Display() {
	//function declaration
	void I(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void N(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void D(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void A(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void Tiranga(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void IAFPlane(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);

	// VARIABLE DECLARATION
	typedef void (*FUNCTION) (GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	FUNCTION  function[] = { I, A , D , N, I , IAFPlane , IAFPlane , IAFPlane }; // 8
	const int NumberOfCharacters = (sizeof(function) / sizeof(function[0]));
	GLfloat coordinates[NumberOfCharacters][5] = {
									{ // I_1
										positionForI_1, 0.0f, 0.0f, 0.0f, 0.0f
									},
									{ // A
										positionForA, 0.0F, 0.0f, 0.0f, 0.0f
									},
									{ // D
										0.0f, 0.0f, 0.0f, 0.0f, 0.0f
									},
									{ // N
										-1.5f, positionForN , 0.0f,0.0f, 0.0f
									},									
									{ // I_2
										1.5f, positionForI_2, 0.0f, 0.0f, 0.0f
									},									
									{ // IAFPlane
										xpositionForIAFPlane, 0.0f, 0.0f, -0.05f, 0.0f
									},
									{ // IAFPlane
										xpositionForUpIAFPlane, ypositionForUpIAFPlane, 0.0f, 0.0f,angleForUpIAFPlane
									},
									{ // IAFPlane
										xpositionForDownIAFPlane, ypositionForDownIAFPlane, 0.0f, 0.0f,angleForDownIAFPlane
									}
	};

	glClear(GL_COLOR_BUFFER_BIT);
	if (showFlag == 1) {
		Tiranga(5.0f, 0.0f, -2.0f, 0.0f, 0.0f);
	}
	for (int i = 0; i < showCharacter; i++) {
		(*function[i])(coordinates[i][0], coordinates[i][1], coordinates[i][2], coordinates[i][3], coordinates[i][4]);
	}
	
	SwapBuffers(ghdc);
}

void Update() {
	// FUNCTION DECLARATION
	if (showI_1 == 1){
		if (positionForI_1 <= FINAL_POSITION_FOR_I_1) {
			positionForI_1 += speed;
		}
		else {
			showA = 1;
			showCharacter = 2;
		}
	}
	if (showA == 1) {
		if (positionForA >= FINAL_POSITION_FOR_A) {
			positionForA -= speed;
		}
		else {
			showD = 1;
			showCharacter = 3;
		}
	}

	if (tricolorsForD.WHITE.red >= 1) {
		showCharacter = 4;
		showN = 1;
	}

	if (showN == 1) {
		if (positionForN >= FINAL_POSITION_FOR_N) {
			positionForN -= speed;
		}
		else {
			showCharacter = 5;
			showI_2 = 1;			
		}
	}

	if (showI_2 == 1) 
	{
		if (positionForI_2 <= FINAL_POSITION_FOR_I_2) {
			positionForI_2 += speed;
		}
		else {
			showCharacter = 8;
			startPlane = 1;
		}
	}
	if (startPlane == 1 && scatterPlane ==0) {
		xpositionForUpIAFPlane += planSpeed;
		if (angleForUpIAFPlane <= 360.0f) {
			angleForUpIAFPlane += planAngle;
		}
		if (ypositionForUpIAFPlane >= -0.05f) {
			ypositionForUpIAFPlane += (-0.006);
		}
		xpositionForDownIAFPlane += planSpeed;
		if (angleForDownIAFPlane >= 0.0f) {
			angleForDownIAFPlane -= planAngle;
		}
		if (ypositionForDownIAFPlane <= -0.05f) {
			ypositionForDownIAFPlane += 0.006;
		}
		xpositionForIAFPlane += planSpeed;
		if (xpositionForIAFPlane > 4.3) {
			scatterPlane = 1;
		}
	}
	if (scatterPlane == 1) {
		xpositionForUpIAFPlane += planSpeed;
		xpositionForDownIAFPlane += planSpeed;
		xpositionForIAFPlane += planSpeed;

		angleForUpIAFPlane += planAngle;
		ypositionForUpIAFPlane -= (-0.006);

		angleForDownIAFPlane -= planAngle;
		ypositionForDownIAFPlane -= 0.006;
	}

	if (xpositionForIAFPlane >= FINAL_POSITION_FOR_A) {
		showFlag = 1;
	}
	if (rotateFlag <= 180.0f) {
		rotateFlag += 0.07;
	}
}

void DrawRectangles(RECTANGLE* rectangles, int numberOfRectangle, int* arrayOfColor) {
	//FUNCTION DECLARATION
	void DrawColor(int, TRICOLORS);
	for (int i = 0; i < numberOfRectangle; i++) {
		glBegin(GL_QUADS);
		DrawColor(arrayOfColor[i * 2], tricolors);
		glVertex3f(rectangles[i].a.x, rectangles[i].a.y, rectangles[i].a.z);
		glVertex3f(rectangles[i].b.x, rectangles[i].b.y, rectangles[i].b.z);
		DrawColor(arrayOfColor[i * 2 + 1], tricolors);
		glVertex3f(rectangles[i].c.x, rectangles[i].c.y, rectangles[i].c.z);
		glVertex3f(rectangles[i].d.x, rectangles[i].d.y, rectangles[i].d.z);
		glEnd();
	}
}

void DrawColor(int color, TRICOLORS tricolors) {
	if (color == ORANGE_ID) {
		glColor3f(tricolors.ORANGE.red, tricolors.ORANGE.green, tricolors.ORANGE.blue);
	}
	else if (color == GREEN_ID) {
		glColor3f(tricolors.GREEN.red, tricolors.GREEN.green, tricolors.GREEN.blue);
	}
	else if (color == WHITE_ID) {
		glColor3f(tricolors.WHITE.red, tricolors.WHITE.green, tricolors.WHITE.blue);
	}
	else if (color == NAVYBLUE_ID) {
		glColor3f(0.0f, 0.19f, 0.56f);
	}
	else if (color == BLACK_ID) {
		glColor3f(0.0f, 0.0f, 0.0f);
	}
	else {
		glColor3f(1.0f, 1.0f, 1.0f);
	}
}


void UpdateColor() {
	//ORANGE
	if (tricolorsForD.ORANGE.red < tricolors.ORANGE.red) {
		tricolorsForD.ORANGE.red += icreaseColorBy;
	}
	if (tricolorsForD.ORANGE.green < tricolors.ORANGE.green) {
		tricolorsForD.ORANGE.green += icreaseColorBy;
	}

	// GREEN
	if (tricolorsForD.GREEN.red < tricolors.GREEN.red) {
		tricolorsForD.GREEN.red += icreaseColorBy;
	}
	if (tricolorsForD.GREEN.green < tricolors.GREEN.green) {
		tricolorsForD.GREEN.green += icreaseColorBy;
	}

	// WHITE
	if (tricolorsForD.WHITE.red < tricolors.WHITE.red) {
		tricolorsForD.WHITE.red += icreaseColorBy;
	}
	if (tricolorsForD.WHITE.green < tricolors.WHITE.green) {
		tricolorsForD.WHITE.green += icreaseColorBy;
	}
	if (tricolorsForD.WHITE.blue < tricolors.WHITE.blue) {
		tricolorsForD.WHITE.blue += icreaseColorBy;
	}
}


// FOR I
void I(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y, GLfloat angle) {
	//FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);
	// VARIABLE DECLARATION
	RECTANGLE rectangleForI[] = {
						{ // ORANGE RECTANGLE
							{ x - hight / 2, y + hight / 2 + width, 0.0f },
							{ x - hight / 2  , y + hight / 2, 0.0f},
							{ x + hight / 2 , y + hight / 2 , 0.0f },
							{ x + hight / 2 , y + hight / 2 + width , 0.0f }
						},

						{ // GREEN RECTANGLE
							{ x - hight / 2, y - hight / 2 , 0.0f },
							{ x - hight / 2  , y - hight / 2 - width, 0.0f},
							{ x + hight / 2 , y - hight / 2 - width, 0.0f },
							{ x + hight / 2 , y - hight / 2  , 0.0f }
						},
						{ // MIDDLE VERTICLE UP RECTANGLE
							{x + width / 2, y + hight / 2, 0.0f },
							{x - width / 2, y + hight / 2, 0.0f },
							{x - width / 2, y , 0.0f },
							{x + width / 2 , y , 0.0f }

						},
						{ // MIDDLE VERTICLE DOWN RECTANGLE
							{x + width / 2 , y , 0.0f },
							{x - width / 2, y , 0.0f },
							{x - width / 2, y - hight / 2 },
							{x + width / 2, y - hight / 2 , 0.0f}
						}
	};
	const int numberOfRectangleForI = sizeof(rectangleForI) / sizeof(rectangleForI[0]);

	int arrayColorForI[numberOfRectangleForI * 2] = { ORANGE_ID, ORANGE_ID, GREEN_ID, GREEN_ID, ORANGE_ID, WHITE_ID, WHITE_ID, GREEN_ID };

	// CODE
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, translatefy, -TRANSLATEZ);

	DrawRectangles(rectangleForI, numberOfRectangleForI, arrayColorForI);
}

void N(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y, GLfloat angle) {
	// FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);
	// VARIABLE DECLARATION
	RECTANGLE rectangleForN[] = {
		// RIGHT TOP SQAURE
		{
			{ x - hight / 2 + width, y + hight / 2 + width, 0.0f },
			{ x - hight / 2  , y + hight / 2 + width, 0.0f},
			{ x - hight / 2  , y + hight / 2, 0.0f},
			{ x - hight / 2 + width, y + hight / 2 , 0.0f }
		},
		{ // RIGHT UP SIDE RECTANGLE
			{ x - hight / 2 + width, y + hight / 2 , 0.0f },
			{ x - hight / 2  , y + hight / 2, 0.0f},
			{ x - hight / 2, y , 0.0f},
			{ x - hight / 2 + width, y , 0.0f }
		},
		{ // RIGHT DOWN SIDE RECTANGLE
			{ x - hight / 2 + width, y , 0.0f },
			{ x - hight / 2, y , 0.0f},
			{ x - hight / 2 , y - hight / 2 , 0.0f },
			{ x - hight / 2 + width, y - hight / 2 , 0.0f }
		},
		{// LEFT BOTTOM SQAURE
			{ x - hight / 2 + width, y - hight / 2 , 0.0f },
			{ x - hight / 2 , y - hight / 2 , 0.0f },
			{ x - hight / 2 , y - hight / 2 - width , 0.0f},
			{ x - hight / 2 + width, y - hight / 2 - width , 0.0f}
		},
		{   // LEFT TOP SQUARE
			{ x + hight / 2 - width , y + hight / 2 + width, 0.0f},
			{ x + hight / 2  , y + hight / 2 + width, 0.0f},
			{ x + hight / 2  , y + hight / 2, 0.0f},
			{ x + hight / 2 - width, y + hight / 2 , 0.0f }
		},
		{ // LEFT UP SIDE RECTANGLE
			{ x + hight / 2 - width, y + hight / 2 , 0.0f },
			{ x + hight / 2  , y + hight / 2, 0.0f},
			{ x + hight / 2, y , 0.0f},
			{ x + hight / 2 - width, y , 0.0f }
		},
		{ // LEFT DOWN SIDE RECTANGLE
			{ x + hight / 2 - width, y , 0.0f },
			{ x + hight / 2, y , 0.0f},
			{ x + hight / 2 , y - hight / 2 , 0.0f },
			{ x + hight / 2 - width, y - hight / 2 , 0.0f }
		},
		{ // LEFT DOWN SQUARE
			{ x + hight / 2 , y - hight / 2 , 0.0f },
			{ x + hight / 2 - width, y - hight / 2 , 0.0f },
			{ x + hight / 2 - width , y - hight / 2 - width, 0.0f },
			{ x + hight / 2  , y - hight / 2 - width , 0.0f }
		},
		{ // MIDDLE UP RECTANGLE
			{ x - hight / 2 + width, y + hight / 2 + width, 0.0f },
			{ x - hight / 2 + width, y + hight / 2 , 0.0f },
			{ x - 0.06f, y , 0.0f },
			{ x + 0.06f, y , 0.0f }
		},
		{// MIDDLE DOWN RECTANGLE
			{ x + 0.06f, y , 0.0f},
			{ x - 0.06f, y , 0.0f},
			{ x + (hight / 2) - width, y - hight / 2 - width , 0.0f },
			{ x + (hight / 2) - width, y - hight / 2 , 0.0f }
		}

	};

	const int numberOfRectangleForN = sizeof(rectangleForN) / sizeof(rectangleForN[0]);

	int arrayColorForN[numberOfRectangleForN * 2] = {
														ORANGE_ID , ORANGE_ID, // RIGHT TOP SQAURE
														ORANGE_ID, WHITE_ID,   // RIGHT UP SIDE RECTANGLE
														WHITE_ID, GREEN_ID,    // RIGHT DOWN SIDE RECTANGLE
														GREEN_ID , GREEN_ID,	 // RIGHT DOWN SQUARE
														ORANGE_ID, ORANGE_ID,	  // LEFT TOP SQAURE
														ORANGE_ID, WHITE_ID,	 // LEFT UP SIDE RECTANGLE
														WHITE_ID, GREEN_ID,	 // LEFT DOWN SIDE RECTANGLE
														GREEN_ID, GREEN_ID,   // LEFT DOWN SQUARE
														ORANGE_ID, WHITE_ID,	 // MIDDLE UP RECTANGLE
														WHITE_ID, GREEN_ID	 // MIDDLE DOWN RECTANGLE
	};

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, translatefy, -TRANSLATEZ);
	DrawRectangles(rectangleForN, numberOfRectangleForN, arrayColorForN);

}

void D(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y, GLfloat angle) {
	// FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);
	void DrawColor(int, TRICOLORS);
	void UpdateColor();

	// VARIABLE DECLARATION
	RECTANGLE rectangleForD[] = {
						{ // ORANGE RECTANGLE
							{ x - hight / 2, y + hight / 2 + width, 0.0f },
							{ x - hight / 2  , y + hight / 2, 0.0f},
							{ x + hight / 2 , y + hight / 2 , 0.0f },
							{ x + hight / 2 , y + hight / 2 + width , 0.0f }
						},
						{ // GREEN RECTANGLE
							{ x - hight / 2, y - hight / 2 , 0.0f },
							{ x - hight / 2  , y - hight / 2 - width, 0.0f},
							{ x + hight / 2 , y - hight / 2 - width, 0.0f },
							{ x + hight / 2 , y - hight / 2  , 0.0f }
						},
						{ // RIGHT VERTICLE UP RECTANGLE
							{x + hight / 2, y + hight / 2, 0.0f },
							{x + hight / 2 - width , y + hight / 2, 0.0f },
							{x + hight / 2 - width , y , 0.0f },
							{x + hight / 2   , y , 0.0f }

						},
						{ // RIGHT VERTICLE DOWN RECTANGLE
							{x + hight / 2   , y , 0.0f },
							{x + hight / 2 - width , y , 0.0f },
							{x + hight / 2 - width , y - hight / 2, 0.0f },
							{x + hight / 2 , y - hight / 2, 0.0f }
						},
						{ // LEFT VERTICLE UP RECTANGLE
							{x - hight / 2 + width , y + hight / 2, 0.0f },
							{x - hight / 2 + width + width , y + hight / 2, 0.0f },
							{x - hight / 2 + width + width , y , 0.0f },
							{x - hight / 2 + width  , y , 0.0f }

						},
						{ // LEFT VERTICLE DOWN RECTANGLE
							{x - hight / 2 + width  , y , 0.0f },
							{x - hight / 2 + width + width , y , 0.0f },
							{x - hight / 2 + width + width , y - hight / 2, 0.0f },
							{x - hight / 2 + width  , y - hight / 2, 0.0f },

						}
	};

	const int numberOfRectangleForD = sizeof(rectangleForD) / sizeof(rectangleForD[0]);

	int arrayColorForD[numberOfRectangleForD * 2] = { ORANGE_ID, ORANGE_ID, GREEN_ID, GREEN_ID, ORANGE_ID, WHITE_ID, WHITE_ID ,GREEN_ID , ORANGE_ID, WHITE_ID, WHITE_ID, GREEN_ID };

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, translatefy, -TRANSLATEZ);
	for (int i = 0; i < numberOfRectangleForD; i++) {
		glBegin(GL_QUADS);
		DrawColor(arrayColorForD[i * 2], tricolorsForD);
		glVertex3f(rectangleForD[i].a.x, rectangleForD[i].a.y, rectangleForD[i].a.z);
		glVertex3f(rectangleForD[i].b.x, rectangleForD[i].b.y, rectangleForD[i].b.z);
		DrawColor(arrayColorForD[i * 2 + 1], tricolorsForD);
		glVertex3f(rectangleForD[i].c.x, rectangleForD[i].c.y, rectangleForD[i].c.z);
		glVertex3f(rectangleForD[i].d.x, rectangleForD[i].d.y, rectangleForD[i].d.z);
		glEnd();
		UpdateColor();		
	}
}

void A(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y, GLfloat angle) {
	// FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);

	//VARIABLE DECLARATION

	RECTANGLE rectangleForA[] = {
								{ // TOP RECTANGLE
									{x + 0.14F , y + hight / 2 + width, 0.0f },
									{x - 0.14F , y + hight / 2 + width, 0.0f},
									{x - width , y + hight / 2 , 0.0f },
									{x + width , y + hight / 2, 0.0f }
								},
								{ // LEFT TOP RACTANGLE
									{x  , y + hight / 2, 0.0f },
									{x - width , y + hight / 2, 0.0f },
									{x - 0.40f , y , 0.0f },
									{x - 0.20f , y , 0.0f }
								},
								{ // LEFT DOWN RECTANGLE
									{x - 0.20f , y , 0.0f },
									{x - 0.40f , y , 0.0f },
									{x - hight / 2 , y - hight / 2, 0.0f },
									{x - hight / 2 + width, y - hight / 2, 0.0f }
								},
								{ // RIGHT TOP RACTANGLE
									{x + width , y + hight / 2, 0.0f },
									{x  , y + hight / 2, 0.0f },
									{x + 0.20f , y , 0.0f },
									{x + 0.40f , y , 0.0f }
								},
								{ // RIGHT DOWN RECTANGLE
									{x + 0.40f , y , 0.0f },
									{x + 0.20f , y , 0.0f },
									{x + hight / 2 - width, y - hight / 2, 0.0f },
									{x + hight / 2 , y - hight / 2, 0.0f }
								},
								{ // RIGHT DOWN SQUARE
									{x + hight / 2 , y - hight / 2, 0.0f },
									{x + hight / 2 - width, y - hight / 2, 0.0f },
									{x + hight / 2 - width + 0.075f , y - hight / 2 - width, 0.0f },
									{x + hight / 2 + 0.075f , y - hight / 2 - width, 0.0f }
								},
								{ // LEFT DOWN RECTANGLE
									{x - hight / 2 + width, y - hight / 2, 0.0f },
									{x - hight / 2 , y - hight / 2, 0.0f },
									{x - hight / 2 - 0.075f, y - hight / 2 - width, 0.0f },
									{x - hight / 2 + width - 0.075f, y - hight / 2 - width, 0.0f }
								},
	};

	const int numberOfRectangleForA = sizeof(rectangleForA) / sizeof(rectangleForA[0]);

	int arrayColorForA[numberOfRectangleForA * 2] = { ORANGE_ID, ORANGE_ID, ORANGE_ID, WHITE_ID, WHITE_ID , GREEN_ID, ORANGE_ID, WHITE_ID, WHITE_ID, GREEN_ID, GREEN_ID , GREEN_ID, GREEN_ID, GREEN_ID };

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, 0.0f, -TRANSLATEZ);
	DrawRectangles(rectangleForA, numberOfRectangleForA, arrayColorForA);
}

void Tiranga(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y, GLfloat angle) {
	// FUNCTION DECLARATION
	void DrawRectangles(RECTANGLE*, int, int*);

	RECTANGLE rectangleForTiranga[] = {
										{ // TOP RECTANGLE
											{x + 0.3f , y + 0.1f, 0.0f },
											{x - 0.3f , y + 0.1f, 0.0f},
											{x - 0.3f, y , 0.0f },
											{x + 0.3f , y , 0.0f }
										},
										{ // TOP RECTANGLE
											{x + 0.3f , y , 0.0f },
											{x - 0.3f , y , 0.0f},
											{x - 0.3f, y - 0.1f, 0.0f },
											{x + 0.3f , y - 0.1f, 0.0f }
										},
										{ // TOP RECTANGLE
											{x + 0.3f , y - 0.1f , 0.0f },
											{x - 0.3f , y - 0.1f , 0.0f},
											{x - 0.3f, y - 0.2f, 0.0f },
											{x + 0.3f , y - 0.2f, 0.0f }
										}
	};
	const int numberOfRectangleForTiranga = sizeof(rectangleForTiranga) / sizeof(rectangleForTiranga[0]);

	int arrayColorForTiranga[numberOfRectangleForTiranga * 2] = { ORANGE_ID, ORANGE_ID, WHITE_ID, WHITE_ID, GREEN_ID, GREEN_ID };

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, translatefy, -TRANSLATEZ);
	DrawRectangles(rectangleForTiranga, numberOfRectangleForTiranga, arrayColorForTiranga);
}

// down Plane
void IAFPlane(GLfloat translatefx, GLfloat translatefy, GLfloat x, GLfloat y, GLfloat angle) {
	// FUNCTION DECLARATION
	void DrawColor(int, TRICOLORS);
	RECTANGLE rectangleForIAFPlane[] = {
											{
												{ x + 0.6f, y + 0.1f, 0.0f},
												{ x - 0.3f, y + 0.1f, 0.0f },
												{ x - 0.3f, y - 0.1f, 0.0f },
												{ x + 0.6f, y - 0.1f, 0.0f}
											},
											{
												{ x + 0.6f, y + 0.1f, 0.0f},
												{ x + 0.6f, y - 0.1f, 0.0f},
												{ x + 0.8f, y , 0.0f},
												{ x + 0.8f, y , 0.0f}
											},
											{
												{ x + 0.3f, y + 0.1f, 0.0f},
												{ x - 0.3f, y + 0.2f, 0.0f},
												{ x - 0.3f, y - 0.2f, 0.0f},
												{ x + 0.3f, y - 0.1f, 0.0f}
											},
											{
												{ x + 0.4f, y + 0.1f, 0.0f },
												{ x , y + 0.4f, 0.0f },
												{ x - 0.1f, y + 0.4f, 0.0f },
												{ x - 0.1f, y + 0.1f, 0.0f }
											},
											{
												{ x + 0.4f, y - 0.1f, 0.0f },
												{ x - 0.1f, y - 0.1f, 0.0f },
												{ x - 0.1f, y - 0.4f, 0.0f},
												{ x , y - 0.4f, 0.0f},
											},
											{ 
												{ x - 0.3f, y + 0.07f, 0.0f },
												{ x - 0.3f, y + 0.19f, 0.0f },
												{ x - 1.0f, y + 0.19f, 0.0f },
												{ x - 1.0f, y + 0.07f, 0.0f }
											},
											{ 
												{ x - 0.3f, y - 0.07f, 0.0f },
												{ x - 0.3f, y + 0.07f, 0.0f },
												{ x - 1.0f, y + 0.07f, 0.0f },
												{ x - 1.0f, y - 0.07f, 0.0f }
											},
											{ 
												{ x - 0.3f, y - 0.19f, 0.0f },
												{ x - 0.3f, y - 0.07f, 0.0f },
												{ x - 1.0f, y - 0.07f, 0.0f },
												{ x - 1.0f, y - 0.19f, 0.0f },
											}
	};
	const int numberOfRectangleForIAFPlane = sizeof(rectangleForIAFPlane) / sizeof(rectangleForIAFPlane[0]);

	int arrayColorForTiranga[numberOfRectangleForIAFPlane * 2] = { NAVYBLUE_ID, NAVYBLUE_ID,NAVYBLUE_ID, NAVYBLUE_ID,NAVYBLUE_ID, NAVYBLUE_ID,NAVYBLUE_ID, NAVYBLUE_ID,NAVYBLUE_ID, NAVYBLUE_ID, ORANGE_ID, BLACK_ID, WHITE_ID, BLACK_ID, GREEN_ID, BLACK_ID };
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(translatefx, translatefy, -8.0f);
	glRotatef(angle, 0.0f, 0.0f, 1.0f);
	DrawRectangles(rectangleForIAFPlane, numberOfRectangleForIAFPlane, arrayColorForTiranga);

}
