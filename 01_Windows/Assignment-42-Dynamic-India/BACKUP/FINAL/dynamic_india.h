// HEADER FILES
#pragma once
#include <windows.h>
#include <gl\GL.h>

#define MYICON 5412
#define IDI_MYICON MAKEINTRESOURCE(MYICON)
#define SOUND 3212
#define IDI_SOUND MAKEINTRESOURCE(SOUND)

#define TRANSLATEZ 8.0f

#define FINAL_POSITION_FOR_I_1 -3.0F
#define FINAL_POSITION_FOR_N 0.0F
#define FINAL_POSITION_FOR_D 0.0F
#define FINAL_POSITION_FOR_I_2 0.0f
#define FINAL_POSITION_FOR_A 3.0F
#define FINAL_POSITION_FOR_TIRANGA 3.0F

enum {
	ORANGE_ID = 1111,
	GREEN_ID,
	WHITE_ID,
	BLACK_ID,
	IAFBLUE_ID,
	NAVYBLUE_ID
};

typedef struct __POINTF {
	GLfloat x, y, z;
} POINTF;

typedef struct __RECTANGLE {
	POINTF a, b, c, d;
} RECTANGLE;


GLfloat width = 0.2f, hight = 1.2f;

typedef struct __COLORF {
	GLfloat red, green, blue;
} COLORF;

COLORF ORANGE = { 1.0f, 0.45f, 0.0f };
COLORF GREEN = { 0.0f, 0.45f, 0.0f };
COLORF WHITE = { 1.0f, 1.0f, 1.0f };

typedef struct __TRICOLORS {
	COLORF ORANGE, GREEN, WHITE;
} TRICOLORS;

TRICOLORS tricolors = {   
						 { 1.0f, 0.45f, 0.0f },
						 { 0.0f, 0.45f, 0.0f },
						 { 1.0f, 1.0f, 1.0f }
						};

TRICOLORS tricolorsForD = {
								{ 0.0f, 0.0f, 0.0f },
								{ 0.0f, 0.0f, 0.0f },
								{ 0.0f, 0.0f, 0.0f }
							};

GLfloat icreaseColorBy = 0.000075f;
